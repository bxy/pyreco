You want to contribute to DocBlockr?

**THEN YOU, SIR OR MADAM, ARE MY FRIEND.**

There's only a couple of things you should know first.

- **The most important thing** is to know there are two main branches. `master` is the "live" stable branch which pushes code into thousands of people's editors. `develop` is the place where development happens.
  - When beginning work on a patch, please start your branches from the latest commit on the `develop` branch. If you've already made some commits, please `git rebase develop` and then continue.
  - When making a pull request, please target it back to the `develop` branch. Pull requests targetting `master` will be rejected.
- Everything else is pretty standard:
  - Trim trailing spaces.
  - Unix line endings.
  - Spaces, not tabs.
  - Run your code through PEP8, or some linting program.

Thank you!

Many thanks to all those who have sent bug reports and especially to those who sent pull requests. Here are the names of those who have contributed code:

- Aleksey Smolenchuk (@lxe)
- Alex Whitman (@alexwhitman)
- Amir Abu Shareb (@yields)
- Andreas (@ryrun)
- Andrew Hanna (@percyhanna)
- Ben Linskey (@blinskey)
- Craig Patik (@cpatik)
- Daniel Julius Lasiman (@danieljl)
- Dany Ouellette (@DanyO)
- Dominique Wahli (@bizoo)
- Gary Jones (@GaryJones)
- Geoffrey Huntley (@ghuntley)
- Jordi Baggiano (@seldaek)
- Josh Freeman (@freejosh)
- Korvin Szanto (@KorvinSzanto)
- Marc-Antoine Parent (@maparent)
- Marc Neuhaus (@mneuhaus)
- Mat Gadd (@Drarok)
- Michael Barany (@mbarany)
- Milos Levacic (@levacic)
- Nick Dowdell (@mikulad13)
- Nick Fisher (@spadgos)
- Pavel Voronin (@pavel-voronin)
- Rafal Chlodnicki (@rchl)
- Roberto Segura (@phproberto)
- Scott Kuroda (@skuroda)
- Simon Aittamaa (@simait)
- Sven Axelsson (@svenax)
- Thanasis Polychronakis (@thanpolas)
- Tiago Santos (@tmcsantos)
- Timo Tijhof (@Krinkle)
- wronex (@wronex)

# DocBlockr Extended Changelog

- **v2.12.2**, *11 Apr 2014*
  - Fix for PHP autocompletions
  - Fix `@name` completion for Javascript
- **v2.12.1**, *4 Mar 2014*
  - Fix for Sublime Text 3
- **v2.12.0**, *4 Mar 2014*
  - Adds support for **TypeScript**, thanks to [Marc-Antoine Parent](https://github.com/maparent)
  - Adds option to add a spacer line after the description (`"jsdocs_spacer_between_sections": "after_description"`), thanks to [Milos Levacic](https://github.com/levacic)
  - PHP autocompletions support only the [PSR-5](https://github.com/phpDocumentor/fig-standards/blob/master/proposed/phpdoc.md) tags, thanks to [Gary Jones](https://github.com/GaryJones)
  - Fix scope issues for Java autocompletions, thanks to [Dominique Wahli](https://github.com/bizoo)
  - Fix for reflowing paragraphs when no rulers are set.
- **v2.11.7**, *3 Nov 2013*
  - Added support for triple `///`, `//!` and `/*!` style comments, thanks to [Jordi Boggiano](https://github.com/seldaek).
  - Added basic **Rust** support, again thanks to Jordi Boggiano.
  - Added an option to use short names for bools and ints (`jsdocs_short_primitives`), thanks to [Mat Gadd](https://github.com/drarok).
  - Fixed a bug with per-section indenting, again thanks to Mat Gadd.
  - Improved handling of Java return type detection, thanks to [Ben Linskey](https://github.com/blinskey)
- **v2.11.6**, *14 Aug 2013*
  - Predefined `@author` tags do not get parsed for column spacing
  - Handles the case when an arguments list contains a comma, for example, within a default value
  - A new keybinding for Windows to re-parse a doc block (<kbd>Alt+W</kbd>)
  - Fixes a regression that some function names were not being parsed correctly
- **v2.11.5**, *11 Aug 2013*
  - Fix for last deploy which accidentally changed the default `var` tag to "property". Default is "type" once again.
- **v2.11.4**, *10 Aug 2013*
  - The tag used on `var` declarations can be customised (eg: to "property" for YUIDoc)
  - Small fix for function declarations in C/C++ (thanks to [Simon Aittamaa](https://github.com/simait))
- **v2.11.3**, *18 June 2013*
  - Adds support for Groovy (thanks to [Tiago Santos](https://github.com/tmcsantos))
  - README has gifs. So many gifs.
- **v2.11.2**, *12 June 2013*
  - Compatibility fixes for ST3, thanks to Marc Neuhaus (@mneuhaus) and Daniel Julius Lasiman (@danieljl).
- **v2.11.1**, *11 May 2013*
  - No changes, just removes some debugging code that wasn't cleaned up in the last release (oops).
- **v2.11.0**, *11 May 2013*
  - It isn't broken in ST3 any more. (yay)
  - New options:
    - `jsdocs_simple_mode` for when you don't want dynamic templates
    - `jsdocs_lower_case_primitives` for YUIDoc which requires lower case for primitive data types
    - `jsdocs_extra_tags_go_after` to put custom text at the end of the docblock
  - Better handling of IIFEs
  - Hotkey for reparsing a block changed to <kbd>alt+shift+tab</kbd> to avoid OS-level conflicts
  - Adding a new line at the start of the docblock is handled properly
  - C/C++: arguments containing square brackets are handled properly
- **v2.10.1**, *19 April 2013*
  - Adds variable substitution in `jsdocs_extra_tags`
  - Fixes indentation bug in `jsdocs_extra_tags`
  - Fixes bug when adding a new line after a docblock which contains text afterwards
  - Fixes link to Pledgie (thanks @Krinkle)
- **v2.10.0**, *21 February 2013*
  - Adds Sublime Text 3 support (thanks to @lxe and @rmarscher)
  - YUI-style `@method` tags can be automatically added with the `jsdocs_autoadd_method_tag` setting (thanks to @maheshjag)
  - Variables starting with `$` are not wiped out when reparsing a doc block (thanks @ryrun)
- **v2.9.3**, *12 December 2012*
  - Fixed bug which stopped regular comments from closing automatically
- **v2.9.2**, *11 December 2012*
  - This one goes out to [Thanasis Polychronakis](https://github.com/thanpolas).
    - Structure of the modules greatly improved
    - Fixes bug with matching languages with hyphens in the name
  - Adds support for CUDA-C++
- **v2.9.1**, *31 October 2012*
  - Thanks to [wronex](https://github.com/wronex), <kbd>Alt+Q</kbd> will reformat the entire DocBlock, with customisable indentation.
  - Thanks to [Pavel Voronin](https://github.com/pavel-voronin), spaces around arguments are handled properly.
  - **C/C++**: Array arguments are accepted
  - **C/C++**: An argument list containing only `void` doesn't output any `@param` tags
  - **PHP**: Arguments with an array as a default value inside multi-line arguments are handled properly
  - <kbd>Ctrl/Cmd + Enter</kbd> and <kbd>Ctrl/Cmd + Shift + Enter</kbd> work inside DocBlocks.
- **v2.9.0**, *1 October 2012*
  - Adds ObjectiveC and ObjectiveC++ support, thanks to some help from [Robb Böhnke](https://github.com/robb)
    - Very buggy code, support isn't great but it's better than nothing (hopefully).
  - Single-line comments inside function definitions are handled
  - Notation rules are applied to functions, which means they can define a return type by their name, eg: `strFoo`
  - Notation rules can define arbitrary tags, for example: functions with a prefix of "_" should get the `@private` tag.
  - Given the above addition, JS functions starting with an underscore are no longer marked as `@private` by default.
- **v2.8.2**, *28 September 2012*
  - When a function is defined across many lines, the parser will find the arguments on extra lines.
- **v2.8.1**, *13 September 2012*
  - Pressing <kbd>tab</kbd> on an empty line will perform a deep indentation instead of moving to the next field
  - Functions starting with `_` will get a `@private` tag in Javascript (thanks to [Andrew Hanna](https://github.com/percyhanna))
- **v2.8.0**, *26 August 2012*
  - New feature: <kbd>Alt+Q</kbd> to reformat the description field of a docblock to make it fit nicely within your ruler.
  - Adds support for C++ (thanks to [Rafał Chłodnicki](https://github.com/rchl))
  - Indenting to the description field works in languages which don't require type information in the docblock.
- **v2.7.4**, *8 August 2012*
  - Fix for Actionscript docblocks not working
- **v2.7.3**, *7 August 2012*
  - No trailing whitespace added on the spacer lines added when `jsdocs_spacer_between_sections` is on (thanks to [Rafał Chłodnicki](https://github.com/rchl))
  - Fixes a bug with detecting variable names when they have a default value in PHP
  - Changes the notation map to not ignore the leading `$` or `_`, meaning that (for example), you could specify that variables starting with `$` are `HTMLElement`s.
- **v2.7.2**, *6 August 2012*
  - Small bug fix, thanks to [djuliusl](https://github.com/djuliusl)
- **v2.7.1**, *5 August 2012*
  - Adds per-section alignment (can be set using `jsdocs_per_section_indent`)
  - Description field for `@return` tag can be disabled using `jsdocs_return_description`. *(Both thanks to [Drarok](https://github.com/Drarok))* 
- **v2.7.0**, *5 August 2012*
  - Adds support for ASDocs (Actionscript)
  - Changes Linux shortcut for reparsing a comment block to <kbd>Alt+Shift+Tab</kbd>
- **v2.6.5**, *19 June 2012*
  - Bugfix for adding linebreaks when not at the start or end of a line
- **v2.6.4**, *4 June 2012*
  - Better support for indentation using tabs
  - YUI tags are supported by the autocomplete
  - When only whitespace exists on a docblock line, and `trim_automatic_white_space` is set to true, the whitespace is removed.
  - Better support for comment blocks opened with `/*`
- **v2.6.3**, *30 April 2012*
  - Fixes the join-lines command <kbd>Ctrl+J</kbd> for CoffeeScript.
- **v2.6.2**, *22 March 2012*
  - PHP `__destruct` functions don't get a return value *(thanks to [Alex Whitman](https://github.com/whitman))*.
- **v2.6.1**, *16 March 2012*
  - Fixes bug whereby the return values of functions which are named `set` or `add`, *etc* were not being guessed correctly.
  - `@return` tags are now given a description field *(thanks to [Nick Dowdell](https://github.com/mikulad13))*.
- **v2.6.0**, *4 March 2012*
  - Added CoffeeScript support
- **v2.5.0**, *11 February 2012*
  - Implemented DocBlock reparsing to re-enable tabstop fields. Hotkey is `Ctrl+Alt+Tab`.
- **v2.4.1**, *2 February 2012*
  - Fixed bug [#36](https://github.com/spadgos/sublime-jsdocs/issues/36) whereby docblocks were not being properly extended inside of `<script>` tags in a HTML document.
- **v2.4.0**, *29 January 2012*
  - `Enter` at the end of a comment block (ie: after the closing `*/`) will insert a newline and de-indent by one space.
- **v2.3.0**, *15 January 2012*
  - `Ctrl+Enter` on a double-slash comment will now decorate that comment.
  - Added a setting (`jsdocs_spacer_between_sections`) to add spacer lines between sections of a docblock.
- **v2.2.2**, *12 January 2012*
  - Separated JS and PHP completions files. PHP completions don't have brackets around type information any more.
  - PHP now uses `@var` (instead of `@type`) for documenting variable declarations.
  - *Both of these changes are thanks to [svenax][svenax]*
- **v2.2.1**, *11 January 2012*
  - DocBlocks can be triggered by pressing `tab` after `/**`
  - Some bugfixes due to auto-complete changes in Sublime Text.
  - Fixed bug where indenting would not work on the first line of a comment.
- **v2.2.0**, *5 January 2012*
  - A configuration option can be set so that either `@return` or `@returns` is used in your documentation. 
  - Language-specific tags now will only show for that language (eg: PHP has no `@interface` tag).
- **v2.1.3**, *31 December 2011*
  - Changed path for macro file to point to `Packages/DocBlockr`. If you are having issues, make sure that the plugin is installed in that location (**not** the previous location `Packages/JSDocs`).
- **v2.1.2**, *31 December 2011*
  - Renamed from *JSDocs* to *DocBlockr*, since it now does more than just Javascript.
- **v2.1.1**, *23 November 2011*
  - Fixed bug which broke the completions list
- **v2.1.0**, *19 November 2011*
  - Added a command to join lines inside a docblock which is smart to leading asterisks
  - Variable types are guessed from their name. `is` and `has` are assumed to be Booleans, and `callback`, `cb`, `done`, `fn` and `next` are assumed to be Functions.
  - You can now define your own patterns for mapping a variable name to a type.
  - Autocomplete works better now. `@` will also insert the "@" character, allowing you to add any tag you like, even if it isn't in the autocomplete list.
  - Added the full set of [PHPDoc][phpdoc] tags.
- **v2.0.0**, *6 November 2011*
  - PHP support added!
  - (Almost) complete rewrite to allow for any new languages to be added easily
    - *Please send feature requests or pull requests for new languages you'd like to add*
  - More options for aligning tags
- **v1.3.0**, *5 November 2011*
  - Improvements to handling of single-line comments
  - Functions beginning with `is` or `has` are assumed to return Booleans
  - Consolidated settings files into `Base File.sublime-settings`. **If you had configured your settings in `jsdocs.sublime-settings`, please move them to the Base File settings.**
  - Setting `jsdocs_extend_double_slashes` controls whether single-line comments are extended.
  - Pressing `tab` in a docblock will tab to match the description block of the previous tag. Use `jsdocs_deep_indent` to toggle this behaviour.
- **v1.2.0**, *6 October 2011*
  - Variable declarations can be documented. `Shift+enter` to make these inline
  - Double slash comments (`// like this`) are extended when `enter` is pressed
  - Class definitions detected and treated slightly differently (no return values pre-filled)
- **v1.1.0**, *3 October 2011*
  - DocBlockr parses the line following the comment to automatically prefill some documentation for you.
  - Settings available via menu
- **v1.0.0**, *28 September 2011*
  - Initial release
  - Comments are automatically closed, extended and indented.

DocBlockr is a package for [Sublime Text 2 & 3][sublime] which makes writing documentation a breeze. DocBlockr supports **JavaScript**, **PHP**, **ActionScript**, **CoffeeScript**, **TypeScript**, **Java**, **Groovy**, **Objective C**, **C**, **C++** and **Rust**.

## Installation ##

### With Package Control ###

With [Package Control][package_control] installed, you can install DocBlockr from inside Sublime Text itself. Open the Command Palette and select "Package Control: Install Package", then search for DocBlockr and you're done!

## Feature requests & bug reports ##

You can leave either of these things [here][issues]. Pull requests are welcomed heartily, but please read [CONTRIBUTING.md][contrib] first! Basically: in this repo, the main development branch is `develop` and the stable 'production' branch is `master`. Please remember to base your branch from `develop` and issue the pull request back to that branch.

## Changelog ##

- **v2.12.2**, *11 Apr 2014*
  - Fix for PHP autocompletions
  - Fix `@name` completion for Javascript
- **v2.12.1**, *4 Mar 2014*
  - Fix for Sublime Text 3
- **v2.12.0**, *4 Mar 2014*
  - Adds support for **TypeScript**, thanks to [Marc-Antoine Parent](https://github.com/maparent)
  - Adds option to add a spacer line after the description (`"jsdocs_spacer_between_sections": "after_description"`), thanks to [Milos Levacic](https://github.com/levacic)
  - PHP autocompletions support only the [PSR-5](https://github.com/phpDocumentor/fig-standards/blob/master/proposed/phpdoc.md) tags, thanks to [Gary Jones](https://github.com/GaryJones)
  - Fix scope issues for Java autocompletions, thanks to [Dominique Wahli](https://github.com/bizoo)
  - Fix for reflowing paragraphs when no rulers are set.
- **v2.11.7**, *3 Nov 2013*
  - Added support for triple `///`, `//!` and `/*!` style comments, thanks to [Jordi Boggiano](https://github.com/seldaek).
  - Added basic **Rust** support, again thanks to Jordi Boggiano.
  - Added an option to use short names for bools and ints (`jsdocs_short_primitives`), thanks to [Mat Gadd](https://github.com/drarok).
  - Fixed a bug with per-section indenting, again thanks to Mat Gadd.
  - Improved handling of Java return type detection, thanks to [Ben Linskey](https://github.com/blinskey)
- **v2.11.6**, *14 Aug 2013*
  - Predefined `@author` tags do not get parsed for column spacing
  - Handles the case when an arguments list contains a comma, for example, within a default value
  - A new keybinding for Windows to re-parse a doc block (<kbd>Alt+W</kbd>)
  - Fixes a regression that some function names were not being parsed correctly

Older history can be found in [the history file](https://github.com/spadgos/sublime-jsdocs/blob/master/HISTORY.md).

## Show your love

[![Click here to lend your support to: DocBlockr and make a donation at pledgie.com!](https://pledgie.com/campaigns/16316.png?skin_name=chrome)](http://pledgie.com/campaigns/16316)


## Usage ##

> Below are some examples of what the package does. Note that there are no keyboard shortcuts required to trigger these completions - just type as normal and it happens for you!

### Docblock completion ###

Pressing **enter** or **tab** after `/**` (or `###*` for Coffee-Script) will yield a new line and will close the comment.

![](http://spadgos.github.io/sublime-jsdocs/images/basic.gif)

Single-asterisk comment blocks behave similarly:

![](http://spadgos.github.io/sublime-jsdocs/images/basic-block.gif)

### Function documentation ###

However, if the line directly afterwards contains a function definition, then its name and parameters are parsed and some documentation is automatically added.

![](http://spadgos.github.io/sublime-jsdocs/images/function-template.gif)

You can then press `tab` to move between the different fields.

If you have many arguments, or long variable names, it might be useful to spread your arguments across multiple lines. DocBlockr will handle this situation too:

![](http://spadgos.github.io/sublime-jsdocs/images/long-args.gif)

In languages which support [type hinting][typehinting] or default values, then those types are prefilled as the datatypes.

![](http://spadgos.github.io/sublime-jsdocs/images/type-hinting.gif)

DocBlockr will try to make an intelligent guess about the return value of the function.

- If the function name is or begins with "set" or "add", then no `@return` is inserted.
- If the function name is or begins with "is" or "has", then it is assumed to return a `Boolean`.
- In Javascript, if the function begins with an uppercase letter then it is assumed that the function is a class definition. No `@return` tag is added.
- In PHP, some of the [magic methods][magicmethods] have their values prefilled:
  - `__construct`, `__destruct`, `__set`, `__unset`, `__wakeup` have no `@return` tag.
  - `__sleep` returns an `Array`.
  - `__toString` returns a `string`.
  - `__isset` returns a `bool`.

### Variable documentation ###

If the line following your docblock contains a variable declaration, DocBlockr will try to determine the data type of the variable and insert that into the comment.

If you press `shift+enter` after the opening `/**` then the docblock will be inserted inline.

![](http://spadgos.github.io/sublime-jsdocs/images/vars.gif)

DocBlockr will also try to determine the type of the variable from its name. Variables starting with `is` or `has` are assumed to be booleans, and `callback`, `cb`, `done`, `fn`, and `next` are assumed to be functions. If you use your own variable naming system (eg: hungarian notation: booleans all start with `b`, arrays start with `arr`), you can define these rules yourself. Modify the `jsdocs_notation_map` setting *(in `Base File.sublime-settings`)* like so:

```json
{
    "jsdocs_notation_map": [
        {
            "prefix": "b", // a prefix, matches only if followed by an underscore or A-Z
            "type": "bool" // translates to "Boolean" in javascript, "bool" in PHP
        },
        {
            "regex": "tbl_?[Rr]ow", // any arbitrary regex to test against the variable name
            "type": "TableRow"      // you can add your own types
        }
    ]
}
```

The notation map can also be used to add arbitrary tags, according to your own code conventions. For example, if your conventions state that functions beginning with an underscore are private, you could add this to the `jsdocs_notation_map`:

```json
{
    "prefix": "_",
    "tags": ["@private"]
}
```

### Comment extension ###

Pressing enter inside a docblock will automatically insert a leading asterisk and maintain your indentation.

![](http://spadgos.github.io/sublime-jsdocs/images/auto-indent.gif)

![](http://spadgos.github.io/sublime-jsdocs/images/auto-indent-2.gif)

This applies to docblock comments `/** like this */` as well as inline double-slash comments `// like this`

![](http://spadgos.github.io/sublime-jsdocs/images/single-line.gif)

In either case, you can press `shift+enter` to stop the automatic extension.

Oftentimes, when documenting a parameter, or adding a description to a tag, your description will cover multiple lines. If the line you are on is directly following a tag line, pressing `tab` will move the indentation to the correct position.

![](http://spadgos.github.io/sublime-jsdocs/images/deep-indent.gif)

### Comment decoration ###

If you write a double-slash comment and then press `Ctrl+Enter`, DocBlockr will 'decorate' that line for you.

    // Foo bar baz<<Ctrl+Enter>>

    -- becomes

    /////////////////
    // Foo bar baz //
    /////////////////

### Reparsing a DocBlock ###

Sometimes, you'll perform some action which clears the fields (sections of text which you can navigate through using `tab`). This leaves you with a number of placeholders in the DocBlock with no easy way to jump to them.

With DocBlockr, you can reparse a comment and reactivate the fields by pressing the hotkey `Alt+Shift+Tab` in OS X or Linux, or `Alt+W` in Windows

### Reformatting paragraphs ###

Inside a comment block, hit `Alt+Q` to wrap the lines to make them fit within your rulers. If you would like subsequent lines in a paragraph to be indented, you can adjust the `jsdocs_indentation_spaces_same_para` setting. For example, a value of `3` might look like this:

    /**
     * Duis sed arcu non tellus eleifend ullamcorper quis non erat. Curabitur
     *   metus elit, ultrices et tristique a, blandit at justo.
     * @param  {String} foo Lorem ipsum dolor sit amet.
     * @param  {Number} bar Nullam fringilla feugiat pretium. Quisque
     *   consectetur, risus eu pellentesque tincidunt, nulla ipsum imperdiet
     *   massa, sit amet adipiscing dolor.
     * @return {[Type]}
     */

### Adding extra tags ###

Finally, typing `@` inside a docblock will show a completion list for all tags supported by [JSDoc][jsdoc], the [Google Closure Compiler][closure], [YUIDoc][yui] or [PHPDoc][phpdoc]. Extra help is provided for each of these tags by prefilling the arguments each expects. Pressing `tab` will move the cursor to the next argument.

## Configuration ##

You can access the configuration settings by selecting `Preferences -> Package Settings -> DocBlockr`.

*The `jsdocs_*` prefix is a legacy from days gone by...*

- `jsdocs_indentation_spaces` *(Number)* The number of spaces to indent after the leading asterisk.

        // jsdocs_indentation_spaces = 1
        /**
         * foo
         */

        // jsdocs_indentation_spaces = 5
        /**
         *     foo
         */

- `jsdocs_align_tags` *(String)* Whether the words following the tags should align. Possible values are `'no'`, `'shallow'` and `'deep'`

    > For backwards compatibility, `false` is equivalent to `'no'`, `true` is equivalent to `'shallow'`

    `'shallow'` will align only the first words after the tag. eg:

        @param    {MyCustomClass} myVariable desc1
        @return   {String} foo desc2
        @property {Number} blahblah desc3

    `'deep'` will align each component of the tags, eg:

        @param    {MyCustomClass} myVariable desc1
        @return   {String}        foo        desc2
        @property {Number}        blahblah   desc3


- `jsdocs_extra_tags` *(Array.String)* An array of strings, each representing extra boilerplate comments to add to *functions*. These can also include arbitrary text (not just tags).

        // jsdocs_extra_tags = ['This is a cool function', '@author nickf', '@version ${1:[version]}']
        /**<<enter>>
        function foo (x) {}

        /**
         * [foo description]
         * This is a cool function
         * @author nickf
         * @version [version]
         * @param  {[type]} x [description]
         * @return {[type]}
         */
        function foo (x) {}

    Basic variable substitution is supported here for the variables `date` and `datetime`, wrapped in double curly brackets.

        // jsdocs_extra_tags = ['@date {{date}}', '@anotherdate {{datetime}}']
        /**<<enter>>
        function foo() {}

        /**
         * [foo description]
         * @date     2013-03-25
         * @datetime 2013-03-25T21:16:25+0100
         * @return   {[type]}
         */

- `jsdocs_extra_tags_go_after` *(Boolean)* If true, the extra tags are placed at the end of the block (after param/return). Default: `false`

- `jsdocs_extend_double_slash` *(Boolean)* Whether double-slash comments should be extended. An example of this feature is described above. Default: `true`

- `jsdocs_deep_indent` *(Boolean)* Whether pressing tab at the start of a line in docblock should indent to match the previous line's description field. An example of this feature is described above. Default: `true`

- `jsdocs_notation_map` *(Array)* An array of notation objects. Each notation object must define either a `prefix` OR a `regex` property, and a `type` property.

- `jsdocs_return_tag` *(String)* The text which should be used for a `@return` tag. By default, `@return` is used, however this can be changed to `@returns` if you use that style.

- `jsdocs_spacer_between_sections` *(Boolean|String)* If true, then extra blank lines are inserted between the sections of the docblock. If set to `"after_description"` then a spacer will only be added between the description and the first tag. Default: `false`.

- `jsdocs_indentation_spaces_same_para` *(Number)* Described above in the *Reformatting paragraphs* section. Default: `1`

- `jsdocs_autoadd_method_tag` *(Boolean)* Add a `@method` tag to docblocks of functions. Default: `false`

- `jsdocs_simple_mode` *(Boolean)* If true, DocBlockr won't add a template when creating a doc block before a function or variable. Useful if you don't want to write Javadoc-style, but still want your editor to help when writing block comments. Default: `false`

- `jsdocs_lower_case_primitives` *(Boolean)* If true, primitive data types are added in lower case, eg "number" instead of "Number". Default: `false`

- `jsdocs_short_primitives` *(Boolean)* If true, the primitives `Boolean` and `Integer` are shortened to `Bool` and `Int`. Default: `false`

- `jsdocs_newline_after_block` *(Boolean)* If true, an extra line break is added after the end of a docblock to separate it from the code. Default `false`

This is my first package for Sublime Text, and the first time I've written any Python, so I heartily welcome feedback and [feature requests or bug reports][issues].

[closure]: http://code.google.com/closure/compiler/docs/js-for-compiler.html
[contrib]: blob/master/CONTRIBUTING.md
[issues]: https://github.com/spadgos/sublime-jsdocs/issues
[jsdoc]: http://code.google.com/p/jsdoc-toolkit/wiki/TagReference
[magicmethods]: http://www.php.net/manual/en/language.oop5.magic.php
[package_control]: http://wbond.net/sublime_packages/package_control
[phpdoc]: http://phpdoc.org/
[sublime]: http://www.sublimetext.com/
[tags]: https://github.com/spadgos/sublime-jsdocs/tags
[typehinting]: http://php.net/manual/en/language.oop5.typehinting.php
[yui]: http://yui.github.com/yuidoc/syntax/index.html


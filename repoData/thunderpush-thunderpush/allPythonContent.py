__FILENAME__ = conf
# -*- coding: utf-8 -*-
#
# thunderpush documentation build configuration file, created by
# sphinx-quickstart on Mon Mar 17 00:24:48 2014.
#
# This file is execfile()d with the current directory set to its
# containing dir.
#
# Note that not all possible configuration values are present in this
# autogenerated file.
#
# All configuration values have a default; values that are commented out
# serve to show the default.

import sys
import os

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#sys.path.insert(0, os.path.abspath('.'))

# -- General configuration ------------------------------------------------

# If your documentation needs a minimal Sphinx version, state it here.
#needs_sphinx = '1.0'

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.autodoc',
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['ntemplates']

# The suffix of source filenames.
source_suffix = '.rst'

# The encoding of source files.
#source_encoding = 'utf-8-sig'

# The master toctree document.
master_doc = 'index'

# General information about the project.
project = u'thunderpush'
copyright = u'2014, Krzysztof Jagiello'

# The version info for the project you're documenting, acts as replacement for
# |version| and |release|, also used in various other places throughout the
# built documents.
#
# The short X.Y version.
version = '0.9.7'
# The full version, including alpha/beta/rc tags.
release = '0.9.7'

# The language for content autogenerated by Sphinx. Refer to documentation
# for a list of supported languages.
#language = None

# There are two options for replacing |today|: either, you set today to some
# non-false value, then it is used:
#today = ''
# Else, today_fmt is used as the format for a strftime call.
#today_fmt = '%B %d, %Y'

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
exclude_patterns = []

# The reST default role (used for this markup: `text`) to use for all
# documents.
#default_role = None

# If true, '()' will be appended to :func: etc. cross-reference text.
#add_function_parentheses = True

# If true, the current module name will be prepended to all description
# unit titles (such as .. function::).
#add_module_names = True

# If true, sectionauthor and moduleauthor directives will be shown in the
# output. They are ignored by default.
#show_authors = False

# The name of the Pygments (syntax highlighting) style to use.
pygments_style = 'sphinx'

# A list of ignored prefixes for module index sorting.
#modindex_common_prefix = []

# If true, keep warnings as "system message" paragraphs in the built documents.
#keep_warnings = False


# -- Options for HTML output ----------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
html_theme = 'default'

# Theme options are theme-specific and customize the look and feel of a theme
# further.  For a list of options available for each theme, see the
# documentation.
#html_theme_options = {}

# Add any paths that contain custom themes here, relative to this directory.
#html_theme_path = []

# The name for this set of Sphinx documents.  If None, it defaults to
# "<project> v<release> documentation".
#html_title = None

# A shorter title for the navigation bar.  Default is the same as html_title.
#html_short_title = None

# The name of an image file (relative to this directory) to place at the top
# of the sidebar.
#html_logo = None

# The name of an image file (within the static path) to use as favicon of the
# docs.  This file should be a Windows icon file (.ico) being 16x16 or 32x32
# pixels large.
#html_favicon = None

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['nstatic']

# Add any extra paths that contain custom files (such as robots.txt or
# .htaccess) here, relative to this directory. These files are copied
# directly to the root of the documentation.
#html_extra_path = []

# If not '', a 'Last updated on:' timestamp is inserted at every page bottom,
# using the given strftime format.
#html_last_updated_fmt = '%b %d, %Y'

# If true, SmartyPants will be used to convert quotes and dashes to
# typographically correct entities.
#html_use_smartypants = True

# Custom sidebar templates, maps document names to template names.
#html_sidebars = {}

# Additional templates that should be rendered to pages, maps page names to
# template names.
#html_additional_pages = {}

# If false, no module index is generated.
#html_domain_indices = True

# If false, no index is generated.
#html_use_index = True

# If true, the index is split into individual pages for each letter.
#html_split_index = False

# If true, links to the reST sources are added to the pages.
#html_show_sourcelink = True

# If true, "Created using Sphinx" is shown in the HTML footer. Default is True.
#html_show_sphinx = True

# If true, "(C) Copyright ..." is shown in the HTML footer. Default is True.
#html_show_copyright = True

# If true, an OpenSearch description file will be output, and all pages will
# contain a <link> tag referring to it.  The value of this option must be the
# base URL from which the finished HTML is served.
#html_use_opensearch = ''

# This is the file name suffix for HTML files (e.g. ".xhtml").
#html_file_suffix = None

# Output file base name for HTML help builder.
htmlhelp_basename = 'thunderpushdoc'


# -- Options for LaTeX output ---------------------------------------------

latex_elements = {
# The paper size ('letterpaper' or 'a4paper').
#'papersize': 'letterpaper',

# The font size ('10pt', '11pt' or '12pt').
#'pointsize': '10pt',

# Additional stuff for the LaTeX preamble.
#'preamble': '',
}

# Grouping the document tree into LaTeX files. List of tuples
# (source start file, target name, title,
#  author, documentclass [howto, manual, or own class]).
latex_documents = [
  ('index', 'thunderpush.tex', u'thunderpush Documentation',
   u'Krzysztof Jagiello', 'manual'),
]

# The name of an image file (relative to this directory) to place at the top of
# the title page.
#latex_logo = None

# For "manual" documents, if this is true, then toplevel headings are parts,
# not chapters.
#latex_use_parts = False

# If true, show page references after internal links.
#latex_show_pagerefs = False

# If true, show URL addresses after external links.
#latex_show_urls = False

# Documents to append as an appendix to all manuals.
#latex_appendices = []

# If false, no module index is generated.
#latex_domain_indices = True


# -- Options for manual page output ---------------------------------------

# One entry per manual page. List of tuples
# (source start file, name, description, authors, manual section).
man_pages = [
    ('index', 'thunderpush', u'thunderpush Documentation',
     [u'Krzysztof Jagiello'], 1)
]

# If true, show URL addresses after external links.
#man_show_urls = False


# -- Options for Texinfo output -------------------------------------------

# Grouping the document tree into Texinfo files. List of tuples
# (source start file, target name, title, author,
#  dir menu entry, description, category)
texinfo_documents = [
  ('index', 'thunderpush', u'thunderpush Documentation',
   u'Krzysztof Jagiello', 'thunderpush', 'One line description of project.',
   'Miscellaneous'),
]

# Documents to append as an appendix to all manuals.
#texinfo_appendices = []

# If false, no module index is generated.
#texinfo_domain_indices = True

# How to display URL addresses: 'footnote', 'no', or 'inline'.
#texinfo_show_urls = 'footnote'

# If true, do not generate a @detailmenu in the "Top" node's menu.
#texinfo_no_detailmenu = False

########NEW FILE########
__FILENAME__ = api
import logging
import tornado.web
from thunderpush.sortingstation import SortingStation

logger = logging.getLogger()

try:
    import simplejson as json
except ImportError:
    import json


def is_authenticated(f):
    """ Decorator used to check if a valid api key has been provided. """

    def run_check(self, *args, **kwargs):
        ss = SortingStation.instance()

        apisecret = self.request.headers.get('X-Thunder-Secret-Key', None)
        messenger = ss.get_messenger_by_apikey(kwargs['apikey'])

        if not messenger or apisecret != messenger.apisecret:
            self.error("Wrong API key/secret.", 401)
            return

        # pass messenger instance to handler
        kwargs['messenger'] = messenger

        f(self, *args, **kwargs)

    return run_check


def is_json(f):
    """ Used to check if the body of the request is valid JSON. """

    def run_check(self, *args, **kwargs):
        try:
            json.loads(self.request.body)
            f(self, *args, **kwargs)
        except ValueError:
            self.error("Request body is not valid JSON.", 400)
            return

    return run_check


class ThunderApiHandler(tornado.web.RequestHandler):
    def response(self, data, code=200):
        if code != 200:
            # if something went wrong, we include returned HTTP code in the
            # JSON response
            data["status"] = code

        self.write(json.dumps(data) + "\n")
        self.set_status(code)

    def error(self, message, code=500):
        self.response({"message": message}, code)

    def prepare(self, *args, **kwargs):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "X-Thunder-Secret-Key")
        self.set_header("Access-Control-Allow-Methods", "GET, POST, DELETE")

    def options(self, *args, **kwargs):
        pass


class ChannelHandler(ThunderApiHandler):
    @is_authenticated
    @is_json
    def post(self, *args, **kwargs):
        """ Sends messages to a channel. """

        messenger = kwargs['messenger']
        channel = kwargs['channel']

        count = messenger.send_to_channel(channel, self.request.body)
        self.response({"count": count})

        logger.debug("Message has been sent to %d users." % count)

    @is_authenticated
    def get(self, *args, **kwargs):
        """ Retrieves the number of users online. """

        messenger = kwargs['messenger']
        channel = kwargs['channel']

        users = \
            [user.userid for user in messenger.get_users_in_channel(channel)]

        self.response({"users": users})


class UserCountHandler(ThunderApiHandler):
    """ Retrieves the number of users online. """

    @is_authenticated
    def get(self, *args, **kwargs):
        messenger = kwargs['messenger']

        self.response({"count": messenger.get_user_count()})


class UserHandler(ThunderApiHandler):
    @is_authenticated
    def get(self, *args, **kwargs):
        """ Retrieves the number of users online. """

        messenger = kwargs['messenger']
        user = kwargs['user']

        is_online = messenger.is_user_online(user)
        self.response({"online": is_online}, 200)

    @is_authenticated
    @is_json
    def post(self, *args, **kwargs):
        """ Sends a message to a user. """

        messenger = kwargs['messenger']
        user = kwargs['user']

        count = messenger.send_to_user(user, self.request.body)
        self.response({"count": count})

        logger.debug("Message has been sent to %d users." % count)

    @is_authenticated
    def delete(self, *args, **kwargs):
        """ Forces logout of a user. """

        messenger = kwargs['messenger']
        user = kwargs['user']

        messenger.force_disconnect_user(user)

        # no response
        self.set_status(204)

########NEW FILE########
__FILENAME__ = handler
import logging

from sockjs.tornado import SockJSConnection
from thunderpush.sortingstation import SortingStation

try:
    import simplejson as json
except ImportError:
    import json

logger = logging.getLogger()


class ThunderSocketHandler(SockJSConnection):
    def on_open(self, info):
        logger.debug("New connection opened.")

        # no messenger object yet, client needs issue CONNECT command first
        self.messenger = None

    def on_message(self, msg):
        logger.debug("Got message: %s" % msg)

        self.process_message(msg)

    def on_close(self):
        if self.connected:
            self.messenger.unregister_user(self)
            self.messenger = None

        logger.debug("User %s has disconnected."
            % getattr(self, "userid", None))

    def force_disconnect(self):
        self.close(9002, "Server closed the connection (intentionally).")

    def process_message(self, msg):
        """
        We assume that every client message comes in following format:
        COMMAND argument1[:argument2[:argumentX]]
        """

        tokens = msg.split(" ")

        messages = {
            'CONNECT': self.handle_connect,
            'SUBSCRIBE': self.handle_subscribe,
            'UNSUBSCRIBE': self.handle_unsubscribe
        }

        try:
            messages[tokens[0]](tokens[1])
        except (KeyError, IndexError):
            logger.warning("Received invalid message: %s." % msg)

    def handle_connect(self, args):
        if self.connected:
            logger.warning("User already connected.")
            return

        try:
            self.userid, self.apikey = args.split(":")
        except ValueError:
            logger.warning("Invalid message syntax.")
            return

        # get singleton instance of SortingStation
        ss = SortingStation.instance()

        # get and store the messenger object for given apikey
        self.messenger = ss.get_messenger_by_apikey(self.apikey)

        if self.messenger:
            self.messenger.register_user(self)
        else:
            self.close(9000, "Invalid API key.")

    def handle_subscribe(self, args):
        if not self.connected:
            logger.warning("User not connected.")

            # close the connection, the user issues commands in a wrong order
            self.close(9001, "Subscribing before connecting.")
            return

        channels = filter(None, args.split(":"))

        for channel in channels:
            self.messenger.subscribe_user_to_channel(self, channel)

    def handle_unsubscribe(self, args):
        if not self.connected:
            logger.warning("User not connected.")

            # close the connection, the user issues commands in a wrong order
            self.close(9001, "Subscribing before connecting.")
            return

        channels = filter(None, args.split(":"))

        for channel in channels:
            self.messenger.unsubscribe_user_from_channel(self, channel)

    def close(self, code=3000, message="Go away!"):
        self.session.close(code, message)

    @property
    def connected(self):
        return bool(self.messenger)

########NEW FILE########
__FILENAME__ = messenger
import logging
import re

logger = logging.getLogger()


class Messenger(object):
    """
    Handles dispatching messages to Channels and Users
    for given client.
    """

    def __init__(self, apikey, apisecret, *args, **kwargs):
        self.apikey = apikey
        self.apisecret = apisecret
        self.users = {}
        self.channels = {}
        self.user_count = 0

    @staticmethod
    def is_valid_channel_name(name):
        return not re.match("^[a-zA-Z0-9_\-\=\@\,\.\;]{1,64}$", name) is None

    def send_to_channel(self, channel, message):
        """
        Sends a message to given channel.
        Returns a count of messages sent.
        """

        data = {'payload': message, 'channel': channel}
        users = self.get_users_in_channel(channel)
        return self._send_to_users(users, data)

    def send_to_user(self, userid, message):
        """
        Sends a message to given user.
        Returns a count of messages sent.
        """

        data = {'payload': message}
        users = self.users.get(userid, [])
        return self._send_to_users(users, data)

    def _send_to_users(self, users, message):
        if users:
            users[0].broadcast(users, message)

        return len(users)

    def register_user(self, user):
        self.users.setdefault(user.userid, []).append(user)

    def subscribe_user_to_channel(self, user, channel):
        if self.is_valid_channel_name(channel):
            self.channels.setdefault(channel, []).append(user)

            logger.debug("User %s subscribed to %s." % (user.userid, channel,))
            logger.debug("User count in %s: %d." %
                (channel, self.get_channel_user_count(channel)))
        else:
            logger.debug("Invalid channel name %s." % channel)

    def unsubscribe_user_from_channel(self, user, channel):
        try:
            self.channels[channel].remove(user)

            # free up the memory used by empty channel index
            if not len(self.channels[channel]):
                del self.channels[channel]

            logger.debug("%s unsubscribed from %s." % (user.userid, channel,))
            logger.debug("User count in %s: %d." %
                (channel, self.get_channel_user_count(channel)))
        except KeyError:
            logger.debug("Channel %s not found." % (channel,))
        except ValueError:
            logger.debug("User %s not found in %s." % (user.userid, channel,))

    def unregister_user(self, user):
        channels_to_free = []

        for name in self.channels.iterkeys():
            try:
                self.channels[name].remove(user)

                # as we can't delete keys from the dict as we are iterating
                # over it, we do it outside of this loop
                if not len(self.channels[name]):
                    channels_to_free.append(name)
            except ValueError:
                pass

        # free up the memory used by empty channel index
        for channel in channels_to_free:
            del self.channels[channel]

        self.users[user.userid].remove(user)

        # free up the memory used by empty user index
        if len(self.users[user.userid]) == 0:
            del self.users[user.userid]

    def force_disconnect_user(self, userid):
        handlers = self.users.get(userid, [])

        for handler in handlers:
            handler.force_disconnect()

    def get_user_count(self):
        return len(self.users)

    def get_connections_count(self):
        return sum([len(connections) for connections in self.users.values()])

    def is_user_online(self, userid):
        return bool(self.users.get(userid, 0))

    def get_channel_user_count(self, channel):
        return len(self.get_users_in_channel(channel))

    def get_users_in_channel(self, channel):
        return self.channels.get(channel, [])

########NEW FILE########
__FILENAME__ = runner
from thunderpush.sortingstation import SortingStation
from thunderpush.handler import ThunderSocketHandler
from thunderpush import api, __version__
from thunderpush import settings

from sockjs.tornado import SockJSRouter

import sys
import tornado.ioloop
import argparse
import logging

logger = logging.getLogger()


def run_app():
    # configure logging level
    if settings.VERBOSE:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)

    ThunderRouter = SockJSRouter(ThunderSocketHandler, "/connect")

    # api urls
    urls = [
        (r"/api/1\.0\.0/(?P<apikey>.+)/users/",
            api.UserCountHandler),
        (r"/api/1\.0\.0/(?P<apikey>.+)/users/(?P<user>.+)/",
            api.UserHandler),
        (r"/api/1\.0\.0/(?P<apikey>.+)/channels/(?P<channel>.+)/",
            api.ChannelHandler),
    ]

    # include sockjs urls
    urls += ThunderRouter.urls

    application = tornado.web.Application(urls, settings.DEBUG)

    ss = SortingStation()

    # Single-client only at the moment.
    ss.create_messenger(settings.APIKEY, settings.APISECRET)

    logger.info("Starting Thunderpush server at %s:%d",
        settings.HOST, settings.PORT)

    application.listen(settings.PORT, settings.HOST)

    try:
        tornado.ioloop.IOLoop.instance().start()
    except KeyboardInterrupt:
        logger.info("Shutting down...")


def update_settings(args):
    args = vars(args)

    for optname in ["PORT", "HOST", "VERBOSE", "DEBUG"]:
        value = args.get(optname, None)

        if not value is None:
            setattr(settings, optname, value)

    settings.APIKEY = args['clientkey']
    settings.APISECRET = args['apikey']


def parse_args(args):
    parser = argparse.ArgumentParser()

    parser.add_argument('-p', '--port',
        default=settings.PORT,
        help='binds server to custom port',
        action="store", type=int, dest="PORT")

    parser.add_argument('-H', '--host',
        default=settings.HOST,
        help='binds server to custom address',
        action="store", type=str, dest="HOST")

    parser.add_argument('-v', '--verbose',
        default=settings.VERBOSE,
        help='verbose mode',
        action="store_true", dest="VERBOSE")

    parser.add_argument('-d', '--debug',
        default=settings.DEBUG,
        help='debug mode (useful for development)',
        action="store_true", dest="DEBUG")

    parser.add_argument('-V', '--version', 
        action='version', version=__version__)

    parser.add_argument('clientkey',
        help='client key')

    parser.add_argument('apikey',
        help='server API key')

    return parser.parse_args(args)


def main():
    args = parse_args(sys.argv[1:])
    update_settings(args)
    run_app()

if __name__ == "__main__":
    main()

########NEW FILE########
__FILENAME__ = settings
# This file contains default settings for this project.
# If you want to customize this settings, use command line arguments
# to do so, instead of modifying this file.

# Although Thunderpush server supports multiple server clients,
# at the moment we make it single-client.

# Public API key for server client.
APIKEY = ''

# Private, secret API key for server client.
APISECRET = ''

# Port to which the server will be bound.
PORT = 8080

# Host to which the server will be binded.
HOST = ''

# Debug mode? Tornado will restart the server whenever any project file
# change. Very useful for development.
DEBUG = False

# This sets the logging level to DEBUG.
VERBOSE = False

########NEW FILE########
__FILENAME__ = sortingstation
import logging
from thunderpush.messenger import Messenger

logger = logging.getLogger()


class SortingStation(object):
    """ Handles dispatching messages to Messengers. """

    _instance = None

    def __init__(self, *args, **kwargs):
        if self._instance:
            raise Exception("SortingStation already initialized.")

        self.messengers_by_apikey = {}

        SortingStation._instance = self

    @staticmethod
    def instance():
        return SortingStation._instance

    def create_messenger(self, apikey, apisecret):
        messenger = Messenger(apikey, apisecret)

        self.messengers_by_apikey[apikey] = messenger

    def delete_messenger(self, messenger):
        del self.messengers_by_apikey[messenger.apikey]

    def get_messenger_by_apikey(self, apikey):
        return self.messengers_by_apikey.get(apikey, None)

########NEW FILE########
__FILENAME__ = test_cli
import unittest
from thunderpush.runner import parse_args


class CLITestCase(unittest.TestCase):
    def test_args(self):
        self.assertRaises(SystemExit, parse_args, [])
        self.assertRaises(SystemExit, parse_args, ['foo'])

        args = vars(parse_args(['-d', '-p 1234', '-H foobar', 'foo', 'bar']))
        self.assertEqual(args.get('clientkey', None), 'foo')
        self.assertEqual(args.get('apikey', None), 'bar')
        self.assertEqual(args.get('DEBUG', None), True)
        self.assertEqual(args.get('PORT', None), 1234)
        self.assertEqual(args.get('HOST', None).strip(), 'foobar')

########NEW FILE########
__FILENAME__ = test_messenger
from thunderpush.messenger import Messenger
import unittest


class DummyThunderSocketHandler(object):
    dummyid = 1

    def __init__(self, *args, **kwargs):
        self.userid = "dummy_%d" % DummyThunderSocketHandler.dummyid
        self.connected = True

        DummyThunderSocketHandler.dummyid += 1

    def send(self, message):
        pass

    def broadcast(self, users, message):
        pass


class MessengerTestCase(unittest.TestCase):
    def setUp(self):
        self.messenger = Messenger('apikey', 'apisecret')

    def tearDown(self):
        self.messenger = None

    def test_is_online(self):
        user1 = DummyThunderSocketHandler()

        self.assertFalse(self.messenger.is_user_online(user1.userid))

        self.messenger.register_user(user1)
        self.assertTrue(self.messenger.is_user_online(user1.userid))

        self.messenger.unregister_user(user1)
        self.assertFalse(self.messenger.is_user_online(user1.userid))

    def test_counters(self):
        user1 = DummyThunderSocketHandler()
        user2 = DummyThunderSocketHandler()

        self.messenger.register_user(user1)
        self.messenger.register_user(user2)
        self.assertEqual(self.messenger.get_user_count(), 2)
        self.assertEqual(self.messenger.get_connections_count(), 2)

        self.messenger.unregister_user(user1)
        self.assertEqual(self.messenger.get_user_count(), 1)
        self.assertEqual(self.messenger.get_connections_count(), 1)

        self.messenger.unregister_user(user2)
        self.assertEqual(self.messenger.get_user_count(), 0)
        self.assertEqual(self.messenger.get_connections_count(), 0)

    def test_user_unregister(self):
        user1 = DummyThunderSocketHandler()

        self.messenger.register_user(user1)
        self.messenger.unregister_user(user1)

        self.assertFalse(user1.userid in self.messenger.users)

    def test_multiple_connections(self):
        # testing multiple connections from same userid
        user1 = DummyThunderSocketHandler()
        user2 = DummyThunderSocketHandler()
        userid = user2.userid = user1.userid

        self.messenger.register_user(user1)
        self.messenger.register_user(user2)

        self.assertEqual(self.messenger.get_user_count(), 1)
        self.assertEqual(self.messenger.get_connections_count(), 2)
        self.assertTrue(userid in self.messenger.users)
        self.assertEqual(len(self.messenger.users[userid]), 2)
        self.assertTrue(user1 in self.messenger.users[userid])
        self.assertTrue(user2 in self.messenger.users[userid])

        self.messenger.unregister_user(user1)

        self.assertEqual(self.messenger.get_user_count(), 1)
        self.assertTrue(userid in self.messenger.users)
        self.assertEqual(len(self.messenger.users[userid]), 1)
        self.assertTrue(user2 in self.messenger.users[userid])

    def test_subscribe(self):
        user1 = DummyThunderSocketHandler()

        self.messenger.register_user(user1)
        self.messenger.subscribe_user_to_channel(user1, "test1")

        self.assertEqual(self.messenger.get_channel_user_count("test1"), 1)
        self.assertTrue(user1 in self.messenger.get_users_in_channel("test1"))

        self.messenger.unregister_user(user1)
        self.assertEqual(self.messenger.get_channel_user_count("test1"), 0)
        self.assertFalse(user1 in self.messenger.get_users_in_channel("test1"))
        self.assertFalse("test1" in self.messenger.channels)

    def test_subscribe(self):
        user1 = DummyThunderSocketHandler()

        self.messenger.register_user(user1)
        self.messenger.subscribe_user_to_channel(user1, "test1")

        self.assertEqual(self.messenger.get_channel_user_count("test1"), 1)
        self.assertTrue(user1 in self.messenger.get_users_in_channel("test1"))

        self.messenger.unsubscribe_user_from_channel(user1, "test1")
        self.assertEqual(self.messenger.get_channel_user_count("test1"), 0)
        self.assertFalse(user1 in self.messenger.get_users_in_channel("test1"))
        self.assertFalse("test1" in self.messenger.channels)

    def test_multiple_subscribe(self):
        # testing multiple subscribtions from same userid
        user1 = DummyThunderSocketHandler()
        user2 = DummyThunderSocketHandler()

        self.messenger.register_user(user1)
        self.messenger.register_user(user2)

        self.messenger.subscribe_user_to_channel(user1, "test1")
        self.messenger.subscribe_user_to_channel(user2, "test1")

        self.assertEqual(self.messenger.get_channel_user_count("test1"), 2)
        self.assertTrue(user1 in self.messenger.get_users_in_channel("test1"))
        self.assertTrue(user2 in self.messenger.get_users_in_channel("test1"))

        self.messenger.unregister_user(user1)

        self.assertEqual(self.messenger.get_channel_user_count("test1"), 1)
        self.assertFalse(user1 in self.messenger.get_users_in_channel("test1"))
        self.assertTrue(user2 in self.messenger.get_users_in_channel("test1"))

    def test_send_to_channel(self):
        count = self.messenger.send_to_channel("test1", "test message")
        self.assertEqual(count, 0)

        user1 = DummyThunderSocketHandler()

        self.messenger.register_user(user1)
        self.messenger.subscribe_user_to_channel(user1, "test1")

        count = self.messenger.send_to_channel("test1", "test message")
        self.assertEqual(count, 1)

    def test_send_to_user(self):
        user1 = DummyThunderSocketHandler()

        self.messenger.register_user(user1)
        count = self.messenger.send_to_user(user1.userid, "test message")

        self.assertEqual(count, 1)

    def test_send_to_multiple_users(self):
        user1 = DummyThunderSocketHandler()
        user2 = DummyThunderSocketHandler()
        userid = user2.userid = user1.userid

        self.messenger.register_user(user1)
        count = self.messenger.send_to_user(userid, "test message")

        self.assertEqual(count, 1)

        self.messenger.register_user(user2)
        count = self.messenger.send_to_user(userid, "test message")

        self.assertEqual(count, 2)


def suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(MessengerTestCase))
    return suite

########NEW FILE########

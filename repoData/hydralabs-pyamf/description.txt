= Addressbook Example =

This folder contains an example of the PyAMF SQLAlchemy adapter.

More info can be found in the documentation:
http://pyamf.org/tutorials/actionscript/addressbook.html

ByteArray example
=================

This example shows how to use the ByteArray class
in ActionScript 3.

To run the example, you need to start the server
(in python/server.py) and running on the same domain
where the SWF resides (in the example, localhost) 
and listening on port 8000.

More info can be found in the documentation:
http://pyamf.org/tutorials/actionscript/bytearray.html

=============
GeoIP Example
=============

This example uses the open source GeoIP APIs for looking
up the location of an IP address. The API includes support
for lookup of country, region, city, latitude, and longitude.

More info can be found in the documentation:
http://pyamf.org/tutorials/actionscript/geoip.html
Guestbook example
=================

This example shows how to create a simple guestbook
using Flex (client) and Twisted (remoting gateway).

More info can be found in the documentation:
http://pyamf.org/tutorials/actionscript/guestbook.html

Please note that the Twisted and Genshi packages are required to
run this example. Genshi is only used to sanitize the incoming
html for the guestbook messages.

Install via setuptools::

  easy_install Twisted
  easy_install Genshi

=================
Ohloh API Example
=================

The Ohloh API allows you to pull data from their database,
using a free API key you can get when you register on their
site. This examples retrieves a account based on the user's
email address and shows the profile associated.

More info can be found in the documentation:
http://pyamf.org/tutorials/actionscript/ohloh.html
RecordSet Example
=================

This folder contains the Flash and Python files
for the `RecordSet` example. 

More info can be found in the documentation:
http://pyamf.org/tutorials/actionscript/recordset.html

Local Shared Object Example
===========================

This example loads all your local Shared Object files and
displays their contents.

More info can be found in the documentation:
http://pyamf.org/tutorials/general/sharedobject.html
====================
Python Shell Example
====================

This example shows how to control a remote Python interpreter
from a Flex client.

More info can be found in the documentation:
http://pyamf.org/tutorials/actionscript/shell.html
= Simple Example =

This folder contains a basic example for PyAMF. 

More info can be found in the documentation:
http://pyamf.org/tutorials/actionscript/simple.html

=============
Socket server
=============

This examples shows how to use Socket class in
ActionScript 3, that allows you to make socket
connections and to read and write raw binary data.

To run the example, you need to start the server
(in python/server.py) and running on the same domain
where the SWF resides (in the example, localhost) 
and listening on port 8000.

More info can be found in the documentation:
http://pyamf.org/tutorials/actionscript/socket.html
=============
UDP example
=============

More info can be found in the documentation:
http://pyamf.org/tutorials/actionscript/udp.html
Google App Engine
=================

This is a Google App Engine example. More info can be found in the documentation:
http://pyamf.org/tutorials/gateways/appengine.html

Authentication
==============

This folder contains the authentication examples
for PyAMF.

More info can be found in the documentation:
http://pyamf.org/tutorials/general/authentication.html

***************
  Hello World 
***************

This folder contains the Hello World examples
for PyAMF. 

More info can be found in the documentation:
http://pyamf.org/tutorials/general/helloworld/index.html
Swing
=====

This folder contains the Jython Swing example for PyAMF. 

More info can be found in the documentation:
http://pyamf.org/tutorials/jython/swing.html

PyAMF_ provides Action Message Format (AMF_) support for Python_ that is
compatible with the `Adobe Flash Player`_. It includes integration with
Python web frameworks like Django_, Pylons_, Twisted_, SQLAlchemy_,
web2py_ and more_.

The `Adobe Integrated Runtime`_ and `Adobe Flash Player`_ use AMF to
communicate between an application and a remote server. AMF encodes
remote procedure calls (RPC) into a compact binary representation that
can be transferred over HTTP/HTTPS or the `RTMP/RTMPS`_ protocol.
Objects and data values are serialized into this binary format, which
increases performance, allowing applications to load data up to 10 times
faster than with text-based formats such as XML or SOAP.

AMF3, the default serialization for ActionScript_ 3.0, provides various
advantages over AMF0, which is used for ActionScript 1.0 and 2.0. AMF3
sends data over the network more efficiently than AMF0. AMF3 supports
sending ``int`` and ``uint`` objects as integers and supports data types
that are available only in ActionScript 3.0, such as ByteArray_,
ArrayCollection_, ObjectProxy_ and IExternalizable_.


.. _PyAMF: 	http://www.pyamf.org
.. _AMF: 	http://en.wikipedia.org/wiki/Action_Message_Format
.. _Python:	http://python.org
.. _Adobe Flash Player: http://en.wikipedia.org/wiki/Flash_Player
.. _Django:	http://djangoproject.com
.. _Pylons:	http://pylonshq.com
.. _Twisted:	http://twistedmatrix.com
.. _SQLAlchemy: http://sqlalchemy.org
.. _web2py:	http://www.web2py.com
.. _more:	http://pyamf.org/tutorials/index.html
.. _Adobe Integrated Runtime: http://en.wikipedia.org/wiki/Adobe_AIR
.. _RTMP/RTMPS:	http://en.wikipedia.org/wiki/Real_Time_Messaging_Protocol
.. _ActionScript: http://dev.pyamf.org/wiki/ActionScript
.. _ByteArray:	http://dev.pyamf.org/wiki/ByteArray
.. _ArrayCollection: http://dev.pyamf.org/wiki/ArrayCollection
.. _ObjectProxy: http://dev.pyamf.org/wiki/ObjectProxy
.. _IExternalizable: http://dev.pyamf.org/wiki/IExternalizable


Bar Problem
============

N friends are playing a game. Each of them has a list of numbers in front of himself.

Each of N friends chooses a number from his list and reports it to the game administrator. Then the game administrator sorts the reported numbers and shouts the K-th largest number.

You want to know the count all possible numbers that the game administrator can shout.

Input Format:

First line of the input contain an integer T, the number of testcases.
Then follow T testcases. For each test case the input is of the following format.

In the first line there are two numbers N and K. In each of next N lines there is an integer a_i followed by a_i integers, ith line describes the list for ith person.

All the numbers in all the lists are unique.

Output Format:

For each testcase print in a separate line the count of numbers that the game administrator can shout.

Sample Input 

      2
      3 3
      3 2 5 3
      3 8 1 6
      3 7 4 9
      20 11
      1 3
      1 2
      11 1 4 55 6 80 8 9 19 11 12 13
      15 14 177 16 17 18 10 20 21 22 37 24 25 26 27 28
      7 190 30 31 32 33 34 35
      12 81 23 195 39 40 41 42 43 49 45 46 47
      15 48 44 50 51 52 53 54 5 121 57 58 59 98 61 62
      3 63 64 65
      10 66 67 68 69 70 71 72 73 74 75
      4 76 91 29 79
      11 7 36 82 83 84 85 86 96 88 89 90
      17 77 92 93 172 95 87 97 60 99 100 101 102 103 135 186 106 107
      10 108 109 110 111 112 113 114 115 116 117
      1 118
      8 119 120 56 122 123 124 125 126
      9 127 128 129 130 131 132 133 134 104
      11 136 137 138 139 140 141 142 143 144 145 146
      20 147 148 149 150 151 152 153 154 159 156 157 158 155 180 161 162 163 164 165 166
      18 167 168 169 170 171 94 173 174 175 176 15 178 179 160 181 182 183 184
      17 185 105 187 188 189 78 191 192 193 194 38 196 197 198 199 200 201

       

Sample Output 

       6
       89 

        

Explanation

In the sample example given, for the first testcse N = 3 and K = 3. List for the first person is {2 5 3}
, {8 1 6} for second and {7 4 9} for the third. In this case all the numbers in {4, 5, 6, 7, 8, 9} have a chance to be the third biggest selected number.


All the numbers that can be shouted are : 

Testcase1:

        4 5 6 7 8 9

Testcase2:

        38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100 101 102 103 104 105 106 107 108 109- 110 111 112 113 114 115 116 117 118 119 120 121 122 123 124 125 126 

Constraints :

        1 <= T <= 5

        1 <= N <= 1000

        1 <= K, a_i <= N

all the numbers in input are positive and will fit in 32-bit integer

The beauty of a number X is the number of 1s in the binary representation of X.

Two players are plaing a game. There is number n written on a black board. The game is played as following:

Each time a player chooses an integer number (0 <= k) so that 2^k is less than n and (n-2^k) has as beautiful as n.
Next he removes n from blackboard and writes n-2^k instead.

The player that can not continue the game (there is no such k that satisfies the constrains) looses the game.

The First player starts the game and they play in turns alternatively. Knowing that both two players play optimally you have to specify the winner.

Input:

First line of the Input contains an integer T, the number of testcase. 0 <= T <= 5.

Then follow T lines, each containing an integer n.

n more than 0 and less than 10^9 +1.

Output

For each testcase print "First Player" if first player can win the game and "Second Player" otherwise.

Sample Input

7
1
2
8
16
42
1000
123

Sample Output

Second Player
First Player
First Player
Second Player
Second Player
First Player
Second Player

Explanation

In the first example n is 1 and first player can't change it so the winner is the second player.

In the second example n is 2, so the first player subtracts 1 (2^0) from n and the second player looses the game.

Mastermind
==========

Mastermind is a game of two players. In the beginning, first player decides a secret key, which is a sequence (s1,s2,...sk) where 0 < si <= n, Then second player makes guesses in rounds, where each guess is of form (g1,g2, ...gk), and after each guess first player calculates the score for the guess. Score for a guess is equal to number of i's for which we have gi = si.

For example if the secret key is (4,2,5,3,1) and the guess is (1,2,3,7,1),then the score is 2, because
g2 = s2 and g5 = s5. 

Given a sequence of guesses, and scores for each guess, your program must decide if there exists at least one secret key that generates those exact scores.

Input

First line of input contains a single integer C (1 <=C <= 100). C test-cases follow. First line of each test-case contains three integers n,k and q. (1 <=n,k <=11, 1<=q<=8). Next q lines contain the guesses.

Each guess consists of k integers gi,1, gi,2,....gi,k separated by a single space, followed by the score for the guess bi (1 <= gi,j <=n for all 1 <=i <=q, 1 <=j <=k; and 0 <= bi <=k )

Output

For each test-case, output "Yes" (without quotes), if there exists at least a secret key which generates those exact scores, otherwise output "No".

Sample Input
2

4 4 2
2 1 2 2 0
2 2 1 1 1
4 4 2
1 2 3 4 4
4 3 2 1 1

Sample Output

Yes 
No

Facebook Programming Challenges
===============================

URL: https://facebook.interviewstreet.com/recruit/challenges

This is a compilation of possible questions to be solved in order to be hired by Facebook
The main complexity of this problems are try to understand the formulation, the problems are really easy to solve.
The solutions are in Python, one of my favourite languages :)

Reverse Polish Notation
=======================

expression consisting of operands and binary operators can be written in Reverse Polish Notation (RPN) by writing both the operands followed by the operator. For example, 3 + (4 * 5) can be written as "3 4 5 * +".
 
 You are given a string consisting of x's and *'s. x represents an operand and * represents a binary operator. It is easy to see that not all such strings represent valid RPN expressions. For example, the "x*x" is not a valid RPN expression, while "xx*" and "xxx**" are valid expressions. What is the minimum number of insert, delete and replace operations needed to convert the given string into a valid RPN expression?
  
Input:
The first line contains the number of test cases T. T test cases follow. Each case contains a string consisting only of characters x and *.
   
Output:
Output T lines, one for each test case containing the least number of operations needed.
    
Constraints:
1 <= T <= 100
The length of the input string will be at most 100.

Sample Input:
5
x
xx*
xxx**
*xx
xx*xx**

Sample Output:
0
0
0
2
0

Explanation:
 
For the first three cases, the input expression is already a valid RPN, so the answer is 0.
For the fourth case, we can perform one delete, and one insert operation: *xx -> xx -> xx*

Secret Decoded
==============

Your task is to decode messages that were encoded with substitution ciphers. In a substitution cipher, all occurrences of a character are replaced by a different character. For example, in a cipher that replaces "a" with "d" and "b" with "e", the message "abb" is encoded as "dee".

The exact character mappings that are used in the substitution ciphers will not be known to you. However, the dictionary of words that were used will be given. You will be given multiple encoded messages to decode (one per line) and they may use different substitution ciphers. The same substitution cipher is used on all of the words in a particular message.

For each scrambled message in the input, your program should output a line with the input line, followed by the string " = " (without the quotes), followed by the decoded message.

NOTE: All inputs are from stdin and output to stdout. The input will be exactly like how it's given in the problem and

your output should exactly match the given example output

Example:

input file:

//dict
hello
there
yello
thorns
//secret
12334 51272
12334 514678


output:
12334 51272 = hello there
12334 514678 = hello thorns

You want to create a staff to use in your martial arts training, and it has to meet some specific requirements.
  
1. You want it to be composed of two smaller staves of equal length so that you can either use it as a single staff or as two smaller ones.

2. You want the full sized staff's center of gravity to be exactly in the middle of the staff.

You have a very, very long branch from which you can cut the pieces for your staff.  The mass of the branch varies significantly throughout it, so you use just any two pieces of the same length.  Given a description of the mass throughout the branch, determine the longest staff you can make, then return three integers on a single line, the first two indicating the first index of each half-staff, and the third indicating the length of each half-staff.
     
The input will be given on a single line as a string of digits [1-9], each digit representing the mass of a section of the branch.  All sections are the same size and the maximum length of the string is 500. Here is an example:
      
41111921111119
11119   11119

If the indicated sections are cut from the branch they will satisfy your requirements.  They are both the same length, and they can be put together as either 9111111119 or 1111991111, both of which have a center of gravity exactly in the center of the staff.


Center of gravity can be determined by taking a weighted average of the mass of each section of the staff.  Given the following distances and masses:
Distance: 12345678
Mass: 22241211

Sum of the mass of each section: 2 + 2 + 2 + 4 + 1 + 2 + 1 + 1 = 15
Weighted sum of the masses:
2*1 + 2*2 + 2*3 + 4*4 + 1*5 + 2*6 + 1*7 + 1*8 = 60
Weighted sum / regular sum = 60 / 15 = 4

This means that the center of mass is in section 4 of the staff.  If we wanted to use this staff the center of gravity would need to be (8+1)/2 = 4.5.

Here is an example problem:

131251141231
----    ----

If we take the sections indicated we get 1312 and 1231.  By reversing the first one and putting them together we get 21311231

Sum of the mass of each section: 2 + 1 + 3 + 1 + 1 + 2 + 3 + 1 = 14
Weight sum of the masses:
2*1 + 1*2 + 3*3 + 1*4 + 1*5 + 2*6 + 3*7 + 1*8 = 63
Weighted sum / regular sum = 63 / 14 = 4.5

This puts the center of mass exactly in the center of the staff, for a perfectly balanced staff.  There isn't a longer staff that can be made from this, so the answer to this problem is

0 8 4

Because the half-staves begin at indices 0 and 8 (in that order) and each is of length 4.

Unique substrings
=================

Given a string, find the number of unique substrings in it. The string will contain a maximum of 1000 characters and each character will be in the range 'a' - 'z' or 'A' - 'Z'. A substring is a contiguous set of characters. 

Sample input
abababababababababababababababababab

Sample output
71

Read the string from STDIN and output to STDOUT


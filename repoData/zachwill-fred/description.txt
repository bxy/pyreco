Copyright (c) 2012, Zach Williams.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

* Redistributions of source code must retain the above copyright notice,
  this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of Zach Williams nor the names of its contributors
  may be used to endorse or promote products derived from this software
  without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

fred
====

[![Build Status](https://travis-ci.org/zachwill/fred.png?branch=master)](https://travis-ci.org/zachwill/fred)

Python wrapper of the St. Louis Federal Reserve Bank's [FRED API web
service](http://api.stlouisfed.org/docs/fred/) for retrieving economic data.

FRED API Documentation:
[http://api.stlouisfed.org/docs/fred/](http://api.stlouisfed.org/docs/fred/)

Sign up for a FRED API key:
[http://api.stlouisfed.org/api_key.html](http://api.stlouisfed.org/api_key.html)

### API Key ###

You can save your API key, and have it automatically accessible, on the command line:

    $ export FRED_API_KEY=my_api_key


Usage
-----

This wrapper hopes to make working with the Fred API as easy as
possible.

```python
>>> import fred

# Save your FRED API key.
>>> fred.key('my_fred_api_key')


# Interact with economic data categories.
>>> fred.category()

>>> fred.categories(24)

>>> fred.children(24)

>>> fred.related(32073)

>>> fred.category(series=True)

>>> fred.category_series(123)


# Interact with economic data releases.
>>> fred.releases()

>>> fred.release(250)

>>> fred.dates()


# Interact with economic data series.
>>> fred.series('GNPCA')

>>> fred.series('GNPCA', release=True)

>>> fred.observations('AAA')

>>> fred.search('search term')

>>> fred.updates()

>>> fred.vintage('AAA')


# Query economic data sources.
>>> fred.sources()

>>> fred.source(23)
```

**NOTE**: Normally, data is returned in dictionary format instead of XML. If you're
looking for XML output, however, just pass in the `xml=True` keyword argument.

```python
>>> import fred

>>> fred.releases(xml=True)
```


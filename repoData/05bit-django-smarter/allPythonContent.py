__FILENAME__ = conf
# -*- coding: utf-8 -*-
#
# django-smarter documentation build configuration file, created by
# sphinx-quickstart on Mon Mar  5 19:54:14 2012.
#
# This file is execfile()d with the current directory set to its containing dir.
#
# Note that not all possible configuration values are present in this
# autogenerated file.
#
# All configuration values have a default; values that are commented out
# serve to show the default.

import sys, os

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#sys.path.insert(0, os.path.abspath('.'))

# -- General configuration -----------------------------------------------------

# If your documentation needs a minimal Sphinx version, state it here.
#needs_sphinx = '1.0'

# Add any Sphinx extension module names here, as strings. They can be extensions
# coming with Sphinx (named 'sphinx.ext.*') or your custom ones.
extensions = ['sphinx.ext.autodoc']

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# The suffix of source filenames.
source_suffix = '.rst'

# The encoding of source files.
#source_encoding = 'utf-8-sig'

# The master toctree document.
master_doc = 'index'

# General information about the project.
project = u'django-smarter'
copyright = u'2012, Alexey Kinyov'

# The version info for the project you're documenting, acts as replacement for
# |version| and |release|, also used in various other places throughout the
# built documents.
#
# The short X.Y version.
version = '1.0'
# The full version, including alpha/beta/rc tags.
release = '1.0 beta'

# The language for content autogenerated by Sphinx. Refer to documentation
# for a list of supported languages.
#language = None

# There are two options for replacing |today|: either, you set today to some
# non-false value, then it is used:
#today = ''
# Else, today_fmt is used as the format for a strftime call.
#today_fmt = '%B %d, %Y'

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
exclude_patterns = ['_build']

# The reST default role (used for this markup: `text`) to use for all documents.
#default_role = None

# If true, '()' will be appended to :func: etc. cross-reference text.
#add_function_parentheses = True

# If true, the current module name will be prepended to all description
# unit titles (such as .. function::).
#add_module_names = True

# If true, sectionauthor and moduleauthor directives will be shown in the
# output. They are ignored by default.
#show_authors = False

# The name of the Pygments (syntax highlighting) style to use.
pygments_style = 'sphinx'

# A list of ignored prefixes for module index sorting.
#modindex_common_prefix = []


# -- Options for HTML output ---------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
html_theme = 'default'

# Theme options are theme-specific and customize the look and feel of a theme
# further.  For a list of options available for each theme, see the
# documentation.
#html_theme_options = {}

# Add any paths that contain custom themes here, relative to this directory.
#html_theme_path = []

# The name for this set of Sphinx documents.  If None, it defaults to
# "<project> v<release> documentation".
#html_title = None

# A shorter title for the navigation bar.  Default is the same as html_title.
#html_short_title = None

# The name of an image file (relative to this directory) to place at the top
# of the sidebar.
#html_logo = None

# The name of an image file (within the static path) to use as favicon of the
# docs.  This file should be a Windows icon file (.ico) being 16x16 or 32x32
# pixels large.
#html_favicon = None

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# If not '', a 'Last updated on:' timestamp is inserted at every page bottom,
# using the given strftime format.
#html_last_updated_fmt = '%b %d, %Y'

# If true, SmartyPants will be used to convert quotes and dashes to
# typographically correct entities.
#html_use_smartypants = True

# Custom sidebar templates, maps document names to template names.
#html_sidebars = {}

# Additional templates that should be rendered to pages, maps page names to
# template names.
#html_additional_pages = {}

# If false, no module index is generated.
#html_domain_indices = True

# If false, no index is generated.
#html_use_index = True

# If true, the index is split into individual pages for each letter.
#html_split_index = False

# If true, links to the reST sources are added to the pages.
#html_show_sourcelink = True

# If true, "Created using Sphinx" is shown in the HTML footer. Default is True.
#html_show_sphinx = True

# If true, "(C) Copyright ..." is shown in the HTML footer. Default is True.
#html_show_copyright = True

# If true, an OpenSearch description file will be output, and all pages will
# contain a <link> tag referring to it.  The value of this option must be the
# base URL from which the finished HTML is served.
#html_use_opensearch = ''

# This is the file name suffix for HTML files (e.g. ".xhtml").
#html_file_suffix = None

# Output file base name for HTML help builder.
htmlhelp_basename = 'django-smarterdoc'


# -- Options for LaTeX output --------------------------------------------------

# The paper size ('letter' or 'a4').
#latex_paper_size = 'letter'

# The font size ('10pt', '11pt' or '12pt').
#latex_font_size = '10pt'

# Grouping the document tree into LaTeX files. List of tuples
# (source start file, target name, title, author, documentclass [howto/manual]).
latex_documents = [
  ('index', 'django-smarter.tex', u'django-smarter Documentation',
   u'Alexey Kinyov', 'manual'),
]

# The name of an image file (relative to this directory) to place at the top of
# the title page.
#latex_logo = None

# For "manual" documents, if this is true, then toplevel headings are parts,
# not chapters.
#latex_use_parts = False

# If true, show page references after internal links.
#latex_show_pagerefs = False

# If true, show URL addresses after external links.
#latex_show_urls = False

# Additional stuff for the LaTeX preamble.
#latex_preamble = ''

# Documents to append as an appendix to all manuals.
#latex_appendices = []

# If false, no module index is generated.
#latex_domain_indices = True


# -- Options for manual page output --------------------------------------------

# One entry per manual page. List of tuples
# (source start file, name, description, authors, manual section).
man_pages = [
    ('index', 'django-smarter', u'django-smarter Documentation',
     [u'Alexey Kinyov'], 1)
]

########NEW FILE########
__FILENAME__ = settings
# Django settings for example project.
import os.path as path
PROJECT_ROOT = path.realpath(path.dirname(__file__))

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': path.join(PROJECT_ROOT, 'example.db'), # Or path to database file if using sqlite3.
        'USER': '',                      # Not used with sqlite3.
        'PASSWORD': '',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.4/ref/settings/#allowed-hosts
ALLOWED_HOSTS = []

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'America/Chicago'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = path.join(PROJECT_ROOT, 'media')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = ''

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'x7t(^sg3a=^9lg7-k)1vv$_##*yyqm*3k3@%0)74r3qqvqz0)t'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'example.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'example.wsgi.application'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',

    'smarter',
    'pages',
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

########NEW FILE########
__FILENAME__ = urls
from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

import smarter
site = smarter.Site()

# Find the views in smarter_views.py
smarter.autodiscover()

# from pages.models import Page, PageFile
from pages.views import PageViews, PageFileViews
site.register(PageViews)
site.register(PageFileViews)

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'example.views.home', name='home'),
    # url(r'^example/', include('example.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Generic views
    url(r'^', include(site.urls)),

    # Views in smarter.site singleton
    url(r'^', include(smarter.site.urls)),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)


from django.conf import settings

if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
        }),
   )

########NEW FILE########
__FILENAME__ = wsgi
"""
WSGI config for example project.

This module contains the WSGI application used by Django's development server
and any production WSGI deployments. It should expose a module-level variable
named ``application``. Django's ``runserver`` and ``runfcgi`` commands discover
this application via the ``WSGI_APPLICATION`` setting.

Usually you will have the standard Django WSGI application here, but it also
might make sense to replace the whole Django WSGI application with a custom one
that later delegates to the Django one. For example, you could introduce WSGI
middleware here, or combine a Django application with an application of another
framework.

"""
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "example.settings")

# This application object is used by any WSGI server configured to use this
# file. This includes Django's development server, if the WSGI_APPLICATION
# setting points here.
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

# Apply WSGI middleware here.
# from helloworld.wsgi import HelloWorldApplication
# application = HelloWorldApplication(application)

########NEW FILE########
__FILENAME__ = manage
#!/usr/bin/env python
import os
import sys
from os.path import realpath, dirname, join as joinpath

lib_dir = realpath(joinpath(dirname(__file__), '..'))
if not lib_dir in sys.path:
    sys.path.append(lib_dir)

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "example.settings")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)

########NEW FILE########
__FILENAME__ = models
from django.db import models
from django.core.urlresolvers import reverse

class Page(models.Model):
    owner = models.ForeignKey('auth.User')
    title = models.CharField(max_length=100)
    text = models.TextField()

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('page-details', kwargs={'pk': self.pk})


class PageFile(models.Model):
    page = models.ForeignKey(Page)
    attachment = models.FileField(upload_to='files')

    def __unicode__(self):
        return unicode(self.attachment)

    def get_absolute_url(self):
        return self.attachment.url

########NEW FILE########
__FILENAME__ = smarter_views
import smarter
from .models import Page

class PageTestViews(smarter.GenericViews):
    'for test_automatic_view_discovery'
    model = Page

smarter.site.register(PageTestViews, base_url='autodiscovery-test/', prefix='autodiscovery-test')


########NEW FILE########
__FILENAME__ = tests
from django.test import TestCase
from django.core.urlresolvers import reverse

class ProjectLevelSmarterTest(TestCase):
    def test_automatic_view_discovery(self):
        '''
        Testing the automatic view discovery
        '''
        self.assertEqual(reverse('autodiscovery-test-index'), '/autodiscovery-test/')

########NEW FILE########
__FILENAME__ = views
import smarter
from .models import Page, PageFile

class PageViews(smarter.GenericViews):
    model = Page

    options = {
        'add': {
            'redirect': lambda view, request, **kwargs: view.get_url('index')
        },
        'edit': {
            'exclude': ('owner',),
            'redirect': lambda view, request, **kwargs: kwargs['obj'].get_absolute_url()
        }
    }

class PageFileViews(smarter.GenericViews):
    model = PageFile

    options = {
        'edit': None,
        'details': None,
        'add': {
            'redirect': lambda view, request, **kwargs: view.get_url('index')
        }
    }
########NEW FILE########
__FILENAME__ = models

########NEW FILE########
__FILENAME__ = tests
"""
Unit tests for django-smarter.
"""
from django.conf.urls import patterns, include, url
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import resolve, reverse, Resolver404
from django.http import HttpResponse
from django.db import models
from django.test import TestCase
from django.test.client import Client
import smarter

# Custom urls for tests
urlpatterns = patterns('',)


def handler404(request):
    """Custom 404 handler for tests."""
    from django.http import HttpResponseNotFound
    return HttpResponseNotFound('Not found: %s' % request.path)


class TestModel(models.Model):
    """Model for tests."""
    text = models.TextField()
    is_published = models.BooleanField(default=True)

    def get_absolute_url(self):
        return ('/test/testmodel/%s/' % self.pk)


class AnotherTestModel(models.Model):
    """Well, another model for tests."""
    another_text = models.TextField()


class TestViews(smarter.GenericViews):
    options = {
        'add': {
            'initial': ('text',),
            'fields': ('text',),
        },

        'publish': {
            'url': r'(?P<pk>\d+)/publish/',
            'exclude': ('text',),
        },

        'details-extended': {
            'url': r'(?P<pk>\d+)/extended/',
            'template': 'details_extended.html',
            'form': None,
        },

        'decorated': {
            'url': r'(?P<pk>\d+)/decorated/',
            'form': None,
            'decorators': (login_required,),
        },

        'protected': {
            'url': r'(?P<pk>\d+)/protected/',
            'form': None,
            'permissions': ('smarter.view_testmodel',)
        },
    }


class AnotherTestViews(smarter.GenericViews):
    options = {
        # 'index': None, # Won't be enabled
        'add': None, # Won't be enabled
    }


class Tests(TestCase):
    urls = 'smarter.tests'

    def setUp(self):
        self.client = Client()
        self.site = smarter.Site()
        self.site.register(TestViews, TestModel)
        self.site.register(AnotherTestViews, AnotherTestModel, base_url='another/')
        TestModel.objects.create(id=1, text='The first object.')

        global urlpatterns
        if not len(urlpatterns):
            urlpatterns += patterns('', url(r'^test/', include(self.site.urls)),)

    def _test_url(self, url, status=200):
        self.assertEqual(self.client.get(url).status_code, status)

    def test_site_urls_registering(self):
        """
        Test registering and unregistering urls.
        """
        self.assertTrue(resolve('/test/testmodel/')) # index
        self.assertTrue(resolve('/test/testmodel/add/')) # add
        self.assertTrue(resolve('/test/testmodel/1/edit/')) # edit
        self.assertTrue(resolve('/test/testmodel/2/')) # details
        self.assertTrue(resolve('/test/testmodel/2/remove/')) # remove
        try:
            self.assertTrue(resolve('/test/testmodel/lalala/')) # no such url
        except Resolver404:
            pass

        #site.unregister(TestModel) #still unimplemented
        #self.assertEqual(len(site.urls), 0)
        #will fail because unregister() is still unimplemented

    def test_urls_reversing(self):
        reverse('testmodel-index')
        reverse('testmodel-add')
        reverse('testmodel-edit', kwargs={'pk': 1})
        reverse('testmodel-remove', kwargs={'pk': 1})
        reverse('testmodel-details', kwargs={'pk': 1})

    def test_generic_views_read(self):
        """
        Test views reading with client requests.
        """
        self._test_url('/test/testmodel/')
        self._test_url('/test/testmodel/add/')
        self._test_url('/test/testmodel/100/', 404)
        TestModel.objects.create(id=100, text='Lalala!')
        self._test_url('/test/testmodel/100/')
        self._test_url('/test/testmodel/100/edit/')
        self._test_url('/test/testmodel/100/remove/')
        self._test_url('/test/fakeprefix-testmodel/', 404)

    def test_initial_option(self):
        r = self.client.get('/test/testmodel/add/?text=Hohoho!')
        self.assertTrue('Hohoho!</textarea>' in r.content)

    def test_fields_option(self):
        r = self.client.get('/test/testmodel/add/')
        self.assertTrue(not 'id_is_published' in r.content)

    def test_exclude_option(self):
        r = self.client.get('/test/testmodel/publish/')
        self.assertTrue(not 'id_text' in r.content)        

    def test_generic_views_write(self):
        """
        Test views writing with client requests.
        """
        r = self.client.post('/test/testmodel/add/', {'text': "Hahaha!"})
        self.assertRedirects(r, '/test/testmodel/2/')
        self.assertEqual(TestModel.objects.get(pk=2).text, "Hahaha!")

        r = self.client.post('/test/testmodel/2/edit/', {'text': "Lalala!"})
        self.assertRedirects(r, '/test/testmodel/2/')
        self.assertEqual(TestModel.objects.get(pk=2).text, "Lalala!")

    def test_custom_views_read(self):
        from django.template import TemplateDoesNotExist
        try:
            self._test_url('/test/testmodel/1/extended/')
            raise Exception("Template was found some way, but it should not!")
        except TemplateDoesNotExist:
            pass

    def test_remove_view(self):
        TestModel.objects.create(id=200, text='Oh! They want to remove me!')
        self._test_url('/test/testmodel/200/remove/')
        self._test_url('/test/testmodel/200/') # GET can't remove
        self.client.post('/test/testmodel/200/remove/')
        self._test_url('/test/testmodel/200/', 404) # POST can!

    def test_decorated_view(self):
        with self.settings(LOGIN_URL='/test/testmodel/'):
            r = self.client.get('/test/testmodel/1/decorated/')
            self.assertRedirects(r, '/test/testmodel/?next=/test/testmodel/1/decorated/')

    def test_disabled_view(self):
        self._test_url('/test/another/')
        self._test_url('/test/another/add/', 404)

    def test_permissions(self):
        self._test_url('/test/testmodel/1/protected/', 403)


class TestSingletonSite(TestCase):
    def test_singleton_site_exists(self):
        from smarter import site
        self.assertTrue(isinstance(site, smarter.Site))

########NEW FILE########

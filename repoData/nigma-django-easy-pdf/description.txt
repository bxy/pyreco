django-easy-pdf
===============

Django PDF rendering, the easy way.

.. image:: https://pypip.in/v/django-easy-pdf/badge.png
    :target: https://pypi.python.org/pypi/django-easy-pdf/
    :alt: Latest Version

.. image:: https://pypip.in/d/django-easy-pdf/badge.png
    :target: https://pypi.python.org/pypi/django-easy-pdf/
    :alt: Downloads

.. image:: https://pypip.in/license/django-easy-pdf/badge.png
    :target: https://pypi.python.org/pypi/django-easy-pdf/
    :alt: License

Developed at `en.ig.ma software shop <http://en.ig.ma>`_.


Overview
--------

This app makes rendering PDF files in Django really easy.
It can be used to create invoices, bills and other documents
from simple HTML markup and CSS styles. You can even embed images
and use custom fonts.

The library provides both Class-Based View that is almost a drop-in
replacement for Django's ``TemplateView`` as well as helper functions
to render PDFs in the backend outside the request scope
(i.e. using Celery workers).


Quickstart
----------

1. Include ``django-easy-pdf``, ``xhtml2pdf>=0.0.6`` and ``reportlab>=2.7,<3``
   in your ``requirements.txt`` file.

2. Add ``easy_pdf`` to ``INSTALLED_APPS``.

3. Create HTML template for PDF document and add a view that will render it:

    .. code-block:: css+django

        {% extends "easy_pdf/base.html" %}

        {% block content %}
            <div id="content">
                <h1>Hi there!</h1>
            </div>
        {% endblock %}

    .. code-block:: python

        from easy_pdf.views import PDFTemplateView

        class HelloPDFView(PDFTemplateView):
            template_name = "hello.html"


Documentation
-------------

The full documentation is at `django-easy-pdf.rtfd.org <http://django-easy-pdf.rtfd.org>`_.

A live demo is at `easy-pdf.herokuapp.com <https://easy-pdf.herokuapp.com/>`_.
You can run it locally after installing dependencies by running ``python demo.py``
script from the cloned repository.

Dependencies
------------

``django-easy-pdf`` depends on:

    - ``django>=1.5.1``
    - ``xhtml2pdf>=0.0.6``
    - ``reportlab>=2.7,<3``


License
-------

``django-easy-pdf`` is released under the MIT license.


Other Resources
---------------

- GitHub repository - https://github.com/nigma/django-easy-pdf
- PyPi Package site - http://pypi.python.org/pypi/django-easy-pdf


Commercial Support
------------------

This app and many other help us build better software
and focus on delivering quality projects faster.
We would love to help you with your next project so get in touch
by dropping an email at en@ig.ma.


Lato font family

================

NOTE: DO NOT INSTALL THE "OTF" and "TTF" formats simultaneously!

Version 1.105; Western+Polish opensource

Created by: tyPoland Lukasz Dziedzic
Creation year: 2013

Copyright (c) 2010-2013 by tyPoland Lukasz Dziedzic with Reserved Font Name "Lato". Licensed under the SIL Open Font License, Version 1.1.

Lato is a trademark of tyPoland Lukasz Dziedzic.

Source URL: http://www.latofonts.com/
License URL: http://scripts.sil.org/OFL

================

Lato is a sanserif typeface family designed in the Summer 2010 by Warsaw-based designer Łukasz Dziedzic (“Lato” means “Summer” in Polish). In December 2010 the Lato family was published under the open-source Open Font License by his foundry tyPoland, with support from Google.

In the last ten or so years, during which Łukasz has been designing type, most of his projects were rooted in a particular design task that he needed to solve. With Lato, it was no different. Originally, the family was conceived as a set of corporate fonts for a large client — who in the end decided to go in different stylistic direction, so the family became available for a public release.

When working on Lato, Łukasz tried to carefully balance some potentially conflicting priorities. He wanted to create a typeface that would seem quite “transparent” when used in body text but would display some original traits when used in larger sizes. He used classical proportions (particularly visible in the uppercase) to give the letterforms familiar harmony and elegance. At the same time, he created a sleek sanserif look, which makes evident the fact that Lato was designed in 2010 — even though it does not follow any current trend.

The semi-rounded details of the letters give Lato a feeling of warmth, while the strong structure provides stability and seriousness. “Male and female, serious but friendly. With the feeling of the Summer,” says Łukasz.

Lato consists of five weights (plus corresponding italics), including a beautiful hairline style.

================

REVISION LOG:

# Version 1.105 (2013-06-04)
Created separate TTF and OTF versions.
The TTF version is autohinted using ttfautohint 0.95.
Changed naming for Microsoft Office 2011 for Mac

# Version 1.104 (2011-11-08)
Merged the distribution again
Autohinted with updated ttfautohint 0.4 (which no longer causes Adobe and iOS problems)
except the Hai and Lig weights which are hinted in FLS 5.1.

# Version 1.102 (2011-10-28)
Added OpenType Layout features
Split between desktop and web versions
Desktop version: all weights autohinted with FontLab Studio
Web version autohinted with ttfautohint 0.4 except the Hai and Lig weights

# Version 1.101 (2011-09-30)
Fixed OS/2 table Unicode and codepage entries

# Version 1.100 (2011-09-12)
Added Polish diacritics to the character set
Weights Hai and Lig autohinted with FontLab Studio
Other weights autohinted with ttfautohint 0.3

# Version 1.011 (2010-12-29)
Added the soft hyphen glyph

# Version 1.010 (2010-12-13)
Initial version released under SIL Open Font License
Western character set

================


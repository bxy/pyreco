__FILENAME__ = demo
#!/bin/env python
#-*- coding: utf-8 -*-

"""
Demo script. Run:

python.exe demo.py
"""

from __future__ import absolute_import, division, print_function, unicode_literals

import os
import logging

logging.basicConfig()

from django.conf import settings
from django.conf.urls import patterns, url
from django.core.wsgi import get_wsgi_application
from django.utils.timezone import now as tznow

basename = os.path.splitext(os.path.basename(__file__))[0]


def rel(*path):
    return os.path.abspath(
        os.path.join(os.path.dirname(__file__), *path)
    ).replace("\\", "/")


if not settings.configured:
    settings.configure(
        DEBUG=True,
        TIMEZONE="UTC",
        INSTALLED_APPS=["easy_pdf"],
        TEMPLATE_DIRS=[rel("tests", "templates")],
        STATIC_ROOT=os.path.abspath(rel("tests", "static")),
        ROOT_URLCONF=basename,
        WSGI_APPLICATION="{}.application".format(basename),
    )

from easy_pdf.views import PDFTemplateView


class HelloPDFView(PDFTemplateView):
    template_name = "hello.html"

    def get_context_data(self, **kwargs):
        return super(HelloPDFView, self).get_context_data(
            pagesize="A4",
            title="Hi there!",
            today=tznow(),
            **kwargs
        )

urlpatterns = patterns("",
    url(r"^$", HelloPDFView.as_view())
)

application = get_wsgi_application()


if __name__ == "__main__":
    from django.core.management import call_command
    call_command("runserver", "8000")

########NEW FILE########
__FILENAME__ = conf
# -*- coding: utf-8 -*-
#
# django-easy-pdf documentation build configuration file, created by
# sphinx-quickstart on Sun Jan 12 20:56:38 2014.
#
# This file is execfile()d with the current directory set to its
# containing dir.
#
# Note that not all possible configuration values are present in this
# autogenerated file.
#
# All configuration values have a default; values that are commented out
# serve to show the default.

import sys
import os
import datetime
import jinja2.filters

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#sys.path.insert(0, os.path.abspath('.'))

cwd = os.getcwd()
parent = os.path.dirname(cwd)
sys.path.append(parent)

import django.conf
django.conf.settings.configure()

import easy_pdf

# -- General configuration ------------------------------------------------

# If your documentation needs a minimal Sphinx version, state it here.
#needs_sphinx = '1.0'

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = ['sphinx.ext.autodoc', 'sphinx.ext.intersphinx']


# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# The suffix of source filenames.
source_suffix = '.rst'

# The encoding of source files.
#source_encoding = 'utf-8-sig'

# The master toctree document.
master_doc = 'index'

# General information about the project.
project = 'django-easy-pdf'
copyright = jinja2.filters.do_mark_safe('%s, <a href="http://en.ig.ma/">Filip Wasilewski</a>' % datetime.date.today().year)

# The version info for the project you're documenting, acts as replacement for
# |version| and |release|, also used in various other places throughout the
# built documents.
#
# The short X.Y version.
version = easy_pdf.__version__
# The full version, including alpha/beta/rc tags.
release = easy_pdf.__version__

# The language for content autogenerated by Sphinx. Refer to documentation
# for a list of supported languages.
#language = None

# There are two options for replacing |today|: either, you set today to some
# non-false value, then it is used:
#today = ''
# Else, today_fmt is used as the format for a strftime call.
#today_fmt = '%B %d, %Y'

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
exclude_patterns = ['_build']

# The reST default role (used for this markup: `text`) to use for all
# documents.
#default_role = None

# If true, '()' will be appended to :func: etc. cross-reference text.
#add_function_parentheses = True

# If true, the current module name will be prepended to all description
# unit titles (such as .. function::).
#add_module_names = True

# If true, sectionauthor and moduleauthor directives will be shown in the
# output. They are ignored by default.
#show_authors = False

# The name of the Pygments (syntax highlighting) style to use.
pygments_style = 'sphinx'

# A list of ignored prefixes for module index sorting.
modindex_common_prefix = [
    "easy_pdf."
]

# If true, keep warnings as "system message" paragraphs in the built documents.
#keep_warnings = False


# -- Options for HTML output ----------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.

try:
    import sphinx_rtd_theme
    html_theme = 'sphinx_rtd_theme'
    html_theme_path = [
        os.path.abspath(os.path.join(os.path.dirname(__file__), "_templates")),
        sphinx_rtd_theme.get_html_theme_path()
    ]
except ImportError:
    html_theme = 'default'

# Theme options are theme-specific and customize the look and feel of a theme
# further.  For a list of options available for each theme, see the
# documentation.
#html_theme_options = {}

# Add any paths that contain custom themes here, relative to this directory.
#html_theme_path = []

# The name for this set of Sphinx documents.  If None, it defaults to
# "<project> v<release> documentation".
#html_title = None

# A shorter title for the navigation bar.  Default is the same as html_title.
#html_short_title = None

# The name of an image file (relative to this directory) to place at the top
# of the sidebar.
#html_logo = None

# The name of an image file (within the static path) to use as favicon of the
# docs.  This file should be a Windows icon file (.ico) being 16x16 or 32x32
# pixels large.
#html_favicon = None

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# Add any extra paths that contain custom files (such as robots.txt or
# .htaccess) here, relative to this directory. These files are copied
# directly to the root of the documentation.
#html_extra_path = []

# If not '', a 'Last updated on:' timestamp is inserted at every page bottom,
# using the given strftime format.
#html_last_updated_fmt = '%b %d, %Y'

# If true, SmartyPants will be used to convert quotes and dashes to
# typographically correct entities.
#html_use_smartypants = True

# Custom sidebar templates, maps document names to template names.
#html_sidebars = {}

# Additional templates that should be rendered to pages, maps page names to
# template names.
#html_additional_pages = {}

# If false, no module index is generated.
#html_domain_indices = True

# If false, no index is generated.
#html_use_index = True

# If true, the index is split into individual pages for each letter.
#html_split_index = False

# If true, links to the reST sources are added to the pages.
#html_show_sourcelink = True

# If true, "Created using Sphinx" is shown in the HTML footer. Default is True.
#html_show_sphinx = True

# If true, "(C) Copyright ..." is shown in the HTML footer. Default is True.
#html_show_copyright = True

# If true, an OpenSearch description file will be output, and all pages will
# contain a <link> tag referring to it.  The value of this option must be the
# base URL from which the finished HTML is served.
#html_use_opensearch = ''

# This is the file name suffix for HTML files (e.g. ".xhtml").
#html_file_suffix = None

# Output file base name for HTML help builder.
htmlhelp_basename = 'django-easy-pdfdoc'


# -- Options for LaTeX output ---------------------------------------------

latex_elements = {
# The paper size ('letterpaper' or 'a4paper').
#'papersize': 'letterpaper',

# The font size ('10pt', '11pt' or '12pt').
#'pointsize': '10pt',

# Additional stuff for the LaTeX preamble.
#'preamble': '',
}

# Grouping the document tree into LaTeX files. List of tuples
# (source start file, target name, title,
#  author, documentclass [howto, manual, or own class]).
latex_documents = [
  ('index', 'django-easy-pdf.tex', u'django-easy-pdf Documentation',
   u'Filip Wasilewski', 'manual'),
]

# The name of an image file (relative to this directory) to place at the top of
# the title page.
#latex_logo = None

# For "manual" documents, if this is true, then toplevel headings are parts,
# not chapters.
#latex_use_parts = False

# If true, show page references after internal links.
#latex_show_pagerefs = False

# If true, show URL addresses after external links.
#latex_show_urls = False

# Documents to append as an appendix to all manuals.
#latex_appendices = []

# If false, no module index is generated.
#latex_domain_indices = True


# -- Options for manual page output ---------------------------------------

# One entry per manual page. List of tuples
# (source start file, name, description, authors, manual section).
man_pages = [
    ('index', 'django-easy-pdf', u'django-easy-pdf Documentation',
     [u'Filip Wasilewski'], 1)
]

# If true, show URL addresses after external links.
#man_show_urls = False


# -- Options for Texinfo output -------------------------------------------

# Grouping the document tree into Texinfo files. List of tuples
# (source start file, target name, title, author,
#  dir menu entry, description, category)
texinfo_documents = [
  ('index', 'django-easy-pdf', u'django-easy-pdf Documentation',
   u'Filip Wasilewski', 'django-easy-pdf', 'One line description of project.',
   'Miscellaneous'),
]

# Documents to append as an appendix to all manuals.
#texinfo_appendices = []

# If false, no module index is generated.
#texinfo_domain_indices = True

# How to display URL addresses: 'footnote', 'no', or 'inline'.
#texinfo_show_urls = 'footnote'

# If true, do not generate a @detailmenu in the "Top" node's menu.
#texinfo_no_detailmenu = False

# Links to Python's docs should reference the most recent version of the 2.x
# branch, which is located at this URL.
intersphinx_mapping = {
    'python': ('http://docs.python.org/', None),
    'sphinx': ('http://sphinx-doc.org/', None),
    'django': ('http://docs.djangoproject.com/en/1.6/', 'http://docs.djangoproject.com/en/1.6/_objects/'),
}

# Python's docs don't change every week.
intersphinx_cache_limit = 2  # days

autodoc_member_order = 'bysource'
autodoc_default_flags = ['undoc-members']

########NEW FILE########
__FILENAME__ = exceptions
#-*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function, unicode_literals


class EasyPDFError(Exception):
    """
    Base error class
    """


class UnsupportedMediaPathException(EasyPDFError):
    """
    Resource not found or unavailable
    """


class PDFRenderingError(EasyPDFError):
    """
    PDF Rendering error. Check HTML template for errors.
    """

    def __init__(self, message, content, log, *args, **kwargs):
        super(PDFRenderingError, self).__init__(message, *args, **kwargs)
        self.content = content
        self.log = log

########NEW FILE########
__FILENAME__ = models
#-*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function, unicode_literals


########NEW FILE########
__FILENAME__ = rendering
#-*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function, unicode_literals

import logging
import os

from django.conf import settings
from django.template import loader, Context, RequestContext
from django.http import HttpResponse
from django.utils.http import urlquote
from django.utils.six import BytesIO

import xhtml2pdf.default
from xhtml2pdf import pisa

from .exceptions import UnsupportedMediaPathException, PDFRenderingError

logger = logging.getLogger("app.pdf")
logger_x2p = logging.getLogger("app.pdf.xhtml2pdf")


def fetch_resources(uri, rel):
    """
    Retrieves embeddable resource from given ``uri``.

    For now only local resources (images, fonts) are supported.

    :param str uri: path or url to image or font resource
    :returns: path to local resource file.
    :rtype: str
    :raises: :exc:`~easy_pdf.exceptions.UnsupportedMediaPathException`
    """
    if settings.STATIC_URL and uri.startswith(settings.STATIC_URL):
        path = os.path.join(settings.STATIC_ROOT, uri.replace(settings.STATIC_URL, ""))
    elif settings.MEDIA_URL and uri.startswith(settings.MEDIA_URL):
        path = os.path.join(settings.MEDIA_ROOT, uri.replace(settings.MEDIA_URL, ""))
    else:
        path = os.path.join(settings.STATIC_ROOT, uri)

    if not os.path.isfile(path):
        raise UnsupportedMediaPathException(
            "media urls must start with {} or {}".format(
                settings.MEDIA_ROOT, settings.STATIC_ROOT
            )
        )

    return path.replace("\\", "/")


def html_to_pdf(content, encoding="utf-8",
                link_callback=fetch_resources, **kwargs):
    """
    Converts html ``content`` into PDF document.

    :param unicode content: html content
    :returns: PDF content
    :rtype: :class:`bytes`
    :raises: :exc:`~easy_pdf.exceptions.PDFRenderingError`
    """
    src = BytesIO(content.encode(encoding))
    dest = BytesIO()

    pdf = pisa.pisaDocument(src, dest, encoding=encoding,
                            link_callback=link_callback, **kwargs)
    if pdf.err:
        logger.error("Error rendering PDF document")
        for entry in pdf.log:
            if entry[0] == xhtml2pdf.default.PML_ERROR:
                logger_x2p.error("line %s, msg: %s, fragment: %s", entry[1], entry[2], entry[3])
        raise PDFRenderingError("Errors rendering PDF", content=content, log=pdf.log)

    if pdf.warn:
        for entry in pdf.log:
            if entry[0] == xhtml2pdf.default.PML_WARNING:
                logger_x2p.warning("line %s, msg: %s, fragment: %s", entry[1], entry[2], entry[3])

    return dest.getvalue()


def encode_filename(filename):
    """
    Encodes filename part for ``Content-Disposition: attachment``.

    >>> print(encode_filename("abc.pdf"))
    filename=abc.pdf
    >>> print(encode_filename("aa bb.pdf"))
    filename*=UTF-8''aa%20bb.pdf
    >>> print(encode_filename(u"zażółć.pdf"))
    filename*=UTF-8''za%C5%BC%C3%B3%C5%82%C4%87.pdf
    """
    # TODO: http://greenbytes.de/tech/webdav/rfc6266.html
    # TODO: http://greenbytes.de/tech/tc2231/

    quoted = urlquote(filename)
    if quoted == filename:
        return "filename=%s" % filename
    else:
        return "filename*=UTF-8''%s" % quoted


def make_response(content, filename=None, content_type="application/pdf"):
    """
    Wraps content into HTTP response.

    If ``filename`` is specified then ``Content-Disposition: attachment``
    header is added to the response.

    Default ``Content-Type`` is ``application/pdf``.

    :param bytes content: response content
    :param str filename: optional filename for file download
    :param str content_type: response content type
    :rtype: :class:`django.http.HttpResponse`
    """
    response = HttpResponse(content, content_type=content_type)
    if filename is not None:
        response["Content-Disposition"] = "attachment; %s" % encode_filename(filename)
    return response


def render_to_pdf(template, context, encoding="utf-8", **kwargs):
    """
    Create PDF document from Django html template.

    :param str template: path to Django template
    :param context: template context
    :type context: :class:`dict` or :class:`django.template.Context`

    :returns: rendered PDF
    :rtype: :class:`bytes`

    :raises: :exc:`~easy_pdf.exceptions.PDFRenderingError`, :exc:`~easy_pdf.exceptions.UnsupportedMediaPathException`
    """
    if not isinstance(context, Context):
        context = Context(context)

    content = loader.render_to_string(template, context)
    return html_to_pdf(content, encoding, **kwargs)


def render_to_pdf_response(request, template, context, filename=None,
                           encoding="utf-8", **kwargs):
    """
    Renders a PDF response using given ``request``, ``template`` and ``context``.

    If ``filename`` param is specified then the response ``Content-Disposition``
    header will be set to ``attachment`` making the browser display
    a "Save as.." dialog.

    :param request: Django request
    :type request: :class:`django.http.HttpRequest`
    :param str template: path to Django template
    :param context: template context
    :type context: :class:`dict` or :class:`django.template.Context`
    :rtype: :class:`django.http.HttpResponse`
    """

    if not isinstance(context, Context):
        if request is not None:
            context = RequestContext(request, context)
        else:
            context = Context(context)

    try:
        pdf = render_to_pdf(template, context, encoding=encoding, **kwargs)
        return make_response(pdf, filename)
    except PDFRenderingError as e:
        logger.exception(e.message)
        return HttpResponse(e.message)

########NEW FILE########
__FILENAME__ = views
#-*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function, unicode_literals

import copy

from django.views.generic.base import TemplateResponseMixin, ContextMixin, View

from .rendering import render_to_pdf_response


class PDFTemplateResponseMixin(TemplateResponseMixin):
    """
    A mixin class that implements PDF rendering and Django response construction.
    """

    #: Optional name of the PDF file for download. Leave blank for display in browser.
    pdf_filename = None

    #: Additional params passed to :func:`render_to_pdf_response`
    pdf_kwargs = None

    def get_pdf_filename(self):
        """
        Returns :attr:`pdf_filename` value by default.

        If left blank the browser will display the PDF inline.
        Otherwise it will pop up the "Save as.." dialog.

        :rtype: :func:`str`
        """
        return self.pdf_filename

    def get_pdf_kwargs(self):
        """
        Returns :attr:`pdf_kwargs` by default.

        The kwargs are passed to :func:`render_to_pdf_response` and
        :func:`xhtml2pdf.pisa.pisaDocument`.

        :rtype: :class:`dict`
        """
        if self.pdf_kwargs is None:
            return {}
        return copy.copy(self.pdf_kwargs)

    def get_pdf_response(self, context, **response_kwargs):
        """
        Renders PDF document and prepares response.

        :returns: Django HTTP response
        :rtype: :class:`django.http.HttpResponse`
        """
        return render_to_pdf_response(
            request=self.request,
            template=self.get_template_names(),
            context=context,
            filename=self.get_pdf_filename(),
            **self.get_pdf_kwargs()
        )

    def render_to_response(self, context, **response_kwargs):
        return self.get_pdf_response(context, **response_kwargs)


class PDFTemplateView(PDFTemplateResponseMixin, ContextMixin, View):
    """
    Concrete view for serving PDF files.

    .. code-block:: python

        class HelloPDFView(PDFTemplateView):
            template_name = "hello.html"
    """

    def get(self, request, *args, **kwargs):
        """
        Handles GET request and returns HTTP response.
        """
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)

########NEW FILE########
__FILENAME__ = runtests
#-*- coding: utf-8 -*-

import os
import sys
from optparse import OptionParser

from django.conf import settings


def rel(*path):
    return os.path.abspath(
        os.path.join(os.path.dirname(__file__), *path)
    ).replace("\\", "/")


if not settings.configured or not os.environ.get("DJANGO_SETTINGS_MODULE"):
    settings.configure(
        DEBUG=True,
        USE_TZ=True,
        DATABASES={
            "default": {
                "ENGINE": "django.db.backends.sqlite3",
            }
        },
        INSTALLED_APPS=[
            "easy_pdf"
        ],
        TEMPLATE_DIRS=[rel("tests", "templates")],
        STATIC_ROOT=os.path.abspath(rel("tests", "static")),
        ROOT_URLCONF="tests.urls",
    )

from django.test.utils import get_runner


def run_tests(verbosity, interactive, failfast, test_labels):
    if not test_labels:
        test_labels = ["tests"]

    if not hasattr(settings, "TEST_RUNNER"):
        settings.TEST_RUNNER = "django.test.runner.DiscoverRunner"
    TestRunner = get_runner(settings)

    test_runner = TestRunner(
        verbosity=verbosity,
        interactive=interactive,
        failfast=failfast
    )

    failures = test_runner.run_tests(test_labels)
    if failures:
        sys.exit(bool(failures))


if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("--failfast", action="store_true",
                      default=False, dest="failfast")
    parser.add_option("--verbosity", action="store",
                      default=1, type=int, dest="verbosity")
    (options, args) = parser.parse_args()
    run_tests(options.verbosity, options.failfast, False, args)

########NEW FILE########
__FILENAME__ = models
#-*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function, unicode_literals

########NEW FILE########
__FILENAME__ = tests
#-*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function, unicode_literals

from django.conf import settings
from django.test.testcases import TestCase
from django.test.client import RequestFactory, Client


class EasyPDFTestCase(TestCase):

    def test_pdf_rendering(self):
        response = self.client.get("/simple/")
        content = response.content
        self.assertEqual(content[:4], "%PDF")

########NEW FILE########
__FILENAME__ = urls
#-*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function, unicode_literals

from django.conf.urls import patterns, url

from easy_pdf.views import PDFTemplateView

urlpatterns = patterns("",
    url(r"^simple/", PDFTemplateView.as_view(template_name="simple.html")),
)

########NEW FILE########

This directory contains all modules falling in the broad category of "logs analysis".
This includes:
- get_server_logs: iterates over 'full' backend logs and merges them per day and instance
- analyze_merged_logs: iterates over merged 'full' backend logs and computes various statistics
- itunes_trends: fetches daily stats from iTunesConnect
- get_table_sizes: looks up dynamodb table sizes

These jobs all touch common stats: S3 logs or dynamodb metrics table.
They all require individual locks to prevent concurrent instances from running.
They are run serially by 'run_logs_analysis.py' to guarantee order and prevent concurrent runs.

Installing the self-signed goviewfinder.com certificate:
download BouncyCastleProvider: http://bouncycastle.org/
$ keytool -importcert -v -trustcacerts -file ~/viewfinder/secrets/goviewfinder.com/goviewfinder.com.crt -alias GoViewfinder -keystore "res/raw/myKeystore.bks" -provider org.bouncycastle.jce.provider.BouncyCastleProvider -providerpath "/tmp/bcprov-jdk16-145.jar" -storetype BKS -storepass local_pwd

Adding custom dns entry for goviewfinder.com to point to machine running the emulator:
https://sites.google.com/a/viewfinder.co/development/developer-setup/android-development

Android-specific protobufs can be placed here, they will be generated into android/clients/gen/ for native code,
and android/client/gen/co/viewfinder/proto/ for java.

# 
# Viewfinder UI Automation 
#
#  Prerequisite:  Python Image Library (PIL)
#    1.  Open Xcode and open Xcode Preferences.
#    2.  Click on the Downloads tab, and you'll see Command Line Tools. 
#        Click the Install button to install the Command Line Tools. 
#        (This provides gcc to compile PIL)
#    3.  Now open the Terminal app, and run the following commands:
#        sudo easy_install pip
#        sudo pip install PIL   
#
#  1.  pull down the latest code
#  2.  cd $VF_HOME/clients/ios/testing
#  3.  ./scripts/run-ui-tests.py --testname=testExportPhotos
#  4.  view the results at 
#	
#	https://www.goviewfinder.com:8443/testing/static/html/summary_results.html
#

Viewfinder marketing website
============================

To work on the marketing site on its own, run server.py and
visit http://localhost:8443 in your browser.

In the full viewfinder repo the marketing site is loaded as a subrepo
and served from the main server via symlinks.
Test coverage is almost non-existent, but it's a start.  Be sure to
set PYTHONPATH apprioriately (generally to the root directory of your
tornado checkout) when running tests to make sure you're getting the
version of the tornado package that you expect.
This directory contains a symlink to allow viewfinder modules to be imported
as "viewfinder.backend..." regardless of the name of the repo directory,
and without accidentally adding siblings of the viewfinder repo to the path.
Viewfinder
==========

This is the complete source for the Viewfinder server and iOS and
Android apps as they existed at the time the Viewfinder service was
shut down. We're releasing this code in the hopes that it can be of
utility to others, either as an archaeological resource or to pick up
the baton and start running with the Viewfinder vision again.

The Viewfinder engineers' days are now filled with other priorities
and we won't be able to provide support or bug fixes for this
code. The code is not as clean nor as well tested as you might find in
other open source projects. Yet it is the reality of what was produced
by a startup.

That said, there is quite a bit of interesting code to look at:

* The Viewfinder server provides a structured database schema with a
  variety of indexing options on top of Amazon's DynamoDB. See
  [schema.py](backend/db/schema.py) and
  [indexers.py](backend/db/indexers.py). Also take a look at the
  [operation.py](backend/db/operation.py) which provides at least once
  execution of idempotent operations.

* The server also provided support for versioning at both the database
  and protocol levels. See [db/versions.py](backend/db/versions.py),
  [base/message.py](backend/base/message.py) and
  [www/json_schema.py](backend/www/json_schema.py).q

* The [ViewfinderTool](clients/ios/Source/ViewfinderTool.h) required
  some nifty maths and fancy OpenGL graphics to implement. What is the
  fastest way to draw a circular gradient? Our answer was to
  approximate one using triangle fans and very simple and fast color
  interpolation in a shader.

* The Viewfinder Client uses [LevelDB]
  (https://code.google.com/p/leveldb/) for all local metadata
  storage. While arguably not as convenient as CoreData, we found
  LevelDB to be easy to use and very very fast. See
  [DB.h](clients/shared/DB.h).

* How to detect duplicate and near duplicate images from the iOS
  camera roll? Perceptual fingerprints. See
  [ImageFingerprint.h](clients/ios/Source/ImageFingerprint.h) and
  [ImageIndex.h](clients/shared/ImageIndex.h).

* Need a full text search engine that can work on a mobile device? We
  built one on top of LevelDB that we used for both searching for
  contacts and searching for photos. See
  [FullTextIndex.h](clients/shared/FullTextIndex.h).

* We used [GYP](https://code.google.com/p/gyp/) for generating Xcode
  project files and Android build files. GYP is awesome and removed
  the serious headache during iOS development of how to handle merge
  conflicts in Xcode project files. And GYP facilitated sharing a
  large [body of code](clients/shared) between the iOS and Android
  code bases.

Setup
-----

We use subrepositories, so after cloning (or pulling any change that includes a change to
the subrepository), you must run

    $ git submodule update --init

Many of the following scripts require certain `PATH` entries or other environment variables.
Set them up with the following (this is intended to be run from `.bashrc` or other shell
initialization scripts; if you do not install it there you will need to repeat this command
in each new terminal):

    $ source scripts/viewfinder.bash

Server
------

To install dependencies (into `~/envs/vf-dev`), run

    $ update-environment

To run unit tests:

    $ run-tests

TODO: add ssl certificates and whatever else local-viewfinder needs, and document running it.

iOS client
----------

Our Xcode project files are generated with `gyp`.  After checking out the code
(and after any pull in which a `.gyp` file changed), run

    $ generate-projects.sh

Open the workspace containing the project, *not* the generated project itself:

    $ open clients/ios/ViewfinderWorkspace.xcworkspace

Android client
--------------

The android client is **unfinished**.  To build it, run

    $ generate-projects-android.sh
    $ vf-android.sh build


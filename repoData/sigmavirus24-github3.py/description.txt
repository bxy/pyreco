.. image::
    https://raw.github.com/sigmavirus24/github3.py/develop/images/gh3-logo.png

github3.py is a comprehensive, actively developed and extraordinarily stable 
wrapper around the GitHub API (v3).

See HISTORY.rst for any "breaking" changes.

Installation
------------

::

    $ pip install github3.py

Dependencies
------------

- requests_  by Kenneth Reitz
- uritemplate.py_ by Ian Cordasco

.. _requests: https://github.com/kennethreitz/requests
.. _uritemplate.py: https://github.com/sigmavirus24/uritemplate

Contributing
------------

Please read the `CONTRIBUTING`_ document.

.. _CONTRIBUTING: https://github.com/sigmavirus24/github3.py/blob/develop/CONTRIBUTING.rst

Testing
~~~~~~~

You can run either ``pip install -r dev-requirements.txt`` to install the 
following before testing or simply ``make test-deps``. It is suggested you do 
this in a virtual enviroment. These need to be installed for the tests to run.

- betamax_
- coverage_ by Ned Batchelder
- mock_ by Michael Foord

.. _betamax: https://github.com/sigmavirus24/betamax
.. _coverage: http://nedbatchelder.com/code/coverage/
.. _mock: http://mock.readthedocs.org/en/latest/

License
-------

Modified BSD license_

.. _license: https://github.com/sigmavirus24/github3.py/blob/develop/LICENSE

Examples
--------

See the docs_ for more examples.

.. _docs: http://github3py.readthedocs.org/en/latest/index.html#more-examples

Testing
~~~~~~~

Install the dependencies from requirements.txt e.g.:

::

    make tests

Author
------

Ian Cordasco (sigmavirus24)

Contact Options
---------------

- Feel free to use the `github3.py`_ tag on StackOverflow for any questions 
  you may have
- If you dislike StackOverflow it is preferred that you send an email to 
  github3.py@librelist.com
- You may also contact (via email) the author directly with 
  questions/suggestions/comments or if you wish to include sensitive data.

.. _github3.py: http://stackoverflow.com/questions/tagged/github3.py

{"name": "README.rst", "encoding": "base64", "url": "https://api.github.com/repos/github3py/github3.py/contents/README.rst?ref=master", "html_url": "https://github.com/github3py/github3.py/blob/master/README.rst", "content": "Z2l0aHViMy5weQo9PT09PT09PT09CgouLiBpbWFnZTo6CiAgICBodHRwczov\nL3NlY3VyZS50cmF2aXMtY2kub3JnL3NpZ21hdmlydXMyNC9naXRodWIzLnB5\nLnBuZz9icmFuY2g9bW9jawogICAgOmFsdDogQnVpbGQgU3RhdHVzCiAgICA6\ndGFyZ2V0OiBodHRwOi8vdHJhdmlzLWNpLm9yZy9zaWdtYXZpcnVzMjQvZ2l0\naHViMy5weQoKLi4gaW1hZ2U6OiBodHRwczovL3B5cGlwLmluL2QvZ2l0aHVi\nMy5weS9iYWRnZS5wbmcKICAgIDphbHQ6IERvd25sb2FkcwogICAgOnRhcmdl\ndDogaHR0cHM6Ly9jcmF0ZS5pby9wYWNrYWdlcy9naXRodWIzLnB5CgpnaXRo\ndWIzLnB5IGlzIGEgY29tcHJlaGVuc2l2ZSwgYWN0aXZlbHkgZGV2ZWxvcGVk\nIGFuZCBleHRyYW9yZGluYXJpbHkgc3RhYmxlIAp3cmFwcGVyIGFyb3VuZCB0\naGUgR2l0SHViIEFQSSAodjMpLgoKU2VlIEhJU1RPUlkucnN0IGZvciBhbnkg\nImJyZWFraW5nIiBjaGFuZ2VzLgoKSW5zdGFsbGF0aW9uCi0tLS0tLS0tLS0t\nLQoKOjoKCiAgICAkIHBpcCBpbnN0YWxsIGdpdGh1YjMucHkKCkRlcGVuZGVu\nY2llcwotLS0tLS0tLS0tLS0KCi0gcmVxdWVzdHNfICBieSBLZW5uZXRoIFJl\naXR6CgouLiBfcmVxdWVzdHM6IGh0dHBzOi8vZ2l0aHViLmNvbS9rZW5uZXRo\ncmVpdHovcmVxdWVzdHMKClRlc3RpbmcKfn5+fn5+fgoKWW91IGNhbiBydW4g\nZWl0aGVyIGBgcGlwIGluc3RhbGwgLXIgcmVxdWlyZW1lbnRzLnR4dGBgIHRv\nIGluc3RhbGwgdGhlIApmb2xsb3dpbmcgYmVmb3JlIHRlc3Rpbmcgb3Igc2lt\ncGx5IGBgbWFrZSB0ZXN0LWRlcHNgYC4gSXQgaXMgc3VnZ2VzdGVkIHlvdSBk\nbyAKdGhpcyBpbiBhIHZpcnR1YWwgZW52aXJvbWVudC4gVGhlc2UgbmVlZCB0\nbyBiZSBpbnN0YWxsZWQgZm9yIHRoZSB0ZXN0cyB0byBydW4uCgotIGV4cGVj\ndGVyXyBieSBHYXJ5IEJlcm5oYXJkdAotIG1vY2tfIGJ5IE1pY2hhZWwgRm9v\ncmQKLSBjb3ZlcmFnZV8gYnkgTmVkIEJhdGNoZWxkZXIKCi4uIF9leHBlY3Rl\ncjogaHR0cHM6Ly9naXRodWIuY29tL2dhcnliZXJuaGFyZHQvZXhwZWN0ZXIK\nLi4gX2NvdmVyYWdlOiBodHRwOi8vbmVkYmF0Y2hlbGRlci5jb20vY29kZS9j\nb3ZlcmFnZS8KLi4gX21vY2s6IGh0dHA6Ly9tb2NrLnJlYWR0aGVkb2NzLm9y\nZy9lbi9sYXRlc3QvCgpMaWNlbnNlCi0tLS0tLS0KCk1vZGlmaWVkIEJTRCBs\naWNlbnNlXwoKLi4gX2xpY2Vuc2U6IGh0dHBzOi8vZ2l0aHViLmNvbS9zaWdt\nYXZpcnVzMjQvZ2l0aHViMy5weS9ibG9iL2RldmVsb3AvTElDRU5TRQoKRXhh\nbXBsZXMKLS0tLS0tLS0KClNlZSB0aGUgZG9jc18gZm9yIG1vcmUgZXhhbXBs\nZXMuCgouLiBfZG9jczogaHR0cDovL2dpdGh1YjNweS5yZWFkdGhlZG9jcy5v\ncmcvZW4vbGF0ZXN0L2luZGV4Lmh0bWwjbW9yZS1leGFtcGxlcwoKVGVzdGlu\nZwp+fn5+fn5+CgpJbnN0YWxsIHRoZSBkZXBlbmRlbmNpZXMgZnJvbSByZXF1\naXJlbWVudHMudHh0IGUuZy46Cgo6OgoKICAgIHBpcCBpbnN0YWxsIC1yIHJl\ncXVpcmVtZW50cy50eHQKICAgICMgb3IgbWFrZSB0ZXN0LWRlcHMKCjo6Cgog\nICAgbWFrZSB0ZXN0cwoKQXV0aG9yCi0tLS0tLQoKSWFuIENvcmRhc2NvIChz\naWdtYXZpcnVzMjQpCgpDb250YWN0IE9wdGlvbnMKLS0tLS0tLS0tLS0tLS0t\nCgotIEl0IGlzIHByZWZlcnJlZCB0aGF0IHlvdSBzZW5kIGFuIGVtYWlsIHRv\nIGdpdGh1YjMucHlAbGlicmVsaXN0LmNvbQotIFlvdSBtYXkgYWxzbyBjb250\nYWN0ICh2aWEgZW1haWwpIHRoZSBhdXRob3IgZGlyZWN0bHkgd2l0aCAKICBx\ndWVzdGlvbnMvc3VnZ2VzdGlvbnMvY29tbWVudHMK\n", "sha": "3f4f0b9a43d13376679ee5710958ca88baa7c421", "_links": {"self": "https://api.github.com/repos/github3py/github3.py/contents/README.rst?ref=master", "git": "https://api.github.com/repos/github3py/github3.py/git/blobs/3f4f0b9a43d13376679ee5710958ca88baa7c421", "html": "https://github.com/github3py/github3.py/blob/master/README.rst"}, "git_url": "https://api.github.com/repos/github3py/github3.py/git/blobs/3f4f0b9a43d13376679ee5710958ca88baa7c421", "path": "README.rst", "type": "file", "size": 1785}

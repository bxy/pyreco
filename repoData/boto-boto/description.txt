Handling of file:// URIs:

This directory contains code to map basic boto connection, bucket, and key
operations onto files in the local filesystem, in support of file://
URI operations.

Bucket storage operations cannot be mapped completely onto a file system
because of the different naming semantics in these types of systems: the
former have a flat name space of objects within each named bucket; the
latter have a hierarchical name space of files, and nothing corresponding to
the notion of a bucket. The mapping we selected was guided by the desire
to achieve meaningful semantics for a useful subset of operations that can
be implemented polymorphically across both types of systems. We considered
several possibilities for mapping path names to bucket + object name:

1) bucket = the file system root or local directory (for absolute vs
relative file:// URIs, respectively) and object = remainder of path.
We discarded this choice because the get_all_keys() method doesn't make
sense under this approach: Enumerating all files under the root or current
directory could include more than the caller intended. For example,
StorageUri("file:///usr/bin/X11/vim").get_all_keys() would enumerate all
files in the file system.

2) bucket is treated mostly as an anonymous placeholder, with the object
name holding the URI path (minus the "file://" part). Two sub-options,
for object enumeration (the get_all_keys() call):
  a) disallow get_all_keys(). This isn't great, as then the caller must
     know the URI type before deciding whether to make this call.
  b) return the single key for which this "bucket" was defined.
     Note that this option means the app cannot use this API for listing
     contents of the file system. While that makes the API less generally
     useful, it avoids the potentially dangerous/unintended consequences
     noted in option (1) above.

We selected 2b, resulting in a class hierarchy where StorageUri is an abstract
class, with FileStorageUri and BucketStorageUri subclasses.

Some additional notes:

BucketStorageUri and FileStorageUri each implement these methods:
  - clone_replace_name() creates a same-type URI with a
    different object name - which is useful for various enumeration cases
    (e.g., implementing wildcarding in a command line utility).
  - names_container() determines if the given URI names a container for
    multiple objects/files - i.e., a bucket or directory.
  - names_singleton() determines if the given URI names an individual object
    or file.
  - is_file_uri() and is_cloud_uri() determine if the given URI is a
    FileStorageUri or BucketStorageUri, respectively

####
boto
####
boto 2.28.0

Released: 8-May-2014

.. image:: https://travis-ci.org/boto/boto.png?branch=develop
        :target: https://travis-ci.org/boto/boto

.. image:: https://pypip.in/d/boto/badge.png
        :target: https://crate.io/packages/boto/

************
Introduction
************

Boto is a Python package that provides interfaces to Amazon Web Services.
At the moment, boto supports:

* Compute

  * Amazon Elastic Compute Cloud (EC2)
  * Amazon Elastic Map Reduce (EMR)
  * AutoScaling
  * Amazon Kinesis

* Content Delivery

  * Amazon CloudFront

* Database

  * Amazon Relational Data Service (RDS)
  * Amazon DynamoDB
  * Amazon SimpleDB
  * Amazon ElastiCache
  * Amazon Redshift

* Deployment and Management

  * AWS Elastic Beanstalk
  * AWS CloudFormation
  * AWS Data Pipeline
  * AWS Opsworks
  * AWS CloudTrail

* Identity & Access

  * AWS Identity and Access Management (IAM)

* Application Services

  * Amazon CloudSearch
  * Amazon Elastic Transcoder
  * Amazon Simple Workflow Service (SWF)
  * Amazon Simple Queue Service (SQS)
  * Amazon Simple Notification Server (SNS)
  * Amazon Simple Email Service (SES)

* Monitoring

  * Amazon CloudWatch

* Networking

  * Amazon Route53
  * Amazon Virtual Private Cloud (VPC)
  * Elastic Load Balancing (ELB)
  * AWS Direct Connect

* Payments and Billing

  * Amazon Flexible Payment Service (FPS)

* Storage

  * Amazon Simple Storage Service (S3)
  * Amazon Glacier
  * Amazon Elastic Block Store (EBS)
  * Google Cloud Storage

* Workforce

  * Amazon Mechanical Turk

* Other

  * Marketplace Web Services
  * AWS Support

The goal of boto is to support the full breadth and depth of Amazon
Web Services.  In addition, boto provides support for other public
services such as Google Storage in addition to private cloud systems
like Eucalyptus, OpenStack and Open Nebula.

Boto is developed mainly using Python 2.6.6 and Python 2.7.3 on Mac OSX
and Ubuntu Maverick.  It is known to work on other Linux distributions
and on Windows.  Most of Boto requires no additional libraries or packages
other than those that are distributed with Python.  Efforts are made
to keep boto compatible with Python 2.5.x but no guarantees are made.

************
Installation
************

Install via `pip`_:

::

    $ pip install boto

Install from source:

::

    $ git clone git://github.com/boto/boto.git
    $ cd boto
    $ python setup.py install

**********
ChangeLogs
**********

To see what has changed over time in boto, you can check out the
release notes at `http://docs.pythonboto.org/en/latest/#release-notes`

***************************
Finding Out More About Boto
***************************

The main source code repository for boto can be found on `github.com`_.
The boto project uses the `gitflow`_ model for branching.

`Online documentation`_ is also available. The online documentation includes
full API documentation as well as Getting Started Guides for many of the boto
modules.

Boto releases can be found on the `Python Cheese Shop`_.

Join our IRC channel `#boto` on FreeNode.
Webchat IRC channel: http://webchat.freenode.net/?channels=boto

Join the `boto-users Google Group`_.

*************************
Getting Started with Boto
*************************

Your credentials can be passed into the methods that create
connections.  Alternatively, boto will check for the existence of the
following environment variables to ascertain your credentials:

**AWS_ACCESS_KEY_ID** - Your AWS Access Key ID

**AWS_SECRET_ACCESS_KEY** - Your AWS Secret Access Key

Credentials and other boto-related settings can also be stored in a
boto config file.  See `this`_ for details.

.. _pip: http://www.pip-installer.org/
.. _release notes: https://github.com/boto/boto/wiki
.. _github.com: http://github.com/boto/boto
.. _Online documentation: http://docs.pythonboto.org
.. _Python Cheese Shop: http://pypi.python.org/pypi/boto
.. _this: http://code.google.com/p/boto/wiki/BotoConfig
.. _gitflow: http://nvie.com/posts/a-successful-git-branching-model/
.. _neo: https://github.com/boto/boto/tree/neo
.. _boto-users Google Group: https://groups.google.com/forum/?fromgroups#!forum/boto-users


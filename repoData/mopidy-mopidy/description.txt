Mopidy.js
=========

Mopidy.js is a JavaScript library that is installed as a part of Mopidy's HTTP
frontend or from npm. The library makes Mopidy's core API available from the
browser or a Node.js environment, using JSON-RPC messages over a WebSocket to
communicate with Mopidy.


Getting it for browser use
--------------------------

Regular and minified versions of Mopidy.js, ready for use, is installed
together with Mopidy. When the HTTP frontend is running, the files are
available at:

- http://localhost:6680/mopidy/mopidy.js
- http://localhost:6680/mopidy/mopidy.min.js

You may need to adjust hostname and port for your local setup.

In the source repo, you can find the files at:

- `mopidy/http/data/mopidy.js`
- `mopidy/http/data/mopidy.min.js`


Getting it for Node.js use
--------------------------

If you want to use Mopidy.js from Node.js instead of a browser, you can install
Mopidy.js using npm:

    npm install mopidy

After npm completes, you can import Mopidy.js using ``require()``:

    var Mopidy = require("mopidy");


Using the library
-----------------

See the [Mopidy.js documentation](http://docs.mopidy.com/en/latest/api/js/).


Building from source
--------------------

1. Install [Node.js](http://nodejs.org/) and npm. There is a PPA if you're
   running Ubuntu:

        sudo apt-get install python-software-properties
        sudo add-apt-repository ppa:chris-lea/node.js
        sudo apt-get update
        sudo apt-get install nodejs

2. Enter the `js/` in Mopidy's Git repo dir and install all dependencies:

        cd js/
        npm install

That's it.

You can now run the tests:

    npm test

To run tests automatically when you save a file:

    npm start

To run tests, concatenate, minify the source, and update the JavaScript files
in `mopidy/http/data/`:

    npm run-script build

To run other [grunt](http://gruntjs.com/) targets which isn't predefined in
`package.json` and thus isn't available through `npm run-script`:

    PATH=./node_modules/.bin:$PATH grunt foo


Changelog
---------

### 0.2.0 (2014-01-04)

- **Backwards incompatible change for Node.js users:**
  `var Mopidy = require('mopidy').Mopidy;` must be changed to
  `var Mopidy = require('mopidy');`

- Add support for [Browserify](http://browserify.org/).

- Upgrade dependencies.

### 0.1.1 (2013-09-17)

- Upgrade dependencies.

### 0.1.0 (2013-03-31)

- Initial release as a Node.js module to the
  [npm registry](https://npmjs.org/).

******
Mopidy
******

Mopidy is a music server which can play music both from multiple sources, like
your local hard drive, radio streams, and from Spotify and SoundCloud. Searches
combines results from all music sources, and you can mix tracks from all
sources in your play queue. Your playlists from Spotify or SoundCloud are also
available for use.

To control your Mopidy music server, you can use one of Mopidy's web clients,
the Ubuntu Sound Menu, any device on the same network which can control UPnP
MediaRenderers, or any MPD client. MPD clients are available for many
platforms, including Windows, OS X, Linux, Android and iOS.

To get started with Mopidy, check out `the docs <http://docs.mopidy.com/>`_.

- `Documentation <http://docs.mopidy.com/>`_
- `Source code <https://github.com/mopidy/mopidy>`_
- `Issue tracker <https://github.com/mopidy/mopidy/issues>`_
- `CI server <https://travis-ci.org/mopidy/mopidy>`_
- `Download development snapshot <https://github.com/mopidy/mopidy/archive/develop.tar.gz#egg=mopidy-dev>`_

- IRC: ``#mopidy`` at `irc.freenode.net <http://freenode.net/>`_
- Mailing list: `mopidy@googlegroups.com <https://groups.google.com/forum/?fromgroups=#!forum/mopidy>`_
- Twitter: `@mopidy <https://twitter.com/mopidy/>`_

.. image:: https://img.shields.io/pypi/v/Mopidy.svg?style=flat
    :target: https://pypi.python.org/pypi/Mopidy/
    :alt: Latest PyPI version

.. image:: https://img.shields.io/pypi/dm/Mopidy.svg?style=flat
    :target: https://pypi.python.org/pypi/Mopidy/
    :alt: Number of PyPI downloads

.. image:: https://img.shields.io/travis/mopidy/mopidy/develop.svg?style=flat
    :target: https://travis-ci.org/mopidy/mopidy
    :alt: Travis CI build status

.. image:: https://img.shields.io/coveralls/mopidy/mopidy/develop.svg?style=flat
   :target: https://coveralls.io/r/mopidy/mopidy?branch=develop
   :alt: Test coverage


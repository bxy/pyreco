These files are metadata templetes for licenses that you
may use on your ponyfiles. These *cannot* be added with
ponysay-tool --edit-apply. You should add them either using
ponysay-tool --edit or with a text editor.

If you choose to add them using a text editor, add the
content between the two $$$ lines. If the files does
not start with a $$$ line, add the content to the top
of the file followed by a $$$ line and with a $$$ line
at the top.

So a ponyfile without any other metadata should begin with
something like

$$$
LICENSE: license-abbr (full license name)
FREE:    yes/no/sharable

license text/links
$$$


Do not forgot to add the source. If you make it yourself,
from scratch, almost from scratch, the tag should be, if
your username is XYZ:

SOURCE: [XYZ]


xterm-colours.xcf a GIMP XCF file template that enumerates all possibles
colours and is limited to them so no other colours can be used. All
other files are templates that are combination of this and pony templates
created for the Desktop ponies project.

The pony templates was created by Bot-chan, Doctor Blade, StarStep and others
originally for 'Desktop ponies' but they mention on 'ponychan' and on they
Deviantart Desktop ponies group that anypony can use them if they give credits
and not make profit from there, respect the authors and they work.
As same time they say that ponies enough different from the templates can by 
concideres differents ponies and for that you can claim completly ownership
if these aren't from MLP:FiM or not owned by another artist.

Fell free to use or modify...

The templates not marked as pegasus can both be normal seaponies
and unicorn seaponies. The templates that are marked as pegasus
can both be pegasus seaponies and alicorn seaponies. 

Alicorn seaponies are not provided by Desktop ponies templates,
but are creates using the Desktop ponies templates for unicorn
and pegasus seaponies.

A script that prints a random pony without
any features at all provided by ponysay.

This script is intended to provide the
ponysay ponies for computers that are
not powerful enought to run ponysay without
taking too long.


`ponysay` — cowsay reimplemention for ponies.

![Derp](http://i.imgur.com/xOJbE.png)

Today your terminal, tomorrow the world!


Installation on GNU/Linux (or other Unix implementations)
---------------------------------------------------------

[Download](https://github.com/erkin/ponysay/releases) or clone the project.
In the terminal, `cd` into the ponysay directory and `./setup.py --freedom=partial install` or `python3 setup.py --freedom=partial install`.
Superuser permissions might be required in order to run `./setup.py --freedom=partial install` without `--private`, on most systems this
can be achieved by running `sudo ./setup.py --freedom=partial install`.
If installing only the completely free ponies is desired, `--freedom=strict` should be used instead of `--freedom=partial`.
For additional information, an extensive [manual in PDF](https://github.com/erkin/ponysay/blob/master/ponysay.pdf?raw=true) is provided.

In order to use ponysay, run:

    ponysay "I am just the cutest pony!"

Or if you have a specific pony in your mind:

    ponysay -f pinkie "Partay!~"

Consult `info ponysay`, `man 6 ponysay` or `ponysay -h` for additional information.
Spanish and Turkish manuals are also available: `man -L es 6 ponysay` and 'man -L tr 6 ponysay` respectively.

#### Arch Linux
The package is in the official repositories as `community/ponysay`. A Git version is also present, named `ponysay-git` in AUR.

#### Chakra
A git version of the package is available as `ponysay-git` in CCR, alongside a stable package called `ponysay`.

#### Gentoo Linux
There is a package for Gentoo, to make installation and updating simple. You can find it in [this overlay](https://github.com/etu/aidstu-overlay). The package is named `games-misc/ponysay`.

#### Debian GNU/Linux
Debian packages can be found [here](http://www.vcheng.org/ponysay/).

#### Ubuntu
There is a PPA available, specifically for ponysay, containing packages for all currently supported Ubuntu releases [here](https://launchpad.net/~vincent-c/+archive/ponysay).

#### Mac OS X

A `ponysay` [Homebrew](https://github.com/mxcl/homebrew) formula is available.

### Print a pony fortune upon terminal startup

This requires the `fortune` utility to be installed. It can install be from the distribution's repositories (might be named `fortune-mod`).
Alternatively, one can just fetch the source code from [here](http://ftp.ibiblio.org/pub/linux/games/amusements/fortune/).

You can try [this](http://www.reddit.com/r/mylittlelinux/comments/srixi/using_ponysay_with_a_ponified_fortune_warning/) script or
[ponypipe](https://github.com/maandree/ponypipe) to ponify fortunes.

Edit your `~/.bashrc` and add this to the end of the file

    fortune | ponysay

Afterwards, every time you open a terminal a pony should give you a fortune.

### Pony quotes

Running `ponysay -q` will print a random pony saying one of its quotes from My Little Pony: Friendship is Magic. The pony can be specified: `ponysay -q pinkie`.
Just as with `-f`, `-q` can be used multiple times to specify a set of ponies from which a single one will be selected randomly.

When running `ponysay -l` or `ponysay -L` the ponies with quotes will be printed in bold or bright (depending on the used terminal).

### Ponies in TTY (Unix VT)

If you have a custom colour palette edit your `~/.bashrc` and add

```
if [ "$TERM" = "linux" ]; then
    function ponysay
    {
        exec ponysay "$@"
        #RESET PALETTE HERE
    }
fi
```

Read the PDF or info manual for more information.


Installation on Microsoft™ Windows®
-----------------------------------
[¯\\\_(ツ)\_/¯](http://fc05.deviantart.net/fs71/i/2011/266/d/e/shrugpony_firefly_by_imaplode-d4aqtvx.png)


Dependencies
------------

### Runtime dependencies

`coreutils`: `stty` in coreutils used to determine size of the terminal.

`python>=3`: `ponysay` is written in Python 3.

### Package building dependencies

`gzip`: Used for compressing manuals (suppressable with `./configure --without-info-compression --without-man-compression`).

`texinfo`: Used for building the info manual (suppressable with `./configure --without-info`).

`python>=3`: The installation process is written in Python 3.

Run `./dependency-test.sh` if things are not working for you.


FAQ
---

__Q:__ The output looks like a mess in _(TTY/PuTTY/other)_!

__A:__ Unfortunately we cannot make it perfect, see [issue 1](//github.com/erkin/ponysay/issues/1). But we have done a lot, read more about how to get the best of the current state of the art has to offer in the [manual](//github.com/erkin/ponysay/blob/master/ponysay.pdf?raw=true).

__Q:__ The output looks like a mess in _(xfce4-terminal/mate-terminal/xterm/[...])_ with _(this)_ font!

__A:__ We use blocks for printing the ponies, if the blocks are misaligned, or if you do not use a truly monospaced font with aligned blocks try another monospaced font, such as 'Fixed [...]' or 'Liberation Mono.'

__Q:__ You are missing _(my-favourite-pony)_!

__A:__ [Ask](//github.com/erkin/ponysay/issues) and we'll add!

__Q:__ Which programs do you use to generate the pony files?

__A:__ The pony files are in fact mostly a bunch of selected [browser ponies](//web.student.tuwien.ac.at/~e0427417/browser-ponies/ponies.html), converted using [util-say](//github.com/maandree/util-say),
Other are taken from desktop ponies, and the others are created specifically for ponysay.

The [PDF manual](//github.com/erkin/ponysay/blob/master/ponysay.pdf?raw=true) should answer most of your questions.


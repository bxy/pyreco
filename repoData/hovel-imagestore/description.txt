ImageStore
==========

An image library, created for easy integration for an existing django project.
Very light, built around django generic views.


Features:
=========

* Optional PrettyPhoto for image/album show
* Albums
* Tagging support
* South support for upgrades


Installation:
=============

* Install with pip or easy install (all dependencies will be installed automatically)
* Symlink or copy imagestore/media/imagestore.css to your MEDIA_ROOT (or write youre own style)
* Add imagestore to your INSTALLED_APPS
* Add imagestore.urls to your urls
* Run ./manage.py syncdb or ./manage.py migrate
* Add jquery and jqueryui load to your template to use tagging autocomplete and/or prettyphoto
* If you want to use prettyPhoto put `prettyPhoto <http://www.no-margin-for-errors.com/projects/prettyphoto-jquery-lightbox-clone/>`__ to your media directory and include imagestore/prettyphoto.html to your template


Configuration:
==============

If IMAGESTORE_SELF_MANAGE is True (default), all created users will get add/delete/change permissions for Images and Albums. If you don't wish users to create albums or upload images set this property to False.


Translation:
============

* Russian
* English


Watermarking:
=============

Use `watermarker <http://pypi.python.org/pypi/watermarker/>`__ sorl integration to add watermark to your images.

ImageStore
==========

An image gallery, created for easy integration for an exiting django project.

`Documentation aviable on ReadTheDocs <http://readthedocs.org/projects/imagestore/>`_

Gallery for site
----------------

* Albums
* Mass upload
* Thumbnails in admin intereface
* Ordering
* Tagging support
* Easy PrettyPhoto integration
* Django-cms integration

Gallery for your site users
---------------------------

* You can use imagestore to create gallery for your users.
* Users can:
    * create albums, upload photos to albums
    * make albums non-public
    * set name, descripion and tags for photos
    * edit infomation about photo or upload new veresion



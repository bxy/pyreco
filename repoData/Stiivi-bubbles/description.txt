Changes in Bubbles
==================

0.2
===

Overview
--------

* New data processing graph and new graph based `Pipeline` with customizable
  execution policy and with pre-execution tests
* New MongoDB backend with a store, data object and few demo ops
* New XLS backend with a store and data object
* New operations (see below)

Operations
----------

New operations:

* `filter_by_range`, `filter_not_empty`: rows, sql
* `split_date`: rows, sql
* `field_filter`: mongo (without `rename`)
* `distinct`: mongo
* `insert`: (rows, sql) and (sql, sql)
* `assert_contains`, `assert_missing`: sql
* `empty_to_missing`: rows – experimental
* `string_to_date`: rows – still experimental, format will change to SQL date
  format

Changed and fixed operations:

* `aggregate` accepts empty measure list – yields only count

New Features
------------

* Added `document` storage data type to represent JSON-like objects
* object store can be cloned using `clone()` which should provide another
  store with different configuration but possibility of mutually composable
  objects
* new `FieldError` exception
* Take into account object's data consumability on object use (naive
  implementation for the time being)
* CSVStore (`csv`) is now able to create CSV targets with `csv_target` factory
  name
* New `Resource` class representing file-like resources with optional call to
  `close()`
* Added `FileSystemStore` for read-only CSV and XLS files with default
  settings.
* Added `Store.exists()`, implemented in SQL backend.
* `ProbeAssertionError` has a `reason` attribute

Pipeline and execution:

* `Graph` and `Node` structure for building operation processing graphs
* operation list has an operation prototype that includes operation operand
  and parameter names
* Added `ExecutionEngine`, currently semi-private, but will serve as basis for
  future custom graph execution policies
* Added `Pipeline.execution_plan`
* Added thread_local - thread local variable storage
* Added `retry_deny` and `retry_allow` to the operation context
* Added insert operation accessible through `Pipeline.insert_into` and
  `Pipeline.insert_into_object`
* Added `test_if_needed()` and `test_if_satisfied()` methods which are fork()
  -like but executed before running the pipeline (see documentation for more
  information)


Changes
-------

* Original `Pipeline` implementation replaced – instead of immediate execution
  a graph is being created. Explicit `run()` is required.
* calling operations decorated with `@experimental` will cause a warning to be
  logged
* renamed module `doc` to `dev`, will contain more development tools in the
  future, such as operation auditing or data object API conformance checking
* `default_context` is now a thread-local variable, created on first use
* `open_resource` now returns a `Resource` object
* renamed engine `prepare_execution_plan` to `execution_plan`
* operation context's `o` accessor was renamed to `op` and now also supports
  getitem: `context.op["duplicates"]` is equal to `context.op.duplicates`.
* data objects should respond to `retained()` and `is_consumable()`
* default field storage type is now `string` instead of `unknown` for
  convenience.
* Removed default setting for debug logging, uses warning level
* Renamed namespace object name customization class variable `_ns_object_name`
  to `__identifier__`

Fixes
-----

* Problem described in the Issue #4 works as expected
* Fixed problem with filter_by_value
* Fixed aggregate key issues


Bubbles
=======

Bubbles is a Python ETL Framework and set of tools. It can be used for
processing, auditing and inspecting data. Focus is on understandability and
transparency of the process.

Project page: http://bubbles.databrewery.org

Blog: http://blog.databrewery.org

About
-----

Bubbles is a Python framework for:

* ETL (extraction, transformation and loading)
* preparation of data for further analysis
* data probing – analysing properties of data, mostly categorical in nature
* data quality monitoring
* virtual data objects – abstraction of table-like structured datasets.
  Datasets are treated the same, no matter whether the source is a text file
  or a database table.

Installation
------------

Requires at least Python 3.3.

To install Bubbles framework type:

    pip install bubbles

To install Bubbles from sources, you can get it from Github: 

    https://github.com/Stiivi/bubbles

Documentation
-------------

[Introduction to bubbles](http://www.slideshare.net/Stiivi/data-brewery-2-data-objects) (Slideshare presentation)

[Operations](http://www.scribd.com/doc/147247069/Bubbles-Brewery2-Operations)
(Scribd document)

Documentation can be found at: http://packages.python.org/bubbles

Sources
-------

Project source repository is being hosted at Github:
https://github.com/Stiivi/bubbles

    git clone git://github.com/Stiivi/bubbles.git

Support
-------

If you have questions, problems or suggestions, you can send a message to the 
Google group or write to the author.

* Report issues here: https://github.com/Stiivi/bubbles/issues
* Google group: http://groups.google.com/group/databrewery

Author
------

Stefan Urbanek <stefan.urbanek@gmail.com>

License
-------

Bubbles is licensed under MIT license with following addition:

    If your version of the Software supports interaction with it remotely 
    through a computer network, the above copyright notice and this permission 
    notice shall be accessible to all users.

Simply said, that if you use it as part of software as a service (SaaS) you 
have to provide the copyright notice in an about, legal info, credits or some 
similar kind of page or info box.

For full license see the LICENSE file.



##[Contributing](http://kivy.org/docs/contribute.html)

There are many ways in which you can contribute to Kivy.
Code patches are just one thing amongst others that you can submit to help the
project. We also welcome feedback, bug reports, feature requests, documentation
improvements, advertisement & advocating, testing, graphics contributions and
many different things. Just talk to us if you want to help, and we will help you
help us.

#Feedback
--------

This is by far the easiest way to contribute something. If you're using
Kivy for your own project, don't hesitate sharing. It doesn't have to be a
high-class enterprise app, obviously. It's just incredibly motivating to
know that people use the things you develop and what it enables them to
do. If you have something that you would like to tell us, please don't
hesitate. Screenshots and videos are also very welcome!
We're also interested in the problems you had when getting started. Please
feel encouraged to report any obstacles you encountered such as missing
documentation, misleading directions or similar.
We are perfectionists, so even if it's just a typo, let us know.

#[Reporting an Issue](http://kivy.org/docs/contribute.html#reporting-an-issue)

If you found anything wrong, a crash, segfault, missing documentation, invalid
spelling, weird example, please take 2 minutes to report the issue. following
the guide lines mentioned [here](http://kivy.org/docs/contribute.html#reporting-an-issue)

##[Code Contributions](http://kivy.org/docs/contribute.html#code-contributions)

Code contributions (patches, new features) are the most obvious way to help with
the project's development. Since this is so common we ask you to follow our
workflow to most efficiently work with us. Adhering to our workflow ensures that
your contribution won't be forgotten or lost. Also, your name will always be
associated with the change you made, which basically means eternal fame in our
code history (you can opt-out if you don't want that).


* [Coding style](http://kivy.org/docs/contribute.html#coding-style)
* [Performances](http://kivy.org/docs/contribute.html#performances)
* [Git & GitHub](http://kivy.org/docs/contribute.html#git-github)
* [Code Workflow](http://kivy.org/docs/contribute.html#code-workflow)

##[Documentation Contributions](http://kivy.org/docs/contribute.html#documentation-contributions)

Documentation contributions generally follow the same workflow as code
contributions, just a bit more lax. We don't ask you to go through all the
hassle just to correct a single typo. For more complex contributions, please
consider following the suggested workflow though.


* #[Docstrings](http://kivy.org/docs/contribute.html#docstrings)


#Unit tests contributions

For testing team, we have the document `contribute-unittest` that
explains how Kivy unit test are working and how you can create your own. Use the
same approach as the `Code Workflow` to [submit new tests](http://kivy.org/docs/contribute.html#unit-tests-contributions).



Kivy - Documentation
====================

You can access the API documentation on web :
  * last released version : http://kivy.org/docs/api-index.html


How to build the documentation
------------------------------

You need to install :

  * Python Sphinx
    - Where apt-get is available :
          apt-get install python-sphinx
    - On Windows :
          Get pip (https://pypi.python.org/pypi/pip). You'll use it to install the dependencies.
          To install pip, do python setup.py install in the pip directory.
          Now do :
          pip install sphinx
          pip install blockdiag
          pip install sphinxcontrib-blockdiag
          pip install seqdiag
          pip install sphinxcontrib-seqdiag
          pip install actdiag
          pip install sphinxcontrib-actdiag
          pip install nwdiag
          pip install sphinxcontrib-nwdiag

  * Latest kivy

Generate documentation using make::
  make html

Documentation will be accessible in build/html/


This is the Kivy Catalog viewer. It serves two purposes:

1. To showcase the various widgets available in Kivy
2. To allow interactive editing of Kivy language files
    to get immediate feedback as to how they work
Known bugs:
* The DropDown
* Some widgets are still missing
Pictures
========

A very simple image browser, supporting the reading only from images/
directory right now.
So put any images files inside images/ directory, and start the app !

Android
-------

You can copy/paste this directory into /sdcard/kivy/pictures in your
android device.


Licences
--------

* faust_github.jpg: lucie's cat accepted to share his face
* 5552597274_de8b3fb5d2_b.jpg: http://www.flickr.com/photos/chialin-gallery/5552597274/sizes/l/
* 5509213687_ffd18df0b9_b.jpg: http://www.flickr.com/photos/orsomk/5509213687/sizes/l/

Icon from http://www.gentleface.com/free_icon_set.html#geticons

Showcase
========

Demonstrate all the possibilities of Kivy toolkit.

Android
-------

You can copy/paste this directory into /sdcard/kivy/showcase in your
android device.


Touchtracer
===========

Touchtracer is a simple example to draw lines under every touches detected
on your hardware.

Android
-------

You can copy/paste this directory into /sdcard/kivy/touchtracer in your
android device.


Kinect Viewer
=============

You must have libfreenect installed on your system to make it work.

How it works
------------

1. The viewer gets the depth value from freenect.
2. Depths are multiplied by 32 to use the full range of 16 bits.
3. Depths are uploaded into a "luminance" texture.
4. We use a shader for mapping the depth to a special color.


Programming for lists is a common task in a wide variety of applications.
An attempt is made here to present a good set of samples.

These include:

    - list_simple.py -- The simplest of examples, using the simplest list
                        adapter, :class:`SimpleListAdapter`. Only the names of
                        the fruits in the fixtures data are used to make list
                        item view instances from a custom class. There is no
                        selection -- it is a bare-bones list of strings.

    - list_cascade.py -- Fruit categories on the left, fruit selection within
                         a fruit category in the middle, and a fruit detail
                         view on the right. Selection cascades from left to
                         right, from the category selection, to the fruit
                         selection, to the detail view.

                         The list views use :class:`ListAdapter` and a custom
                         subclass of :class:`ListItemButton` for the list
                         item class. Data for fruits comes from a fixtures.py
                         file that is used in several of the examples.

    - list_cascade_dict.py -- Exactly the same layout and functionality as
                              list_cascade.py, except the list views use
                              :class:`DictAdapter` and the fixtures data is
                              used in an appropriate way for dictionaries.

    - list_cascade_images.py -- Same as the list_cascade_dict.py example, but
                                with thumbnail images of fruits shown in
                                custom list item view class instances, and in
                                the detail view.

    - list_master_detail.py -- Uses a :class:`DictAdapter`. Simpler than the
                               cascade examples. Illustrates use of the terms.

    - list_kv.py -- A simple example to show use of a kv template.

    - list_composite.py -- Uses :class:`CompositeListItem` for list item views
                           comprised by two :class:`ListItemButton`s and one
                           :class:`ListItemLabel`. Illustrates how to construct
                           the fairly involved args_converter used with
                           :class:`CompositeListItem`.

    - list_two_up -- Presents two list views, each using :class:`DictAdapter`.
                     list view on the left is configured for multiple
                     selection. As selection changes in the left list, the
                     selected items form the content for the list on the
                     right, which is constantly updated.

    - list_ops.py -- Seven list views are shown at the bottom, each focusing
                     on one of the available operations for list
                     adapters: scroll_to, trim_to_sel, trim_left_of_sel, etc.
                     At the top is a display that shows individual items
                     selected across the seven lists, along with a total of
                     all selected items for the lists.


Package hosting:
	PPA stable			https://launchpad.net/~kivy-team/+archive/kivy
	PPA daily			https://launchpad.net/~kivy-team/+archive/kivy-daily
	
	OBS stable (not maintained)	https://build.opensuse.org/project/show?project=home%3Athopiekar%3Akivy
	OBS testing (not maintained)	https://build.opensuse.org/project/show?project=home%3Athopiekar%3Akivy-testing

	COBS (not maintained)		https://build.pub.meego.com/project/show?project=home%3Athopiekar%3Akivy

PPA recipes:
	stable		https://code.launchpad.net/~thopiekar/+recipe/kivy-stable
	daily		https://code.launchpad.net/~thopiekar/+recipe/kivy-daily

Related instructions:
	http://kivy.org/docs/installation/installation-linux.html

The files in the win32 and osx subdirectories are 
source and resource files that are used in the respective 
portable packaged versions of kivy for each OS.  Here, they
are under version controll.  

setup.py copies these files into the portable distribution 
package that is created when you launch 
'setup.py build_portable'


For example the win32 dir has the READMEand bat file which 
sets up the ENV variables and launches the python interpreter.

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
READ THIS FIRST
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

This version of Kivy is a portable win32 version, 32 bits. (it work also for
64 bits Windows.) This means everything you need to run kivy (including 
python and all other dependencies etc) are included.

This README only addresses the things specific to the portable version of kivy.  
For general information on how to get started, where to find the documentation 
and configuration see the README file in the kivy directory about Kivy.

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


Running portable Kivy
=====================

The same directory that has this README file in it, contains a file called kivy.bat
running this kivy.bat file will set up the environment for kivy and starts the 
python interpreter with any arguments you passed

Example
~~~~~~~

If you open a command line and go to the directory (or add it to your PATH) 
You can run the following:

kivy test.py -w  <-- will run test.py as a python script with kivy ready to use


Run a Kivy application without going to the command line
========================================================

Three options :

1. You can drag your python script on top the kivy.bat file and it will launch

2. If you right click on your python script (.py ending or whatever you named it), 
you can select properties and select an application to open this type of file with.
Navigate to the folder that includes this README and select the kivy.bat file.  
Now all you have to do is double click (check do this always for this file type 
to make this the default)

3. Install the Python Launcher for Windows. (Comes with Python 3.3 -- See Python PEP-397)
* in each of your main.py files, add a first line of:
   #!/usr/bin/kivy
* create a file named C:\Windows\py.ini containing something like:
   [commands]
   kivy="c:\<path>\<to>\<your>\kivy.bat"

If you already have Python installed
====================================

The portable Kivy version shouldn't cause any conflicts and cooperate fairly well 
(at least if it's Python 2.7, otherwise some modules might cause problems if there
is entries on PYTHONPATH)


Install Kivy as a standard python module
========================================

Please refer to the install instructions in the complete README :
* Inside the kivy folder inside this one
* Kivy documentation at http://kivy.org/docs/


Install development environment inside your current shell
=========================================================

If you want to develop with Kivy's python, you may just want to load the
environment, and stay in your console. Inside a git bash / mingsys console, you
can type :

  source /path/to/kivyenv.sh

And it will load the whole enviroment of Kivy. This will give you an access to:

  * Python binaries (python, pythonw, easy_install, pip)
  * Cython binaries (cython)
  * Gstreamer binaries (gst-inspect, gst-launch, ...)
  * Pre-configured PYTHONPATH for gst and Kivy

Please note that if you already have a Python installed on your system, it will be
not used.

Kivy
====

<img align="right" height="256" src="https://raw.githubusercontent.com/kivy/kivy/master/kivy/data/logo/kivy-icon-256.png"/>

Innovative User Interfaces Made Easy.

Kivy is a [Python](https://www.python.org) framework for the development of
multi-touch enabled media rich applications. The aim is to allow for quick and
easy interaction design and rapid prototyping whilst making your code reusable
and deployable.

Kivy is written in Python and [Cython](http://cython.org/), based on OpenGL ES
2, supports various input devices and has an extensive widget library. With the
same codebase, you can target Windows, OSX, Linux, Android and iOS. All our
widgets are built with multitouch support.

Kivy is MIT licensed, actively developed by a great community and is supported
by many projects managed by the Kivy organisation.

[![Kivy's Coverage](https://coveralls.io/repos/kivy/kivy/badge.png?branch=master)](https://coveralls.io/r/kivy/kivy?branch=master)

Installation, Documentation, Examples
-------------------------------------

Extensive installation instructions as well as tutorials and general
documentation, including an API reference, can be found at: http://kivy.org/docs/

An offline version in PDF format is also available.
Kivy ships with a bunch of examples that can be found in the examples/ folder.


Support
-------

If you need assistance, you can ask for help on our mailing list:

* Google Group : http://groups.google.com/group/kivy-users
* Email        : kivy-users@googlegroups.com

We also have an IRC channel (expect responses mostly in the day hours of CET):

* Server  : irc.freenode.net
* Channel : #kivy

Other projects
---------------

Have a look at our complementary projects:

- [Buildozer](http://github.com/kivy/buildozer): A robot that will do
  everything needed to package your application for iOS and Android. Ask him,
  wait, it's done.
- [Plyer](http://github.com/kivy/plyer): Library for accessing features of your
  hardware such as the Accelerometer, Camera etc.
- [Pyjnius](http://github.com/kivy/pyjnius): Dynamic access to the Java/Android
  API from Python? Easy!
- [Pyobjus](http://github.com/kivy/pyobjus): Dynamic access to the
  Objective-C/iOS API from Python (wip).
- [Python for Android](http://github.com/kivy/python-for-android): Toolchain
  for building and packaging Kivy applications into Android APK's.
- [Kivy iOS](http://github.com/kivy/kivy-ios): Toolchain for building and
  packaging Kivy applications into iOS IPA's.
- [Audiostream](http://github.com/kivy/audiostream): Library for direct access
  to the Microphone and Speaker (for iOS and Android).
- [Garden](http://github.com/kivy-garden): Explore User's widgets and libraries



Licenses
--------

- Kivy is released under the terms of the MIT License. Please refer to the
  LICENSE file.
- The provided fonts DroidSans.ttf and DroidSansMono.ttf are licensed and
  distributed under the terms of the
  [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0).
- The current UI design has been adapted from Moblintouch theme's SVGs
  and is licensed under the terms of the
  [LGPLv2.1](http://www.gnu.org/licenses/old-licenses/lgpl-2.1).




django-compress provides an automated system for compressing CSS and 
JavaScript files. By default, it only outputs compressed files while not in 
DEBUG-mode. That means you can still debug and edit your source files while 
coding, and when going to production, the compressed files will be 
automatically generated.

Support for jsmin and CSSTidy is included and enabled by default (but can 
easily be disabled). Support for YUI Compressor is also supported out of the 
box.

django-compress includes template tags for outputting the URLs to the 
CSS/JavaScript?-files and some other goodies to improve the performance of 
serving static media.

django-compress is available at github[1] and Google Code[2]. You can always 
access the latest and greatest code from both git and Subversion.

The documentation is available online at Github[3], or under docs/ in the 
source.

[1] http://github.com/pelme/django-compress/tree/master
[2] http://code.google.com/p/django-compress/
[3] http://github.com/pelme/django-compress/tree/master/docs

django-compress is no longer maintained
=======================================

This project has not seen any updates in a long time. I have simply not had the time to keep it up to date.

When django-compress started, there were no other asset managers for Django. Today, it looks quite different, with a wide variety of high quality packages. I am glad to see that some of the code and ideas has been the foundation for other asset managers.

Since there are so many other awesome alternatives today, and I am not going to maintain django-compress anymore.

Are you looking for an asset manager for your project?
------------------------------------------------------

See http://djangopackages.com/grids/g/asset-managers/ for a fairly complete list of available asset managers for Django.

Are you already using django-compress?
--------------------------------------
If you already are already using django-compress, you are strongly encouraged to
switch to django-pipeline[1], which is a well maintained fork, with a lot of new features.

See django-pipelines documentation [2] for more information on the changes and features.

[1] https://github.com/cyberdelia/django-pipeline

[2] http://django-pipeline.readthedocs.org/


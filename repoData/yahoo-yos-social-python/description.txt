
DJANGO EXAMPLE PACKAGE
======================

This package implements an example consumer and server for the Django
Python web framework.  You can get Django (and learn more about it) at

  http://www.djangoproject.com/

SETUP
=====

 1. Install the OpenID library, version 2.0.0 or later.

 2. Install Django 0.95.1.

    If you find that the examples run on even newer versions of
    Django, please let us know!

 3. Modify djopenid/settings.py appropriately; you may wish to change
    the database type or path, although the default settings should be
    sufficient for most systems.

 4. In examples/djopenid/ run:

    python manage.py syncdb

 5. To run the example consumer or server, run

    python manage.py runserver PORT

    where PORT is the port number on which to listen.

    Note that if you want to try both the consumer and server at the
    same time, run the command twice with two different values for
    PORT.

 6. Point your web browser at the server at

    http://localhost:PORT/

    to begin.

ABOUT THE CODE
==============

The example server and consumer code provided in this package are
intended to be instructional in the use of this OpenID library.  While
it is not recommended to use the example code in production, the code
should be sufficient to explain the general use of the library.

If you aren't familiar with the Django web framework, you can quickly
start looking at the important code by looking in the 'views' modules:

  djopenid.consumer.views
  djopenid.server.views

Each view is a python callable that responds to an HTTP request.
Regardless of whether you use a framework, your application should
look similar to these example applications.

CONTACT
=======

Please send bug reports, patches, and other feedback to

  dev@lists.openidenabled.com

Python OpenID library example code
==================================

The examples directory contains working code illustrating the use of
the library for performing OpenID authentication, both as a consumer
and a server. There are two kinds of examples, one that can run
without any external dependencies, and one that uses the Django Web
framework. The examples do not illustrate how to use all of the
features of the library, but they should be a good starting point to
see how to use this library with your code.

Both the Django libraries and the BaseHTTPServer examples require that
the OpenID library is installed or that it has been added to Python's
search path (PYTHONPATH environment variable or sys.path).

The Django example is probably a good place to start reading the
code. There is little that is Django-specific about the OpenID logic
in the example, and it should be easy to port to any framework. To run
the django examples, see the README file in the djopenid subdirectory.

The other examples use Python's built-in BaseHTTPServer and have a
good deal of ad-hoc dispatching and rendering code mixed in

Using the BaseHTTPServer examples
=================================

This directory contains a working server and consumer that use this
OpenID library. They are both written using python's standard
BaseHTTPServer.


To run the example system:

1. Make sure you've installed the library, as explained in the
   installation instructions.

2. Start the consumer server:

        python consumer.py --port 8001


3. In another terminal, start the identity server:

        python server.py --port 8000

   (Hit Ctrl-C in either server's window to stop that server.)


4. Open your web broswer, and go to the consumer server:

        http://localhost:8001/

   Note that all pages the consumer server shows will have "Python OpenID
   Consumer Example" across the top.


5. Enter an identity url managed by the sample identity server:

        http://localhost:8000/id/bob


6. The browser will be redirected to the sample server, which will be
   requesting that you log in to proceed.  Enter the username for the
   identity URL into the login box:

        bob

   Note that all pages the identity server shows will have "Python
   OpenID Server Example" across the top.


7. After you log in as bob, the server example will ask you if you
   want to allow http://localhost:8001/ to know your identity.  Say
   yes.


8. You should end up back on the consumer site, at a page indicating
   you've logged in successfully.


That's a basic OpenID login procedure.  You can continue through it,
playing with variations to see how they work.  The python code is
intended to be a straightforward example of how to use the python
OpenID library to function as either an identity server or consumer.

Getting help
============

Please send bug reports, patches, and other feedback to

  dev@lists.openidenabled.com

Yahoo! Social SDK - Python
==========================

Find documentation and support on Yahoo! Developer Network: http://developer.yahoo.com

 * Yahoo! Application Platform - http://developer.yahoo.com/yap/
 * Yahoo! Social APIs - http://developer.yahoo.com/social/
 * Yahoo! Query Language - http://developer.yahoo.com/yql/

Hosted on GitHub: http://github.com/yahoo/yos-social-python/tree/master

License
=======

@copyright: Copyrights for code authored by Yahoo! Inc. is licensed under the following terms:
@license:   BSD Open Source License

Yahoo! Social SDK
Software License Agreement (BSD License)
Copyright (c) 2009, Yahoo! Inc.
All rights reserved.

Redistribution and use of this software in source and binary forms, with
or without modification, are permitted provided that the following
conditions are met:

* Redistributions of source code must retain the above
  copyright notice, this list of conditions and the
  following disclaimer.

* Redistributions in binary form must reproduce the above
  copyright notice, this list of conditions and the
  following disclaimer in the documentation and/or other
  materials provided with the distribution.

* Neither the name of Yahoo! Inc. nor the names of its
  contributors may be used to endorse or promote products
  derived from this software without specific prior
  written permission of Yahoo! Inc.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


The Yahoo! Social Python SDK code is subject to the BSD license, see the LICENSE file.


Requirements
============

The following dependencies are bundled with the Yahoo! Python SDK, but are under
terms of a separate license. See the bundled LICENSE files for more information:

 * SimpleJSON - http://code.google.com/p/simplejson
 * OAuth - http://code.google.com/p/oauth
 * OpenID - http://openidenabled.com/python-openid


Install
=======

To install the library simply make sure that this package is in your PYTHON PATH.
The sdk also requires the oauth and simplejson modules:

    easy_install oauth simplejson
    python setup.py install


Examples
========

## Fetching YQL:

    import yahoo.yql

    response = yahoo.yql.YQLQuery().execute('select * from delicious.feeds.popular')
    if 'query' in response and 'results' in response['query']:
      print response['query']['results']
    elif 'error' in response:
      print 'YQL query failed with error: "%s".' % response['error']['description']
    else:
      print 'YQL response malformed.'


## Fetching Social Data:

    import yahoo.application

    # Yahoo! OAuth Credentials - http://developer.yahoo.com/dashboard/

    CONSUMER_KEY      = '##'
    CONSUMER_SECRET   = '##'
    APPLICATION_ID    = '##'
    CALLBACK_URL      = '##'

    oauthapp      = yahoo.application.OAuthApplication(CONSUMER_KEY, CONSUMER_SECRET, APPLICATION_ID, CALLBACK_URL)

    # Fetch request token
    request_token = oauthapp.get_request_token(CALLBACK_URL)

    # Redirect user to authorization url
    redirect_url  = oauthapp.get_authorization_url(request_token)

    # Exchange request token for authorized access token
    verifier  = self.request.get('oauth_verifier') # must fetch oauth_verifier from request

    access_token  = oauthapp.get_access_token(request_token, verifier)

    # update access token
    oauthapp.token = access_token

    profile = oauthapp.getProfile()

    print profile


## Signing with SimpleAuth (OpenID + OAuth):

    See the bundled sample code in examples/simpleauth/simpleauth.py.


## Fetching people and activities with OpenSocial:

    See the bundled sample code in examples/opensocial/profile.py.


Tests
=====

The Yahoo! Python SDK comes with a test suite to validate functionality. The tests also
show functional examples and results. To run the test suite, simply execute the test suite:

    python test/run_unit_tests.py


delegated-20060809.xrds    - results from proxy.xri.net, determined by 
                             Drummond and Kevin to be incorrect.
delegated-20060809-r1.xrds - Drummond's 1st correction
delegated-20060809-r2.xrds - Drummond's 2nd correction

spoofs: keturn's (=!E4)'s attempts to log in with Drummond's i-number (=!D2)
spoof1.xrds
spoof2.xrds
spoof3.xrds - attempt to steal @!C0!D2 by having "at least one" CanonicalID
    match the $res service ProviderID.

ref.xrds - resolving @ootao*test.ref, which refers to a neustar XRI.


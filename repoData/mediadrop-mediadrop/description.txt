To upgrade the database simply run:

	cd mediadrop_install
	paster setup-app deployment.ini

This will run all the pending migrations necessary to bring your
database up-to-date.

Please note that we only support upgrades from MediaDrop 0.9.0
(or any later version). If you happen to run MediaDrop 0.7 or 0.8
please upgrade to 0.9.0 first.

Plase note: MediaDrop was formerly known as "MediaCore CE".

MediaDrop
----------

MediaDrop is a modular video, audio, and podcast publication platform which can
be extended with plugins (previously known as "MediaCore Community Edition").

The offical website is [http://mediadrop.net](http://mediadrop.net/) contains
more information about the software including
[installation documentation](http://mediadrop.net/docs/install/).

If you require help with MediaDrop customization or installation, check out our
friendly [community forums](http://mediadrop.net/community).




The Life of a Data Project

We're scraping the FDIC failed bank list.

In the process, we're showing how to:

* write a reusable scraper
* save the data to a csv
* save the data to a db
* do. stuff. with. data.
* rinse and repeat.

This code repo stores code related to the PythonJournos group.

This directory contains code snippets showing how to access data in CSVs.
Below are descriptions of each snippet, ordered roughly by increasing
level of complexity.


read_data_from_CSV_1.py - The most basic way to read data from a CSV
glob_usage_example.py - Using glob to get a list of filenames for processing

This directory contains scripts that demonstrate basic Python in a web-scraping context.

* *failed_banks_scrape.py* - download and parse a single Web page
* *la_election_scrape.py* - look for links and process those links, which then leads to more links
* *fec_efiles_scrape.py* - make a POST request to fetch links for (and download) campaign finance reports 


__FILENAME__ = settings
import os

ACCOUNT_ACTIVATION_DAYS = 2
REGISTRATION_TEMPLATE_DIR = os.path.join(os.path.dirname(__file__), "templates")

########NEW FILE########

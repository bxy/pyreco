# D3

**D3** is a small, free JavaScript library for manipulating HTML documents
based on data. D3 can help you quickly visualize your data as HTML or SVG,
handle interactivity, and incorporate smooth transitions and staged animations
into your pages. You can use D3 as a visualization framework (like Protovis),
or you can use it to build dynamic pages (like jQuery).

### Browser Support

D3 should work on any browser, with minimal requirements such as JavaScript
and the [W3C DOM](http://www.w3.org/DOM/) API. By default D3 requires the
[Selectors API](http://www.w3.org/TR/selectors-api/) Level 1, but you can
preload [Sizzle](http://sizzlejs.com/) for compatibility with older browsers.
Some of the included D3 examples use additional browser features, such as
[SVG](http://www.w3.org/TR/SVG/) and [CSS3
Transitions](http://www.w3.org/TR/css3-transitions/). These features are not
required to use D3, but are useful for visualization! D3 is not a
compatibility layer. The examples should work on Firefox, Chrome (Chromium),
Safari (WebKit), Opera and IE9.

Note: Chrome has strict permissions for reading files out of the local file
system. Some examples use AJAX which works differently via HTTP instead of local
files. For the best experience, load the D3 examples from your own machine via
HTTP. Any static file web server will work; for example you can run Python's
built-in server:

    python -m SimpleHTTPServer 8888

Once this is running, go to: <http://localhost:8888/examples/>

# D3

**D3** is a small, free JavaScript library for manipulating HTML documents
based on data. D3 can help you quickly visualize your data as HTML or SVG,
handle interactivity, and incorporate smooth transitions and staged animations
into your pages. You can use D3 as a visualization framework (like Protovis),
or you can use it to build dynamic pages (like jQuery).

### Browser Support

D3 should work on any browser, with minimal requirements such as JavaScript
and the [W3C DOM](http://www.w3.org/DOM/) API. By default D3 requires the
[Selectors API](http://www.w3.org/TR/selectors-api/) Level 1, but you can
preload [Sizzle](http://sizzlejs.com/) for compatibility with older browsers.
Some of the included D3 examples use additional browser features, such as
[SVG](http://www.w3.org/TR/SVG/) and [CSS3
Transitions](http://www.w3.org/TR/css3-transitions/). These features are not
required to use D3, but are useful for visualization! D3 is not a
compatibility layer. The examples should work on Firefox, Chrome (Chromium),
Safari (WebKit), Opera and IE9.

Note: Chrome has strict permissions for reading files out of the local file
system. Some examples use AJAX which works differently via HTTP instead of local
files. For the best experience, load the D3 examples from your own machine via
HTTP. Any static file web server will work; for example you can run Python's
built-in server:

    python -m SimpleHTTPServer 8888

Once this is running, go to: <http://localhost:8888/examples/>

Modifed from the example at of D3
http://mbostock.github.com/d3/ex/force.html

Run the file force.py to generate the force.json data file needed for this to work.

Then copy all of the files in this directory to a webserver and load force.html.


These files are for testing the methods and functions in NetworkX
The nose testing package is required for all tests:
http://somethingaboutorange.com/mrl/projects/nose

The tests also demonstrate the usage of many of the features of NetworkX.

There are a few ways to run the tests. 

The simplest way is to import networkx and run the test() function.

>>> import networkx
>>> networkx.test()

or::

   python -c "import networkx; networkx.test()

If you have the source package and the nose testing package you
can test the complete package from the unpacked source directory with::

   python setup_egg.py nosetests

The python module benchmark.py can be used to compare relative speed of small
code bits using the timeit module for different graph classes.

NetworkX

NetworkX is a Python package for the creation, manipulation, and
study of the structure, dynamics, and functions of complex networks.  

Copyright (C) 2004-2014 NetworkX Developers
Aric Hagberg <hagberg@lanl.gov>
Dan Schult <dschult@colgate.edu>
Pieter Swart <swart@lanl.gov>

Distributed with a BSD license; see LICENSE.txt.

See http://networkx.github.io/ for more information.


__FILENAME__ = conf
# -*- coding: utf-8 -*-
#
# Flask-Sijax documentation build configuration file, created by
# sphinx-quickstart on Sun Mar  6 02:01:07 2011.
#
# This file is execfile()d with the current directory set to its containing dir.
#
# Note that not all possible configuration values are present in this
# autogenerated file.
#
# All configuration values have a default; values that are commented out
# serve to show the default.

import sys, os

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#sys.path.insert(0, os.path.abspath('.'))

sys.path.append(os.path.abspath('_themes'))

# -- General configuration -----------------------------------------------------

# If your documentation needs a minimal Sphinx version, state it here.
#needs_sphinx = '1.0'

# Add any Sphinx extension module names here, as strings. They can be extensions
# coming with Sphinx (named 'sphinx.ext.*') or your custom ones.
extensions = ['sphinx.ext.autodoc', 'sphinx.ext.intersphinx']

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# The suffix of source filenames.
source_suffix = '.rst'

# The encoding of source files.
#source_encoding = 'utf-8-sig'

# The master toctree document.
master_doc = 'index'

# General information about the project.
project = u'Flask-Sijax'
copyright = u'2011, Slavi Pantaleev'

# The version info for the project you're documenting, acts as replacement for
# |version| and |release|, also used in various other places throughout the
# built documents.
#
# The short X.Y version.
version = '0.3.3'
# The full version, including alpha/beta/rc tags.
release = '0.3.3'

# The language for content autogenerated by Sphinx. Refer to documentation
# for a list of supported languages.
#language = None

# There are two options for replacing |today|: either, you set today to some
# non-false value, then it is used:
#today = ''
# Else, today_fmt is used as the format for a strftime call.
#today_fmt = '%B %d, %Y'

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
exclude_patterns = ['_build']

# The reST default role (used for this markup: `text`) to use for all documents.
#default_role = None

# If true, '()' will be appended to :func: etc. cross-reference text.
#add_function_parentheses = True

# If true, the current module name will be prepended to all description
# unit titles (such as .. function::).
#add_module_names = True

# If true, sectionauthor and moduleauthor directives will be shown in the
# output. They are ignored by default.
#show_authors = False

# The name of the Pygments (syntax highlighting) style to use.
pygments_style = 'sphinx'

# A list of ignored prefixes for module index sorting.
#modindex_common_prefix = []


# -- Options for HTML output ---------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
html_theme = 'flask_small'

# Theme options are theme-specific and customize the look and feel of a theme
# further.  For a list of options available for each theme, see the
# documentation.
html_theme_options = {'github_fork': 'spantaleev/flask-sijax', 'index_logo': False}

# Add any paths that contain custom themes here, relative to this directory.
html_theme_path = ['_themes']

# The name for this set of Sphinx documents.  If None, it defaults to
# "<project> v<release> documentation".
#html_title = None

# A shorter title for the navigation bar.  Default is the same as html_title.
#html_short_title = None

# The name of an image file (relative to this directory) to place at the top
# of the sidebar.
#html_logo = None

# The name of an image file (within the static path) to use as favicon of the
# docs.  This file should be a Windows icon file (.ico) being 16x16 or 32x32
# pixels large.
#html_favicon = None

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# If not '', a 'Last updated on:' timestamp is inserted at every page bottom,
# using the given strftime format.
#html_last_updated_fmt = '%b %d, %Y'

# If true, SmartyPants will be used to convert quotes and dashes to
# typographically correct entities.
#html_use_smartypants = True

# Custom sidebar templates, maps document names to template names.
#html_sidebars = {}

# Additional templates that should be rendered to pages, maps page names to
# template names.
#html_additional_pages = {}

# If false, no module index is generated.
#html_domain_indices = True

# If false, no index is generated.
#html_use_index = True

# If true, the index is split into individual pages for each letter.
#html_split_index = False

# If true, links to the reST sources are added to the pages.
#html_show_sourcelink = True

# If true, "Created using Sphinx" is shown in the HTML footer. Default is True.
#html_show_sphinx = True

# If true, "(C) Copyright ..." is shown in the HTML footer. Default is True.
#html_show_copyright = True

# If true, an OpenSearch description file will be output, and all pages will
# contain a <link> tag referring to it.  The value of this option must be the
# base URL from which the finished HTML is served.
#html_use_opensearch = ''

# This is the file name suffix for HTML files (e.g. ".xhtml").
#html_file_suffix = None

# Output file base name for HTML help builder.
htmlhelp_basename = 'Flask-Sijaxdoc'


# -- Options for LaTeX output --------------------------------------------------

# The paper size ('letter' or 'a4').
#latex_paper_size = 'letter'

# The font size ('10pt', '11pt' or '12pt').
#latex_font_size = '10pt'

# Grouping the document tree into LaTeX files. List of tuples
# (source start file, target name, title, author, documentclass [howto/manual]).
latex_documents = [
  ('index', 'Flask-Sijax.tex', u'Flask-Sijax Documentation',
   u'Slavi Pantaleev', 'manual'),
]

# The name of an image file (relative to this directory) to place at the top of
# the title page.
#latex_logo = None

# For "manual" documents, if this is true, then toplevel headings are parts,
# not chapters.
#latex_use_parts = False

# If true, show page references after internal links.
#latex_show_pagerefs = False

# If true, show URL addresses after external links.
#latex_show_urls = False

# Additional stuff for the LaTeX preamble.
#latex_preamble = ''

# Documents to append as an appendix to all manuals.
#latex_appendices = []

# If false, no module index is generated.
#latex_domain_indices = True


# -- Options for manual page output --------------------------------------------

# One entry per manual page. List of tuples
# (source start file, name, description, authors, manual section).
man_pages = [
    ('index', 'flask-sijax', u'Flask-Sijax Documentation',
     [u'Slavi Pantaleev'], 1)
]

# Example configuration for intersphinx: refer to the Python standard library.
intersphinx_mapping = {
    'sijax': ('http://packages.python.org/Sijax/', None),
}

########NEW FILE########
__FILENAME__ = chat
# -*- coding: utf-8 -*-

"""A chat/shoutbox using Sijax."""

import os, sys

path = os.path.join('.', os.path.dirname(__file__), '../')
sys.path.append(path)

from flask import Flask, g, render_template
import flask_sijax

app = Flask(__name__)

# The path where you want the extension to create the needed javascript files
# DON'T put any of your files in this directory, because they'll be deleted!
app.config["SIJAX_STATIC_PATH"] = os.path.join('.', os.path.dirname(__file__), 'static/js/sijax/')

# You need to point Sijax to the json2.js library if you want to support
# browsers that don't support JSON natively (like IE <= 7)
app.config["SIJAX_JSON_URI"] = '/static/js/sijax/json2.js'

flask_sijax.Sijax(app)

class SijaxHandler(object):
    """A container class for all Sijax handlers.

    Grouping all Sijax handler functions in a class
    (or a Python module) allows them all to be registered with
    a single line of code.
    """

    @staticmethod
    def save_message(obj_response, message):
        message = message.strip()
        if message == '':
            return obj_response.alert("Empty messages are not allowed!")

        # Save message to database or whatever..

        import time, hashlib
        time_txt = time.strftime("%H:%M:%S", time.gmtime(time.time()))
        message_id = 'message_%s' % hashlib.sha256(time_txt).hexdigest()

        message = """
        <div id="%s" style="opacity: 0;">
            [<strong>%s</strong>] %s
        </div>
        """ % (message_id, time_txt, message)

        # Add message to the end of the container
        obj_response.html_append('#messages', message)

        # Clear the textbox and give it focus in case it has lost it
        obj_response.attr('#message', 'value', '')
        obj_response.script("$('#message').focus();")

        # Scroll down the messages area
        obj_response.script("$('#messages').attr('scrollTop', $('#messages').attr('scrollHeight'));")

        # Make the new message appear in 400ms
        obj_response.script("$('#%s').animate({opacity: 1}, 400);" % message_id)

    @staticmethod
    def clear_messages(obj_response):
        # Delete all messages from the database

        # Clear the messages container
        obj_response.html('#messages', '')

        # Clear the textbox
        obj_response.attr('#message', 'value', '')

        # Ensure the texbox has focus
        obj_response.script("$('#message').focus();")


@flask_sijax.route(app, "/")
def index():
    if g.sijax.is_sijax_request:
        # The request looks like a valid Sijax request
        # Let's register the handlers and tell Sijax to process it
        g.sijax.register_object(SijaxHandler)
        return g.sijax.process_request()

    return render_template('chat.html')

if __name__ == '__main__':
    app.run(debug=True, port=8080)

########NEW FILE########
__FILENAME__ = comet
# -*- coding: utf-8 -*-

"""A demonstration of Comet streaming functionality using Sijax."""

import os, sys

path = os.path.join('.', os.path.dirname(__file__), '../')
sys.path.append(path)

from flask import Flask, g, render_template
import flask_sijax

app = Flask(__name__)

# The path where you want the extension to create the needed javascript files
# DON'T put any of your files in this directory, because they'll be deleted!
app.config["SIJAX_STATIC_PATH"] = os.path.join('.', os.path.dirname(__file__), 'static/js/sijax/')

# You need to point Sijax to the json2.js library if you want to support
# browsers that don't support JSON natively (like IE <= 7)
app.config["SIJAX_JSON_URI"] = '/static/js/sijax/json2.js'

flask_sijax.Sijax(app)

def comet_do_work_handler(obj_response, sleep_time):
    import time

    for i in range(6):
        width = '%spx' % (i * 80)
        obj_response.css('#progress', 'width', width)
        obj_response.html('#progress', width)

        # Yielding tells Sijax to flush the data to the browser.
        # This only works for Streaming functions (Comet or Upload)
        # and would not work for normal Sijax functions
        yield obj_response

        if i != 5:
            time.sleep(sleep_time)


@flask_sijax.route(app, "/")
def index():
    if g.sijax.is_sijax_request:
        # The request looks like a valid Sijax request
        # Let's register the handlers and tell Sijax to process it
        g.sijax.register_comet_callback('do_work', comet_do_work_handler)
        return g.sijax.process_request()

    return render_template('comet.html')

if __name__ == '__main__':
    app.run(debug=True, port=8080)

########NEW FILE########
__FILENAME__ = hello
# -*- coding: utf-8 -*-

"""A very simple example demonstrating Sijax and the Flask-Sijax extension."""

import os, sys

path = os.path.join('.', os.path.dirname(__file__), '../')
sys.path.append(path)


from flask import Flask, g, render_template
import flask_sijax

app = Flask(__name__)

# The path where you want the extension to create the needed javascript files
# DON'T put any of your files in this directory, because they'll be deleted!
app.config["SIJAX_STATIC_PATH"] = os.path.join('.', os.path.dirname(__file__), 'static/js/sijax/')

# You need to point Sijax to the json2.js library if you want to support
# browsers that don't support JSON natively (like IE <= 7)
app.config["SIJAX_JSON_URI"] = '/static/js/sijax/json2.js'

flask_sijax.Sijax(app)

# Regular flask view function - Sijax is unavailable here
@app.route("/")
def hello():
    return "Hello World!<br /><a href='/sijax'>Go to Sijax test</a>"

# Sijax enabled function - notice the `@Sijax.route` decorator
# used instead of `@app.route` (above).
@flask_sijax.route(app, "/sijax")
def hello_sijax():
    # Sijax handler function receiving 2 arguments from the browser
    # The first argument (obj_response) is passed automatically
    # by Sijax (much like Python passes `self` to object methods)
    def hello_handler(obj_response, hello_from, hello_to):
        obj_response.alert('Hello from %s to %s' % (hello_from, hello_to))
        obj_response.css('a', 'color', 'green')

    # Another Sijax handler function which receives no arguments
    def goodbye_handler(obj_response):
        obj_response.alert('Goodbye, whoever you are.')
        obj_response.css('a', 'color', 'red')

    if g.sijax.is_sijax_request:
        # The request looks like a valid Sijax request
        # Let's register the handlers and tell Sijax to process it
        g.sijax.register_callback('say_hello', hello_handler)
        g.sijax.register_callback('say_goodbye', goodbye_handler)
        return g.sijax.process_request()

    return render_template('hello.html')

if __name__ == '__main__':
    app.run(debug=True, port=8080)

########NEW FILE########
__FILENAME__ = upload
# -*- coding: utf-8 -*-

"""Demonstration of the Sijax Upload plugin.

The Sijax Upload plugin allows you to easily transform any form
on your page into a "Sijax-enabled" form. Such forms will be submitted
using Sijax (ajax upload) and the upload will be handled by your Sijax handler
function in the same way regular Sijax handler functions are called.

"""

import os, sys

path = os.path.join('.', os.path.dirname(__file__), '../')
sys.path.append(path)

from flask import Flask, g, render_template
import flask_sijax

app = Flask(__name__)

# The path where you want the extension to create the needed javascript files
# DON'T put any of your files in this directory, because they'll be deleted!
app.config["SIJAX_STATIC_PATH"] = os.path.join('.', os.path.dirname(__file__), 'static/js/sijax/')

# You need to point Sijax to the json2.js library if you want to support
# browsers that don't support JSON natively (like IE <= 7)
app.config["SIJAX_JSON_URI"] = '/static/js/sijax/json2.js'

flask_sijax.Sijax(app)

class SijaxHandler(object):
    """A container class for all Sijax handlers.

    Grouping all Sijax handler functions in a class
    (or a Python module) allows them all to be registered with
    a single line of code.
    """

    @staticmethod
    def _dump_data(obj_response, files, form_values, container_id):
        def dump_files():
            if 'file' not in files:
                return 'Bad upload'

            file_data = files['file']
            file_name = file_data.filename
            if file_name is None:
                return 'Nothing uploaded'

            file_type = file_data.content_type
            file_size = len(file_data.read())
            return 'Uploaded file %s (%s) - %sB' % (file_name, file_type, file_size)

        html = """Form values: %s<hr />Files: %s"""
        html = html % (str(form_values), dump_files())

        obj_response.html('#%s' % container_id, html)

    @staticmethod
    def form_one_handler(obj_response, files, form_values):
        SijaxHandler._dump_data(obj_response, files, form_values, 'formOneResponse')

    @staticmethod
    def form_two_handler(obj_response, files, form_values):
        SijaxHandler._dump_data(obj_response, files, form_values, 'formTwoResponse')

        obj_response.reset_form()
        obj_response.html_append('#formTwoResponse', '<br />Form elements were reset!<br />Doing some more work (2 seconds)..')

        # Send the data to the browser now
        yield obj_response

        from time import sleep
        sleep(2)

        obj_response.html_append('#formTwoResponse', '<br />Finished!')


@flask_sijax.route(app, "/")
def index():
    # Notice how we're doing callback registration on each request,
    # instead of only when needed (when the request is a Sijax request).
    # This is because `register_upload_callback` returns some javascript
    # that prepares the form on the page.
    form_init_js = ''
    form_init_js += g.sijax.register_upload_callback('formOne', SijaxHandler.form_one_handler)
    form_init_js += g.sijax.register_upload_callback('formTwo', SijaxHandler.form_two_handler)

    if g.sijax.is_sijax_request:
        # The request looks like a valid Sijax request
        # The handlers are already registered above.. we can process the request
        return g.sijax.process_request()

    return render_template('upload.html', form_init_js=form_init_js)

if __name__ == '__main__':
    app.run(debug=True, port=8080)

########NEW FILE########
__FILENAME__ = flask_sijax
# -*- coding: utf-8 -*-

from __future__ import absolute_import

from werkzeug.wsgi import ClosingIterator

from flask import g, request, Response, _request_ctx_stack

import sijax

class Sijax(object):
    """Helper class that you'll use to interact with Sijax.

    This class tries to look like :class:`sijax.Sijax`,
    although the API differs slightly in order to make things easier for you.
    """

    def __init__(self, app=None):
        self._request_uri = None

        #: Reference to the underlying :class:`sijax.Sijax` object
        self._sijax = None

        #: The URI to json2.js (JSON support for browsers without native one)
        self._json_uri = None

        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        app.before_request(self._on_before_request)

        static_path = app.config.get('SIJAX_STATIC_PATH', None)
        if static_path is not None:
            sijax.helper.init_static_path(static_path)

        self._json_uri = app.config.get('SIJAX_JSON_URI', None)

        app.extensions = getattr(app, 'extensions', {})
        app.extensions['sijax'] = self

    def _on_before_request(self):
        g.sijax = self

        self._sijax = sijax.Sijax()
        self._sijax.set_data(request.form)

        url_relative = request.url[len(request.host_url) - 1:]
        self._sijax.set_request_uri(url_relative)

        if self._json_uri is not None:
            self._sijax.set_json_uri(self._json_uri)

    def set_request_uri(self, uri):
        """Changes the request URI from the automatically detected one.

        The automatically detected URI is the relative URI of the
        current request, as detected by Flask/Werkzeug.

        You can override the detected URI with another one
        (for the current request only), by using this function.
        """
        self._sijax.set_request_uri(uri)

    def register_callback(self, *args, **kwargs):
        """Registers a single callback function.

        Refer to :meth:`sijax.Sijax.register_callback`
        for more details - this is a direct proxy to it.
        """
        self._sijax.register_callback(*args, **kwargs)

    def register_object(self, *args, **kwargs):
        """Registers all "public" callable attributes of the given object.

        The object could be anything (module, class, class instance, etc.)

        This makes mass registration of functions a lot easier.

        Refer to :meth:`sijax.Sijax.register_object`
        for more details - this is a direct proxy to it.
        """
        self._sijax.register_object(*args, **kwargs)

    def register_comet_callback(self, *args, **kwargs):
        """Registers a single Comet callback function
        (see :ref:`comet-plugin`).

        Refer to :func:`sijax.plugin.comet.register_comet_callback`
        for more details - its signature differs slightly.

        This method's signature is the same, except that the first
        argument that :func:`sijax.plugin.comet.register_comet_callback`
        expects is the Sijax instance, and this method
        does that automatically, so you don't have to do it.
        """
        sijax.plugin.comet.register_comet_callback(self._sijax, *args, **kwargs)

    def register_comet_object(self, *args, **kwargs):
        """Registers all functions from the object as Comet functions
        (see :ref:`comet-plugin`).

        This makes mass registration of functions a lot easier.

        Refer to :func:`sijax.plugin.comet.register_comet_object`
        for more details -ts signature differs slightly.

        This method's signature is the same, except that the first
        argument that :func:`sijax.plugin.comet.register_comet_object`
        expects is the Sijax instance, and this method
        does that automatically, so you don't have to do it.
        """
        sijax.plugin.comet.register_comet_object(self._sijax, *args, **kwargs)

    def register_upload_callback(self, *args, **kwargs):
        """Registers an Upload function (see :ref:`upload-plugin`)
        to handle a certain form.

        Refer to :func:`sijax.plugin.upload.register_upload_callback`
        for more details.

        This method passes some additional arguments to your handler
        functions - the ``flask.request.files`` object.

        Your upload handler function's signature should look like this::

            def func(obj_response, files, form_values)

        :return: string - javascript code that initializes the form
        """
        if 'args_extra' not in kwargs:
            kwargs['args_extra'] = [request.files]
        return sijax.plugin.upload.register_upload_callback(self._sijax, *args, **kwargs)

    def register_event(self, *args, **kwargs):
        """Registers a new event handler.

        Refer to :meth:`sijax.Sijax.register_event`
        for more details - this is a direct proxy to it.
        """
        self._sijax.register_event(*args, **kwargs)

    @property
    def is_sijax_request(self):
        """Tells whether the current request is meant to be handled by Sijax.

        Refer to :attr:`sijax.Sijax.is_sijax_request` for more details -
        this is a direct proxy to it.
        """
        return self._sijax.is_sijax_request

    def process_request(self):
        """Processes the Sijax request and returns the proper response.

        Refer to :meth:`sijax.Sijax.process_request` for more details.
        """
        response = self._sijax.process_request()
        return _make_response(response)

    def execute_callback(self, *args, **kwargs):
        """Executes a callback and returns the proper response.

        Refer to :meth:`sijax.Sijax.execute_callback` for more details.
        """
        response = self._sijax.execute_callback(*args, **kwargs)
        return _make_response(response)

    def get_js(self):
        """Returns the javascript code that sets up the client for this request.

        This code is request-specific, be sure to put it on each page that needs
        to use Sijax.
        """
        return self._sijax.get_js()


def route(app_or_blueprint, rule, **options):
    """An alternative to :meth:`flask.Flask.route` or :meth:`flask.Blueprint.route` that
    always adds the ``POST`` method to the allowed endpoint request methods.

    You should use this for all your view functions that would need to use Sijax.

    We're doing this because Sijax uses ``POST`` for data passing,
    which means that every endpoint that wants Sijax support
    would have to accept ``POST`` requests.

    Registering functions that would use Sijax should happen like this::

        @flask_sijax.route(app, '/')
        def index():
            pass

    If you remember to make your view functions accessible via POST
    like this, you can avoid using this decorator::

        @app.route('/', methods=['GET', 'POST'])
        def index():
            pass
    """
    def decorator(f):
        methods = options.pop('methods', ('GET', 'POST'))
        if 'POST' not in methods:
            methods = tuple(methods) + ('POST',)
        options['methods'] = methods
        app_or_blueprint.add_url_rule(rule, None, f, **options)
        return f
    return decorator


def _make_response(sijax_response):
    """Takes a Sijax response object and returns a
    valid Flask response object."""
    from types import GeneratorType

    if isinstance(sijax_response, GeneratorType):
        # Streaming response using a generator (non-JSON response).
        # Upon returning a response, Flask would automatically destroy
        # the request data and uploaded files - done by `flask.ctx.RequestContext.auto_pop()`
        # We can't allow that, since the user-provided callback we're executing
        # from within the generator may want to access request data/files.
        # That's why we'll tell Flask to preserve the context and we'll clean up ourselves.

        request.environ['flask._preserve_context'] = True

        # Clean-up code taken from `flask.testing.TestingClient`
        def clean_up_context():
            top = _request_ctx_stack.top
            if top is not None and top.preserved:
                top.pop()

        # As per the WSGI specification, `close()` would be called on iterator responses.
        # Let's wrap the iterator in another one, which will forward that `close()` call to our clean-up callback.
        response = Response(ClosingIterator(sijax_response, clean_up_context), direct_passthrough=True)
    else:
        # Non-streaming response - a single JSON string
        response = Response(sijax_response)

    return response

########NEW FILE########
__FILENAME__ = tests
# -*- coding: utf-8 -*-

from __future__ import with_statement
import unittest

import flask
import flask_sijax


def _assert_response_json(context, string):
    from sijax.helper import json
    try:
        obj = json.loads(string)
    except:
        context.fail('Cannot decode JSON!')
    else:
        context.assertTrue(isinstance(obj, list))
        for item in obj:
            context.assertTrue(isinstance(item, dict))
            context.assertTrue('type' in item)

class SijaxFlaskTestCase(unittest.TestCase):

    def test_route_always_adds_post_method(self):
        class FlaskMock(object):
            def __init__(self):
                self.methods = None

            def add_url_rule(self_mock, rule, view_func, endpoint, **options):
                self.assertEqual('/rule', rule)
                self_mock.methods = options.pop('methods', None)

        def try_route(methods_expected, **options):
            app = FlaskMock()
            decorator = flask_sijax.route(app, '/rule', **options)
            decorator(lambda a: a)
            self.assertEqual(tuple(methods_expected), app.methods)

        try_route(('GET', 'POST'))
        try_route(('GET', 'POST'), methods=('GET',))
        try_route(('POST', ), methods=())
        try_route(('GET', 'PUT', 'POST'), methods=('GET', 'PUT'))
        try_route(('GET', 'DELETE', 'POST'), methods=('GET', 'DELETE'))

    def test_sijax_helper_object_is_only_bound_to_g_in_a_request_context(self):
        app = flask.Flask(__name__)

        helper = flask_sijax.Sijax(app)

        with app.test_request_context():
            app.preprocess_request()
            self.assertEqual(id(helper), id(flask.g.sijax))

        # Make sure that access fails when outside of a request context
        try:
            flask.g.sijax
        except RuntimeError:
            # RuntimeError('working outside of request context')
            pass
        else:
            self.fail('Bound to g in a non-request context!')

    def test_delayed_app_initialization_works(self):
        # Makes sure that an app object can be provided at a later point
        # and that Sijax would still be registered correctly.
        app = flask.Flask(__name__)
        helper = flask_sijax.Sijax()
        helper.init_app(app)
        with app.test_request_context():
            app.preprocess_request()
            self.assertEqual(id(helper), id(flask.g.sijax))

    def test_json_uri_config_is_used(self):
        uri = '/some/json_uri.here'

        app = flask.Flask(__name__)
        app.config['SIJAX_JSON_URI'] = uri
        helper = flask_sijax.Sijax(app)
        with app.test_request_context():
            app.preprocess_request()

            js = helper.get_js()
            self.assertTrue(uri in js)

    def test_request_uri_changing_works(self):
        # The request URI is automatically detected,
        # but could be changed to something else on each request
        # Changes should not be preserved throughout different requests though

        app = flask.Flask(__name__)

        helper = flask_sijax.Sijax(app)

        with app.test_request_context():
            app.preprocess_request()

            js = helper.get_js()
            self.assertTrue('Sijax.setRequestUri("/");' in js)

            helper.set_request_uri('http://something.else/')

            js = helper.get_js()
            self.assertFalse('Sijax.setRequestUri("/");' in js)
            self.assertTrue('Sijax.setRequestUri("http://something.else/");' in js)

        # Ensure that the changed request uri was valid for the previous request only
        with app.test_request_context():
            app.preprocess_request()

            js = helper.get_js()
            self.assertTrue('Sijax.setRequestUri("/");' in js)
            self.assertFalse('Sijax.setRequestUri("http://something.else/");' in js)

        # Test that a more complex request url (with query string, etc) works
        with app.test_request_context('/relative/url?query=string&is=here'):
            app.preprocess_request()

            js = helper.get_js()
            self.assertTrue('Sijax.setRequestUri("/relative/url?query=string&is=here");' in js)

    def test_registering_callbacks_in_a_non_request_context_fails(self):
        app = flask.Flask(__name__)
        helper = flask_sijax.Sijax(app)

        try:
            helper.register_callback('test', lambda r: r)
            self.fail('Callback registered, but failure was expected!')
        except AttributeError:
            # helper._sijax (and flask.g.sijax)
            pass

    def test_registering_callbacks_in_a_request_context_with_no_preprocessing_fails(self):
        app = flask.Flask(__name__)
        helper = flask_sijax.Sijax(app)

        with app.test_request_context():
            try:
                helper.register_callback('test', lambda r: r)
                self.fail('Callback registered, but failure was expected!')
            except AttributeError:
                # helper._sijax (and flask.g.sijax)
                pass

    def test_register_callback_works(self):
        call_history = []

        def callback(obj_response):
            call_history.append('callback')
            obj_response.alert('test')


        app = flask.Flask(__name__)
        helper = flask_sijax.Sijax(app)

        with app.test_request_context():
            app.preprocess_request()

            helper.register_callback('test', callback)

            # no data, cannot determine request as a sijax request
            self.assertFalse(helper.is_sijax_request)

            cls_sijax = helper._sijax.__class__
            helper._sijax.set_data({cls_sijax.PARAM_REQUEST: 'test', cls_sijax.PARAM_ARGS: '[]'})

            self.assertTrue(helper.is_sijax_request)
            response = helper.process_request()
            self.assertEqual(['callback'], call_history)
            _assert_response_json(self, response)

    def test_upload_callbacks_receive_the_expected_arguments(self):
        # Upload callbacks should have the following signature:
        #   def function(obj_response, flask_request_files, form_values)
        # The extension should ensure that the proper arguments are passed
        import sijax
        from types import GeneratorType

        app = flask.Flask(__name__)
        helper = flask_sijax.Sijax(app)

        call_history = []

        def callback(obj_response, files, form_values):
            call_history.append(form_values)
            call_history.append(id(files))

        with app.test_request_context():
            app.preprocess_request()

            helper.register_upload_callback('form_id', callback)
            func_name = sijax.plugin.upload.func_name_by_form_id('form_id')

            cls_sijax = helper._sijax.__class__

            post = {cls_sijax.PARAM_REQUEST: func_name, cls_sijax.PARAM_ARGS: '["form_id"]', 'post_key': 'val'}
            helper._sijax.set_data(post)
            self.assertTrue(helper.is_sijax_request)
            response = helper._sijax.process_request()
            self.assertTrue(isinstance(response, GeneratorType))
            for r in response: pass

            expected_history = [{'post_key': 'val'}, id(flask.request.files)]
            self.assertEqual(expected_history, call_history)

    def test_sijax_helper_passes_correct_post_data(self):
        # It's expected that the Sijax Helper class passes `flask.request.form`
        # as post data in the "on before request" stage
        app = flask.Flask(__name__)
        helper = flask_sijax.Sijax(app)

        with app.test_request_context():
            app.preprocess_request()
            self.assertEqual(id(helper._sijax.get_data()), id(flask.request.form))

    def test_process_request_returns_a_string_or_a_flask_response_object(self):
        # flask_sijax.Sijax.process_request should return a string for regular functions
        # and a Flask.Response object for functions that use a generator (streaming functions)
        from sijax.response import StreamingIframeResponse

        app = flask.Flask(__name__)
        helper = flask_sijax.Sijax(app)

        with app.test_request_context():
            app.preprocess_request()

            cls_sijax = helper._sijax.__class__

            post = {cls_sijax.PARAM_REQUEST: 'callback', cls_sijax.PARAM_ARGS: '[]'}
            helper._sijax.set_data(post)
            helper.register_callback('callback', lambda r: r)
            response = helper.process_request()
            self.assertTrue(isinstance(response, type('string')))

            helper.register_callback('callback', lambda r: r, response_class=StreamingIframeResponse)
            response = helper.process_request()
            self.assertTrue(isinstance(response, flask.Response))

if __name__ == '__main__':
    unittest.main()

########NEW FILE########

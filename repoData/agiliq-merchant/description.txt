This is an example app to test Django-Merchant.

It is a test webstore which showcases the various Backends.

Its has two things which you can buy.

1. The Foo Story: Its a non-fiction book about Foo.
2. Will the real Bar please stand up?: Its a fantasy fan-fiction about Bar. 

Users choose what they will buy, and they are give option to pay via various Backends.

After they make payments they are sent a mail describing the payments made, and the book is delivered.


----------------
Django-Merchant
----------------

|TravisCI|_

.. |TravisCI| image:: https://api.travis-ci.org/agiliq/merchant.png?branch=master
.. _TravisCI: https://travis-ci.org/agiliq/merchant

Django-Merchant is a django application that enables you to use
multiple payment processors from a single API.

Gateways
---------

Following gateways are supported:

* Authorize.net
* Paypal
* Eway
* Braintree Payments (Server to Server)
* Stripe
* Paylane
* WePay
* Beanstream
* Chargebee
* Global Iris

Off-Site Processing
--------------------

* Paypal
* RBS WorldPay
* Google Checkout
* Amazon FPS
* Braintree Payments (Transparent Redirect)
* Stripe.js
* eWAY
* Authorize.net Direct Post Method
* Global Iris RealMPI

Other
-----

* Bitcoin

Documentation
--------------

Documentation is automatically built and published online at:

http://readthedocs.org/docs/django-merchant/en/latest/

Support
--------

There is a mailing list where discussion regarding the development
and usage of merchant happens:

http://groups.google.com/group/django-merchant/


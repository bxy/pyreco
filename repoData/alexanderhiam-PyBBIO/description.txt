###Changelog for PyBBIO  
http://github.com/alexanderhiam/PyBBIO   

#### version 0.8.5
 * Dropped mmap GPIO, use Kernel drivers
 * PWM fully working
 * I2C support
 * Fixed USR LEDs

#### version 0.8
 * Added tools/ directory for install helpers
 * Support for Kernel >= 3.8:
   * GPIO
   * Serial
   * ADC
   * Added tool to generate and compile DT overlays
 * misc. fixes


#### version 0.6
 * Moved to multi-file package structure
 * New structure and install method can now easily support platforms besides 
   the Beaglbone
 * Swithed to setuptools instead of distutils for setup.py
 * All memory access moved to C extension to increase speed
 * Created this changelog!

ADS786x - v0.1

Copyright 2012 Alexander Hiam
ahiam@marlboro.edu

ADS786x is a library for PyBBIO to interface with TI's ADS786x1 series
analog-to-digital converters (ADS7866-12bit, ADS7867-10bit, ADS7868-8bit).

See documentation here:
 https://github.com/alexanderhiam/PyBBIO/wiki/ADS786x
As well as the included example program:
 PyBBIO/examples/ADS786x_test.py

ADS786x is released as part of PyBBIO under its Apache 2.0 license.
See PyBBIO/LICENSE.txt

BBIOServer - v1.2

Copyright 2012 Alexander Hiam
ahiam@marlboro.edu

BBIOServer is a dynamic web interface library for PyBBIO.

See documentation here:
 https://github.com/alexanderhiam/PyBBIO/wiki/BBIOServer
As well as the included example programs:
 PyBBIO/examples/BBIOServer_test.py
and 
 PyBBIO/examples/BBIOServer_mobile_test.py

BBIOServer is released as part of PyBBIO under its Apache 2.0 license.
See PyBBIO/LICENSE.txt

---------------------------------------------------

BBIOServer uses and includes a copy of the minified JQuery library v1.7.2,
released under the MIT license. 

JQuery license details:

Copyright (c) 2011 John Resig, http://jquery.com/

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

---------------------------------------------------


DACx311 - v0.1

Copyright 2012 Alexander Hiam
ahiam@marlboro.edu

DACx311 is a library for PyBBIO to interface with TI's DACx311 series
digital-to-analog converters (DAC5311-8bit, DAC6311-10bit, DAC7311-12bit,
DAC8311-14bit).

See documentation here:
 https://github.com/alexanderhiam/PyBBIO/wiki/DACx311
As well as the included example program:
 PyBBIO/examples/DACx311_test.py

DACx311 is released as part of PyBBIO under its Apache 2.0 license.
See PyBBIO/LICENSE.txt

EventIO - v0.2

Copyright 2012 Alexander Hiam <ahiam@marlboro.edu>

EventIO provides basic multi-process event-driven programming
for PyBBIO (https://github.com/alexanderhiam/PyBBIO). 

Documentation:
  https://github.com/alexanderhiam/PyBBIO/wiki/EventIO

Example program:
  PyBBIO/examples/EventIO_test.py

EventIO is released as part of PyBBIO under its Apache 2.0 license.
See PyBBIO/LICENSE.txt

MAX31855 - v0.2

Copyright 2012 Alexander Hiam
ahiam@marlboro.edu

MAX31855 is a library for PyBBIO to interface with Maxim's MAX31855 
thermocouple amplifier.

See documentation here:
 https://github.com/alexanderhiam/PyBBIO/wiki/MAX31855
As well as the included example program:
 PyBBIO/examples/MAX31855_test.py

MAX31855 is released as part of PyBBIO under its Apache 2.0 license.
See PyBBIO/LICENSE.txt

SafeProcess - v0.1

Copyright 2012 Alexander Hiam <ahiam@marlboro.edu>

For PyBBIO - https://github.com/alexanderhiam/PyBBIO

Provides a wrapper for Python's mutliprocessing.Process class
which will be terminated during PyBBIO's cleanup.

Documentation:
  https://github.com/alexanderhiam/PyBBIO/wiki/SafeProcess

Example program:
  PyBBIO/examples/SafeProcess_test.py

See the Python docs for multiprocessing.Process:
  http://docs.python.org/library/multiprocessing.html#multiprocessing.Process

SafeProcess is released as part of PyBBIO under its Apache 2.0
license. See PyBBIO/LICENSE.txt


Servo - v0.1

Copyright 2012 Alexander Hiam
ahiam@marlboro.edu

Library for controlling servo motors with the BeagleBone's PWM pins.

See documentation here:
 https://github.com/alexanderhiam/PyBBIO/wiki/Servo
As well as the included example programs:
 PyBBIO/examples/Servo_sweep.py

Based on the Arduino Servo library:
 http://www.arduino.cc/en/Reference/Servo

Servo is released as part of PyBBIO under its Apache 2.0 license.
See PyBBIO/LICENSE.txt


### PyBBIO v0.8.5
http://github.com/alexanderhiam/PyBBIO  
Copyright (c) 2012-2014 - Alexander Hiam <hiamalexander@gmail.com>    

PyBBIO is a Python library for Arduino-style hardware IO support on the TI 
Beaglebone. It currently supports basic digital IO through digitalRead() 
and digitalWrite() functions, ADC support through analogRead(), PWM support 
analogWrite() as well as a few utility functions for changing PWM frequency
etc., and an Arduino-style UART interface. SPI and I2C are on the way, so 
keep checking the Github page for updates.  

#### Documentation can be found at http://github.com/alexanderhiam/PyBBIO/wiki  

#License

    PyBBIO is licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.


�     �][o�6~ϯ�WW���)��6l��g��[�,�r�K�HY�|�lY"�2���L���s�O�G�y��y�$ˣ4����M�$�t%ˉ��˯o������"����2�k�~"�٫��*�ͻ����Ɋ�9.�̊�+'��8��6
�PNG

Learning Linux C/C++ Programming from Scratch

Prerequisite
============

$ sudo apt-get install python-setuptools
$ sudo easy_install sphinx

Build
=====

$ make html
$ make epub

The build result will be generated under _build/html and _build/epub respectively.

TODO
====

Generate images under images directory from dia files under dia directory automatically through Makefile, rather than manually.



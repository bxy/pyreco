# Contributing

## Where to Begin

Development is based on milestones. Each milestone will be implemented in its own branch (e.g. branch `v2.0.4`).

The first thing you should do is determining the branch to base your edits on.

If you are fixing an issue that already exists in the issue tracker, see which milestone it belongs to and commit your contribution to the branch with the same name as the milestone. If the issue doesn't belong to a milestone;

* __push-access users__: assign it to the closest milestone.
* __non-push-access users__: pick the closest milestone.

If your contribution is independent from the issue tracker, pick the closest milestone and create your commits in that branch.

If the milestone branch doesn't exist yet;

* __push-access users__: create it. The format is e.g. `v2.0.4` and increments decimally. It is _not semver_.
* __non-push-access users__: pick `master` branch.

After you've done with your commits;

* __push-access users__: push your local branch to the same named branch on GitHub.
* __non-push-access users__: send a pull request from your local branch to the same named branch on GitHub.

Each milestone will have a pull request that will be kept open until the milestone is completed. The purpose of this is having a showcase and discussion page for the milestone. You can describe there what you have implemented, preferably using screenshots. It's also an early feedback place for the users about your contribution.

## General Guidelines

* Code formatting:
    - __Indentation__: 4 spaces
    - __Line endings__: `\n`
* If your contribution deserves a place in `README.md`, please do so. Preferably in the same commit with your modifications.
* If you want to add screenshots to `README.md`, put your images under `screenshots/` directory. For creating your screenshots, you can use markdown files under `samples/` directory.
* We create __changelog__ files for Package Manager updates. They are under `messages/` directory.

    If your contribution deserves a place under one of the "Bug Fixes", "New Features", "Changes" titles, please do so. Preferably in the same commit with your modifications.

    You have to edit the changelog that belongs to the branch you've worked on. For example, if you have based your commits on branch `v2.0.4`, then the changelog you should edit is `messages/2.0.4.md`.

    If the changelog file doesn't exist yet, create it by copying `messages/template.md` as a template.

* If you are defining a new __key binding__, please define for all the 3 OSs in their own `.sublime-keymap` files. You have to insert your edits into the exacly same place in the 3 files.
* If you want to introduce a new setting key for one of the `.sublime-settings` files, use `mde.` prefix in your setting key.
* For testing your changes, you can use the test files under `tests/`. You can extend those files to add new tests and edge cases.

## Publishing to Package Control

When the milestone is completed, push-access users can publish the new version to the Package Control. Creating the new version __tag__ on the GitHub repository is enough for this. For example, if the name of the milestone is `v2.0.4`, the tag should be `2.0.4`.

The update process may take __up to an hour__ depending on the crawl frequency by the Package Control.

# MarkdownEditing 2.0.1 Changelog

Your _MarkdownEditing_ plugin is updated. Enjoy new version. For any type of feedback you can use [GitHub issues][issues].

## Bug Fixes

* When autocomplete popup is visible, pressing <kbd>Enter</kbd> now always inserts the autocomplete item. Previously, if the cursor was for example in a list, <kbd>Enter</kbd> was inserting new list item ignoring the active autocomplete popup.
* Symbols in fenced code blocks will no longer be displayed in the symbol list (<kbd>Ctrl</kbd> <kbd>R</kbd>).

## New Features

* Markdown headers will be displayed in the Project Symbol List (<kbd>Ctrl</kbd> <kbd>Shift</kbd> <kbd>R</kbd>). They will start with `#`, so you will know they belong to markdown files at a glance. Also they will be on top of the list because of the presedence of `#`.
* Bold and italic markers are configurable now. Follow `Preferences > Package Settings > Markdown Editing` in the menu. See README for further help.

[issues]: https://github.com/SublimeText-Markdown/MarkdownEditing/issues

# MarkdownEditing 2.0.2 Changelog

Your _MarkdownEditing_ plugin is updated. Enjoy new version. For any type of feedback you can use [GitHub issues][issues].

## New Features

* Ctrl B / Ctrl I will unbold/unitalicize selected text if it is already bold/italic.

## Changes

* Some setting keys are changed to achieve better consistency. If you used any of these in your user setting files, please reflect these changes to those files. Changes:
    - `match_header_hashes`       -> `mde_match_header_hashes`
    - `me_keep_centered`          -> `mde_keep_centered`
    - `me_distraction_free_mode`  -> `mde_distraction_free_mode`

[issues]: https://github.com/SublimeText-Markdown/MarkdownEditing/issues

# MarkdownEditing 2.0.3 Changelog

Your _MarkdownEditing_ plugin is updated. Enjoy new version. For any type of feedback you can use [GitHub issues][issues].

## Bug Fixes

* "Gather missing links" feature was broken in ST3, fixed.
* "Fix Underlined Headers" feature was broken in ST3, fixed (thanks to @gillibrand).
* A bug related to link definitions was causing CPU hanging. Fixed. ([#111][], [#112][])
* Link and email address detection improved.

## New Features

* "Gather missing links" feature is now more intelligent
* Command palette items improved
* Ctrl+B and Ctrl+I will unbold / unitalicize words without selecting them if they are already bold / italic. Cursor can be at anywhere of the word. Applies only to `_` style bolds / italics. This is because ST word selection (Ctrl+D) doesn't include `*` characters.
* Link and email address highlighting improved.

## Changes

* These files are renamed. If you have used them in your user keymap file, you need to update them.
    - `macros/Make Word Bold.sublime-macro`     -> `macros/Transform Word - Bold.sublime-macro`
    - `macros/Make Word Italic.sublime-macro`   -> `macros/Transform Word - Italic.sublime-macro`

[issues]: https://github.com/SublimeText-Markdown/MarkdownEditing/issues
[#111]: https://github.com/SublimeText-Markdown/MarkdownEditing/issues/111
[#112]: https://github.com/SublimeText-Markdown/MarkdownEditing/issues/112

# MarkdownEditing 2.0.4 Changelog

Your _MarkdownEditing_ plugin is updated. Enjoy new version. For any type of feedback you can use [GitHub issues][issues].

## Bug Fixes

* Separator (`***`) background color was almost unnoticeable, fixed for all the themes.
* Space obligation after `>` in blockquotes was removed in previous versions but it didn't apply to the _standard_ Markdown syntax file. Fixed.
* `<`, `>` and `&` characters inside list paragraphs was highlighted as error for _standard_ Markdown syntax. Fixed.
* Auto increment on ordered lists was broken for multi-digit numbers. Fixed. ([#109][])

## New Features

* Caret is now taller similar to iA Writer. Works in only Sublime Text Build 3057 and above.
* MultiMarkdown now imports GitHub flavored markdown instead of standard markdown.
    _Description_: MultiMarkdown syntax file doesn't redefine things, instead imports standard markdown syntax file. With this version, it imports GFM syntax file instead of the standard one. GFM has had many bug fixes and improvements over the standard md syntax file. So hopefully, MultiMarkdown users will benefit from this improvements, too.
* Pressing `Tab` on a blank list item now indents it. `Shift Tab` will do the reverse. List bullet will change on each indent/unindent. You can disable automatic list bullet switching or choose which bullets to be used, in your settings file.

## Changes

* Highlighting current line is now disabled. If you want it back, you can do so in your user settings file which is one of:
    - `Packages/User/Markdown.sublime-settings`
    - `Packages/User/Markdown (Standard).sublime-settings`
    - `Packages/User/MultiMarkdown.sublime-settings`

    If you are using the (immature) focus theme, you have to reenable "highlighting current line"; because, the focus theme relies on that. On the other hand, the theme is likely to be changed a lot or totally replaced by a new feature in the future.

* Listening to _too many_ users, now the plugin doesn't set any font settings in the defaults. If you were happy with the default font settings, you may want to re-add them to your user settings file of your favorite flavor which is one of:
    - `Packages/User/Markdown.sublime-settings`
    - `Packages/User/Markdown (Standard).sublime-settings`
    - `Packages/User/MultiMarkdown.sublime-settings`

    The font settings for Mac/Linux were:

        "font_face": "Menlo",
        "font_options": [ "subpixel_antialias", "no_round", "directwrite"],

    and for Windows:

        "font_face": "Consolas"

* Bold and italic key bindings (`Ctrl B/I`) are changed due to some conflicts that are reported several time by users. They are now:
    - `Super Alt B/I` for OS X
    - `Ctrl Shift B/I` for Windows/Linux

* Setting keys changed. If you used any of these in your user setting files, please reflect these changes to those files. Changes:
    * `mde_match_header_hashes`       -> `mde.match_header_hashes`
    * `mde_keep_centered`             -> `mde.keep_centered`
    * `mde_distraction_free_mode`     -> `mde.distraction_free_mode`

[issues]: https://github.com/SublimeText-Markdown/MarkdownEditing/issues
[#109]: https://github.com/SublimeText-Markdown/MarkdownEditing/issues/109

# MarkdownEditing 2.0.5 Changelog

Your _MarkdownEditing_ plugin is updated. Enjoy new version. For any type of feedback you can use [GitHub issues][issues].

## Bug Fixes

* Fix a bug where `Enter` didn't remove _empty_ ordered list item if it's multi-digit.
* Some keybindings (`*`, `_` etc) were irrelevant for code blocks/spans. They are now disabled for inline and block code.
* Headers that don't have a blank line before and after didn't appear in the symbol list. Fixed.
* Improved blockquote extending (via `Enter`): Multilevel blockquotes weren't supported, now they are.

## New Features

* At the end of a blockquote line, pressing `Enter` will now automatically extend blockquote.
* If a list item contains a [GFM task][], pressing `Enter` at the end of the line will continue with a new blank task.
* Selecting some text and pressing `>` will now convert it to blockquote. The first and the last line don't have to be fully selected; partial select works, too.
* Fenced code blocks now support C# highlighting (identifier may be one of `cs`, `csharp`, `c#` as in GFM).

## News

* We have started a [Tips wiki][tips] for the userbase to share experiences with each other.

[issues]: https://github.com/SublimeText-Markdown/MarkdownEditing/issues
[GFM task]: https://help.github.com/articles/github-flavored-markdown
[tips]: https://github.com/SublimeText-Markdown/MarkdownEditing/wiki/Tips

# MarkdownEditing 2.0.6 Changelog

Your _MarkdownEditing_ plugin is updated. Enjoy new version. For any type of feedback you can use [GitHub issues][issues].

## Bug Fixes

* Header indentation in the symbol list was broken. Fixed. (Fixes [#150][])

[issues]: https://github.com/SublimeText-Markdown/MarkdownEditing/issues
[#150]: https://github.com/SublimeText-Markdown/MarkdownEditing/issues/150

# MarkdownEditing 2.0.7 Changelog

Your _MarkdownEditing_ plugin is updated. Enjoy new version. For any type of feedback you can use [GitHub issues][issues].

## Bug Fixes

* Fix a Unix bug where pressing `Enter` twice in a list didn't work as expected ([#154][]).

## New Features

* Fenced code blocks now supports Lisp. Fixes #156
* Added command for command palette to convert underlined (SETEXT) headers to hashed (ATX) headers. Works for both selections and whole document.

[issues]: https://github.com/SublimeText-Markdown/MarkdownEditing/issues
[#154]: https://github.com/SublimeText-Markdown/MarkdownEditing/issues/154

# MarkdownEditing 2.0.8 Changelog

Your _MarkdownEditing_ plugin is updated. Enjoy new version. For any type of feedback you can use [GitHub issues][issues].

## Bug Fixes

* Autopairing `*` had a small bug, fixed. Thanks to [@bordaigorl][] (Fixes [#166][])
* In lists, only the first list bullet was given a scope. This caused to break custom color schemes. Fixed by [@bordaigorl][].

## New Features

* Block codes are now highlighted in Monokai color scheme. You can see screenshots [here][#168]

[issues]: https://github.com/SublimeText-Markdown/MarkdownEditing/issues
[#166]: https://github.com/SublimeText-Markdown/MarkdownEditing/pull/166
[#168]: https://github.com/SublimeText-Markdown/MarkdownEditing/pull/168
[@bordaigorl]: https://github.com/bordaigorl

# MarkdownEditing 2.0.9 Changelog

Your _MarkdownEditing_ plugin is updated. Enjoy new version. For any type of feedback you can use [GitHub issues][issues].

## Changes

* Listening to the users, Monokai color scheme inside code blocks reverted back. If you have things to say, discussion is [here][#168].

[issues]: https://github.com/SublimeText-Markdown/MarkdownEditing/issues
[#168]: https://github.com/SublimeText-Markdown/MarkdownEditing/pull/168

# MarkdownEditing {version} Changelog

Your _MarkdownEditing_ plugin is updated. Enjoy new version. For any type of feedback you can use [GitHub issues][issues].

## Bug Fixes

## New Features

## Changes

[issues]: https://github.com/SublimeText-Markdown/MarkdownEditing/issues

# MarkdownEditing

Markdown plugin for Sublime Text. Provides a decent Markdown color scheme (light and dark) with more __robust__ syntax highlighting and useful Markdown editing features for Sublime Text. 3 flavors are supported: Standard Markdown, __GitHub flavored Markdown__, MultiMarkdown.

![MarkdownEditing][github]

[Dark][github 2] and [yellow][github 3] theme available.

> Your kind donations will help [me](https://github.com/maliayas) pause my daily job and put more serious effort into the development of this plugin for the next 2 milestones ([2.0.5](https://github.com/SublimeText-Markdown/MarkdownEditing/issues?milestone=1&state=open) and [2.2.0](https://github.com/SublimeText-Markdown/MarkdownEditing/issues?milestone=2&state=open)). When they are completed, donation button will be removed. Thanks.
> 
> <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_donations&amp;business=W2NXRPD43YSCU&amp;lc=TR&amp;item_name=Ali%20Ayas&amp;item_number=Open%20Source&amp;currency_code=USD&amp;bn=PP%2dDonationsBF%3abtn_donate_SM%2egif%3aNonHosted"><img src="https://www.paypalobjects.com/en_US/i/btn/btn_donate_SM.gif" alt="[paypal]" /></a>

## Overview

* [Features](#features)
* [Key Bindings](#key-bindings)
* [GFM Specific Features](#gfm-specific-features)
* [Commands for Command Palette](#commands-for-command-palette)
* [Installation](#installation)
* [Configuration](#configuration)
* [Tips](#tips)
* [Similar Plugins](#similar-plugins)
* [Known Bugs](#known-bugs)
* [Contributing](#contributing)
* [Credits](#credits)
* [License](#license)

## Features

* Asterisks and underscores are autopaired and will wrap selected text
    - If you start an empty pair and hit backspace, both elements are deleted
    - If you start an empty pair and hit space, the right element is deleted
* Backticks are paired
* At the end of a list item, pressing <kbd>Enter</kbd> will automatically insert the new list item bullet.
    - Pressing <kbd>Tab</kbd> on the blank list item will indent it and switch the list bullet to another one (Order is `*`, `-`, `+` in a cycle).
    - Pressing <kbd>Shift</kbd> <kbd>Tab</kbd> on the blank list item will unindent it in the same way as above.
    - Sequential <kbd>Tab</kbd> s or <kbd>Shift</kbd> <kbd>Tab</kbd> s are supported.
    - You can disable automatic bullet switching or choose which bullets to be used, in your settings file.
    - If a list item contains a [GFM task][GFM], pressing <kbd>Enter</kbd> at the end of the line will continue with a new blank task.
* At the end of a blockquote line, pressing <kbd>Enter</kbd> will automatically extend blockquote.
* Selecting some text and pressing <kbd>&gt;</kbd> will convert it to blockquote. The first and the last line don't have to be fully selected; partial select works, too.
* Left bracket pairing is modified to eliminate the selection and leave the cursor at a point where you can insert a `[]` or `()` pair for a link
* Displays Markdown headers in the Project Symbol List (<kbd>Ctrl</kbd> <kbd>Shift</kbd> <kbd>R</kbd>). They will start with `#`, so you will know they belong to markdown files at a glance. Also they will be on top of the list because of the presedence of `#`.
* <kbd>~</kbd> wraps selected text with `~~` (strikethrough).
* Typing `#` when there's a selection will surround it with `#` to make it a headline. Multiple presses add additional hashes, increasing the level of the header. Once you hit 6 hashes, it will reset to 0 on the next press. The `mde.match_header_hashes` will determine if the `#` are mirrored on both sides or just at the beginning of the line.
* Typing return at the end of a line that begins with hashmarks will insert closing hashmarks on the headline. They're not required for Markdown, it's just aesthetics, and you can change the `mde.match_header_hashes` option in your settings to disable.
* Setext-style headers can be completed with `Tab`. That is, typing `Tab` on a line containing only `=` or `-` characters will add or remove enough characters to it to match the length of the line above.

## Key Bindings

| OS X | Windows/Linux | Description |
|------|---------------|-------------|
| <kbd>⌘</kbd><kbd>⌥</kbd><kbd>V</kbd> | <kbd>Ctrl</kbd><kbd>Win</kbd><kbd>V</kbd> | Pastes the contents of the clipboard as an inline link on selected text.
| <kbd>⌘</kbd><kbd>⌥</kbd><kbd>R</kbd> | <kbd>Ctrl</kbd><kbd>Win</kbd><kbd>R</kbd> | Pastes the contents of the clipboard as a reference link.
| <kbd>⌘</kbd><kbd>⌥</kbd><kbd>K</kbd> | <kbd>Ctrl</kbd><kbd>Win</kbd><kbd>K</kbd> | Inserts a standard inline link.
| <kbd>⌘</kbd><kbd>⇧</kbd><kbd>K</kbd> | <kbd>Shift</kbd><kbd>Win</kbd><kbd>K</kbd> | Inserts an inline image.
| <kbd>⌘</kbd><kbd>⌥</kbd><kbd>B</kbd> <kbd>⌘</kbd><kbd>⌥</kbd><kbd>I</kbd> | <kbd>Ctrl</kbd><kbd>Shift</kbd><kbd>B</kbd> <kbd>Ctrl</kbd><kbd>Shift</kbd><kbd>I</kbd> | These are bound to bold and italic. They work both with and without selections. If there is no selection, they will just transform the word under the cursor. These keybindings will unbold/unitalicize selection if it is already bold/italic.
| <kbd>⌘</kbd><kbd>^</kbd><kbd>1...6</kbd> | <kbd>Ctrl</kbd><kbd>1...6</kbd> | These will add the corresponding number of hashmarks for headlines. Works on blank lines and selected text in tandem with the above headline tools. If you select an entire existing headline, the current hashmarks will be removed and replaced with the header level you requested. This command respects the `mde.match_header_hashes` preference setting.
| <kbd>⌘</kbd><kbd>⇧</kbd><kbd>6</kbd> | <kbd>Ctrl</kbd><kbd>⇧</kbd><kbd>6</kbd> | Inserts a footnote and jump to its definition. If your cursor is in a definition, it will jump back to the marker.
| <kbd>⌥</kbd><kbd>⇧</kbd><kbd>F</kbd> | <kbd>Alt</kbd><kbd>Shift</kbd><kbd>F</kbd> | Locates footnote markers without definitions and inserts their markers for the definition.
| <kbd>⌥</kbd><kbd>⇧</kbd><kbd>G</kbd> | <kbd>Alt</kbd><kbd>Shift</kbd><kbd>G</kbd> | Locates link references without definitions and inserts their labels at the bottom for the definition.

## GFM Specific Features

Underscores in words doesn't mess with bold or italic style:

![underscore-in-words][github 5]

Fenced code blocks gets syntax highlighting inside:

![fenced-code-block][github 6]

Keyboard shortcuts gets highlighted like in GitHub:

![keyboard-shortcut][github 7]

Strikethrough is supported:

![strikethrough][github 8]

## Commands for Command Palette

### Fix Underlined Headers

Adjusts every setext-style header to add or remove `=` or `-` characters as needed to match the lengths of their header text.

### Convert Underlined Headers to ATX

Converts every setext-style header into an ATX style header. If something is selected only the headers in the selections will be converted, otherwise the conversion will be applied to the whole view.

### Add Missing Link Labels

Scans document for referenced link usages (`[some link][some_ref]` and `[some link][]`) and checks if they are all defined. If there are undefined link references, command will automatically create their definition snippet at the bottom of the file.

## Installation

_Note_: Sublime text has a native tiny package for Markdown. However, when MarkdownEditing is enabled, native package causes some conflicts. For this reason, MarkdownEditing will automatically disable it. Since it doesn't bring anything new over MarkdownEditing, this is not a loss. But remember, when you disable MarkdownEditing, you have to reenable the native one manually (if you want).

If you are using Sublime Text 2, you have to disable the native package _manually_. To do that, add `Markdown` to your `ignored_packages` list in ST user settings:

    "ignored_packages": [..., "Markdown"],

### [Package Control][wbond]

The preferred method of installation is via [Sublime Package Control][wbond].

1. [Install Sublime Package Control][wbond 2]
2. From inside Sublime Text, open Package Control's Command Pallet: <kbd>CTRL</kbd> <kbd>SHIFT</kbd> <kbd>P</kbd> (Windows, Linux) or <kbd>CMD</kbd> <kbd>SHIFT</kbd> <kbd>P</kbd> on Mac.
3. Type `install package` and hit Return. A list of available packages will be displayed.
4. Type `MarkdownEditing` and hit Return. The package will be downloaded to the appropriate directory.
5. Restart Sublime Text to complete installation. Open a Markdown file and this custom theme. The features listed above should now be available.

### Manual Installation

1. Download or clone this repository to a directory `MarkdownEditing` in the Sublime Text Packages directory for your platform:
    * Mac: `git clone https://github.com/SublimeText-Markdown/MarkdownEditing.git ~/Library/Application\ Support/Sublime\ Text\ 2/Packages/MarkdownEditing`
    * Windows: `git clone https://github.com/SublimeText-Markdown/MarkdownEditing.git %APPDATA%\Sublime/ Text/ 2/\MarkdownEditing`
    * Linux: `git clone https://github.com/SublimeText-Markdown/MarkdownEditing.git ~/.Sublime\ Text\ 2/Packages/MarkdownEditing`
2. Restart Sublime Text to complete installation. Open a Markdown file and this custom theme. The features listed above should now be available.

## Configuration

The plugin contains 3 different Markdown flavors: Standard Markdown, GitHub flavored Markdown, MultiMarkdown. Default is GitHub flavored Markdown. If you want to set another one as default, open a Markdown file and select your flavor from the menu: `View > Syntax > Open all with current extension as`. You're done.

You may want to have a look at the default settings files. They are located at:

    Packages/MarkdownEditing/Markdown.sublime-settings         [GitHub flavored Markdown]
    Packages/MarkdownEditing/Markdown (Standard).sublime-settings
    Packages/MarkdownEditing/MultiMarkdown.sublime-settings

If you want to override any of the default settings, you can open the appropriate user settings file using the `Preferences > Package Settings > Markdown Editing` menu. Each flavor has a different settings file.

Bold and italic markers are configurable through ST shell variables. You can use `Preferences > Package Settings > Markdown Editing` menu to see the default settings file. In order to override it, copy & paste its content into the user settings file (`Packages/User/Bold and Italic Markers.tmPreferences`) from the menu and make your edits. It is pretty straightforward.

In order to activate the dark or the yellow theme, put one of these lines to your user settings file of the flavor (`Packages/User/[flavor].sublime-settings`):

    "color_scheme": "Packages/MarkdownEditing/MarkdownEditor-Dark.tmTheme",
    "color_scheme": "Packages/MarkdownEditing/MarkdownEditor-Yellow.tmTheme",

If you want to go with your already existing theme, you can reenable it with the same method as above. Keep in mind that, that theme may not cover all the parts of the Markdown syntax that this plugin defines.

By default, when you install the plugin, files with these extensions will be assigned to Markdown syntax: "md", "txt", "mdown", "markdown", "markdn". If you want to prevent any of these extensions to be opened as Markdown, follow these steps:

1. Click on the language menu at bottom right
2. Select "Open all with current extension as"
3. Choose your preferred syntax for that extension

## Tips

We are maintaining a [tips section][tips] in our [Wiki][]. Jump there to learn from others or share your experiences with others.

## Similar Plugins

* [Knockdown][]

     Knockdown offers useful Markdown features and a custom Markdown theme. All of its unique features except its theme are ported to MarkdownEditing and some of them are actually improved further in MarkdownEditing.
* [Sublime Markdown Extended][]
* [SmartMarkdown][]

## Known Bugs

* Setext-style headers (`===` and `---`) do not show up in the symbol list. This is due to a Sublime Text limitation (see [#158][]). However, we are able to put a placeholder to indicate the existence of the header. We encourage you to use Atx-style headers (`#`).

## Contributing

See `CONTRIBUTING.md` file.

## Credits

MarkdownEditing was originally created by [Brett Terpstra][brettterpstra] and has become a community project with the goal of consolidating the best features from the varied collection of Markdown packages for Sublime Text. Current development is headed up by [Ali Ayas][github 9].

Related blog posts from Brett:
* http://brettterpstra.com/2012/05/17/markdown-editing-for-sublime-text-2-humble-beginnings/
* http://brettterpstra.com/2013/11/23/markdownediting-for-sublime-text-updates/

This plugin contains portions of code from [Knockdown][].

Footnote commands were submitted by [J. Nicholas Geist][github 4] and originated at [geekabouttown][geekabouttown].

## License

MarkdownEditing is released under the [MIT License][opensource].

[TableEditor]:                 https://github.com/vkocubinsky/SublimeTableEditor
[Knockdown]:                   https://github.com/aziz/knockdown/
[Sublime Markdown Extended]:   https://github.com/jonschlinkert/sublime-markdown-extended
[SmartMarkdown]:               https://github.com/demon386/SmartMarkdown
[Typewriter]:                  https://github.com/alehandrof/Typewriter
[OpenUrl]:                     https://github.com/noahcoad/open-url
[brettterpstra]: http://brettterpstra.com
[geekabouttown]: http://geekabouttown.com/posts/sublime-text-2-markdown-footnote-goodness
[github]: https://raw.github.com/SublimeText-Markdown/MarkdownEditing/master/screenshots/light.png
[github 2]: https://raw.github.com/SublimeText-Markdown/MarkdownEditing/master/screenshots/dark.png
[github 3]: https://raw.github.com/SublimeText-Markdown/MarkdownEditing/master/screenshots/yellow.png
[github 4]: https://github.com/jngeist
[github 5]: https://raw.github.com/SublimeText-Markdown/MarkdownEditing/master/screenshots/underscore-in-words.png
[github 6]: https://raw.github.com/SublimeText-Markdown/MarkdownEditing/master/screenshots/fenced-code-block.png
[github 7]: https://raw.github.com/SublimeText-Markdown/MarkdownEditing/master/screenshots/keyboard-shortcut.png
[github 8]: https://raw.github.com/SublimeText-Markdown/MarkdownEditing/master/screenshots/strikethrough.png
[github 9]: https://github.com/maliayas
[opensource]: http://www.opensource.org/licenses/MIT
[wbond]: http://wbond.net/sublime_packages/package_control
[wbond 2]: http://wbond.net/sublime_packages/package_control/installation
[FullScreenStatus]: https://github.com/maliayas/SublimeText_FullScreenStatus
[macstories]: http://www.macstories.net/roundups/sublime-text-2-and-markdown-tips-tricks-and-links/
[tips]: https://github.com/SublimeText-Markdown/MarkdownEditing/wiki/Tips
[Wiki]: https://github.com/SublimeText-Markdown/MarkdownEditing/wiki
[GFM]: https://help.github.com/articles/github-flavored-markdown
[#158]: https://github.com/SublimeText-Markdown/MarkdownEditing/issues/158

```html
<!DOCTYPE html>
<html lang="en">
<head
    <meta charset="utf-8">
    <title>Hi!</title>
</head>
<body>
    <p>Hello, world!</p>
</body>
</html>
```

~~~~~~java
public class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Hello, world!");
    }
}
~~~~~~

```sql
SELECT "Hello, world!" FROM DUMMY -- Hello, world!
SELECT "Hello, world!" FROM DUMMY -- Hello, world!
```

# Sample Markdown

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.

## Text basics

This is *italic* and this is **bold**.  Another _italic_ and another __bold__.

> __Here is some quotation__. Lorem ipsum dolor sit amet, consectetur
> adipisicing elit, sed do eiusmod tempor incididunt ut labore et
> dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.

## Links

This is an [example inline link](http://example.com/) and [another one with a title](http://example.com/ "Hello, world"). And [another][someref] one.

## Code

This is inline code: `some code here`.

    <script>
        document.location = 'http://example.com/?q=markdown+cheat+sheet';
    </script>

```java
public class HelloWorld {
   public static void main(String[] args) {
       System.out.println("Hello, world!");
   }
}
```

[someref]: http://example.com "rich web apps"
[MarkdownREF]: http://daringfireball.net/projects/markdown/basics
[gfm]: http://github.github.com/github-flavored-markdown/

Markdown files in this folder can be used for tasks like:

* Taking screenshots after a syntax highlighting update in the plugin,
* Checking if the plugin works legitimately in general.

# Strikethrough

~~This text should be parsed as _strikethroughed_.~~

~~There may be __bold__ or _italic_ text inside strikethroughed text.~~

~~There may be a keyboard shortcut like <kbd>Enter</kbd> inside strikethroughed text.~~

__There may be ~~strikethroughed text~~ inside bold text.__
_There may be ~~strikethroughed text~~ inside italic text._

~~ If there is a space in the beginning or end, it won't work as per the [GFM][GFM] docs ~~

~~Strikethrough can be applied to
multiple lines. Just keep in mind
not to put any space in the beginning or end.~~

# Underscore In Words

The word `complicated` must be neither bold nor italic below:

perform_complicated_task
perform__complicated__task

But the first part below is italic and bold respectively:

_perform_complicated_task
__perform__complicated__task

# Keyboard Shortcuts

Keyboard shortcuts below should be highlighted:

---

A keyboard shortcut <kbd>Enter</kbd> can be in paragraph.

* A keyboard shortcut <kbd>Enter</kbd> can be in list.

_A keyboard shortcut <kbd>Enter</kbd> can be in italic._
__A keyboard shortcut <kbd>Enter</kbd> can be in bold.__

~~A keyboard shortcut <kbd>Enter</kbd> can be in deleted text.~~

<p>A keyboard shortcut <kbd>Enter</kbd> can be in HTML.</p>

<div>
    A keyboard shortcut <kbd>Enter</kbd> can be in block level tags.
</div>

# Fenced Code Blocks

## In / Near List Items

Below fenced code blocks _should_ be highlighted.

---

* List item

    ```js
    for (var i = 0; i < 10; i++) {
        console.log(i);
    }
    ```

* List item

```js
for (var i = 0; i < 10; i++) {
    console.log(i);
}
```

---

Below are _not_ valid fenced code blocks according to the [GFM docs][GFM]. It says there must be a blank line before the code block. However, GitHub highlights them. So, they _should_ be highlighted.

---

* List item
    ```js
    for (var i = 0; i < 10; i++) {
        console.log(i);
    }
    ```

* List item
```js
for (var i = 0; i < 10; i++) {
    console.log(i);
}
```

## In / Near Paragraphs

Below is _not_ a _fenced_ code block, just a normal code block.

---

Paragraph

    ```js
    for (var i = 0; i < 10; i++) {
        console.log(i);
    }
    ```

---

Below 2 blocks are fenced code blocks. They _should_ be highlighted.

---

Paragraph

```js
for (var i = 0; i < 10; i++) {
    console.log(i);
}
```

Paragraph
```js
for (var i = 0; i < 10; i++) {
    console.log(i);
}
```

---

Below is not any type of code block. It _should not_ be highlighted.

---

Paragraph
    ```js
    for (var i = 0; i < 10; i++) {
        console.log(i);
    }
    ```

[GFM]: https://help.github.com/articles/github-flavored-markdown

Source: https://github.com/textmate/markdown.tmbundle/blob/master/Tests/test-minimal.markdown

Inline styles
===============

_italic_

_italic one_ not italic _italic two_

_italic\__

_italic \__

stuff * not italic*

*italic__*

_all _ italic_

_italic
end italic_

\\\\_italic\\_

\\\\_italic\\\_\\\\_

\\\\_italic\\_

\_ not italic _

_not italic _

\\\\_not italic\_

_not italic \_

\\\_not italic\\_

_not italic

not end italic_

__bold__

**bold\***

`raw more`

``dobule ` raw``

`raw \` more`

Headings
================

heading 2
----------

## heading 2

### heading 3

###### heading 6

Horizontal lines
=================

***

* * *

___

__ __ __

- - - 

----------------


Block formatting
================

Lists
----------------

 * This *is a list!*
 * This is another list item.
   But this one spans *two* lines. 
 * Another list item with __inline__ formatting
 * This one is tricky  
 * *This is a list*

   Because this should still be a list item.

1. This is a list item too
2. This list is numbered

1986\. This shouldn't be a list.

Code block
---------------

	asdfsdafasdf
	This is code.
	Isn't it pretty!

Quotes
---------------

> Here is a quote block
This quote continues on.  Line breaking is OK in markdown
> Here it is again
> Lah-di-dah
> I should really match headings in here too:
> ## This is a heading in a block quote

These are some example files to test if the syntax highlighting works correctly. They include some edge cases, too.


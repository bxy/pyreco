__FILENAME__ = agents
AGENTS = [
            "Avant Browser/1.2.789rel1 (http://www.avantbrowser.com)",
            "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/532.5 (KHTML, like Gecko) Chrome/4.0.249.0 Safari/532.5",
            "Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/532.9 (KHTML, like Gecko) Chrome/5.0.310.0 Safari/532.9",
            "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.7 (KHTML, like Gecko) Chrome/7.0.514.0 Safari/534.7",
            "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/534.14 (KHTML, like Gecko) Chrome/9.0.601.0 Safari/534.14",
            "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.14 (KHTML, like Gecko) Chrome/10.0.601.0 Safari/534.14",
            "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.20 (KHTML, like Gecko) Chrome/11.0.672.2 Safari/534.20",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/534.27 (KHTML, like Gecko) Chrome/12.0.712.0 Safari/534.27",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.24 Safari/535.1",
            "Mozilla/5.0 (Windows NT 6.0) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.120 Safari/535.2",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.36 Safari/535.7",
            "Mozilla/5.0 (Windows; U; Windows NT 6.0 x64; en-US; rv:1.9pre) Gecko/2008072421 Minefield/3.0.2pre",
            "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.10) Gecko/2009042316 Firefox/3.0.10",
            "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-GB; rv:1.9.0.11) Gecko/2009060215 Firefox/3.0.11 (.NET CLR 3.5.30729)",
            "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6 GTB5",
            "Mozilla/5.0 (Windows; U; Windows NT 5.1; tr; rv:1.9.2.8) Gecko/20100722 Firefox/3.6.8 ( .NET CLR 3.5.30729; .NET4.0E)",
            "Mozilla/5.0 (Windows NT 6.1; rv:2.0.1) Gecko/20100101 Firefox/4.0.1",
            "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:2.0.1) Gecko/20100101 Firefox/4.0.1",
            "Mozilla/5.0 (Windows NT 5.1; rv:5.0) Gecko/20100101 Firefox/5.0",
            "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:6.0a2) Gecko/20110622 Firefox/6.0a2",
            "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:7.0.1) Gecko/20100101 Firefox/7.0.1",
            "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:2.0b4pre) Gecko/20100815 Minefield/4.0b4pre",
            "Mozilla/4.0 (compatible; MSIE 5.5; Windows NT 5.0 )",
            "Mozilla/4.0 (compatible; MSIE 5.5; Windows 98; Win 9x 4.90)",
            "Mozilla/5.0 (Windows; U; Windows XP) Gecko MultiZilla/1.6.1.0a",
            "Mozilla/2.02E (Win95; U)",
            "Mozilla/3.01Gold (Win95; I)",
            "Mozilla/4.8 [en] (Windows NT 5.1; U)",
            "Mozilla/5.0 (Windows; U; Win98; en-US; rv:1.4) Gecko Netscape/7.1 (ax)",
            "Opera/7.50 (Windows XP; U)",
            "Opera/7.50 (Windows ME; U) [en]",
            "Opera/7.51 (Windows NT 5.1; U) [en]",
            "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0; en) Opera 8.0",
            "Mozilla/5.0 (Windows; U; WinNT4.0; en-US; rv:1.2b) Gecko/20021001 Phoenix/0.2",
            "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.23) Gecko/20090825 SeaMonkey/1.1.18",
            "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/527  (KHTML, like Gecko, Safari/419.3) Arora/0.6 (Change: )",
            "Mozilla/5.0 (Windows; U; ; en-NZ) AppleWebKit/527  (KHTML, like Gecko, Safari/419.3) Arora/0.8.0",
            "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Avant Browser; Avant Browser; .NET CLR 1.0.3705; .NET CLR 1.1.4322; Media Center PC 4.0; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.8 (KHTML, like Gecko) Beamrise/17.2.0.9 Chrome/17.0.939.0 Safari/535.8",
            "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/18.6.872.0 Safari/535.2 UNTRUSTED/1.0 3gpp-gba UNTRUSTED/1.0",
            "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1061.1 Safari/536.3",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.6 (KHTML, like Gecko) Chrome/20.0.1092.0 Safari/536.6",
            "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.6 (KHTML, like Gecko) Chrome/20.0.1090.0 Safari/536.6",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/22.0.1207.1 Safari/537.1",
            "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:10.0.1) Gecko/20100101 Firefox/10.0.1",
            "Mozilla/5.0 (Windows NT 6.1; rv:12.0) Gecko/20120403211507 Firefox/12.0",
            "Mozilla/5.0 (Windows NT 6.0; rv:14.0) Gecko/20100101 Firefox/14.0.1",
            "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:15.0) Gecko/20120427 Firefox/15.0a1",
            "Mozilla/5.0 (Windows NT 6.2; Win64; x64; rv:16.0) Gecko/16.0 Firefox/16.0",
            "iTunes/9.0.2 (Windows; N)",
            "Mozilla/5.0 (compatible; Konqueror/4.5; Windows) KHTML/4.5.4 (like Gecko)",
            "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/533.1 (KHTML, like Gecko) Maxthon/3.0.8.2 Safari/533.1",
            "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)",
            "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0)",
            "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0)",
            "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; Trident/4.0)",
            "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident/4.0)",
            "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; Trident/5.0)",
            "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)",
            "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.2; Trident/5.0)",
            "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.2; WOW64; Trident/5.0)",
            "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)",
            "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; Trident/6.0)",
            "Mozilla/5.0 (compatible; MSIE 10.6; Windows NT 6.1; Trident/5.0; InfoPath.2; SLCC1; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; .NET CLR 2.0.50727) 3gpp-gba UNTRUSTED/1.0",
            "Opera/9.25 (Windows NT 6.0; U; en)",
            "Opera/9.80 (Windows NT 5.2; U; en) Presto/2.2.15 Version/10.10",
            "Opera/9.80 (Windows NT 5.1; U; ru) Presto/2.7.39 Version/11.00",
            "Opera/9.80 (Windows NT 6.1; U; en) Presto/2.7.62 Version/11.01",
            "Opera/9.80 (Windows NT 5.1; U; zh-tw) Presto/2.8.131 Version/11.10",
            "Opera/9.80 (Windows NT 6.1; U; es-ES) Presto/2.9.181 Version/12.00",
            "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/531.21.8 (KHTML, like Gecko) Version/4.0.4 Safari/531.21.10",
            "Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/533.17.8 (KHTML, like Gecko) Version/5.0.1 Safari/533.17.8",
            "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/533.19.4 (KHTML, like Gecko) Version/5.0.2 Safari/533.18.5",
            "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-GB; rv:1.9.1.17) Gecko/20110123 (like Firefox/3.x) SeaMonkey/2.0.12",
            "Mozilla/5.0 (Windows NT 5.2; rv:10.0.1) Gecko/20100101 Firefox/10.0.1 SeaMonkey/2.7.1",
            "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_8; en-US) AppleWebKit/532.8 (KHTML, like Gecko) Chrome/4.0.302.2 Safari/532.8",
            "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_4; en-US) AppleWebKit/534.3 (KHTML, like Gecko) Chrome/6.0.464.0 Safari/534.3",
            "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_5; en-US) AppleWebKit/534.13 (KHTML, like Gecko) Chrome/9.0.597.15 Safari/534.13",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_2) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/14.0.835.186 Safari/535.1",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.54 Safari/535.2",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.36 Safari/535.7",
            "Mozilla/5.0 (Macintosh; U; Mac OS X Mach-O; en-US; rv:2.0a) Gecko/20040614 Firefox/3.0.0 ",
            "Mozilla/5.0 (Macintosh; U; PPC Mac OS X 10.5; en-US; rv:1.9.0.3) Gecko/2008092414 Firefox/3.0.3",
            "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.5; en-US; rv:1.9.1) Gecko/20090624 Firefox/3.5",
            "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.6; en-US; rv:1.9.2.14) Gecko/20110218 AlexaToolbar/alxf-2.0 Firefox/3.6.14",
            "Mozilla/5.0 (Macintosh; U; PPC Mac OS X 10.5; en-US; rv:1.9.2.15) Gecko/20110303 Firefox/3.6.15",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.6; rv:2.0.1) Gecko/20100101 Firefox/4.0.1",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.6; rv:5.0) Gecko/20100101 Firefox/5.0",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.6; rv:9.0) Gecko/20100101 Firefox/9.0",
            "Mozilla/4.0 (compatible; MSIE 5.15; Mac_PowerPC)",
            "Mozilla/5.0 (Macintosh; U; PPC Mac OS X; en-US) AppleWebKit/125.4 (KHTML, like Gecko, Safari) OmniWeb/v563.15",
            "Opera/9.0 (Macintosh; PPC Mac OS X; U; en)",
            "Mozilla/5.0 (Macintosh; U; PPC Mac OS X; en) AppleWebKit/125.2 (KHTML, like Gecko) Safari/85.8",
            "Mozilla/5.0 (Macintosh; U; PPC Mac OS X; en) AppleWebKit/125.2 (KHTML, like Gecko) Safari/125.8",
            "Mozilla/5.0 (Macintosh; U; PPC Mac OS X; fr-fr) AppleWebKit/312.5 (KHTML, like Gecko) Safari/312.3",
            "Mozilla/5.0 (Macintosh; U; PPC Mac OS X; en) AppleWebKit/418.8 (KHTML, like Gecko) Safari/419.3",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.6; rv:2.0.1) Gecko/20100101 Firefox/4.0.1 Camino/2.2.1",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.6; rv:2.0b6pre) Gecko/20100907 Firefox/4.0b6pre Camino/2.2a1pre",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_0) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1063.0 Safari/536.3",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.4 (KHTML like Gecko) Chrome/22.0.1229.79 Safari/537.4",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_2; rv:10.0.1) Gecko/20100101 Firefox/10.0.1",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:16.0) Gecko/20120813 Firefox/16.0",
            "iTunes/4.2 (Macintosh; U; PPC Mac OS X 10.2)",
            "iTunes/9.0.3 (Macintosh; U; Intel Mac OS X 10_6_2; en-ca)",
            "Mozilla/5.0 (Macintosh; U; Intel Mac OS X; en-US) AppleWebKit/528.16 (KHTML, like Gecko, Safari/528.16) OmniWeb/v622.8.0.112941",
            "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_6; en-US) AppleWebKit/528.16 (KHTML, like Gecko, Safari/528.16) OmniWeb/v622.8.0",
            "Opera/9.20 (Macintosh; Intel Mac OS X; U; en)",
            "Opera/9.64 (Macintosh; PPC Mac OS X; U; en) Presto/2.1.1",
            "Opera/9.80 (Macintosh; Intel Mac OS X; U; en) Presto/2.6.30 Version/10.61",
            "Opera/9.80 (Macintosh; Intel Mac OS X 10.4.11; U; en) Presto/2.7.62 Version/11.00",
            "Opera/9.80 (Macintosh; Intel Mac OS X 10.6.8; U; fr) Presto/2.9.168 Version/11.52",
            "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_2; en-us) AppleWebKit/531.21.8 (KHTML, like Gecko) Version/4.0.4 Safari/531.21.10",
            "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_5; de-de) AppleWebKit/534.15  (KHTML, like Gecko) Version/5.0.3 Safari/533.19.4",
            "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_6; en-us) AppleWebKit/533.20.25 (KHTML, like Gecko) Version/5.0.4 Safari/533.20.27",
            "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_7; en-us) AppleWebKit/534.20.8 (KHTML, like Gecko) Version/5.1 Safari/534.20.8",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/534.55.3 (KHTML, like Gecko) Version/5.1.3 Safari/534.53.10",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.5; rv:10.0.1) Gecko/20100101 Firefox/10.0.1 SeaMonkey/2.7.1",
            "ELinks (0.4pre5; Linux 2.6.10-ac7 i686; 80x33)",
            "ELinks/0.9.3 (textmode; Linux 2.6.9-kanotix-8 i686; 127x41)",
            "ELinks/0.12~pre5-4",
            "Links/0.9.1 (Linux 2.4.24; i386;)",
            "Links (2.1pre15; Linux 2.4.26 i686; 158x61)",
            "Links (2.3pre1; Linux 2.6.38-8-generic x86_64; 170x48)",
            "Lynx/2.8.5rel.1 libwww-FM/2.14 SSL-MM/1.4.1 GNUTLS/0.8.12",
            "w3m/0.5.1",
            "Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/532.4 (KHTML, like Gecko) Chrome/4.0.237.0 Safari/532.4 Debian",
            "Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/532.8 (KHTML, like Gecko) Chrome/4.0.277.0 Safari/532.8",
            "Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/532.9 (KHTML, like Gecko) Chrome/5.0.309.0 Safari/532.9",
            "Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.7 (KHTML, like Gecko) Chrome/7.0.514.0 Safari/534.7",
            "Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/540.0 (KHTML, like Gecko) Ubuntu/10.10 Chrome/9.1.0.0 Safari/540.0",
            "Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.15 (KHTML, like Gecko) Chrome/10.0.613.0 Safari/534.15",
            "Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.15 (KHTML, like Gecko) Ubuntu/10.10 Chromium/10.0.613.0 Chrome/10.0.613.0 Safari/534.15",
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/534.24 (KHTML, like Gecko) Ubuntu/10.10 Chromium/12.0.703.0 Chrome/12.0.703.0 Safari/534.24",
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.20 Safari/535.1",
            "Mozilla/5.0 Slackware/13.37 (X11; U; Linux x86_64; en-US) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.41",
            "Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.1 (KHTML, like Gecko) Ubuntu/11.04 Chromium/14.0.825.0 Chrome/14.0.825.0 Safari/535.1",
            "Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.2 (KHTML, like Gecko) Ubuntu/11.10 Chromium/15.0.874.120 Chrome/15.0.874.120 Safari/535.2",
            "Mozilla/5.0 (X11; U; Linux; i686; en-US; rv:1.6) Gecko Epiphany/1.2.5",
            "Mozilla/5.0 (X11; U; Linux i586; en-US; rv:1.7.3) Gecko/20040924 Epiphany/1.4.4 (Ubuntu)",
            "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.6) Gecko/20040614 Firefox/0.8",
            "Mozilla/5.0 (X11; U; Linux x86_64; sv-SE; rv:1.8.1.12) Gecko/20080207 Ubuntu/7.10 (gutsy) Firefox/2.0.0.12",
            "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.11) Gecko/2009060309 Ubuntu/9.10 (karmic) Firefox/3.0.11",
            "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.1.2) Gecko/20090803 Ubuntu/9.04 (jaunty) Shiretoko/3.5.2",
            "Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.1.5) Gecko/20091107 Firefox/3.5.5",
            "Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.1.3) Gecko/20091020 Linux Mint/8 (Helena) Firefox/3.5.3",
            "Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.2.9) Gecko/20100915 Gentoo Firefox/3.6.9",
            "Mozilla/5.0 (X11; U; Linux i686; pl-PL; rv:1.9.0.2) Gecko/20121223 Ubuntu/9.25 (jaunty) Firefox/3.8",
            "Mozilla/5.0 (X11; Linux i686; rv:2.0b6pre) Gecko/20100907 Firefox/4.0b6pre",
            "Mozilla/5.0 (X11; Linux i686 on x86_64; rv:2.0.1) Gecko/20100101 Firefox/4.0.1",
            "Mozilla/5.0 (X11; Linux i686; rv:2.0.1) Gecko/20100101 Firefox/4.0.1",
            "Mozilla/5.0 (X11; Linux x86_64; rv:2.0.1) Gecko/20100101 Firefox/4.0.1",
            "Mozilla/5.0 (X11; Linux x86_64; rv:2.2a1pre) Gecko/20100101 Firefox/4.2a1pre",
            "Mozilla/5.0 (X11; Linux i686; rv:5.0) Gecko/20100101 Firefox/5.0",
            "Mozilla/5.0 (X11; Linux i686; rv:6.0) Gecko/20100101 Firefox/6.0",
            "Mozilla/5.0 (X11; Linux x86_64; rv:7.0a1) Gecko/20110623 Firefox/7.0a1",
            "Mozilla/5.0 (X11; Linux i686; rv:8.0) Gecko/20100101 Firefox/8.0",
            "Mozilla/5.0 (X11; Linux x86_64; rv:10.0.1) Gecko/20100101 Firefox/10.0.1",
            "Mozilla/5.0 (X11; U; Linux; i686; en-US; rv:1.6) Gecko Galeon/1.3.14",
            "Mozilla/5.0 (X11; U; Linux ppc; en-US; rv:1.8.1.13) Gecko/20080313 Iceape/1.1.9 (Debian-1.1.9-5)",
            "Mozilla/5.0 (X11; U; Linux i686; pt-PT; rv:1.9.2.3) Gecko/20100402 Iceweasel/3.6.3 (like Firefox/3.6.3) GTB7.0",
            "Mozilla/5.0 (X11; Linux x86_64; rv:5.0) Gecko/20100101 Firefox/5.0 Iceweasel/5.0",
            "Mozilla/5.0 (X11; Linux i686; rv:6.0a2) Gecko/20110615 Firefox/6.0a2 Iceweasel/6.0a2",
            "Konqueror/3.0-rc4; (Konqueror/3.0-rc4; i686 Linux;;datecode)",
            "Mozilla/5.0 (compatible; Konqueror/3.3; Linux 2.6.8-gentoo-r3; X11;",
            "Mozilla/5.0 (compatible; Konqueror/3.5; Linux 2.6.30-7.dmz.1-liquorix-686; X11) KHTML/3.5.10 (like Gecko) (Debian package 4:3.5.10.dfsg.1-1 b1)",
            "Mozilla/5.0 (compatible; Konqueror/3.5; Linux; en_US) KHTML/3.5.6 (like Gecko) (Kubuntu)",
            "Mozilla/5.0 (X11; Linux x86_64; en-US; rv:2.0b2pre) Gecko/20100712 Minefield/4.0b2pre",
            "Mozilla/5.0 (X11; U; Linux; i686; en-US; rv:1.6) Gecko Debian/1.6-7",
            "MSIE (MSIE 6.0; X11; Linux; i686) Opera 7.23",
            "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.1) Gecko/20061024 Firefox/2.0 (Swiftfox)",
            "Mozilla/5.0 (X11; U; Linux; en-US) AppleWebKit/527  (KHTML, like Gecko, Safari/419.3) Arora/0.10.1",
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.9 Safari/536.5",
            "Mozilla/5.0 (X11; CrOS i686 2268.111.0) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.57 Safari/536.11",
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.4 (KHTML like Gecko) Chrome/22.0.1229.56 Safari/537.4",
            "Mozilla/4.0 (compatible; Dillo 3.0)",
            "Mozilla/5.0 (X11; U; Linux i686; en-us) AppleWebKit/528.5  (KHTML, like Gecko, Safari/528.5 ) lt-GtkLauncher",
            "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.1.16) Gecko/20120421 Gecko Firefox/11.0",
            "Mozilla/5.0 (X11; Linux i686; rv:12.0) Gecko/20100101 Firefox/12.0 ",
            "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:14.0) Gecko/20100101 Firefox/14.0.1",
            "Mozilla/5.0 (X11; Linux i686; rv:16.0) Gecko/20100101 Firefox/16.0",
            "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.8) Gecko Galeon/2.0.6 (Ubuntu 2.0.6-2)",
            "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.1.16) Gecko/20080716 (Gentoo) Galeon/2.0.6",
            "Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.1.13) Gecko/20100916 Iceape/2.0.8",
            "Mozilla/5.0 (X11; Linux i686; rv:14.0) Gecko/20100101 Firefox/14.0.1 Iceweasel/14.0.1",
            "Mozilla/5.0 (X11; Linux x86_64; rv:15.0) Gecko/20120724 Debian Iceweasel/15.02",
            "Mozilla/5.0 (compatible; Konqueror/4.2; Linux) KHTML/4.2.4 (like Gecko) Slackware/13.0",
            "Mozilla/5.0 (compatible; Konqueror/4.3; Linux) KHTML/4.3.1 (like Gecko) Fedora/4.3.1-3.fc11",
            "Mozilla/5.0 (compatible; Konqueror/4.4; Linux) KHTML/4.4.1 (like Gecko) Fedora/4.4.1-1.fc12",
            "Mozilla/5.0 (compatible; Konqueror/4.4; Linux 2.6.32-22-generic; X11; en_US) KHTML/4.4.3 (like Gecko) Kubuntu",
            "Midori/0.1.10 (X11; Linux i686; U; en-us) WebKit/(531).(2) ",
            "Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.0.3) Gecko/2008092814 (Debian-3.0.1-1)",
            "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9a3pre) Gecko/20070330",
            "Opera/9.64 (X11; Linux i686; U; Linux Mint; nb) Presto/2.1.1",
            "Opera/9.80 (X11; Linux i686; U; en) Presto/2.2.15 Version/10.10",
            "Opera/9.80 (X11; Linux x86_64; U; pl) Presto/2.7.62 Version/11.00",
            "Mozilla/5.0 (X11; Linux i686) AppleWebKit/534.34 (KHTML, like Gecko) QupZilla/1.2.0 Safari/534.34",
            "Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.1.17) Gecko/20110123 SeaMonkey/2.0.12",
            "Mozilla/5.0 (X11; Linux i686; rv:10.0.1) Gecko/20100101 Firefox/10.0.1 SeaMonkey/2.7.1",
            "Mozilla/5.0 (X11; U; Linux x86_64; us; rv:1.9.1.19) Gecko/20110430 shadowfox/7.0 (like Firefox/7.0",
            "Mozilla/5.0 (X11; U; Linux i686; it; rv:1.9.2.3) Gecko/20100406 Firefox/3.6.3 (Swiftfox)",
            "Uzbl (Webkit 1.3) (Linux i686 [i686])",
            "ELinks (0.4.3; NetBSD 3.0.2PATCH sparc64; 141x19)",
            "Links (2.1pre15; FreeBSD 5.3-RELEASE i386; 196x84)",
            "Lynx/2.8.7dev.4 libwww-FM/2.14 SSL-MM/1.4.1 OpenSSL/0.9.8d",
            "w3m/0.5.1",
            "Mozilla/5.0 (X11; U; FreeBSD i386; en-US) AppleWebKit/532.0 (KHTML, like Gecko) Chrome/4.0.207.0 Safari/532.0",
            "Mozilla/5.0 (X11; U; OpenBSD i386; en-US) AppleWebKit/533.3 (KHTML, like Gecko) Chrome/5.0.359.0 Safari/533.3",
            "Mozilla/5.0 (X11; U; FreeBSD x86_64; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.648.204 Safari/534.16",
            "Mozilla/5.0 (X11; U; SunOS sun4m; en-US; rv:1.4b) Gecko/20030517 Mozilla Firebird/0.6",
            "Mozilla/5.0 (X11; U; FreeBSD i386; en-US; rv:1.6) Gecko/20040406 Galeon/1.3.15",
            "Mozilla/5.0 (compatible; Konqueror/3.5; NetBSD 4.0_RC3; X11) KHTML/3.5.7 (like Gecko)",
            "Mozilla/5.0 (compatible; Konqueror/3.5; SunOS) KHTML/3.5.1 (like Gecko)",
            "Mozilla/5.0 (X11; U; FreeBSD; i386; en-US; rv:1.7) Gecko",
            "Mozilla/4.77 [en] (X11; I; IRIX;64 6.5 IP30)",
            "Mozilla/4.8 [en] (X11; U; SunOS; 5.7 sun4u)",
            "Mozilla/5.0 (Unknown; U; UNIX BSD/SYSV system; C -) AppleWebKit/527  (KHTML, like Gecko, Safari/419.3) Arora/0.10.2",
            "Mozilla/5.0 (X11; FreeBSD amd64) AppleWebKit/536.5 (KHTML like Gecko) Chrome/19.0.1084.56 Safari/536.5",
            "Mozilla/5.0 (X11; FreeBSD amd64) AppleWebKit/537.4 (KHTML like Gecko) Chrome/22.0.1229.79 Safari/537.4",
            "Mozilla/5.0 (X11; U; OpenBSD arm; en-us) AppleWebKit/531.2  (KHTML, like Gecko) Safari/531.2  Epiphany/2.30.0",
            "Mozilla/5.0 (X11; U; FreeBSD amd64; en-us) AppleWebKit/531.2  (KHTML, like Gecko) Safari/531.2  Epiphany/2.30.0",
            "Mozilla/5.0 (X11; U; SunOS i86pc; en-US; rv:1.9.1b3) Gecko/20090429 Firefox/3.1b3",
            "Mozilla/5.0 (X11; U; OpenBSD i386; en-US; rv:1.9.1) Gecko/20090702 Firefox/3.5",
            "Mozilla/5.0 (X11; U; FreeBSD i386; de-CH; rv:1.9.2.8) Gecko/20100729 Firefox/3.6.8",
            "Mozilla/5.0 (X11; FreeBSD amd64; rv:5.0) Gecko/20100101 Firefox/5.0",
            "Mozilla/5.0 (compatible; Konqueror/4.1; DragonFly) KHTML/4.1.4 (like Gecko)",
            "Mozilla/5.0 (compatible; Konqueror/4.1; OpenBSD) KHTML/4.1.4 (like Gecko)",
            "Mozilla/5.0 (compatible; Konqueror/4.5; NetBSD 5.0.2; X11; amd64; en_US) KHTML/4.5.4 (like Gecko)",
            "Mozilla/5.0 (compatible; Konqueror/4.5; FreeBSD) KHTML/4.5.4 (like Gecko)",
            "Mozilla/5.0 (X11; U; NetBSD amd64; en-US; rv:1.9.2.15) Gecko/20110308 Namoroka/3.6.15",
            "NetSurf/1.2 (NetBSD; amd64)",
            "Opera/9.80 (X11; FreeBSD 8.1-RELEASE i386; Edition Next) Presto/2.12.388 Version/12.10",
            "Mozilla/5.0 (X11; U; SunOS i86pc; en-US; rv:1.8.1.12) Gecko/20080303 SeaMonkey/1.1.8",
            "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; BOLT/2.800) AppleWebKit/534.6 (KHTML, like Gecko) Version/5.0 Safari/534.6.3",
            "Mozilla/4.0 (compatible; MSIE 6.0; Windows CE; IEMobile 6.12; Microsoft ZuneHD 4.3)",
            "Mozilla/1.22 (compatible; MSIE 5.01; PalmOS 3.0) EudoraWeb 2.1",
            "Mozilla/5.0 (WindowsCE 6.0; rv:2.0.1) Gecko/20100101 Firefox/4.0.1",
            "Mozilla/5.0 (X11; U; Linux armv61; en-US; rv:1.9.1b2pre) Gecko/20081015 Fennec/1.0a1",
            "Mozilla/5.0 (Maemo; Linux armv7l; rv:2.0.1) Gecko/20100101 Firefox/4.0.1 Fennec/2.0.1",
            "Mozilla/5.0 (Maemo; Linux armv7l; rv:10.0.1) Gecko/20100101 Firefox/10.0.1 Fennec/10.0.1",
            "Mozilla/5.0 (Windows; U; Windows CE 5.1; rv:1.8.1a3) Gecko/20060610 Minimo/0.016",
            "Mozilla/5.0 (X11; U; Linux armv6l; rv 1.8.1.5pre) Gecko/20070619 Minimo/0.020",
            "Mozilla/5.0 (X11; U; Linux arm7tdmi; rv:1.8.1.11) Gecko/20071130 Minimo/0.025",
            "Mozilla/4.0 (PDA; PalmOS/sony/model prmr/Revision:1.1.54 (en)) NetFront/3.0",
            "Opera/9.51 Beta (Microsoft Windows; PPC; Opera Mobi/1718; U; en)",
            "Opera/9.60 (J2ME/MIDP; Opera Mini/4.1.11320/608; U; en) Presto/2.2.0",
            "Opera/9.60 (J2ME/MIDP; Opera Mini/4.2.14320/554; U; cs) Presto/2.2.0",
            "Opera/9.80 (S60; SymbOS; Opera Mobi/499; U; ru) Presto/2.4.18 Version/10.00",
            "Opera/10.61 (J2ME/MIDP; Opera Mini/5.1.21219/19.999; en-US; rv:1.9.3a5) WebKit/534.5 Presto/2.6.30",
            "POLARIS/6.01 (BREW 3.1.5; U; en-us; LG; LX265; POLARIS/6.01/WAP) MMP/2.0 profile/MIDP-2.1 Configuration/CLDC-1.1",
            "Mozilla/5.0 (Linux; U; Android 2.0; en-us; Droid Build/ESD20) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17",
            "Mozilla/5.0 (iPad; U; CPU OS 4_2_1 like Mac OS X; ja-jp) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8C148 Safari/6533.18.5",
            "Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_2_1 like Mac OS X; da-dk) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8C148 Safari/6533.18.5",
            "Mozilla/5.0 (iPad; CPU OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5355d Safari/8536.25",
            "Mozilla/5.0 (Linux; U; Android 3.0.1; fr-fr; A500 Build/HRI66) AppleWebKit/534.13 (KHTML, like Gecko) Version/4.0 Safari/534.13",
            "Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B334b Safari/531.21.10",
            "Mozilla/5.0 (iPad; U; CPU OS 4_2_1 like Mac OS X; ja-jp) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8C148 Safari/6533.18.5",
            "Mozilla/5.0 (iPad; U; CPU OS 4_3 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8F190 Safari/6533.18.5",
            "Mozilla/5.0 (iPad; CPU OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5355d Safari/8536.25",
            "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_7;en-us) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Safari/530.17",
            "Mozilla/5.0 (hp-tablet; Linux; hpwOS/3.0.2; U; de-DE) AppleWebKit/534.6 (KHTML, like Gecko) wOSBrowser/234.40.1 Safari/534.6 TouchPad/1.0",
            "Mozilla/5.0 (Linux; U; Android 3.0; en-us; Xoom Build/HRI39) AppleWebKit/525.10  (KHTML, like Gecko) Version/3.0.4 Mobile Safari/523.12.2",
            "Mozilla/5.0 (Linux; U; Android 1.5; de-de; Galaxy Build/CUPCAKE) AppleWebKit/528.5  (KHTML, like Gecko) Version/3.1.2 Mobile Safari/525.20.1",
            "Mozilla/5.0 (Linux; U; Android 2.2; en-ca; GT-P1000M Build/FROYO) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1",
            "Mozilla/5.0 (Linux; U; Android 3.0.1; en-us; GT-P7100 Build/HRI83) AppleWebkit/534.13 (KHTML, like Gecko) Version/4.0 Safari/534.13",
            "Mozilla/4.0 (compatible; Linux 2.6.22) NetFront/3.4 Kindle/2.0 (screen 600x800)",
            "Mozilla/5.0 (Linux U; en-US)  AppleWebKit/528.5  (KHTML, like Gecko, Safari/528.5 ) Version/4.0 Kindle/3.0 (screen 600x800; rotate)",
            "Mozilla/5.0 (Linux; U; Android 3.0.1; fr-fr; A500 Build/HRI66) AppleWebKit/534.13 (KHTML, like Gecko) Version/4.0 Safari/534.13",
            "Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B334b Safari/531.21.10",
            "Mozilla/5.0 (iPad; U; CPU OS 4_2_1 like Mac OS X; ja-jp) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8C148 Safari/6533.18.5",
            "Mozilla/5.0 (iPad; U; CPU OS 4_3 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8F190 Safari/6533.18.5",
            "Mozilla/5.0 (iPhone; U; CPU like Mac OS X; en) AppleWebKit/420  (KHTML, like Gecko) Version/3.0 Mobile/1A543a Safari/419.3",
            "Mozilla/5.0 (iPhone; U; CPU iPhone OS 2_0 like Mac OS X; en-us) AppleWebKit/525.18.1 (KHTML, like Gecko) Version/3.1.1 Mobile/5A347 Safari/525.200",
            "Mozilla/5.0 (iPhone; U; CPU iPhone OS 3_0 like Mac OS X; en-us) AppleWebKit/528.18 (KHTML, like Gecko) Version/4.0 Mobile/7A341 Safari/528.16",
            "Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_0 like Mac OS X; en-us) AppleWebKit/532.9 (KHTML, like Gecko) Version/4.0.5 Mobile/8A293 Safari/531.22.7",
            "Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_2_1 like Mac OS X; da-dk) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8C148 Safari/6533.18.5",
            "Mozilla/5.0 (iPhone; U; CPU iPhone OS 5_1_1 like Mac OS X; da-dk) AppleWebKit/534.46.0 (KHTML, like Gecko) CriOS/19.0.1084.60 Mobile/9B206 Safari/7534.48.3",
            "Mozilla/5.0 (iPod; U; CPU iPhone OS 2_2_1 like Mac OS X; en-us) AppleWebKit/525.18.1 (KHTML, like Gecko) Version/3.1.1 Mobile/5H11a Safari/525.20",
            "Mozilla/5.0 (iPod; U; CPU iPhone OS 3_1_1 like Mac OS X; en-us) AppleWebKit/528.18 (KHTML, like Gecko) Mobile/7C145",
            "nook browser/1.0",
            "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_7;en-us) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Safari/530.17",
            "Mozilla/5.0 (Linux; U; Android 2.3.4; en-us; BNTV250 Build/GINGERBREAD) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Safari/533.1",
            "BlackBerry7100i/4.1.0 Profile/MIDP-2.0 Configuration/CLDC-1.1 VendorID/103",
            "BlackBerry8300/4.2.2 Profile/MIDP-2.0 Configuration/CLDC-1.1 VendorID/107 UP.Link/6.2.3.15.0",
            "BlackBerry8320/4.2.2 Profile/MIDP-2.0 Configuration/CLDC-1.1 VendorID/100",
            "BlackBerry8330/4.3.0 Profile/MIDP-2.0 Configuration/CLDC-1.1 VendorID/105",
            "BlackBerry9000/4.6.0.167 Profile/MIDP-2.0 Configuration/CLDC-1.1 VendorID/102",
            "BlackBerry9530/4.7.0.167 Profile/MIDP-2.0 Configuration/CLDC-1.1 VendorID/102 UP.Link/6.3.1.20.0",
            "BlackBerry9700/5.0.0.351 Profile/MIDP-2.1 Configuration/CLDC-1.1 VendorID/123",
            "Mozilla/5.0 (BlackBerry; U; BlackBerry 9800; en) AppleWebKit/534.1  (KHTML, Like Gecko) Version/6.0.0.141 Mobile Safari/534.1",
            "Mozilla/5.0 (Linux; U; Android 1.5; en-us; sdk Build/CUPCAKE) AppleWebkit/528.5  (KHTML, like Gecko) Version/3.1.2 Mobile Safari/525.20.1",
            "Mozilla/5.0 (Linux; U; Android 2.1; en-us; Nexus One Build/ERD62) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17",
            "Mozilla/5.0 (Linux; U; Android 2.2; en-us; Nexus One Build/FRF91) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1",
            "Mozilla/5.0 (hp-tablet; Linux; hpwOS/3.0.2; U; de-DE) AppleWebKit/534.6 (KHTML, like Gecko) wOSBrowser/234.40.1 Safari/534.6 TouchPad/1.0",
            "Mozilla/5.0 (Linux; U; Android 1.5; en-us; htc_bahamas Build/CRB17) AppleWebKit/528.5  (KHTML, like Gecko) Version/3.1.2 Mobile Safari/525.20.1",
            "Mozilla/5.0 (Linux; U; Android 2.1-update1; de-de; HTC Desire 1.19.161.5 Build/ERE27) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17",
            "HTC_Dream Mozilla/5.0 (Linux; U; Android 1.5; en-ca; Build/CUPCAKE) AppleWebKit/528.5  (KHTML, like Gecko) Version/3.1.2 Mobile Safari/525.20.1",
            "Mozilla/5.0 (Linux; U; Android 2.2; en-us; Sprint APA9292KT Build/FRF91) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1",
            "Mozilla/5.0 (Linux; U; Android 1.5; de-ch; HTC Hero Build/CUPCAKE) AppleWebKit/528.5  (KHTML, like Gecko) Version/3.1.2 Mobile Safari/525.20.1",
            "Mozilla/5.0 (Linux; U; Android 2.2; en-us; ADR6300 Build/FRF91) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1",
            "Mozilla/5.0 (Linux; U; Android 2.1; en-us; HTC Legend Build/cupcake) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17",
            "Mozilla/5.0 (Linux; U; Android 1.5; de-de; HTC Magic Build/PLAT-RC33) AppleWebKit/528.5  (KHTML, like Gecko) Version/3.1.2 Mobile Safari/525.20.1 FirePHP/0.3",
            "Mozilla/5.0 (Linux; U; Android 4.0.3; de-ch; HTC Sensation Build/IML74K) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30",
            "HTC-ST7377/1.59.502.3 (67150) Opera/9.50 (Windows NT 5.1; U; en) UP.Link/6.3.1.17.0",
            "Mozilla/5.0 (Linux; U; Android 1.6; en-us; HTC_TATTOO_A3288 Build/DRC79) AppleWebKit/528.5  (KHTML, like Gecko) Version/3.1.2 Mobile Safari/525.20.1",
            "LG-LX550 AU-MIC-LX550/2.0 MMP/2.0 Profile/MIDP-2.0 Configuration/CLDC-1.1",
            "POLARIS/6.01(BREW 3.1.5;U;en-us;LG;LX265;POLARIS/6.01/WAP;)MMP/2.0 profile/MIDP-201 Configuration /CLDC-1.1",
            "LG-GC900/V10a Obigo/WAP2.0 Profile/MIDP-2.1 Configuration/CLDC-1.1",
            "Mozilla/4.0 (compatible; MSIE 4.01; Windows CE; PPC; MDA Pro/1.0 Profile/MIDP-2.0 Configuration/CLDC-1.1)",
            "Mozilla/5.0 (Linux; U; Android 1.0; en-us; dream) AppleWebKit/525.10  (KHTML, like Gecko) Version/3.0.4 Mobile Safari/523.12.2",
            "Mozilla/5.0 (Linux; U; Android 1.5; en-us; T-Mobile G1 Build/CRB43) AppleWebKit/528.5  (KHTML, like Gecko) Version/3.1.2 Mobile Safari 525.20.1",
            "Mozilla/5.0 (Linux; U; Android 1.5; en-gb; T-Mobile_G2_Touch Build/CUPCAKE) AppleWebKit/528.5  (KHTML, like Gecko) Version/3.1.2 Mobile Safari/525.20.1",
            "Mozilla/5.0 (Linux; U; Android 2.0; en-us; Droid Build/ESD20) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17",
            "Mozilla/5.0 (Linux; U; Android 2.2; en-us; Droid Build/FRG22D) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1",
            "MOT-L7v/08.B7.5DR MIB/2.2.1 Profile/MIDP-2.0 Configuration/CLDC-1.1 UP.Link/6.3.0.0.0",
            "Mozilla/5.0 (Linux; U; Android 2.0; en-us; Milestone Build/ SHOLS_U2_01.03.1) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17",
            "Mozilla/5.0 (Linux; U; Android 2.0.1; de-de; Milestone Build/SHOLS_U2_01.14.0) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17",
            "MOT-V9mm/00.62 UP.Browser/6.2.3.4.c.1.123 (GUI) MMP/2.0",
            "MOTORIZR-Z8/46.00.00 Mozilla/4.0 (compatible; MSIE 6.0; Symbian OS; 356) Opera 8.65 [it] UP.Link/6.3.0.0.0",
            "MOT-V177/0.1.75 UP.Browser/6.2.3.9.c.12 (GUI) MMP/2.0 UP.Link/6.3.1.13.0",
            "Mozilla/5.0 (Linux; U; Android 3.0; en-us; Xoom Build/HRI39) AppleWebKit/525.10  (KHTML, like Gecko) Version/3.0.4 Mobile Safari/523.12.2",
            "portalmmm/2.0 N410i(c20;TB) ",
            "Nokia3230/2.0 (5.0614.0) SymbianOS/7.0s Series60/2.1 Profile/MIDP-2.0 Configuration/CLDC-1.0",
            "Mozilla/5.0 (SymbianOS/9.2; U; Series60/3.1 Nokia5700/3.27; Profile/MIDP-2.0 Configuration/CLDC-1.1) AppleWebKit/413 (KHTML, like Gecko) Safari/413",
            "Mozilla/5.0 (SymbianOS/9.2; U; Series60/3.1 Nokia6120c/3.70; Profile/MIDP-2.0 Configuration/CLDC-1.1) AppleWebKit/413 (KHTML, like Gecko) Safari/413",
            "Nokia6230/2.0 (04.44) Profile/MIDP-2.0 Configuration/CLDC-1.1",
            "Nokia6230i/2.0 (03.80) Profile/MIDP-2.0 Configuration/CLDC-1.1",
            "Mozilla/4.1 (compatible; MSIE 5.0; Symbian OS; Nokia 6600;452) Opera 6.20 [en-US]",
            "Nokia6630/1.0 (2.39.15) SymbianOS/8.0 Series60/2.6 Profile/MIDP-2.0 Configuration/CLDC-1.1",
            "Nokia7250/1.0 (3.14) Profile/MIDP-1.0 Configuration/CLDC-1.0",
            "Mozilla/4.0 (compatible; MSIE 5.0; Series80/2.0 Nokia9500/4.51 Profile/MIDP-2.0 Configuration/CLDC-1.1)",
            "Mozilla/5.0 (Symbian/3; Series60/5.2 NokiaC6-01/011.010; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/525 (KHTML, like Gecko) Version/3.0 BrowserNG/7.2.7.2 3gpp-gba",
            "Mozilla/5.0 (Symbian/3; Series60/5.2 NokiaC7-00/012.003; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/525 (KHTML, like Gecko) Version/3.0 BrowserNG/7.2.7.3 3gpp-gba",
            "Mozilla/5.0 (SymbianOS/9.1; U; en-us) AppleWebKit/413 (KHTML, like Gecko) Safari/413 es50",
            "Mozilla/5.0 (Symbian/3; Series60/5.2 NokiaE6-00/021.002; Profile/MIDP-2.1 Configuration/CLDC-1.1) AppleWebKit/533.4 (KHTML, like Gecko) NokiaBrowser/7.3.1.16 Mobile Safari/533.4 3gpp-gba",
            "Mozilla/5.0 (SymbianOS/9.1; U; en-us) AppleWebKit/413 (KHTML, like Gecko) Safari/413 es65",
            "Mozilla/5.0 (Symbian/3; Series60/5.2 NokiaE7-00/010.016; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/525 (KHTML, like Gecko) Version/3.0 BrowserNG/7.2.7.3 3gpp-gba",
            "Mozilla/5.0 (SymbianOS/9.1; U; en-us) AppleWebKit/413 (KHTML, like Gecko) Safari/413 es70",
            "Mozilla/5.0 (SymbianOS/9.2; U; Series60/3.1 NokiaE90-1/07.24.0.3; Profile/MIDP-2.0 Configuration/CLDC-1.1 ) AppleWebKit/413 (KHTML, like Gecko) Safari/413 UP.Link/6.2.3.18.0",
            "NokiaN70-1/5.0609.2.0.1 Series60/2.8 Profile/MIDP-2.0 Configuration/CLDC-1.1 UP.Link/6.3.1.13.0",
            "Mozilla/5.0 (SymbianOS/9.1; U; en-us) AppleWebKit/413 (KHTML, like Gecko) Safari/413",
            "NokiaN73-1/3.0649.0.0.1 Series60/3.0 Profile/MIDP2.0 Configuration/CLDC-1.1",
            "Mozilla/5.0 (Symbian/3; Series60/5.2 NokiaN8-00/014.002; Profile/MIDP-2.1 Configuration/CLDC-1.1; en-us) AppleWebKit/525 (KHTML, like Gecko) Version/3.0 BrowserNG/7.2.6.4 3gpp-gba",
            "Mozilla/5.0 (SymbianOS/9.1; U; en-us) AppleWebKit/413 (KHTML, like Gecko) Safari/413",
            "Mozilla/5.0 (MeeGo; NokiaN9) AppleWebKit/534.13 (KHTML, like Gecko) NokiaBrowser/8.5.0 Mobile Safari/534.13",
            "Mozilla/5.0 (SymbianOS/9.1; U; de) AppleWebKit/413 (KHTML, like Gecko) Safari/413",
            "Mozilla/5.0 (SymbianOS/9.2; U; Series60/3.1 NokiaN95/10.0.018; Profile/MIDP-2.0 Configuration/CLDC-1.1) AppleWebKit/413 (KHTML, like Gecko) Safari/413 UP.Link/6.3.0.0.0",
            "Mozilla/5.0 (MeeGo; NokiaN950-00/00) AppleWebKit/534.13 (KHTML, like Gecko) NokiaBrowser/8.5.0 Mobile Safari/534.13",
            "Mozilla/5.0 (SymbianOS/9.4; Series60/5.0 NokiaN97-1/10.0.012; Profile/MIDP-2.1 Configuration/CLDC-1.1; en-us) AppleWebKit/525 (KHTML, like Gecko) WicKed/7.1.12344",
            "Mozilla/5.0 (Symbian/3; Series60/5.2 NokiaX7-00/021.004; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/533.4 (KHTML, like Gecko) NokiaBrowser/7.3.1.21 Mobile Safari/533.4 3gpp-gba",
            "Mozilla/5.0 (webOS/1.3; U; en-US) AppleWebKit/525.27.1 (KHTML, like Gecko) Version/1.0 Safari/525.27.1 Desktop/1.0",
            "Mozilla/4.0 (compatible; MSIE 6.0; Windows 98; PalmSource/hspr-H102; Blazer/4.0) 16;320x320",
            "SEC-SGHE900/1.0 NetFront/3.2 Profile/MIDP-2.0 Configuration/CLDC-1.1 Opera/8.01 (J2ME/MIDP; Opera Mini/2.0.4509/1378; nl; U; ssr)",
            "Mozilla/5.0 (Linux; U; Android 1.5; de-de; Galaxy Build/CUPCAKE) AppleWebKit/528.5  (KHTML, like Gecko) Version/3.1.2 Mobile Safari/525.20.1",
            "Mozilla/5.0 (Linux; U; Android 2.2; en-ca; GT-P1000M Build/FROYO) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1",
            "Mozilla/5.0 (Linux; U; Android 4.0.3; de-de; Galaxy S II Build/GRJ22) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30",
            "Mozilla/5.0 (Linux; U; Android 3.0.1; en-us; GT-P7100 Build/HRI83) AppleWebkit/534.13 (KHTML, like Gecko) Version/4.0 Safari/534.13",
            "SAMSUNG-S8000/S8000XXIF3 SHP/VPP/R5 Jasmine/1.0 Nextreaming SMM-MMS/1.2.0 profile/MIDP-2.1 configuration/CLDC-1.1 FirePHP/0.3",
            "Mozilla/5.0 (Linux; U; Android 1.5; en-us; SPH-M900 Build/CUPCAKE) AppleWebKit/528.5  (KHTML, like Gecko) Version/3.1.2 Mobile Safari/525.20.1",
            "SAMSUNG-SGH-A867/A867UCHJ3 SHP/VPP/R5 NetFront/35 SMM-MMS/1.2.0 profile/MIDP-2.0 configuration/CLDC-1.1 UP.Link/6.3.0.0.0",
            "SEC-SGHX210/1.0 UP.Link/6.3.1.13.0",
            "Mozilla/5.0 (Linux; U; Android 1.5; fr-fr; GT-I5700 Build/CUPCAKE) AppleWebKit/528.5  (KHTML, like Gecko) Version/3.1.2 Mobile Safari/525.20.1",
            "SEC-SGHX820/1.0 NetFront/3.2 Profile/MIDP-2.0 Configuration/CLDC-1.1",
            "SonyEricssonK310iv/R4DA Browser/NetFront/3.3 Profile/MIDP-2.0 Configuration/CLDC-1.1 UP.Link/6.3.1.13.0",
            "SonyEricssonK550i/R1JD Browser/NetFront/3.3 Profile/MIDP-2.0 Configuration/CLDC-1.1",
            "SonyEricssonK610i/R1CB Browser/NetFront/3.3 Profile/MIDP-2.0 Configuration/CLDC-1.1",
            "SonyEricssonK750i/R1CA Browser/SEMC-Browser/4.2 Profile/MIDP-2.0 Configuration/CLDC-1.1",
            "Opera/9.80 (J2ME/MIDP; Opera Mini/5.0.16823/1428; U; en) Presto/2.2.0",
            "SonyEricssonK800i/R1CB Browser/NetFront/3.3 Profile/MIDP-2.0 Configuration/CLDC-1.1 UP.Link/6.3.0.0.0",
            "SonyEricssonK810i/R1KG Browser/NetFront/3.3 Profile/MIDP-2.0 Configuration/CLDC-1.1",
            "Opera/8.01 (J2ME/MIDP; Opera Mini/1.0.1479/HiFi; SonyEricsson P900; no; U; ssr)",
            "SonyEricssonS500i/R6BC Browser/NetFront/3.3 Profile/MIDP-2.0 Configuration/CLDC-1.1",
            "Mozilla/5.0 (SymbianOS/9.4; U; Series60/5.0 SonyEricssonP100/01; Profile/MIDP-2.1 Configuration/CLDC-1.1) AppleWebKit/525 (KHTML, like Gecko) Version/3.0 Safari/525",
            "SonyEricssonT68/R201A",
            "SonyEricssonT100/R101",
            "SonyEricssonT610/R201 Profile/MIDP-1.0 Configuration/CLDC-1.0",
            "SonyEricssonT650i/R7AA Browser/NetFront/3.3 Profile/MIDP-2.0 Configuration/CLDC-1.1",
            "SonyEricssonW580i/R6BC Browser/NetFront/3.3 Profile/MIDP-2.0 Configuration/CLDC-1.1",
            "SonyEricssonW660i/R6AD Browser/NetFront/3.3 Profile/MIDP-2.0 Configuration/CLDC-1.1",
            "SonyEricssonW810i/R4EA Browser/NetFront/3.3 Profile/MIDP-2.0 Configuration/CLDC-1.1 UP.Link/6.3.0.0.0",
            "SonyEricssonW850i/R1ED Browser/NetFront/3.3 Profile/MIDP-2.0 Configuration/CLDC-1.1",
            "SonyEricssonW950i/R100 Mozilla/4.0 (compatible; MSIE 6.0; Symbian OS; 323) Opera 8.60 [en-US]",
            "SonyEricssonW995/R1EA Profile/MIDP-2.1 Configuration/CLDC-1.1 UNTRUSTED/1.0",
            "Mozilla/5.0 (Linux; U; Android 1.6; es-es; SonyEricssonX10i Build/R1FA016) AppleWebKit/528.5  (KHTML, like Gecko) Version/3.1.2 Mobile Safari/525.20.1",
            "Mozilla/5.0 (Linux; U; Android 1.6; en-us; SonyEricssonX10i Build/R1AA056) AppleWebKit/528.5  (KHTML, like Gecko) Version/3.1.2 Mobile Safari/525.20.1",
            "Opera/9.5 (Microsoft Windows; PPC; Opera Mobi; U) SonyEricssonX1i/R2AA Profile/MIDP-2.0 Configuration/CLDC-1.1",
            "SonyEricssonZ800/R1Y Browser/SEMC-Browser/4.1 Profile/MIDP-2.0 Configuration/CLDC-1.1 UP.Link/6.3.0.0.0",
            "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; winfx; .NET CLR 1.1.4322; .NET CLR 2.0.50727; Zune 2.0) ",
            "Mozilla/4.0 (compatible; MSIE 6.0; Windows CE; IEMobile 6.12; Microsoft ZuneHD 4.3)",
            "Mozilla/5.0 (Linux; U; Android 0.5; en-us) AppleWebKit/522  (KHTML, like Gecko) Safari/419.3",
            "Mozilla/5.0 (Linux; U; Android 1.1; en-gb; dream) AppleWebKit/525.10  (KHTML, like Gecko) Version/3.0.4 Mobile Safari/523.12.2",
            "HTC_Dream Mozilla/5.0 (Linux; U; Android 1.5; en-ca; Build/CUPCAKE) AppleWebKit/528.5  (KHTML, like Gecko) Version/3.1.2 Mobile Safari/525.20.1",
            "Mozilla/5.0 (Linux; U; Android 2.0; en-us; Droid Build/ESD20) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17",
            "Mozilla/5.0 (Linux; U; Android 2.1; en-us; Nexus One Build/ERD62) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17",
            "Mozilla/5.0 (Linux; U; Android 2.2; en-us; Sprint APA9292KT Build/FRF91) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1",
            "Mozilla/5.0 (Linux; U; Android 2.2; en-us; ADR6300 Build/FRF91) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1",
            "Mozilla/5.0 (Linux; U; Android 2.2; en-ca; GT-P1000M Build/FROYO) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1",
            "Mozilla/5.0 (Android; Linux armv7l; rv:2.0.1) Gecko/20100101 Firefox/4.0.1 Fennec/2.0.1",
            "Mozilla/5.0 (Linux; U; Android 3.0.1; fr-fr; A500 Build/HRI66) AppleWebKit/534.13 (KHTML, like Gecko) Version/4.0 Safari/534.13",
            "Mozilla/5.0 (Linux; U; Android 3.0; en-us; Xoom Build/HRI39) AppleWebKit/525.10  (KHTML, like Gecko) Version/3.0.4 Mobile Safari/523.12.2",
            "Mozilla/5.0 (Linux; U; Android 4.0.3; de-ch; HTC Sensation Build/IML74K) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30",
            "Mozilla/5.0 (Linux; U; Android 4.0.3; de-de; Galaxy S II Build/GRJ22) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30",
            "Opera/9.80 (Android 4.0.4; Linux; Opera Mobi/ADR-1205181138; U; pl) Presto/2.10.254 Version/12.00",
            "Mozilla/5.0 (Android; Linux armv7l; rv:10.0.1) Gecko/20100101 Firefox/10.0.1 Fennec/10.0.1",
            "Mozilla/5.0 (iPhone; U; CPU like Mac OS X; en) AppleWebKit/420  (KHTML, like Gecko) Version/3.0 Mobile/1A543a Safari/419.3",
            "Mozilla/5.0 (iPhone; U; CPU iPhone OS 2_0 like Mac OS X; en-us) AppleWebKit/525.18.1 (KHTML, like Gecko) Version/3.1.1 Mobile/5A347 Safari/525.200",
            "Mozilla/5.0 (iPod; U; CPU iPhone OS 2_2_1 like Mac OS X; en-us) AppleWebKit/525.18.1 (KHTML, like Gecko) Version/3.1.1 Mobile/5H11a Safari/525.20",
            "Mozilla/5.0 (iPhone; U; CPU iPhone OS 3_0 like Mac OS X; en-us) AppleWebKit/528.18 (KHTML, like Gecko) Version/4.0 Mobile/7A341 Safari/528.16",
            "Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B334b Safari/531.21.10",
            "Mozilla/5.0 (iPad; U; CPU OS 4_2_1 like Mac OS X; ja-jp) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8C148 Safari/6533.18.5",
            "Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_2_1 like Mac OS X; da-dk) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8C148 Safari/6533.18.5",
            "Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_3 like Mac OS X; de-de) AppleWebKit/533.17.9 (KHTML, like Gecko) Mobile/8F190",
            "Mozilla/5.0 (iPhone; U; CPU iPhone OS 5_1_1 like Mac OS X; da-dk) AppleWebKit/534.46.0 (KHTML, like Gecko) CriOS/19.0.1084.60 Mobile/9B206 Safari/7534.48.3",
            "Mozilla/5.0 (X11; Linux i686 on x86_64; rv:2.0.1) Gecko/20100101 Firefox/4.0.1 Fennec/2.0.1",
            "Mozilla/5.0 (Maemo; Linux armv7l; rv:2.0.1) Gecko/20100101 Firefox/4.0.1 Fennec/2.0.1",
            "Mozilla/5.0 (webOS/1.3; U; en-US) AppleWebKit/525.27.1 (KHTML, like Gecko) Version/1.0 Safari/525.27.1 Desktop/1.0",
            "Mozilla/4.0 (compatible; MSIE 6.0; Windows 98; PalmSource/hspr-H102; Blazer/4.0) 16;320x320",
            "Mozilla/5.0 (Symbian/3; Series60/5.2 NokiaN8-00/014.002; Profile/MIDP-2.1 Configuration/CLDC-1.1; en-us) AppleWebKit/525 (KHTML, like Gecko) Version/3.0 BrowserNG/7.2.6.4 3gpp-gba",
            "Mozilla/5.0 (Symbian/3; Series60/5.2 NokiaX7-00/021.004; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/533.4 (KHTML, like Gecko) NokiaBrowser/7.3.1.21 Mobile Safari/533.4 3gpp-gba",
            "Mozilla/5.0 (SymbianOS/9.2; U; Series60/3.1 NokiaE90-1/07.24.0.3; Profile/MIDP-2.0 Configuration/CLDC-1.1 ) AppleWebKit/413 (KHTML, like Gecko) Safari/413 UP.Link/6.2.3.18.0",
            "Mozilla/5.0 (SymbianOS 9.4; Series60/5.0 NokiaN97-1/10.0.012; Profile/MIDP-2.1 Configuration/CLDC-1.1; en-us) AppleWebKit/525 (KHTML, like Gecko) WicKed/7.1.12344",
            "Opera/9.80 (S60; SymbOS; Opera Mobi/499; U; ru) Presto/2.4.18 Version/10.00",
            "Mozilla/4.0 (compatible; MSIE 6.0; Windows CE; IEMobile 6.12; Microsoft ZuneHD 4.3)",
            "Mozilla/4.0 (compatible; MSIE 7.0; Windows Phone OS 7.0; Trident/3.1; IEMobile/7.0) Asus;Galaxy6",
            "Mozilla/5.0 (compatible; MSIE 9.0; Windows Phone OS 7.5; Trident/5.0; IEMobile/9.0)",
            "DoCoMo/2.0 SH901iC(c100;TB;W24H12)",
            "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.7) Gecko/20060909 Firefox/1.5.0.7 MG(Novarra-Vision/6.9)",
            "Mozilla/4.0 (compatible; MSIE 6.0; j2me) ReqwirelessWeb/3.5",
            "Vodafone/1.0/V802SE/SEJ001 Browser/SEMC-Browser/4.1",
            "BlackBerry7520/4.0.0 Profile/MIDP-2.0 Configuration/CLDC-1.1 UP.Browser/5.0.3.3 UP.Link/5.1.2.12 (Google WAP Proxy/1.0)",
            "Nokia6100/1.0 (04.01) Profile/MIDP-1.0 Configuration/CLDC-1.0",
            "Nokia6630/1.0 (2.3.129) SymbianOS/8.0 Series60/2.6 Profile/MIDP-2.0 Configuration/CLDC-1.1",
            "Mozilla/2.0 (compatible; Ask Jeeves/Teoma)",
            "Baiduspider ( http://www.baidu.com/search/spider.htm)",
            "Mozilla/5.0 (compatible; bingbot/2.0  http://www.bing.com/bingbot.htm)",
            "Mozilla/5.0 (compatible; Exabot/3.0;  http://www.exabot.com/go/robot) ",
            "FAST-WebCrawler/3.8 (crawler at trd dot overture dot com; http://www.alltheweb.com/help/webmaster/crawler)",
            "AdsBot-Google ( http://www.google.com/adsbot.html)",
            "Mozilla/5.0 (compatible; Googlebot/2.1;  http://www.google.com/bot.html)",
            "Googlebot/2.1 ( http://www.googlebot.com/bot.html)",
            "Googlebot-Image/1.0",
            "Mediapartners-Google",
            "DoCoMo/2.0 N905i(c100;TB;W24H16) (compatible; Googlebot-Mobile/2.1;  http://www.google.com/bot.html)",
            "Mozilla/5.0 (iPhone; U; CPU iPhone OS) (compatible; Googlebot-Mobile/2.1;  http://www.google.com/bot.html)",
            "SAMSUNG-SGH-E250/1.0 Profile/MIDP-2.0 Configuration/CLDC-1.1 UP.Browser/6.2.3.3.c.1.101 (GUI) MMP/2.0 (compatible; Googlebot-Mobile/2.1;  http://www.google.com/bot.html)",
            "Googlebot-News",
            "Googlebot-Video/1.0",
            "Mozilla/4.0 (compatible; GoogleToolbar 4.0.1019.5266-big; Windows XP 5.1; MSIE 6.0.2900.2180)",
            "Mozilla/5.0 (en-us) AppleWebKit/525.13 (KHTML, like Gecko; Google Web Preview) Version/3.1 Safari/525.13",
            "msnbot/1.0 ( http://search.msn.com/msnbot.htm)",
            "msnbot/1.1 ( http://search.msn.com/msnbot.htm)",
            "msnbot/0.11 ( http://search.msn.com/msnbot.htm)",
            "msnbot-media/1.1 ( http://search.msn.com/msnbot.htm)",
            "Mozilla/5.0 (compatible; Yahoo! Slurp; http://help.yahoo.com/help/us/ysearch/slurp)",
            "Mozilla/5.0 (compatible; Yahoo! Slurp China; http://misc.yahoo.com.cn/help.html)",
            "EmailWolf 1.00",
            "Gaisbot/3.0 (robot@gais.cs.ccu.edu.tw; http://gais.cs.ccu.edu.tw/robot.php)",
            "grub-client-1.5.3; (grub-client-1.5.3; Crawl your own stuff with http://grub.org)",
            "Gulper Web Bot 0.2.4 (www.ecsl.cs.sunysb.edu/~maxim/cgi-bin/Link/GulperBot)",
            "Mozilla/3.0 (compatible; NetPositive/2.1.1; BeOS)",
            "Mozilla/5.0 (BeOS; U; BeOS BePC; en-US; rv:1.9a1) Gecko/20060702 SeaMonkey/1.5a",
            "Download Demon/3.5.0.11",
            "Offline Explorer/2.5",
            "SuperBot/4.4.0.60 (Windows XP)",
            "WebCopier v4.6",
            "Web Downloader/6.9",
            "WebZIP/3.5 (http://www.spidersoft.com)",
            "Wget/1.9 cvs-stable (Red Hat modified)",
            "Wget/1.9.1",
            "Bloglines/3.1 (http://www.bloglines.com)",
            "everyfeed-spider/2.0 (http://www.everyfeed.com)",
            "FeedFetcher-Google; ( http://www.google.com/feedfetcher.html)",
            "Gregarius/0.5.2 ( http://devlog.gregarius.net/docs/ua)",
            "Mozilla/5.0 (PLAYSTATION 3; 2.00)",
            "Mozilla/5.0 (PLAYSTATION 3; 1.10)",
            "Mozilla/4.0 (PSP (PlayStation Portable); 2.00)",
            "Opera/9.30 (Nintendo Wii; U; ; 2047-7; en)",
            "wii libnup/1.0",
            "Java/1.6.0_13",
            "libwww-perl/5.820",
            "Peach/1.01 (Ubuntu 8.04 LTS; U; en)",
            "Python-urllib/2.5",
            "HTMLParser/1.6",
            "Jigsaw/2.2.5 W3C_CSS_Validator_JFouffa/2.0",
            "W3C_Validator/1.654",
            "W3C_Validator/1.305.2.12 libwww-perl/5.64",
            "P3P Validator",
            "CSSCheck/1.2.2",
            "WDG_Validator/1.6.2",
            "facebookscraper/1.0( http://www.facebook.com/sharescraper_help.php)",
            "grub-client-1.5.3; (grub-client-1.5.3; Crawl your own stuff with http://grub.org)",
            "iTunes/4.2 (Macintosh; U; PPC Mac OS X 10.2)",
            "Microsoft URL Control - 6.00.8862",
            "SearchExpress",
]
########NEW FILE########
__FILENAME__ = db
from linkedin import settings
import pymongo

class MongoDBClient(object):
    def __init__(self, col, index=None):        
        connection = pymongo.Connection(settings.MONGODB_SERVER, settings.MONGODB_PORT)
        self.db = connection[settings.MONGODB_DB]
        self.collection = self.db[col]
        if index:
            self.collection.create_index(index, unique=True)
            
    def get_collection(self):
        return self.collection
    
    def _walk(self):
        """
        generator of all the documents in this collection
        """
        skip = 0
        limit = 1000
        hasMore = True
        while hasMore:
            res = self.collection.find(skip=skip, limit=limit)
            hasMore = (res.count(with_limit_and_skip=True) == limit)
            for x in res:
                yield x
            skip += limit
        
    def walk(self):
        """
        return all the documents in this collection
        """
        docs = []
        for doc in self._walk():
            docs.append(doc)
        return docs
    

########NEW FILE########
__FILENAME__ = items
# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/topics/items.html

from scrapy.item import Item, Field

class LinkedinItem(Item):
    # define the fields for your item here like:
    # name = Field()
    pass


class PersonProfileItem(Item):
    _id = Field()
    url = Field()
    name = Field()
    also_view = Field()
    education = Field()
    locality = Field()
    industry = Field()
    summary = Field()
    specilities = Field()
    skills = Field()
    interests = Field()
    group = Field()
    honors = Field()
    education = Field()
    experience = Field()
    overview_html = Field()
    homepage = Field()
    

########NEW FILE########
__FILENAME__ = middleware
from scrapy import log
from proxy import PROXIES
from agents import AGENTS

import random

"""
Custom proxy provider. 
"""
class CustomHttpProxyMiddleware(object):
    
    def process_request(self, request, spider):
        # TODO implement complex proxy providing algorithm
        if self.use_proxy(request):
            p = random.choice(PROXIES)
            try:
                request.meta['proxy'] = "http://%s" % p['ip_port']
            except Exception, e:
                log.msg("Exception %s" % e, _level=log.CRITICAL)
                
    
    def use_proxy(self, request):
        """
        using direct download for depth <= 2
        using proxy with probability 0.3
        """
        if "depth" in request.meta and int(request.meta['depth']) <= 2:
            return False
        i = random.randint(1, 10)
        return i <= 2
    
    
"""
change request header nealy every time
"""
class CustomUserAgentMiddleware(object):
    def process_request(self, request, spider):
        agent = random.choice(AGENTS)
        request.headers['User-Agent'] = agent

########NEW FILE########
__FILENAME__ = HtmlParser
from linkedin.items import PersonProfileItem
from bs4 import UnicodeDammit
from w3lib.url import url_query_cleaner
import random
import LinkedinParser


class HtmlParser:    
    @staticmethod
    def extract_person_profile(hxs):
        personProfile = PersonProfileItem()
        ## Person name
        nameField = {}
        nameSpan = hxs.select("//span[@id='name']/span")
        if nameSpan and len(nameSpan) == 1:
            nameSpan = nameSpan[0]
            givenNameSpan = nameSpan.select("span[@class='given-name']")
            if givenNameSpan and len(givenNameSpan) == 1:
                givenNameSpan = givenNameSpan[0]
                nameField['given_name'] = givenNameSpan.select("text()").extract()[0]
            familyNameSpan = nameSpan.select("span[@class='family-name']")
            if familyNameSpan and len(familyNameSpan) == 1:
                familyNameSpan = familyNameSpan[0]
                nameField['family_name'] = familyNameSpan.select("text()").extract()[0]
            personProfile['name'] = nameField
        else:
            return None
        
        headline = hxs.select("//dl[@id='headline']")
        if headline and len(headline) == 1:
            headline = headline[0]
            ## locality
            locality = headline.select("dd/span[@class='locality']/text()").extract()
            if locality and len(locality) == 1:
                personProfile['locality'] = locality[0].strip()
            ## industry
            industry = headline.select("dd[@class='industry']/text()").extract()
            if industry and len(industry) == 1:
                personProfile['industry'] = industry[0].strip()
        
        ## overview
        overview = hxs.select("//dl[@id='overview']").extract()
        if overview and len(overview) == 1:
            personProfile['overview_html'] = overview[0]
            homepage = LinkedinParser.parse_homepage(overview[0])
            if homepage:
                personProfile['homepage'] = homepage
            
        ## summary
        summary = hxs.select("//div[@id='profile-summary']/div[@class='content']/p[contains(@class,'summary')]/text()").extract()
        if summary and len(summary) > 0:
            personProfile['summary'] = ''.join(x.strip() for x in summary)
        
        ## specilities
        specilities = hxs.select("//div[@id='profile-specialties']/p/text()").extract()
        if specilities and len(specilities) == 1:
            specilities = specilities[0].strip()
            personProfile['specilities'] = specilities
        
        ## skills
        skills = hxs.select("//ol[@id='skills-list']/li/span/a/text()").extract()
        if skills and len(skills) > 0:
            personProfile['skills'] = [x.strip() for x in skills]
            
        additional = hxs.select("//div[@id='profile-additional']")
        if additional and len(additional) == 1:
            additional = additional[0]
            ## interests
            interests = additional.select("div[@class='content']/dl/dd[@class='interests']/p/text()").extract()
            if interests and len(interests) == 1:
                personProfile['interests'] = interests[0].strip()
            ## groups
            g = additional.select("div[@class='content']/dl/dd[@class='pubgroups']")
            if g and len(g) == 1:
                groups = {}
                g = g[0]
                member = g.select("p/text()").extract()
                if member and len(member) > 0:
                    groups['member'] = ''.join(member[0].strip())
                gs = g.select("ul[@class='groups']/li[contains(@class,'affiliation')]/div/a/strong/text()").extract()
                if gs and len(gs) > 0:
                    groups['affilition'] = gs
                personProfile['group'] = groups
            ## honors
            honors = additional.select("div[@class='content']/dl/dd[@class='honors']/p/text()").extract()
            if honors and len(honors) > 0:
                personProfile['honors'] = [x.strip() for x in honors]
        
        ## education
        education = hxs.select("//div[@id='profile-education']")
        schools = []
        if education and len(education) == 1:
            education = education[0]
            school_list = education.select("div[contains(@class,'content')]//div[contains(@class,'education')]")
            if school_list and len(school_list) > 0:
                for school in school_list:
                    s = {}
                    name = school.select("h3[contains(@class,'org')]/text()").extract()
                    if name and len(name) == 1:
                        s['name'] = name[0].strip()
                    degree = school.select("h4[@class='details-education']/span[@class='degree']/text()").extract()
                    if degree and len(degree) == 1:
                        s['degree'] = degree[0].strip()
                    major = school.select("h4[@class='details-education']/span[@class='major']/text()").extract()
                    if major and len(major) == 1:
                        s['major'] = major[0].strip()
                    period = school.select("p[@class='period']")
                    if period and len(period) == 1:
                        period = period[0]
                        start = period.select("abbr[@class='dtstart']/text()").extract()
                        end = period.select("abbr[@class='dtend']/text()").extract()
                        if len(start) == 1:
                            s['start'] = start[0]
                        if len(end) == 1:
                            s['end'] = end[0]
                    desc = school.select("p[contains(@class,'desc')]/text()").extract()
                    if len(desc) == 1:
                        s['desc'] = desc[0].strip()
                    schools.append(s)
                personProfile['education'] = schools 
        
        ## experience
        experience = hxs.select("//div[@id='profile-experience']")
        if experience and len(experience) == 1:
            es = []
            experience = experience[0]
            exps = experience.select("//div[contains(@class,'experience')]")
            if len(exps) > 0:
                for e in exps:
                    je = {}
                    title = e.select("div[@class='postitle']//span[@class='title']/text()").extract()
                    if len(title) > 0:
                        je['title'] = title[0].strip()
                    org = e.select("div[@class='postitle']//span[contains(@class,'org')]/text()").extract() 
                    if len(org) > 0:
                        je['org'] = org[0].strip()
                    start = e.select("p[@class='period']/abbr[@class='dtstart']/text()").extract()
                    if len(start) > 0:
                        je['start'] = start[0].strip()
                    end = e.select("p[@class='period']/abbr[@class='dtstamp']/text()").extract()
                    if len(end) > 0:
                        je['end'] = end[0].strip()
                    location = e.select("p[@class='period']/abbr[@class='location']/text()").extract()
                    if len(location) > 0:
                        je['location'] = location[0]
                    desc = e.select("p[contains(@class,'description')]/text()").extract()
                    if len(desc) > 0:
                        je['desc'] = "".join(x.strip() for x in desc)
                    es.append(je)
            personProfile['experience'] = es
                    
        ## Also view
        alsoViewProfileList = []
        divExtra = hxs.select("//div[@id='extra']")
        if divExtra and len(divExtra) == 1:
            divExtra = divExtra[0]
            divAlsoView = divExtra.select("//div[@class='leo-module mod-util browsemap']")
            if divAlsoView and len(divAlsoView) == 1:
                divAlsoView = divAlsoView[0]
                alsoViewList = divAlsoView.select("div[@class='content']/ul/li/strong/a/@href").extract()
                if alsoViewList:
                    for alsoViewItem in alsoViewList:
                        alsoViewItem = UnicodeDammit(alsoViewItem).markup
                        item = HtmlParser.get_also_view_item(alsoViewItem)
                        alsoViewProfileList.append(item)
                    personProfile['also_view'] = alsoViewProfileList
        return personProfile

    @staticmethod
    def get_also_view_item(dirtyUrl):
        item = {}
        url = HtmlParser.remove_url_parameter(dirtyUrl)
        item['linkedin_id'] = url 
        item['url'] = HtmlParser.get_linkedin_id(url)
        return item
        
        
    @staticmethod
    def remove_url_parameter(url):
        return url_query_cleaner(url)
    
    @staticmethod
    def get_linkedin_id(url):
        find_index = url.find("linkedin.com/")
        if find_index >= 0:
            return url[find_index + 13:].replace('/', '-')
        return None
        

########NEW FILE########
__FILENAME__ = LinkedinParser
from bs4 import BeautifulSoup
from urllib2 import urlparse

def parse_homepage(html):
    soup = BeautifulSoup(html)
    websites = soup.find_all('dd', 'websites')
    if websites and len(websites) > 0:
        websites = websites[0]
        sites = websites.find_all('li')
        if sites and len(sites) > 0:
            result = {}
            for site in sites:
                site_name = site.text.strip()
                original = site.a.get('href')
                url_parse = urlparse.urlparse(original).query
                query_parse = urlparse.parse_qs(url_parse)
                if 'url' in query_parse:
                    result[site_name] = query_parse['url']
            return result
    return None


########NEW FILE########
__FILENAME__ = pipelines
# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/topics/item-pipeline.html
from scrapy.conf import settings
from scrapy import log

class LinkedinPipeline(object):
    def process_item(self, item, spider):
        return item


# Copyright 2011 Julien Duponchelle <julien@duponchelle.info>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
class MongoDBPipeline(object):
    def __init__(self):
        import pymongo
        connection = pymongo.Connection(settings['MONGODB_SERVER'], settings['MONGODB_PORT'])
        self.db = connection[settings['MONGODB_DB']]
        self.collection = self.db[settings['MONGODB_COLLECTION']]
        if self.__get_uniq_key() is not None:
            self.collection.create_index(self.__get_uniq_key(), unique=True)

    def process_item(self, item, spider):
        if self.__get_uniq_key() is None:
            self.collection.insert(dict(item))
        else:
            self.collection.update(
                            {self.__get_uniq_key(): item[self.__get_uniq_key()]},
                            dict(item),
                            upsert=True)  
        log.msg("Item wrote to MongoDB database %s/%s" %
                    (settings['MONGODB_DB'], settings['MONGODB_COLLECTION']),
                    level=log.DEBUG, spider=spider)  
        return item

    def __get_uniq_key(self):
        if not settings['MONGODB_UNIQ_KEY'] or settings['MONGODB_UNIQ_KEY'] == "":
            return None
        return settings['MONGODB_UNIQ_KEY']
########NEW FILE########
__FILENAME__ = proxy
PROXIES = [
			{"ip_port":"218.94.149.114:8080"},
			{"ip_port":"202.51.120.58:8080"},
			{"ip_port":"180.244.214.180:8080"},
			{"ip_port":"186.216.160.147:8080"},
			{"ip_port":"61.153.149.205:8080"},
			{"ip_port":"221.176.214.246:8080"},
			{"ip_port":"59.172.208.186:8080"},
			{"ip_port":"61.167.49.188:8080"},
			{"ip_port":"118.98.35.251:8080"},
			{"ip_port":"221.195.42.195:8080"},
			{"ip_port":"222.89.55.123:8080"},
			{"ip_port":"222.124.35.116:8080"},
			{"ip_port":"219.159.105.180:8080"},
			{"ip_port":"200.27.114.228:8080"},
			{"ip_port":"120.35.31.101:8080"},
			{"ip_port":"222.124.147.105:8080"},
			{"ip_port":"222.169.15.234:8080"},
			{"ip_port":"200.111.115.173:8080"},
			{"ip_port":"195.178.34.206:8080"},
			{"ip_port":"78.134.255.41:8080"},
			{"ip_port":"78.134.255.42:8080"},
			{"ip_port":"190.255.58.244:8080"},
			{"ip_port":"186.215.231.211:8080"},
			{"ip_port":"122.225.22.22:8080"},
			{"ip_port":"190.151.111.202:8080"},
			{"ip_port":"58.83.224.217:8080"},
			{"ip_port":"186.194.7.185:8080"},
			{"ip_port":"118.97.209.218:8080"},
			{"ip_port":"180.211.180.194:8080"},
			{"ip_port":"88.85.125.78:8080"},
			{"ip_port":"88.85.108.16:8080"},
			{"ip_port":"110.139.60.228:8080"},
			{"ip_port":"190.85.37.90:8080"},
			{"ip_port":"116.66.204.50:8080"},
			{"ip_port":"118.97.103.82:8080"},
			{"ip_port":"122.0.66.102:8080"},
			{"ip_port":"190.111.17.161:8080"},
			{"ip_port":"180.139.91.27:8080"},
			{"ip_port":"218.16.145.109:8080"},
			{"ip_port":"189.114.111.190:8080"},
			{"ip_port":"222.83.160.45:8080"},
			{"ip_port":"119.235.26.129:8080"},
			{"ip_port":"190.108.83.21:8080"},
			{"ip_port":"217.146.208.162:8080"},
			{"ip_port":"190.111.121.57:8080"},
			{"ip_port":"190.0.50.38:8080"},
			{"ip_port":"109.123.126.253:8080"},
			{"ip_port":"190.96.64.234:8080"},
			{"ip_port":"84.41.108.74:8080"},
			{"ip_port":"189.90.127.24:8080"},
			{"ip_port":"202.51.113.81:8080"},
			{"ip_port":"2.185.108.83:8080"},
			{"ip_port":"41.78.239.194:8080"},
			{"ip_port":"41.78.103.42:8080"},
			{"ip_port":"41.203.89.186:8080"},
			{"ip_port":"218.25.15.19:8080"},
			{"ip_port":"222.169.11.234:8080"},
			{"ip_port":"41.203.92.50:8080"},
			{"ip_port":"221.7.159.224:8080"},
			{"ip_port":"59.172.208.189:8080"},
			{"ip_port":"72.64.146.135:8080"},
			{"ip_port":"77.38.171.87:8080"},
			{"ip_port":"80.78.65.186:8080"},
			{"ip_port":"80.90.27.60:8080"},
			{"ip_port":"222.134.74.30:8080"},
			{"ip_port":"85.185.157.15:8080"},
			{"ip_port":"89.251.103.130:8080"},
			{"ip_port":"91.144.44.65:8080"},
			{"ip_port":"94.56.129.24:8080"},
			{"ip_port":"95.215.48.146:8080"},
			{"ip_port":"115.124.74.14:8080"},
			{"ip_port":"117.41.182.188:8080"},
			{"ip_port":"118.97.16.106:8080"},
			{"ip_port":"119.254.90.18:8080"},
			{"ip_port":"124.81.113.183:8080"},
			{"ip_port":"124.195.124.202:8080"},
			{"ip_port":"125.39.238.240:8080"},
			{"ip_port":"125.69.132.100:8080"},
			{"ip_port":"173.213.108.111:8080"},
			{"ip_port":"182.23.41.90:8080"},
			{"ip_port":"186.4.110.36:8080"},
			{"ip_port":"186.38.35.74:8080"},
			{"ip_port":"186.215.202.163:8080"},
			{"ip_port":"187.72.138.99:8080"},
			{"ip_port":"188.93.20.179:8080"},
			{"ip_port":"188.254.250.115:8080"},
			{"ip_port":"190.0.52.214:8080"},
			{"ip_port":"41.190.16.17:8080"},
			{"ip_port":"190.0.57.98:8080"},
			{"ip_port":"190.0.58.58:8080"},
			{"ip_port":"190.85.55.187:8080"},
			{"ip_port":"190.116.87.4:8080"},
			{"ip_port":"190.144.162.238:8080"},
			{"ip_port":"190.248.129.62:8080"},
			{"ip_port":"193.107.168.26:8080"},
			{"ip_port":"195.3.254.159:8080"},
			{"ip_port":"200.5.113.202:8080"},
			{"ip_port":"200.195.131.76:8080"},
			{"ip_port":"200.195.136.150:8080"},
			{"ip_port":"200.195.176.77:8080"},
			{"ip_port":"200.196.51.130:8080"},
			{"ip_port":"202.69.102.243:8080"},
			{"ip_port":"202.77.111.74:8080"},
			{"ip_port":"202.97.159.227:8080"},
			{"ip_port":"202.148.26.114:8080"},
			{"ip_port":"202.162.219.122:8080"},
			{"ip_port":"203.185.128.75:8080"},
			{"ip_port":"213.192.60.99:8080"},
			{"ip_port":"213.131.41.6:8080"},
			{"ip_port":"213.222.148.141:8080"},
			{"ip_port":"93.114.61.245:8080"},
			{"ip_port":"218.15.164.131:8080"},
			{"ip_port":"219.83.71.250:8080"},
			{"ip_port":"219.83.100.195:8080"},
			{"ip_port":"219.159.198.57:8080"},
			{"ip_port":"180.96.19.25:8080"},
			{"ip_port":"186.153.120.42:8080"},
			{"ip_port":"220.113.5.198:8080"},
			{"ip_port":"146.219.18.10:80"},
			{"ip_port":"79.170.50.25:80"},
			{"ip_port":"112.25.12.37:80"},
			{"ip_port":"201.2.240.54:80"},
			{"ip_port":"211.141.73.219:80"},
			{"ip_port":"186.201.231.178:80"},
			{"ip_port":"218.6.13.35:80"},
			{"ip_port":"221.194.179.39:80"},
			{"ip_port":"221.194.179.40:80"},
			{"ip_port":"200.66.85.215:80"},
			{"ip_port":"72.247.26.145:80"},
			{"ip_port":"222.89.226.18:80"},
			{"ip_port":"198.106.123.35:80"},
			{"ip_port":"200.27.114.228:80"},
			{"ip_port":"222.240.224.131:80"},
			{"ip_port":"123.50.56.206:80"},
			{"ip_port":"187.109.56.102:80"},
			{"ip_port":"59.57.15.71:80"},
			{"ip_port":"103.10.56.11:80"},
			{"ip_port":"61.135.208.184:80"},
			{"ip_port":"61.157.217.31:80"},
			{"ip_port":"88.41.153.22:80"},
			{"ip_port":"212.33.200.174:80"},
			{"ip_port":"202.115.207.25:80"},
			{"ip_port":"95.172.68.150:80"},
			{"ip_port":"116.236.205.100:80"},
			{"ip_port":"119.97.146.152:80"},
			{"ip_port":"221.12.89.189:80"},
			{"ip_port":"198.106.147.36:80"},
			{"ip_port":"112.25.12.39:80"},
			{"ip_port":"112.25.12.36:80"},
			{"ip_port":"122.72.0.227:80"},
			{"ip_port":"122.56.15.70:80"},
			{"ip_port":"210.19.83.197:80"},
			{"ip_port":"122.72.2.200:80"},
			{"ip_port":"202.3.247.118:80"},
			{"ip_port":"202.103.215.199:80"},
			{"ip_port":"202.105.233.40:80"},
			{"ip_port":"203.20.238.21:80"},
			{"ip_port":"210.19.83.202:80"},
			{"ip_port":"220.248.162.130:80"},
			{"ip_port":"221.7.228.138:80"},
			{"ip_port":"222.165.175.118:80"},
			{"ip_port":"130.94.148.99:80"},
			{"ip_port":"219.239.66.253:80"},
			{"ip_port":"119.46.68.228:80"},
			{"ip_port":"124.158.18.230:80"},
			{"ip_port":"59.46.173.75:80"},
			{"ip_port":"210.13.71.76:80"},
			{"ip_port":"61.234.169.138:80"},
			{"ip_port":"219.153.71.171:80"},
			{"ip_port":"210.13.71.77:80"},
			{"ip_port":"80.58.29.174:80"},
			{"ip_port":"80.58.250.68:80"},
			{"ip_port":"211.154.83.35:80"},
			{"ip_port":"222.92.116.10:80"},
			{"ip_port":"202.99.213.83:80"},
			{"ip_port":"211.7.242.67:80"},
			{"ip_port":"64.90.0.185:80"},
			{"ip_port":"64.209.134.133:80"},
			{"ip_port":"66.83.34.50:80"},
			{"ip_port":"101.44.1.22:80"},
			{"ip_port":"101.44.1.26:80"},
			{"ip_port":"206.180.107.103:80"},
			{"ip_port":"101.44.1.23:80"},
			{"ip_port":"117.211.123.62:80"},
			{"ip_port":"203.77.192.92:80"},
			{"ip_port":"119.6.73.235:80"},
			{"ip_port":"119.233.255.51:80"},
			{"ip_port":"119.233.255.60:80"},
			{"ip_port":"122.72.20.124:80"},
			{"ip_port":"122.72.20.125:80"},
			{"ip_port":"122.72.20.126:80"},
			{"ip_port":"122.72.20.127:80"},
			{"ip_port":"122.72.112.148:80"},
			{"ip_port":"122.72.112.166:80"},
			{"ip_port":"123.139.155.104:80"},
			{"ip_port":"123.139.155.106:80"},
			{"ip_port":"124.205.178.62:80"},
			{"ip_port":"196.4.89.15:80"},
			{"ip_port":"183.95.132.76:80"},
			{"ip_port":"200.43.29.2:80"},
			{"ip_port":"195.122.135.117:80"},
			{"ip_port":"122.252.60.10:80"},
			{"ip_port":"194.159.14.158:80"},
			{"ip_port":"202.186.33.164:80"},
			{"ip_port":"212.142.138.130:80"},
			{"ip_port":"221.176.168.178:80"},
			{"ip_port":"206.224.254.17:80"},
			{"ip_port":"207.144.99.112:80"},
			{"ip_port":"195.243.192.198:80"},
			{"ip_port":"211.5.227.122:80"},
			{"ip_port":"61.187.64.20:80"},
			{"ip_port":"167.192.8.9:80"},
			{"ip_port":"72.167.162.209:80"},
			{"ip_port":"174.36.27.137:80"},
			{"ip_port":"202.108.50.72:80"},
			{"ip_port":"203.171.227.115:80"},
			{"ip_port":"211.154.83.37:80"},
			{"ip_port":"211.161.152.98:80"},
			{"ip_port":"211.161.152.100:80"},
			{"ip_port":"211.161.152.105:80"},
			{"ip_port":"211.161.152.108:80"},
			{"ip_port":"211.161.152.107:80"},
			{"ip_port":"211.161.152.109:80"},
			{"ip_port":"97.87.24.113:80"},
			{"ip_port":"211.162.121.182:80"},
			{"ip_port":"173.201.41.198:80"},
			{"ip_port":"200.54.92.187:80"},
			{"ip_port":"211.167.112.15:80"},
			{"ip_port":"213.222.148.141:80"},
			{"ip_port":"115.236.98.109:80"},
			{"ip_port":"218.21.64.5:80"},
			{"ip_port":"186.136.72.41:80"},
			{"ip_port":"218.21.91.214:80"},
			{"ip_port":"217.218.98.13:80"},
			{"ip_port":"217.218.98.16:80"},
			{"ip_port":"218.23.49.155:80"},
			{"ip_port":"112.25.12.38:80"},
			{"ip_port":"218.29.54.105:80"},
			{"ip_port":"218.247.138.40:80"},
			{"ip_port":"219.234.130.38:80"},
			{"ip_port":"219.234.130.39:80"},
			{"ip_port":"220.195.192.172:80"},
			{"ip_port":"221.130.7.232:80"},
			{"ip_port":"222.141.199.150:80"},
			{"ip_port":"92.39.54.161:80"},
			{"ip_port":"221.130.162.48:81"},
			{"ip_port":"221.194.177.162:81"},
			{"ip_port":"210.14.70.21:81"},
			{"ip_port":"187.63.15.61:3128"},
			{"ip_port":"125.88.75.151:3128"},
			{"ip_port":"200.153.191.213:3128"},
			{"ip_port":"118.96.31.91:3128"},
			{"ip_port":"118.97.208.194:3128"},
			{"ip_port":"203.114.112.101:3128"},
			{"ip_port":"219.130.39.9:3128"},
			{"ip_port":"212.93.195.229:3128"},
			{"ip_port":"113.53.240.90:3128"},
			{"ip_port":"200.153.191.224:3128"},
			{"ip_port":"201.238.150.239:3128"},
			{"ip_port":"60.191.220.241:3128"},
			{"ip_port":"119.2.3.222:3128"},
			{"ip_port":"201.238.227.202:3128"},
			{"ip_port":"122.194.119.156:3128"},
			{"ip_port":"202.183.155.171:3128"},
			{"ip_port":"78.39.68.145:3128"},
			{"ip_port":"187.0.222.167:3128"},
			{"ip_port":"190.85.37.90:3128"},
			{"ip_port":"201.64.254.228:3128"},
			{"ip_port":"222.165.175.118:3128"},
			{"ip_port":"41.75.201.146:3128"},
			{"ip_port":"177.19.208.138:3128"},
			{"ip_port":"189.80.149.98:3128"},
			{"ip_port":"202.182.172.2:3128"},
			{"ip_port":"72.64.146.135:3128"},
			{"ip_port":"72.64.146.136:3128"},
			{"ip_port":"195.158.108.84:3128"},
			{"ip_port":"41.89.130.2:3128"},
			{"ip_port":"49.212.161.19:3128"},
			{"ip_port":"49.248.103.28:3128"},
			{"ip_port":"60.190.129.52:3128"},
			{"ip_port":"61.7.252.67:3128"},
			{"ip_port":"118.96.153.64:3128"},
			{"ip_port":"122.3.237.161:3128"},
			{"ip_port":"122.113.28.52:3128"},
			{"ip_port":"173.213.108.111:3128"},
			{"ip_port":"182.253.17.130:3128"},
			{"ip_port":"186.42.198.234:3128"},
			{"ip_port":"186.113.26.34:3128"},
			{"ip_port":"186.113.26.35:3128"},
			{"ip_port":"186.113.26.36:3128"},
			{"ip_port":"186.113.26.37:3128"},
			{"ip_port":"218.14.227.197:3128"},
			{"ip_port":"189.91.223.42:3128"},
			{"ip_port":"190.128.138.114:3128"},
			{"ip_port":"190.158.248.250:3128"},
			{"ip_port":"190.196.19.107:3128"},
			{"ip_port":"200.27.183.100:3128"},
			{"ip_port":"200.153.150.142:3128"},
			{"ip_port":"200.166.194.135:3128"},
			{"ip_port":"202.47.88.65:3128"},
			{"ip_port":"202.47.88.46:3128"},
			{"ip_port":"202.95.129.210:3128"},
			{"ip_port":"202.95.155.55:3128"},
			{"ip_port":"202.138.249.50:3128"},
			{"ip_port":"81.177.144.176:3128"},
			{"ip_port":"211.100.49.9:3128"},
			{"ip_port":"211.100.61.202:3128"},
			{"ip_port":"211.100.61.204:3128"},
			{"ip_port":"200.54.92.187:3128"},
			{"ip_port":"213.197.81.50:3128"},
			{"ip_port":"186.201.27.66:3128"},
			{"ip_port":"218.29.131.11:3128"},
			{"ip_port":"218.84.126.82:3128"},
			{"ip_port":"59.37.163.156:3128"},
			{"ip_port":"221.2.228.202:8000"},
			{"ip_port":"81.201.60.208:1080"},
			{"ip_port":"59.124.175.83:444"},
]

########NEW FILE########
__FILENAME__ = reload_proxy
# -*- coding: utf-8 -*-
import os
import re
import time
import datetime
from urllib2 import URLError, HTTPError
import sys
import urllib2

######################################
# Loader
######################################
class ProxyResource:
    def __init__(self):
        self.html_getter = WebPageDownloader();
        # self.filename = os.path.join(self.settings.resourcedir, "proxies.txt")

    def load_proxycn(self):
        '''
        '''
        # class resource
        psource_template = "http://www.proxycn.cn/html_proxy/port%s-%s.html"
        psource_ports = (8080, 80, 81, 3128, 8000, 1080, 444)
        p_nextpage = re.compile("<a href=[^>]*>下一页</a>")
        p_proxy = re.compile("<TR [^>]* onDblClick=\"clip\\('([\\d.]+):(\\d+)'\\);alert\\('已拷贝到剪贴板!'\\)[^\\x00]+?<TD class=\"list\">\\d+</TD><TD class=\"list\">(\\w*)</TD><TD class=\"list\">([^<]*)</TD><TD class=\"list\">([^<]*)</TD>")

        # another place
        proxy_urls = []
        proxy_urls.append("http://www.cnproxy.com/proxy1.html");
        proxy_urls.append("http://www.cnproxy.com/proxy2.html");
        proxy_urls.append("http://www.cnproxy.com/proxy3.html");
        proxy_urls.append("http://www.cnproxy.com/proxy4.html");
        proxy_urls.append("http://www.cnproxy.com/proxy5.html");
        proxy_urls.append("http://www.cnproxy.com/proxy6.html");
        proxy_urls.append("http://www.cnproxy.com/proxy7.html");
        proxy_urls.append("http://www.cnproxy.com/proxy8.html");
        proxy_urls.append("http://www.cnproxy.com/proxy9.html");
        proxy_urls.append("http://www.cnproxy.com/proxy10.html");
        p_proxy2 = re.compile("<tr><td>(\\d+\\.\\d+\\.\\d+\\.\\d+)<SCRIPT type=text/javascript>[^<]+?</SCRIPT></td><td>(.+?)</td><td>")

        proxies = []
        for port in psource_ports:
            page = 0
            hasNext = True
            while hasNext:
                page += 1
                purl = psource_template % (port, page)
                hasNext = self.__loadProxyFromURL(proxies, purl, p_proxy)
        # load from 2 place
        #        for url in proxy_urls:
        #            self.__loadProxyFromURL2(url, p_proxy2)

        return proxies

    def load_proxyServer(self):
        proxies = []
        url = "http://www.proxynova.com/get_proxies.php?proxy_type=2&btn_submit=Download+all+Proxies"
        self.__loadFromProxyServer(proxies, url)
        return proxies


    def __loadFromProxyServer(self, proxies, url):
        print "load proxies from %s " % url
        source = self.html_getter.getHtmlRetry(url, 3)
        source = unicode(source, "UTF-8").encode("UTF-8")
        print source        
        results = source.split("\n")[2:-2]
        count = 0
        if results is not None:
            for x in results:
                result = x.split(":")
                print "hi " + x
                ip = result[0]
                port = result[1]
                print "length:%s ip:%s  port:%s " % (len(result), ip, port)        
                model = ProxyModel(ip, port, "proxyServer")
                proxies.append(model)
                count += 1
        print "---proxyLoader---:load proxy from proxyServer get %s " % count
        
    def __loadProxyFromURL(self, proxies, url, pattern):
        '''Put model into ProxyModel, return has_next_page.'''

        p_nextpage = re.compile("<a href=[^>]*>下一页</a>")  # copy

        print "---proxyloader---:load proxy from %s" % url
        source = self.html_getter.getHtmlRetry(url, 3)
        source = unicode(source, "gbk").encode("UTF-8")
        foundNextPage = p_nextpage.search(source)

        results = pattern.findall(source)
        count = 0
        if results is not None:
            for result in results:
                ip = result[0]
                port = result[1]

                model = ProxyModel(ip, port, result[2].strip().lower())
                model.location = result[3]
                model.validate_date = result[4]
                proxies.append(model)
                count += 1
        print "---proxyloader---:load proxy from %s (get %s)" % (url, count)
        return foundNextPage


    def saveToFile(self, file_abspath, proxies):
        '''Save list of ProxyModel in a file.'''
        if os.path.exists(file_abspath):
            os.remove(file_abspath)
            print "$proxy/> remove %s." % file_abspath

        # write to file
        f = file(file_abspath, "w")
        for proxyModel in proxies:
            f.write(proxyModel.to_line())
            f.write("\n")
        f.close()
        print "$proxy/> write proxies to %s." % f.name

def test_load_proxycn():
    proxyRes = ProxyResource()
    results = proxyRes.load_proxycn()
    for model in results:
        print model
    proxyRes.saveToFile('/tmp/proxies.text', results);

def test_load_5uproxy_net():
    proxyRes = ProxyResource()
    results = proxyRes.load_proxycn()
    for model in results:
        print model
    proxyRes.saveToFile('/tmp/proxies.text', results);

######################################
# Model
######################################
class ProxyModel:
    def __init__(self, ip, port, type):
        self.ip = ip
        self.port = port
        self.type = type
        self.location = None
        self.validate_date = None

        '''
        这个值的作用：PriorityQueue的值。调整策略：
        值为0-100，默认值为50. 当第一次访问不通, 加30分钟不能访问的时间。错误计数＋1
        '''
        self.value = 50                # PriorityQueue Sort Value.
        self.invalid_time = None    # Wait time, don't use this proxy before this time.
        self.cnt_failed = 0
        self.cnt_success = 0
        self.cnt_banned = 0

    def take_a_rest(self, seconds=0, minutes=0):
        self.invalid_time = datetime.datetime.now() + datetime.timedelta(seconds=seconds, minutes=minutes)
        #print "%s - %s = %s" % (datetime.now(), self.invalid_time, datetime.now() > self.invalid_time)

    def isInRest(self):
        if self.invalid_time is None: return False
        if datetime.datetime.now() < self.invalid_time:
            return True
        return False

    def __str__(self):
        return "proxy(%s>%s:%s)" % (self.type, self.ip, self.port)

    def __cmp__(self, other):
        if not isinstance(other, ProxyModel):
            return (-1)
        return cmp(self.value, other.value)

    def to_line(self):
        return "%s:%s\t%s\t%s\t%s\t%s" % (self.ip, self.port, self.type, self.value, self.location, self.validate_date)


class ProxyKey:
    def __init__(self, ip, value):
        self.ip = ip
        self.value = 50

    def __str__(self):
        return "proxykey(%s, %s)" % (self.ip, self.value)

    def __cmp__(self, other):
        if not isinstance(other, ProxyKey):
            return (-1)
        return cmp(self.value, other.value)



######################################
# Web util
######################################
class WebPageDownloader:
    '''Retrieve html from internet by url (optional via a proxy).'''


    def __init__(self):
        self.default_timeout = 40

    #@return: Source Html of url. 
    def getHtmlRetry(self, url, retry=0):
        if retry <= 0: retry = 20 # default retry 20 times.
        html = None
        source = None
        retry_count = 0
        while retry > 0:
            retry -= 1
            retry_count += 1

            error_msg = None
            try:
                proxy_handler = urllib2.ProxyHandler({})
                opener = urllib2.build_opener(proxy_handler)
                opener.addheaders = REQUEST_HEADER
                html = opener.open(url)
                source = html.read()
            except HTTPError, e:
                error_msg = "Error [%s, %s]" % (e, "")
            except URLError, e:
                error_msg = "Error [%s, %s]" % (e.reason, "")
            except:
                error_msg = "Error [%s, %s]" % (sys.exc_info(), "")

            # on error
            if error_msg is not None:
                print "[X] HtmlGetter err:%s, retry:%s." % (error_msg, retry_count)

            if error_msg is None and self.validate_html(html):
                print "[v] success access webpage."
                return source

        #~ end while

        if retry == 0:
            return None # meet max retry times. also None
        print "should not be here."
        return None

    #
    # Validators
    #
    def validate_html(self, html):
        if html is None : return False
        if html.code in HTTPErrors:
            print "error found: %s: %s " % HTTPErrors[html.code]
            return False
        else:
            return True;




######################################
# Constants
######################################

# HTTP Errors found.
HTTPErrors = {
    #100: ('Continue', 'Request received, please continue'),
    #101: ('Switching Protocols', 'Switching to new protocol; obey Upgrade header'),

    #200: ('OK', 'Request fulfilled, document follows'),
    #201: ('Created', 'Document created, URL follows'),
    #202: ('Accepted', 'Request accepted, processing continues off-line'),
    #203: ('Non-Authoritative Information', 'Request fulfilled from cache'),
    #204: ('No Content', 'Request fulfilled, nothing follows'),
    #205: ('Reset Content', 'Clear input form for further input.'),
    #206: ('Partial Content', 'Partial content follows.'),

    #300: ('Multiple Choices', 'Object has several resources -- see URI list'),
    #301: ('Moved Permanently', 'Object moved permanently -- see URI list'),
    #302: ('Found', 'Object moved temporarily -- see URI list'),
    #303: ('See Other', 'Object moved -- see Method and URL list'),
    #304: ('Not Modified', 'Document has not changed since given time'),
    #305: ('Use Proxy', 'You must use proxy specified in Location to access this resource.'),
    #307: ('Temporary Redirect', 'Object moved temporarily -- see URI list'),

    400: ('Bad Request', 'Bad request syntax or unsupported method'),
    401: ('Unauthorized', 'No permission -- see authorization schemes'),
    402: ('Payment Required', 'No payment -- see charging schemes'),
    403: ('Forbidden', 'Request forbidden -- authorization will not help'),
    404: ('Not Found', 'Nothing matches the given URI'),
    405: ('Method Not Allowed', 'Specified method is invalid for this server.'),
    406: ('Not Acceptable', 'URI not available in preferred format.'),
    407: ('Proxy Authentication Required', 'You must authenticate with this proxy before proceeding.'),
    408: ('Request Timeout', 'Request timed out; try again later.'),
    409: ('Conflict', 'Request conflict.'),
    410: ('Gone', 'URI no longer exists and has been permanently removed.'),
    411: ('Length Required', 'Client must specify Content-Length.'),
    412: ('Precondition Failed', 'Precondition in headers is false.'),
    413: ('Request Entity Too Large', 'Entity is too large.'),
    414: ('Request-URI Too Long', 'URI is too long.'),
    415: ('Unsupported Media Type', 'Entity body in unsupported format.'),
    416: ('Requested Range Not Satisfiable', 'Cannot satisfy request range.'),
    417: ('Expectation Failed', 'Expect condition could not be satisfied.'),

    500: ('Internal Server Error', 'Server got itself in trouble'),
    501: ('Not Implemented', 'Server does not support this operation'),
    502: ('Bad Gateway', 'Invalid responses from another server/proxy.'),
    503: ('Service Unavailable', 'The server cannot process the request due to a high load'),
    504: ('Gateway Timeout', 'The gateway server did not receive a timely response'),
    505: ('HTTP Version Not Supported', 'Cannot fulfill request.'),
}



# download parameters
REQUEST_HEADER = [
    ('User-agent', 'Mozilla/5.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; iCafeMedia; .NET CLR 2.0.50727; CIBA)'),
    ('Accept', '*/*'),
    ('Accept-Charset', 'gzip, deflate'),
    ('Cookie', 'Tango_UserReference=38D8FF1624305B16496E9808; MTCCK=1; _csuid=48feeef505683659; cookmcnt=999; CID=1459382; cookMemberName=YunFan; cookMemberID=61448; savedEmail=liyunfan@genscriptcorp.com; DLDExec=OK; __utma=232384002.1655516880.1231991960.1231994793.1232000250.3; __utmb=232384002; __utmc=232384002; __utmz=232384002.1231991960.1.1.utmccn=(direct)|utmcsr=(direct)|utmcmd=(none)'),
#    ('Cookie', 'PREF=ID=ec3c076862e0b02b:U=cac317b6394968ad:LD=en:NR=10:NW=1:CR=2:TM=1330664473:LM=1335417048:GM=1:S=50GQeoV9xkTgrLrN; SS=DQAAAL0AAABvypgyHPqwSw0-rjn-XTDSRpzskVndpZ-rItaXJ7nFuYurPB1psFe9FyVm68eetmO04GnSt_yZ_bx3OZ_cEUcDpt8By3257clGanp-2YNVSvZHYZ5wyBmlh-Y1l8XWV3rLJaqyoXZ5gCZD_sZvrc6WRbGQedkXnMfKGak6rxPKeHp9E9otyK_d4zdLc-y5w2zf2dEQvOUwwxx-tsEkjs2Kd_I09h4qAUB81hORPnx78vFJZ917KoDAdOSzBc8TGWE; HSID=A0_nqPOdpqC042XTn; APISID=Ff1hrOKL1Z7wYBcf/AosfTiZgxii-N7NFo; GSP=ID=ec3c076862e0b02b:IN=e5ffd187aee42e82+8b9a455bd1c58d67:CF=4:UI=1:S=v2gHmeovcUDrBDUT; NID=60=AqhxHbvNGgXuytsC3ZH-hf5Egwye_6UoWoJRrHBSn0hlmb9Zpj5jc7Rf4i7U7gtdKaEk-G4wf_JpZFp6lPwSIhKHcp-MwPLYT1b5HRhBLUJoanR1paNxec6goOGAwPoMYvykytG22FZ4H7lImLG-V9EKGCE6qG8vTmN0NzCBDmCN4D77_B_6qUOi7QI; GDSESS=ID=74c0ae72a299270f:TM=1338800889:C=c:IP=166.111.134.53-:S=ADSvE-cUtJiPIgpC4XwuimeW8fipwOtgEA; SID=DQAAAC4BAAB31ZEj49yN182_gq2_DX6OqtCe9AGhXaD4FwRWtvNZmxBB-d4zctSWtsK_KNQIvH9vJ26dFG1usmUtt8_a1SOP8qFUCnkPnGuDSl8-jkHjBfEOAEKSQBZSOU3qKOsSB05JVTcGvyL3GYUrozeHpDs8GaAafpJdlxNln85ZGS_WPHgNbvSl5fbisovVV1xNPRhilfxFU5tTluitFWh_0L5dtPgFKmbiV4wKEAV2xTef5VDVAro3JjLgQRVJciNqLFJxvgYCJet6AaxQRKYGl97P_KCb3CRWzLE5c918YBEsgIHyxp-93tZS9xPryjkXSCsG1N95h2DzkaxeQaa13X-hwSohxk2feW8jkAlGLv2IL3J1RNzIg8c0EsMIdUXYE8nMgi-mCr5dJDEZXwWXa3nE'),
    ('Accept-Language', 'en')
]


######################################
# Main
######################################
if __name__ == "__main__":
    proxyRes = ProxyResource()
    results = proxyRes.load_proxycn()
    with open("proxy.py", 'w') as f:
        f.write("PROXIES = [\n")
        for model in results:
            print model
            f.write("\t\t\t{\"ip_port\":\"%s:%s\"},\n" % (model.ip, model.port))
        f.write("]\n")

########NEW FILE########
__FILENAME__ = settings
# Scrapy settings for linkedin project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/topics/settings.html
#
import os

BOT_NAME = 'linkedin'

SPIDER_MODULES = ['linkedin.spiders']
NEWSPIDER_MODULE = 'linkedin.spiders'

DOWNLOADER_MIDDLEWARES = {
    'linkedin.middleware.CustomHttpProxyMiddleware': 543,
    'linkedin.middleware.CustomUserAgentMiddleware': 545,
}

########### Item pipeline
ITEM_PIPELINES = [
                  "linkedin.pipelines.MongoDBPipeline",
]

MONGODB_SERVER = 'localhost'
MONGODB_PORT = 27017
MONGODB_DB = 'scrapy'
MONGODB_COLLECTION = 'person_profiles'
MONGODB_UNIQ_KEY = '_id'
###########

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'linkedin (+http://www.yourdomain.com)'

# Enable auto throttle
AUTOTHROTTLE_ENABLED = True

COOKIES_ENABLED = False

# Set your own download folder
DOWNLOAD_FILE_FOLDER = os.path.join(os.path.dirname(os.path.realpath(__file__)), "download_file")



########NEW FILE########
__FILENAME__ = LinkedinSpider
from scrapy.selector import HtmlXPathSelector
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.http import Request
from scrapy import log
from linkedin.items import LinkedinItem, PersonProfileItem
from os import path
from linkedin.parser.HtmlParser import HtmlParser
import os
import urllib
import string
from bs4 import UnicodeDammit
from linkedin.db import MongoDBClient

class LinkedinspiderSpider(CrawlSpider):
    name = 'LinkedinSpider'
    allowed_domains = ['linkedin.com']
    start_urls = [ "http://www.linkedin.com/directory/people/%s.html" % s 
                   for s in "abcdefghijklmnopqrstuvwxyz" ]

    rules = (
        #Rule(SgmlLinkExtractor(allow=r'Items/'), callback='parse_item', follow=True),
    )

    def __init__(self):
        pass
        
    def parse(self, response):
        """
        default parse method, rule is not useful now
        """
        response = response.replace(url=HtmlParser.remove_url_parameter(response.url))
        hxs = HtmlXPathSelector(response)
        index_level = self.determine_level(response)
        if index_level in [1, 2, 3, 4]:
            self.save_to_file_system(index_level, response)
            relative_urls = self.get_follow_links(index_level, hxs)
            if relative_urls is not None:
                for url in relative_urls:
                    yield Request(url, callback=self.parse)
        elif index_level == 5:
            personProfile = HtmlParser.extract_person_profile(hxs)
            linkedin_id = self.get_linkedin_id(response.url)
            linkedin_id = UnicodeDammit(urllib.unquote_plus(linkedin_id)).markup
            if linkedin_id:
                personProfile['_id'] = linkedin_id
                personProfile['url'] = UnicodeDammit(response.url).markup
                yield personProfile
    
    def determine_level(self, response):
        """
        determine the index level of current response, so we can decide wether to continue crawl or not.
        level 1: people/[a-z].html
        level 2: people/[A-Z][\d+].html
        level 3: people/[a-zA-Z0-9-]+.html
        level 4: search page, pub/dir/.+
        level 5: profile page
        """
        import re
        url = response.url
        if re.match(".+/[a-z]\.html", url):
            return 1
        elif re.match(".+/[A-Z]\d+.html", url):
            return 2
        elif re.match(".+/people/[a-zA-Z0-9-]+.html", url):
            return 3
        elif re.match(".+/pub/dir/.+", url):
            return 4
        elif re.match(".+/search/._", url):
            return 4
        elif re.match(".+/pub/.+", url):
            return 5
        return None
    
    def save_to_file_system(self, level, response):
        """
        save the response to related folder
        """
        if level in [1, 2, 3, 4, 5]:
            fileName = self.get_clean_file_name(level, response)
            if fileName is None:
                return
            
            fn = path.join(self.settings["DOWNLOAD_FILE_FOLDER"], str(level), fileName)
            self.create_path_if_not_exist(fn)
            if not path.exists(fn):
                with open(fn, "w") as f:
                    f.write(response.body)
    
    def get_clean_file_name(self, level, response):
        """
        generate unique linkedin id, now use the url
        """
        url = response.url
        if level in [1, 2, 3]:
            return url.split("/")[-1]
        
        linkedin_id = self.get_linkedin_id(url)
        if linkedin_id:
            return linkedin_id
        return None
        
    def get_linkedin_id(self, url):
        find_index = url.find("linkedin.com/")
        if find_index >= 0:
            return url[find_index + 13:].replace('/', '-')
        return None
        
    def get_follow_links(self, level, hxs):
        if level in [1, 2, 3]:
            relative_urls = hxs.select("//ul[@class='directory']/li/a/@href").extract()
            relative_urls = ["http://linkedin.com" + x for x in relative_urls]
            return relative_urls
        elif level == 4:
            relative_urls = relative_urls = hxs.select("//ol[@id='result-set']/li/h2/strong/a/@href").extract()
            relative_urls = ["http://linkedin.com" + x for x in relative_urls]
            return relative_urls

    def create_path_if_not_exist(self, filePath):
        if not path.exists(path.dirname(filePath)):
            os.makedirs(path.dirname(filePath))
            

########NEW FILE########

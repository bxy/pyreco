include any html static content here

============================================================
Modbus Implementations
============================================================

There are a few reference implementations that you can use
to test modbus serial

------------------------------------------------------------
pymodbus
------------------------------------------------------------

You can use pymodbus as a testing server by simply modifying
one of the run scripts supplied here. There is an
asynchronous version and a synchronous version (that really
differ in how mnay dependencies you are willing to have).
Regardless of which one you choose, they can be started
quite easily::

    ./asynchronous-server.py
    ./synchronous-server.py

Currently, each version has implementations of the following:

- modbus tcp
- modbus udp
- modbus udp binary
- modbus ascii serial
- modbus ascii rtu

------------------------------------------------------------
Modbus Driver
------------------------------------------------------------

Included are reference implementations of a modbus client
and server using the modbus driver library (as well as
the relevant source code). Both programs have a wealth of
options and can be used to test either side of your
application::

    tools/reference/diagslave -h # (server)
    tools/reference/modpoll -h # (client)

------------------------------------------------------------
jamod
------------------------------------------------------------

Jamod is a complete modbus implementation for the java jvm.
Included are a few simple reference servers using the
library, however, a great deal more can be produced using
it. I have not tested it, however, it may even be possible
to use this library in conjunction with jython to interop
between your python code and this library:

* http://jamod.sourceforge.net/

------------------------------------------------------------
nmodbus
------------------------------------------------------------

Although there is not any code included in this package,
nmodbus is a complete implementation of the modbus protocol
for the .net clr. The site has a number of examples that can
be tuned for your testing needs:

* http://code.google.com/p/nmodbus/

============================================================
Serial Loopback Testing
============================================================

In order to test the serial implementations, one needs to
create a loopback connection (virtual serial port). This can
be done in a number of ways.

------------------------------------------------------------
Linux
------------------------------------------------------------

For linux, there are three ways that are included with this
distribution.

One is to use the socat utility. The following will get one
going quickly::

    sudo apt-get install socat
    sudo socat PTY,link=/dev/pts/13, PTY,link=/dev/pts/14
    # connect the master to /dev/pts/13
    # connect the client to /dev/pts/14

Next, you can include the loopback kernel driver included in
the tools/nullmodem/linux directory::

    sudo ./run

------------------------------------------------------------
Windows
------------------------------------------------------------

For Windows, simply use the com2com application that is in
the directory tools/nullmodem/windows. Instructions are
included in the Readme.txt.

------------------------------------------------------------
Generic
------------------------------------------------------------

For most unix based systems, there is a simple virtual serial
forwarding application in the tools/nullmodem/ directory::

    make run
    # connect the master to the master output
    # connect the client to the client output

Or for a tried and true method, simply connect a null modem
cable between two of your serial ports and then simply reference
those.

============================================================
Contributed Implementations
============================================================

There are a few example implementations of custom utilities
interacting with the pymodbus library just to show what is
possible.

------------------------------------------------------------
SqlAlchemy Database Datastore Backend
------------------------------------------------------------

This module allows one to use any database available through
the sqlalchemy package as a datastore for the modbus server.
This could be useful to have many servers who have data they
agree upon and is transactional.

------------------------------------------------------------
Redis Datastore Backend
------------------------------------------------------------

This module allows one to use redis as a modbus server
datastore backend. This achieves the same thing as the
sqlalchemy backend, however, it is much more lightweight and
easier to set up.

------------------------------------------------------------
Binary Coded Decimal Payload
------------------------------------------------------------

This module allows one to write binary coded decimal data to
the modbus server using the payload encoder/decoder
interfaces.

------------------------------------------------------------
Message Generator and Parser
------------------------------------------------------------

These are two utilities that can be used to create a number
of modbus messages for any of the available protocols as well
as to decode the messages and print descriptive text about
them.

Also included are a number of request and response messages
in tx-messages and rx-messages.

============================================================================
 Pymodbus Functional Tests
============================================================================

Modbus Clients
--------------------------------------------------------------------------- 

The following can be run to validate the pymodbus clients against a running
modbus instance. For these tests, the following are used as references::

* jamod
* modpoll

Modbus Servers
--------------------------------------------------------------------------- 

The following can be used to create a null modem loopback for testing the
serial implementations::

* tty0tty (linux)
* com0com (windows)

Specialized Datastores
--------------------------------------------------------------------------- 

The following can be run to validate the pymodbus specializes datastores.
For these tests, the following are used as references:

* sqlite (for the database datastore)
* redis (for the redis datastore)
* modpoll (for the remote slave datastore)



tty0tty - linux null modem emulator 


This is the tty0tty directory tree:

  module         - linux kernel module null-modem
  pts		 - null-modem using ptys (without handshake lines)


pts: 

  When run connect two pseudo-ttys and show the connection names:

  (/dev/pts/1) <=> (/dev/pts/2) 

  the connection is:
  
  TX -> RX
  RX <- TX 	



module:

 The module is tested in kernel 2.6.26-2 (debian) and kernel 2.6.30

  When loaded, create 8 ttys interconnected:
  /dev/tnt0  <=>  /dev/tnt1 
  /dev/tnt2  <=>  /dev/tnt3 
  /dev/tnt4  <=>  /dev/tnt5 
  /dev/tnt6  <=>  /dev/tnt7 

  the connection is:
  
  TX   ->  RX
  RX   <-  TX 	
  RTS  ->  CTS
  CTS  <-  RTS
  DSR  <-  DTR
  CD   <-  DTR
  DTR  ->  DSR
  DTR  ->  CD
  

Requirements:

  for module build is necessary kernel-headers or kernel source
  ( in debian use apt-get install linux-image-2.6.26-2-amd64 for example)


For e-mail suggestions :  lcgamboa@yahoo.com

                 =============================
                 Null-modem emulator (com0com)
                 =============================

INTRODUCTION
============

The Null-modem emulator is an open source kernel-mode virtual serial
port driver for Windows, available freely under GPL license.
You can create an unlimited number of virtual COM port
pairs and use any pair to connect one application to another.
Each COM port pair provides two COM ports with default names starting
at CNCA0 and CNCB0. The output to one port is the input from other
port and vice versa.

Usually one port of the pair is used by Windows application that
requires a COM port to communicate with a device and other port is
used by device emulation program.

For example, to send/receive faxes over IP you can connect Windows Fax
application to CNCA0 port and t38modem (http://t38modem.sourceforge.net/)
to CNCB0 port. In this case the t38modem is a fax modem emulation program.

In conjunction with the hub4com the com0com allows you to
  - handle data and signals from a single real serial device by a number of
    different applications. For example, several applications can share data
    from one GPS device;
  - use real serial ports of remote computer like if they exist on the local
    computer (supports RFC 2217).

The homepage for com0com project is http://com0com.sourceforge.net/.


INSTALLING
==========

NOTE (Windows Vista/Windows Server 2008/Windows 7):
  Before installing/uninstalling the com0com driver or adding/removing/changing
  ports the User Account Control (UAC) should be turned off (require reboot).

NOTE (x64-based Windows Vista/Windows Server 2008/Windows 7):
  The com0com.sys is a test-signed kernel-mode driver that will not load by
  default. To enable test signing, enter command:

    bcdedit.exe -set TESTSIGNING ON

  and reboot the computer.

NOTE:
  Turning off UAC or enabling test signing will impair computer security.

Simply run the installer (setup.exe). An installation wizard will guide
you through the required steps.
If the Found New Hardware Wizard will pop up then
  - select "No, not this time" and click Next;
  - select "Install the software automatically (Recommended)" and click Next.
The one COM port pair with names CNCA0 and CNCB0 will be available on your
system after the installation.

You can add more pairs with the Setup Command Prompt:

  1. Launch the Setup Command Prompt shortcut.
  2. Enter the install command, for example:

       command> install - -

The system will create 3 new virtual devices. One of the devices has
name "com0com - bus for serial port pair emulator" and other two of
them have name "com0com - serial port emulator" and located on CNCAn
and CNCBn ports.

To get more info enter the help command, for example:

       command> help

Alternatively to setup ports you can invoke GUI-based setup utility by
launching Setup shortcut (Microsoft .NET Framework 2.0 is required).

TESTING
=======

  1. Start the HyperTerminal on CNCA0 port.
  2. Start the HyperTerminal on CNCB0 port.
  3. The output to CNCA0 port should be the input from CNCB0 port and
     vice versa.


UNINSTALLING
============

Simply launch the com0com's Uninstall shortcut in the Start Menu or remove
the "Null-modem emulator (com0com)" entry from the "Add/Remove Programs"
section in the Control Panel. An uninstallation wizard will guide
you through the required steps.

HINT: To uninstall the old version of com0com (distributed w/o installer)
install the new one and then uninstall it.


FAQs & HOWTOs
=============

Q. Is it possible to run com0com on Windows 9x platform?
A. No, it is not possible. You need Windows 2000 platform or newer.

Q. Is it possible to install or uninstall com0com silently (with no user
   intervention and no user interface)?
A. Yes, it's possible with /S option, for example:

     setup.exe /S
     "%ProgramFiles%\com0com\uninstall.exe" /S

   You can specify the installation directory with /D option, for example:

     setup.exe /S /D=C:\Program Files\com0com

   NOTE: Silent installation of com0com will not install any port pairs.

Q. Is it possible to change the names CNCA0 and CNCB0 to COM2 and COM3?
A. Yes, it's possible. To change the names:

   1. Launch the Setup Command Prompt shortcut.
   2. Enter the change commands, for example:

      command> change CNCA0 PortName=COM2
      command> change CNCB0 PortName=COM3

Q. The baud rate setting does not seem to make a difference: data is always
   transferred at the same speed. How to enable the baud rate emulation?
A. To enable baud rate emulation for transferring data from CNCA0 to CNCB0:

   1. Launch the Setup Command Prompt shortcut.
   2. Enter the change command, for example:

      command> change CNCA0 EmuBR=yes

Q. The HyperTerminal test succeeds, but I get a failure when trying to open the
   port with CreateFile("CNCA0", ...). GetLastError() returns ERROR_FILE_NOT_FOUND.
A. You must prefix the port name with the special characters "\\.\". Try to open
   the port with CreateFile("\\\\.\\CNCA0", ...).

Q. My application hangs during its startup when it sends anything to one paired
   COM port. The only way to unhang it is to start HyperTerminal, which is connected
   to the other paired COM port. I didn't have this problem with physical serial
   ports.
A. Your application can hang because receive buffer overrun is disabled by
   default. You can fix the problem by enabling receive buffer overrun for the
   receiving port. Also, to prevent some flow control issues you need to enable
   baud rate emulation for the sending port. So, if your application use port CNCA0
   and other paired port is CNCB0, then:

   1. Launch the Setup Command Prompt shortcut.
   2. Enter the change commands, for example:

      command> change CNCB0 EmuOverrun=yes
      command> change CNCA0 EmuBR=yes

Q. I have to write an application connected to one side of the com0com port pair,
   and I don't want users to 'see' all the virtual ports created by com0com, but
   only the really available ones.
A. if your application use port CNCB0 and other (used by users) paired port is CNCA0,
   then CNCB0 can be 'hidden' and CNCA0 can be 'shown' on opening CNCB0 by your
   application. To enable it:

   1. Launch the Setup Command Prompt shortcut.
   2. Enter the change commands:

      command> change CNCB0 ExclusiveMode=yes
      command> change CNCA0 PlugInMode=yes

Q. When I add a port pair, why does Windows XP always pops up a Found New Hardware
   Wizard? The drivers are already there and it can install them silently in the
   background and report when the device is ready.
A. It's because there is not signed com0com.cat catalog file. It can be created on
   your test computer by this way:

   1. Create a catalog file, for example:

      cd "C:\Program Files\com0com"
      inf2cat /driver:. /os:XP_X86

   2. Create a test certificate, for example:

      makecert -r -n "CN=com0com (test)" -sv com0com.pvk com0com.cer
      pvk2pfx -pvk com0com.pvk -spc com0com.cer -pfx com0com.pfx

   3. Sign the catalog file by test certificate, for example:

      signtool sign /v /f com0com.pfx com0com.cat

   4. Install a test certificate to the Trusted Root Certification Authorities
      certificate store and the Trusted Publishers certificate store, for example:

      certmgr -add com0com.cer -s -r localMachine root
      certmgr -add com0com.cer -s -r localMachine trustedpublisher

   The inf2cat tool can be installed with the Winqual Submission Tool.
   The makecert, pvk2pfx, signtool and certmgr tools can be installed with the
   Platform Software Development Kit (SDK).

Q. How to monitor and get the paired port settings (baud rate, byte size, parity
   and stop bits)?
A. It can be done with extended IOCTL_SERIAL_LSRMST_INSERT. See example in

   http://com0com.sourceforge.net/examples/LSRMST_INSERT/tstser.cpp

Q. To transfer state to CTS and DSR they wired to RTS and DTR. How to transfer
   state to DCD and RING?
A. The OUT1 can be wired to DCD and OUT2 to RING. Use extended
   IOCTL_SERIAL_SET_MODEM_CONTROL and IOCTL_SERIAL_GET_MODEM_CONTROL to change
   state of OUT1 and OUT2.  See example in

   http://com0com.sourceforge.net/examples/MODEM_CONTROL/tstser.cpp

Q. What version am I running?
A. In the device manager, the driver properties page shows the version and date
   of the com0com.inf file, while the driver details page shows a version of
   com0com.sys file. The version of com0com.sys file is the version that you
   are running.

Q. I'm able to use some application to talk to some hardware using com2tcp when
   both the com2tcp 'server' and 'client' are running on the same computer.
   When I try to move the client to a remote computer the application gives me
   a timeout message and has no settings to increase the timeout. How to fix
   the problem?
A. Try to ajust AddRTTO and AddRITO params for application's COM port:

   1. Launch the Setup Command Prompt shortcut.
   2. Enter the change command, for example:

      command> change CNCA0 AddRTTO=100,AddRITO=100

Q. I would like to be able to add, remove and rename virtual comm ports from my
   own custom application. Is there an API that I can use or some command line
   utility that will do the job?
A. The setupc.exe is a command line utility that will do the job. To get more
   info enter:

      setupc help

   BTW: The setupg.exe is a GUI wrapper for setupc.exe.

Q. I need to use com0com ports with an application that doesn't recognize
   com0com ports as "real" com ports. It does not see a com0com port even
   though I have changed it's name to COMx. Is there a com0com settings that
   will make the port appear to be a "real" com port?
A. No, there is not, but you can "deceive" the application this way:

   1. With the "Add/Remove Hardware" wizard install new standard serial port.
      You don't need a real serial hardware to do it. Select non conflicted
      IO/IRQ resources.
   2. With the "Device Manager" disable the newly created port (let it be
      COM4).
   3. Launch the Setup Command Prompt shortcut.
   4. Install the pair of ports, were one of them has name COM4, for example:

      command> install PortName=COM4 -

      Ignore a warning about the COM4 is "in use" (press Continue).

Q. Is it possible to configure the com0com to randomly corrupt the data? It
   would be nice to have this feature so that we can test our application
   robustness.
A. Yes, it's possible by setting EmuNoise parameter:

   1. Launch the Setup Command Prompt shortcut.
   2. Enter the change command, for example:

      command> change CNCA0 EmuNoise=0.00001,EmuBR=yes,EmuOverrun=yes
      command> change CNCB0 EmuNoise=0.00001,EmuBR=yes,EmuOverrun=yes

   Now each character frame (including idle frames) will be corrupted with
   probability 0.00001.

Q. What is the maximum number of port pairs that can be defined?
A. It depends from your system. The com0com itself has internal limit
   1000000 port pairs.

Q. In my application, users could be installing up to 250 com port pairs.
   Initially, the installation is fairly quick, but each additional com port
   generally takes longer to install than the previous one. It quickly
   becomes unacceptable for a user to be expected to wait for the installation.
A. It's because the installing of each next port pair requires to update driver
   for all installed pairs. You can speed up installing of multiple com port
   pairs by using install commands with --no-update option and finish them by
   update command, for example:

      command> --no-update install - -
      command> --no-update install - -
      ...
      command> --no-update install - -
      command> update

   Another example:

      > cd /D "%ProgramFiles%\com0com"
      > FOR /L %i IN (0,1,249) DO setupc --no-update install - -
      > setupc update

Q. I am using the 64-bit version of com0com and I am having trouble. I'd like
   to debug this, but I can not find any free serial port monitor software,
   like portmon that works with a 64-bit OS. Does anyone know of any?
A. You can try to use internal com0com's tracing for debuging:

      - get trace.reg file from com0com's source;
      - import trace.reg to the Registry;
      - reload driver (or reboot system);
      - do your tests and watch results in C:\com0com.log file.

   To disable tracing reinstall com0com or import trace_disable.reg to the
   Registry and reload driver.

Modbus Reference Implementations
============================================================

The following are used as reference implementations for the
various portions of the modbus protocol.

Checksum Calculations
------------------------------------------------------------

The two reference implementations for the lrc and crc computations
were taken from the appendix of the fieldtalk serial implementation
guide.

Reference Server and Client
------------------------------------------------------------

The two reference implementations were provided by the modbus
driver team (http://www.modbusdriver.com/modpoll.html). Attached
is the license that the binaries are released under.

.. note::

   The enclosed binaries will only work on linux 2.6,
   however the site does provide binaries for a number of
   alternate systems as well.

Code
------------------------------------------------------------

I believe that this is the code for the two supplied utilities:

* http://www.modbusdriver.com/doc/libmbusslave/examples.html
* http://dankohn.info/projects/Fieldpoint_module/fieldtalk/samples/diagslave/diagslave.cpp

============================================================
Summary
============================================================

Pymodbus is a full Modbus protocol implementation using twisted for its
asynchronous communications core.  It can also be used without any third
party dependencies (aside from pyserial) if a more lightweight project is
needed.  Furthermore, it should work fine under any python version > 2.3
with a python 3.0 branch currently being maintained as well.

============================================================
Features
============================================================

------------------------------------------------------------
Client Features
------------------------------------------------------------

  * Full read/write protocol on discrete and register
  * Most of the extended protocol (diagnostic/file/pipe/setting/information)
  * TCP, UDP, Serial ASCII, Serial RTU, and Serial Binary
  * asynchronous(powered by twisted) and synchronous versions
  * Payload builder/decoder utilities

------------------------------------------------------------
Server Features
------------------------------------------------------------

  * Can function as a fully implemented modbus server
  * TCP, UDP, Serial ASCII, Serial RTU, and Serial Binary
  * asynchronous(powered by twisted) and synchronous versions
  * Full server control context (device information, counters, etc)
  * A number of backing contexts (database, redis, a slave device)

============================================================
Use Cases
============================================================

Although most system administrators will find little need for a Modbus
server on any modern hardware, they may find the need to query devices on
their network for status (PDU, PDR, UPS, etc).  Since the library is written
in python, it allows for easy scripting and/or integration into their existing
solutions.

Continuing, most monitoring software needs to be stress tested against
hundreds or even thousands of devices (why this was originally written), but
getting access to that many is unwieldy at best.  The pymodbus server will allow
a user to test as many devices as their base operating system will allow (*allow*
in this case means how many Virtual IP addresses are allowed).

For more information please browse the project documentation:
http://readthedocs.org/docs/pymodbus/en/latest/index.html

------------------------------------------------------------
Example Code
------------------------------------------------------------

For those of you that just want to get started fast, here you go::

    from pymodbus.client.sync import ModbusTcpClient
    
    client = ModbusTcpClient('127.0.0.1')
    client.write_coil(1, True)
    result = client.read_coils(1,1)
    print result.bits[0]
    client.close()

For more advanced examples, check out the examples included in the
respository. If you have created any utilities that meet a specific
need, feel free to submit them so others can benefit.

Also, if you have questions, please ask them on the mailing list
so that others can benefit from the results and so that I can
trace them. I get a lot of email and sometimes these requests
get lost in the noise: http://groups.google.com/group/pymodbus

------------------------------------------------------------
Installing
------------------------------------------------------------

You can install using pip or easy install by issuing the following
commands in a terminal window (make sure you have correct
permissions or a virtualenv currently running)::

    easy_install -U pymodbus
    pip install  -U pymodbus

Otherwise you can pull the trunk source and install from there::

    git clone git://github.com/bashwork/pymodbus.git
    cd pymodbus
    python setup.py install

Either method will install all the required dependencies
(at their appropriate versions) for your current python distribution.

If you would like to install pymodbus without the twisted dependency,
simply edit the setup.py file before running easy_install and comment
out all mentions of twisted.  It should be noted that without twisted,
one will only be able to run the synchronized version as the
asynchronous versions uses twisted for its event loop.

------------------------------------------------------------
Current Work In Progress
------------------------------------------------------------

Since I don't have access to any live modbus devices anymore
it is a bit hard to test on live hardware. However, if you would
like your device tested, I accept devices via mail or by IP address.

That said, the current work mainly involves polishing the library as
I get time doing such tasks as:

  * Fixing bugs/feature requests
  * Architecture documentation
  * Functional testing against any reference I can find
  * The remaining edges of the protocol (that I think no one uses)
   
------------------------------------------------------------
License Information
------------------------------------------------------------

Pymodbus is built on top of code developed from/by:
  * Copyright (c) 2001-2005 S.W.A.C. GmbH, Germany.
  * Copyright (c) 2001-2005 S.W.A.C. Bohemia s.r.o., Czech Republic.
  * Hynek Petrak <hynek@swac.cz>
  * Twisted Matrix

Released under the BSD License


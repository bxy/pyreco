## Authors

This project was started and continues to be led by Brian E. Granger
(ellisonbg AT gmail DOT com). Min Ragan-Kelley (benjaminrk AT gmail DOT
com) is the primary developer of pyzmq at this time.

The following people have contributed to the project:

- Alexander Else (alexander DOT else AT team DOT telstra DOT com)
- Alexander Pyhalov (apyhalov AT gmail DOT com)
- Alexandr Emelin (frvzmb AT gmail DOT com)
- Andrea Crotti (andrea DOT crotti DOT 0 AT gmail DOT com)
- Andrew Gwozdziewycz (git AT apgwoz DOT com)
- Baptiste Lepilleur (baptiste DOT lepilleur AT gmail DOT com)
- Brandyn A. White (bwhite AT dappervision DOT com)
- Brian E. Granger (ellisonbg AT gmail DOT com)
- Carlos A. Rocha (carlos DOT rocha AT gmail DOT com)
- Chris Laws (clawsicus AT gmail DOT com)
- Christian Wyglendowski (christian AT bu DOT mp)
- Christoph Gohlke (cgohlke AT uci DOT edu)
- Cyril Holweck (cyril DOT holweck AT free DOT fr)
- Dan Colish (dcolish AT gmail DOT com)
- Daniel Lundin (dln AT eintr DOT org)
- Daniel Truemper (truemped AT googlemail DOT com)
- Douglas Creager (douglas DOT creager AT redjack DOT com)
- Eduardo Stalinho (eduardooc DOT 86 AT gmail DOT com)
- Erick Tryzelaar (erick DOT tryzelaar AT gmail DOT com)
- Erik Tollerud (erik DOT tollerud AT gmail DOT com)
- FELD Boris (lothiraldan AT gmail DOT com)
- Fantix King (fantix DOT king AT gmail DOT com)
- Felipe Cruz (felipecruz AT loogica DOT net)
- Fernando Perez (Fernando DOT Perez AT berkeley DOT edu)
- Frank Wiles (frank AT revsys DOT com)
- Gavrie Philipson (gavriep AT il DOT ibm DOT com)
- Godefroid Chapelle (gotcha AT bubblenet DOT be)
- Greg Ward (greg AT gerg DOT ca)
- Guido Goldstein (github AT a-nugget DOT de)
- Ivo Danihelka (ivo AT danihelka DOT net)
- Iyed (iyed DOT bennour AT gmail DOT com)
- Jim Garrison (jim AT garrison DOT cc)
- John Gallagher (johnkgallagher AT gmail DOT com)
- Julian Taylor (jtaylor DOT debian AT googlemail DOT com)
- Justin Bronder (jsbronder AT gmail DOT com)
- Justin Riley (justin DOT t DOT riley AT gmail DOT com)
- Marc Abramowitz (marc AT marc-abramowitz DOT com)
- Michel Pelletier (pelletier DOT michel AT gmail DOT com)
- Michel Zou (xantares09 AT hotmail DOT com)
- Min Ragan-Kelley (benjaminrk AT gmail DOT com)
- Nicholas Piël (nicholas AT nichol DOT as)
- Nick Pellegrino (npellegrino AT mozilla DOT com)
- Nicolas Delaby (nicolas DOT delaby AT ezeep DOT com)
- Ondrej Certik (ondrej AT certik DOT cz)
- Paul Colomiets (paul AT colomiets DOT name)
- Pawel Jasinski (pawel DOT jasinski AT gmail DOT com)
- Phus Lu (phus DOT lu AT gmail DOT com)
- Robert Buchholz (rbu AT goodpoint DOT de)
- Robert Jordens (jordens AT gmail DOT com)
- Ryan Cox (ryan DOT a DOT cox AT gmail DOT com)
- Ryan Kelly (ryan AT rfk DOT id DOT au)
- Scott Maxwell (scott AT codecobblers DOT com)
- Scott Sadler (github AT mashi DOT org)
- Stefan Friesel (sf AT cloudcontrol DOT de)
- Stefan van der Walt (stefan AT sun DOT ac DOT za)
- Stephen Diehl (stephen DOT m DOT diehl AT gmail DOT com)
- Thomas Kluyver (takowl AT gmail DOT com)
- Thomas Spura (tomspur AT fedoraproject DOT org)
- Tigger Bear (Tigger AT Tiggers-Mac-mini DOT local)
- Torsten Landschoff (torsten DOT landschoff AT dynamore DOT de)
- Vadim Markovtsev (v DOT markovtsev AT samsung DOT com)
- Zbigniew Jędrzejewski-Szmek (zbyszek AT in DOT waw DOT pl)
- hugo shi (hugoshi AT bleb2 DOT (none))
- jdgleeson (jdgleeson AT mac DOT com)
- kyledj (kyle AT bucebuce DOT com)
- spez (steve AT hipmunk DOT com)
- stu (stuart DOT axon AT jpcreative DOT co DOT uk)
- xantares (xantares AT fujitsu-l64 DOT (none))

as reported by:

    git log --all --format='- %aN (%aE)' | sort -u | sed 's/@/ AT /1' | sed -e 's/\./ DOT /g'

with some adjustments.

### Not in git log

- Brandon Craig-Rhodes (brandon AT rhodesmill DOT org)
- Eugene Chernyshov (chernyshov DOT eugene AT gmail DOT com)
- Craig Austin (craig DOT austin AT gmail DOT com)

### gevent\_zeromq, now zmq.green

- Travis Cline (travis DOT cline AT gmail DOT com)
- Ryan Kelly (ryan AT rfk DOT id DOT au)
- Zachary Voase (z AT zacharyvoase DOT com)


# Opening an Issue

For a good bug report:

1. [Search][] for existing Issues, both on GitHub and in general with Google/Stack Overflow before posting a duplicate question.
2. Update to pyzmq master, if possible, especially if you are already using git. It's
   possible that the bug you are about to report has already been fixed.

Many things reported as pyzmq Issues are often just libzmq-related,
and don't have anything to do with pyzmq itself.
These are better directed to [zeromq-dev][].

When making a bug report, it is helpful to tell us as much as you can about your system
(such as pyzmq version, libzmq version, Python version, OS Version, how you built/installed pyzmq and libzmq, etc.)

The basics:

```python
import sys
import zmq

print "libzmq-%s" % zmq.zmq_version()
print "pyzmq-%s" % zmq.pyzmq_version()
print "Python-%s" % sys.version
```

Which will give something like:

    libzmq-3.3.0
    pyzmq-2.2dev
    Python-2.7.2 (default, Jun 20 2012, 16:23:33) 
    [GCC 4.2.1 Compatible Apple Clang 4.0 (tags/Apple/clang-418.0.60)]

[search]: https://github.com/zeromq/pyzmq/issues
[zeromq-dev]: mailto:zeromq-dev@zeromq.org


# Licensing and contributing to PyZMQ

PyZMQ uses different licenses for different parts of the code.

The 'core' of PyZMQ (located in zmq/core) is licensed under LGPLv3.
This just  means that if you make any changes to how that code works,
you must release  those changes under the LGPL.
If you just *use* pyzmq, then you can use any license you want for your own code.

We don't feel that the restrictions imposed by the LGPL make sense for the 
'non-core' functionality in pyzmq (derivative code must *also* be LGPL or GPL), 
especially for examples and utility code, so we have relicensed all 'non-core' 
code under the more permissive BSD (specifically Modified BSD aka New BSD aka 
3-clause BSD), where possible. This means that you can copy this code and build 
your own apps without needing to license your own code with the LGPL or GPL.

## Your contributions

**Pull Requests are welcome!**

When you contribute to PyZMQ, your contributions are made under the same 
license as the file you are working on. Any new, original code should be BSD 
licensed.

We don't enforce strict style, but when in doubt [PEP8][] is a good guideline.
The only thing we really don't like is mixing up 'cleanup' in real work.

Examples are copyright their respective authors, and BSD unless otherwise 
specified by the author. You can LGPL (or GPL or MIT or Apache, etc.) your own new 
examples if you like, but we strongly encourage using the default BSD license.

[PEP8]: http://www.python.org/dev/peps/pep-0008

## Inherited licenses in pyzmq

Some code outside the core is taken from other open-source projects, and 
inherits that project's license.

* zmq/eventloop contains files inherited and adapted from [tornado][], and inherits the Apache license

* zmq/ssh/forward.py is from [paramiko][], and inherits LGPL

* zmq/devices/monitoredqueue.pxd is derived from the zmq_device function in 
libzmq, and inherits LGPL

* perf examples are (c) iMatix, and LGPL

[tornado]: http://www.tornadoweb.org
[paramiko]: http://www.lag.net/paramiko
These examples use Python2 syntax.  Due to the change in Python from bytestring str objects
to unicode str objects, 2to3 does not perform an adequate transform of the code.  Examples
can be valid on both Python2.5 and Python3, but such code is less readable than it should be.

As a result, the Python3 examples are kept in a separate repo:

https://github.com/minrk/pyzmq-py3k-examples


The differences are very small, but important.
# PyZMQ: Python bindings for ØMQ

[![Build Status](https://travis-ci.org/zeromq/pyzmq.svg?branch=master)](https://travis-ci.org/zeromq/pyzmq)

This package contains Python bindings for [ØMQ](http://www.zeromq.org).
ØMQ is a lightweight and fast messaging implementation.

PyZMQ should work with any Python ≥ 2.6 (including Python 3), as well as PyPy.
The Cython backend used by CPython supports libzmq ≥ 2.1.4 (including 3.2.x and 4.x),
but the CFFI backend used by PyPy only supports libzmq ≥ 3.2.2 (including 4.x).

For a summary of changes to pyzmq, see our
[changelog](http://zeromq.github.io/pyzmq/changelog.html).

### ØMQ 3.x, 4.x

PyZMQ ≥ 2.2.0 fully supports the 3.x and 4.x APIs of libzmq,
developed at [zeromq/libzmq](https://github.com/zeromq/libzmq).
No code to change, no flags to pass,
just build pyzmq against the latest and it should work.

PyZMQ does not support the old libzmq 2 API on PyPy.

## Documentation

See PyZMQ's Sphinx-generated
[documentation](http://zeromq.github.com/pyzmq) on GitHub for API
details, and some notes on Python and Cython development. If you want to
learn about using ØMQ in general, the excellent [ØMQ
Guide](http://zguide.zeromq.org/py:all) is the place to start, which has a
Python version of every example. We also have some information on our
[wiki](https://github.com/zeromq/pyzmq/wiki).

## Downloading

Unless you specifically want to develop PyZMQ, we recommend downloading
the PyZMQ source code, eggs, or wheels from
[PyPI](http://pypi.python.org/pypi/pyzmq). On Windows, you can get `.exe` installers
from [Christoph Gohlke](http://www.lfd.uci.edu/~gohlke/pythonlibs/#pyzmq).

You can also get the latest source code from our GitHub repository, but
building from the repository will require that you install Cython
version 0.16 or later.

## Building and installation

For more detail on building pyzmq, see [our Wiki](https://github.com/zeromq/pyzmq/wiki/Building-and-Installing-PyZMQ).

We build eggs and wheels for OS X and Windows, so you can get a binary on those platforms with either:

    pip install pyzmq

or

    easy_install pyzmq

but compiling from source with `pip install pyzmq` should work in most environments.

When compiling pyzmq (e.g. installing with pip on Linux),
it is generally recommended that zeromq be installed separately, via homebrew, apt, yum, etc.
If this is not available, pyzmq will *try* to build libzmq as a Python Extension,
though this is not guaranteed to work.

To build pyzmq from the git repo (including release tags) requires Cython.

## Old versions

For libzmq 2.0.x, use pyzmq release 2.0.10.1.

pyzmq-2.1.11 was the last version of pyzmq to support Python 2.5,
and pyzmq ≥ 2.2.0 requires Python ≥ 2.6.
pyzmq-13.0.0 introduces PyPy support via CFFI, which only supports libzmq-3.2.2 and newer.

PyZMQ releases ≤ 2.2.0 matched libzmq versioning, but this is no longer the case,
starting with PyZMQ 13.0.0 (it was the thirteenth release, so why not?).
PyZMQ ≥ 13.0 follows semantic versioning conventions accounting only for PyZMQ itself.



OpenStack Nova Documentation README
===================================
Included documents:

- developer reference guide(devref)
- man pages

Dependencies
------------
Building this documentation can be done in a regular Nova development
environment, such as the virtualenv created by ``run_tests.sh`` or
``tools/install_venv.py``.  A leaner but sufficient environment can be
created by starting with one that is suitable for running Nova (such
as the one created by DevStack) and then using pip to install
oslosphinx.

Building the docs
-----------------
From the root nova directory::

  python setup.py build_sphinx

Building just the man pages
---------------------------
from the root nova directory::

  python setup.py build_sphinx -b man


Installing the man pages
-------------------------
After building the man pages, they can be found in ``doc/build/man/``.
You can install the man page onto your system by following the following steps:

Example for ``nova-scheduler``::

  mkdir /usr/local/man/man1
  install -g 0 -o 0 -m 0644 doc/build/man/nova-scheduler.1  /usr/local/man/man1/nova-scheduler.1
  gzip /usr/local/man/man1/nova-scheduler.1
  man nova-scheduler

To generate the sample nova.conf file, run the following
command from the top level of the nova directory:

tox -egenconfig

This is a database migration repository.

More information at
http://code.google.com/p/sqlalchemy-migrate/

openstack-common
----------------

A number of modules from openstack-common are imported into this project.

These modules are "incubating" in openstack-common and are kept in sync
with the help of openstack-common's update.py script. See:

  https://wiki.openstack.org/wiki/Oslo#Syncing_Code_from_Incubator

The copy of the code should never be directly modified here. Please
always update openstack-common first and then run the script to copy
the changes across.

Api Samples
===========

This part of the tree contains templates for API samples. The
documentation in doc/api_samples is completely autogenerated from the
tests in this directory.

To add a new api sample, add tests for the common passing and failing
cases in this directory for your extension, and modify test_samples.py
for your tests. There should be both JSON and XML tests included.

Then run the following command:

  GENERATE_SAMPLES=True tox -epy27 nova.tests.integrated

Which will create the files on doc/api_samples.

If new tests are added or the .tpl files are changed due to bug fixes, the
samples must be regenerated so they are in sync with the templates, as
there is an additional test which reloads the documentation and
ensures that it's in sync.

Debugging sample generation
---------------------------

If a .tpl is changed, its matching .xml and .json must be removed
else the samples won't be generated. If an entirely new extension is
added, a directory for it must be created before its samples will
be generated.

=====================================
OpenStack Nova Testing Infrastructure
=====================================

This README file attempts to provide current and prospective contributors with
everything they need to know in order to start creating unit tests for nova.

Note: the content for the rest of this file will be added as the work items in
the following blueprint are completed:
  https://blueprints.launchpad.net/nova/+spec/consolidate-testing-infrastructure


Test Types: Unit vs. Functional vs. Integration
-----------------------------------------------

TBD

Writing Unit Tests
------------------

TBD

Using Fakes
~~~~~~~~~~~

TBD

test.TestCase
-------------
The TestCase class from nova.test (generally imported as test) will
automatically manage self.stubs using the stubout module and self.mox
using the mox module during the setUp step. They will automatically
verify and clean up during the tearDown step.

If using test.TestCase, calling the super class setUp is required and
calling the super class tearDown is required to be last if tearDown
is overridden.

Writing Functional Tests
------------------------

TBD

Writing Integration Tests
-------------------------

TBD

Tests and Exceptions
--------------------
A properly written test asserts that particular behavior occurs. This can
be a success condition or a failure condition, including an exception.
When asserting that a particular exception is raised, the most specific
exception possible should be used.

In particular, testing for Exception being raised is almost always a
mistake since it will match (almost) every exception, even those
unrelated to the exception intended to be tested.

This applies to catching exceptions manually with a try/except block,
or using assertRaises().

Example::

    self.assertRaises(exception.InstanceNotFound, db.instance_get_by_uuid,
                      elevated, instance_uuid)

If a stubbed function/method needs a generic exception for testing
purposes, test.TestingException is available.

Example::

    def stubbed_method(self):
        raise test.TestingException()
    self.stubs.Set(cls, 'inner_method', stubbed_method)

    obj = cls()
    self.assertRaises(test.TestingException, obj.outer_method)


Stubbing and Mocking
--------------------

Whenever possible, tests SHOULD NOT stub and mock out the same function.

If it's unavoidable, tests SHOULD define stubs before mocks since the
`TestCase` cleanup routine will un-mock before un-stubbing. Doing otherwise
results in a test that leaks stubbed functions, causing hard-to-debug
interference between tests [1]_.

If a mock must take place before a stub, any stubs after the mock call MUST be
manually unset using `self.cleanUp` calls within the test.


.. [1] https://bugs.launchpad.net/nova/+bug/1180671

General Bare-metal Provisioning README
======================================

:Authors:
  [USC/ISI] Mikyung Kang <mkkang@isi.edu>, David Kang <dkang@isi.edu>

  [NTT DOCOMO] Ken Igarashi <igarashik@nttdocomo.co.jp>

  [VirtualTech Japan Inc.] Arata Notsu <notsu@virtualtech.jp>
:Date:   2012-08-02
:Version: 2012.8
:Wiki: http://wiki.openstack.org/GeneralBareMetalProvisioningFramework

Code changes
------------

::

  nova/nova/virt/baremetal/*
  nova/nova/virt/driver.py
  nova/nova/tests/baremetal/*
  nova/nova/tests/compute/test_compute.py
  nova/nova/compute/manager.py
  nova/nova/compute/resource_tracker.py
  nova/nova/manager.py
  nova/nova/scheduler/driver.py
  nova/nova/scheduler/filter_scheduler.py
  nova/nova/scheduler/host_manager.py
  nova/nova/scheduler/baremetal_host_manager.py
  nova/bin/bm_deploy_server
  nova/bin/nova-bm-manage

Additional setting for bare-metal provisioning [nova.conf]
----------------------------------------------------------

::

  # baremetal database connection
  baremetal_sql_connection = mysql://$ID:$Password@$IP/nova_bm

  # baremetal compute driver
  compute_driver = nova.virt.baremetal.driver.BareMetalDriver
  baremetal_driver = {nova.virt.baremetal.tilera.Tilera | nova.virt.baremetal.pxe.PXE}
  power_manager = {nova.virt.baremetal.tilera_pdu.Pdu | nova.virt.baremetal.ipmi.Ipmi}

  # flavor_extra_specs this baremetal compute
  flavor_extra_specs = cpu_arch:{tilepro64 | x86_64 | arm}

  # TFTP root
  baremetal_tftp_root = /tftpboot

  # baremetal scheduler host manager
  scheduler_host_manager = nova.scheduler.baremetal_host_manager.BaremetalHostManager


Non-PXE (Tilera) Bare-metal Provisioning
----------------------------------------

1. tilera-bm-instance-creation.rst

2. tilera-bm-installation.rst

PXE Bare-metal Provisioning
---------------------------

1. pxe-bm-instance-creation.rst

2. pxe-bm-installation.rst


Hyper-V Volumes Management
=============================================

To enable the  volume features, the first thing that needs to be done is to
enable the iSCSI service on the Windows compute nodes and set it to start
automatically.

sc config msiscsi start= auto
net start msiscsi

In Windows Server 2012, it's important to execute the following commands to
prevent having the volumes being online by default:

diskpart
san policy=OfflineAll
exit

How to check if your iSCSI configuration is working properly:

On your OpenStack controller:

1. Create a volume with e.g. "nova volume-create 1" and note the generated
volume id

On Windows:

2. iscsicli QAddTargetPortal <your_iSCSI_target>
3. iscsicli ListTargets

The output should contain the iqn related to your volume:
iqn.2010-10.org.openstack:volume-<volume_id>

How to test Boot from volume in Hyper-V from the OpenStack dashboard:

1. Fist of all create a volume
2. Get the volume ID of the created volume
3. Upload and untar to the Cloud controller the next VHD image:
http://dev.opennebula.org/attachments/download/482/ttylinux.vhd.gz
4. sudo dd if=/path/to/vhdfileofstep3 
of=/dev/nova-volumes/volume-XXXXX <- Related to the ID of step 2
5. Launch an instance from any image (this is not important because we are
just booting from a volume) from the dashboard, and don't forget to select
boot from volume and select the volume created in step2. Important: Device
name must be "vda".

This directory contains files that are required for the XenAPI support.
They should be installed in the XenServer / Xen Cloud Platform dom0.

If you install them manually, you will need to ensure that the newly
added files are executable. You can do this by running the following
command (from dom0):

chmod a+x /etc/xapi.d/plugins/*

OpenStack Nova README
=====================

OpenStack Nova provides a cloud computing fabric controller,
supporting a wide variety of virtualization technologies,
including KVM, Xen, LXC, VMware, and more. In addition to
its native API, it includes compatibility with the commonly
encountered Amazon EC2 and S3 APIs.

OpenStack Nova is distributed under the terms of the Apache
License, Version 2.0. The full terms and conditions of this
license are detailed in the LICENSE file.

Nova primarily consists of a set of Python daemons, though
it requires and integrates with a number of native system
components for databases, messaging and virtualization
capabilities.

To keep updated with new developments in the OpenStack project
follow `@openstack <http://twitter.com/openstack>`_ on Twitter.

To learn how to deploy OpenStack Nova, consult the documentation
available online at:

   http://docs.openstack.org

For information about the different compute (hypervisor) drivers
supported by Nova, read this page on the wiki:

   https://wiki.openstack.org/wiki/HypervisorSupportMatrix

In the unfortunate event that bugs are discovered, they should
be reported to the appropriate bug tracker. If you obtained
the software from a 3rd party operating system vendor, it is
often wise to use their own bug tracker for reporting problems.
In all other cases use the master OpenStack bug tracker,
available at:

   http://bugs.launchpad.net/nova

Developers wishing to work on the OpenStack Nova project should
always base their work on the latest Nova code, available from
the master GIT repository at:

   https://git.openstack.org/cgit/openstack/nova

Developers should also join the discussion on the mailing list,
at:

   http://lists.openstack.org/cgi-bin/mailman/listinfo/openstack-dev

Any new code must follow the development guidelines detailed
in the HACKING.rst file, and pass all unit tests. Further
developer focused documentation is available at:

   http://docs.openstack.org/developer/nova/

For information on how to contribute to Nova, please see the
contents of the CONTRIBUTING.rst file.

-- End of broadcast

This generate_sample.sh tool is used to generate etc/nova/nova.conf.sample

Run it from the top-level working directory i.e.

  $> ./tools/config/generate_sample.sh -b ./ -p nova -o etc/nova

Watch out for warnings about modules like libvirt, qpid and zmq not
being found - these warnings are significant because they result
in options not appearing in the generated config file.


The analyze_opts.py tool is used to find options which appear in
/etc/nova/nova.conf but not in etc/nova/nova.conf.sample
This helps identify options in the nova.conf file which are not used by nova.
The tool also identifies any options which are set to the default value.

Run it from the top-level working directory i.e.

  $> ./tools/config/analyze_opts.py


To open VNC ports on your ESX host, use the openstackvncfirewall.zip
file from the following github repo

git clone https://github.com/openstack-vmwareapi-team/Tools.git


__FILENAME__ = conf
# -*- coding: utf-8 -*-
#
# Requests documentation build configuration file, created by
# sphinx-quickstart on Sun Feb 13 23:54:25 2011.
#
# This file is execfile()d with the current directory set to its containing dir.
#
# Note that not all possible configuration values are present in this
# autogenerated file.
#
# All configuration values have a default; values that are commented out
# serve to show the default.

import sys, os, re

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
sys.path.insert(0, os.path.abspath('..'))
import expecter

version = ''
reg = re.compile(r"version='(.*)',$")
with open('../setup.py') as fd:
    for line in fd:
        m = reg.search(line)
        if m:
            version = m.groups()[0]
            break

# -- General configuration -----------------------------------------------------

# If your documentation needs a minimal Sphinx version, state it here.
#needs_sphinx = '1.0'

# Add any Sphinx extension module names here, as strings. They can be extensions
# coming with Sphinx (named 'sphinx.ext.*') or your custom ones.
extensions = ['sphinx.ext.autodoc']

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# The suffix of source filenames.
source_suffix = '.rst'

# The encoding of source files.
#source_encoding = 'utf-8-sig'

# The master toctree document.
master_doc = 'index'

# General information about the project.
project = u'expecter'
copyright = u'Gary Bernhardt'

# The version info for the project you're documenting, acts as replacement for
# |version| and |release|, also used in various other places throughout the
# built documents.
#
# The short X.Y version.
# The full version, including alpha/beta/rc tags.
release = version

# The language for content autogenerated by Sphinx. Refer to documentation
# for a list of supported languages.
#language = None

# There are two options for replacing |today|: either, you set today to some
# non-false value, then it is used:
#today = ''
# Else, today_fmt is used as the format for a strftime call.
#today_fmt = '%B %d, %Y'

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
exclude_patterns = ['_build']

# The reST default role (used for this markup: `text`) to use for all documents.
#default_role = None

# If true, '()' will be appended to :func: etc. cross-reference text.
#add_function_parentheses = True

# If true, the current module name will be prepended to all description
# unit titles (such as .. function::).
#add_module_names = True

# If true, sectionauthor and moduleauthor directives will be shown in the
# output. They are ignored by default.
#show_authors = False

# The name of the Pygments (syntax highlighting) style to use.
# pygments_style = 'flask_theme_support.FlaskyStyle'

# A list of ignored prefixes for module index sorting.
#modindex_common_prefix = []


# -- Options for HTML output ---------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
html_theme = 'default'

# Theme options are theme-specific and customize the look and feel of a theme
# further.  For a list of options available for each theme, see the
# documentation.
#html_theme_options = {}

# Add any paths that contain custom themes here, relative to this directory.
#html_theme_path = []

# The name for this set of Sphinx documents.  If None, it defaults to
# "<project> v<release> documentation".
#html_title = None

# A shorter title for the navigation bar.  Default is the same as html_title.
#html_short_title = None

# The name of an image file (relative to this directory) to place at the top
# of the sidebar.
#html_logo = None


# The name of an image file (within the static path) to use as favicon of the
# docs.  This file should be a Windows icon file (.ico) being 16x16 or 32x32
# pixels large.
#html_favicon = None

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
#html_static_path = ['_static']

# If not '', a 'Last updated on:' timestamp is inserted at every page bottom,
# using the given strftime format.
#html_last_updated_fmt = '%b %d, %Y'

# If true, SmartyPants will be used to convert quotes and dashes to
# typographically correct entities.
#html_use_smartypants = True

# Custom sidebar templates, maps document names to template names.
# Custom sidebar templates, maps document names to template names.
#html_sidebars = {}

# Additional templates that should be rendered to pages, maps page names to
# template names.
#html_additional_pages = {}

# If false, no module index is generated.
#html_domain_indices = True

# If false, no index is generated.
#html_use_index = True

# If true, the index is split into individual pages for each letter.
#html_split_index = False

# If true, links to the reST sources are added to the pages.
#html_show_sourcelink = False

# If true, "Created using Sphinx" is shown in the HTML footer. Default is True.
#html_show_sphinx = False

# If true, "(C) Copyright ..." is shown in the HTML footer. Default is True.
#html_show_copyright = True

# If true, an OpenSearch description file will be output, and all pages will
# contain a <link> tag referring to it.  The value of this option must be the
# base URL from which the finished HTML is served.
#html_use_opensearch = ''

# This is the file name suffix for HTML files (e.g. ".xhtml").
#html_file_suffix = None

# Output file base name for HTML help builder.
htmlhelp_basename = 'expecter_doc'


# -- Options for LaTeX output --------------------------------------------------

# The paper size ('letter' or 'a4').
#latex_paper_size = 'letter'

# The font size ('10pt', '11pt' or '12pt').
#latex_font_size = '10pt'

# Grouping the document tree into LaTeX files. List of tuples
# (source start file, target name, title, author, documentclass [howto/manual]).
latex_documents = [
  ('index', 'expecter.tex', u'Expecter Documentation',
   u'Gary Bernhardt', 'manual'),
]

# The name of an image file (relative to this directory) to place at the top of
# the title page.
#latex_logo = None

# For "manual" documents, if this is true, then toplevel headings are parts,
# not chapters.
#latex_use_parts = False

# If true, show page references after internal links.
#latex_show_pagerefs = False

# If true, show URL addresses after external links.
#latex_show_urls = False

# Additional stuff for the LaTeX preamble.
#latex_preamble = ''

# Documents to append as an appendix to all manuals.
#latex_appendices = []

# If false, no module index is generated.
#latex_domain_indices = True


# -- Options for manual page output --------------------------------------------

# One entry per manual page. List of tuples
# (source start file, name, description, authors, manual section).
man_pages = [
    ('index', 'expecter', u'Expecter Documentation',
     [u'Gary Bernhardt'], 1)
]

# If true, show URL addresses after external links.
#man_show_urls = False

# -- Options for Texinfo output ------------------------------------------------

# Grouping the document tree into Texinfo files. List of tuples
# (source start file, target name, title, author,
#  dir menu entry, description, category)
texinfo_documents = [
  ('index', 'expecter', u'Expecter Documentation', u'Gary Bernhardt',
   'expecter', 'Expecter gadget helps you write assertions', 'Miscellaneous'),
]

# Documents to append as an appendix to all manuals.
texinfo_appendices = []

########NEW FILE########
__FILENAME__ = expecter
__all__ = ['expect']
import difflib

try:
    import builtins as __builtins__
except ImportError:
    import __builtin__ as __builtins__


basestring = getattr(__builtins__, 'basestring', (str, bytes))


class expect(object):
    """
    All assertions are written using :class:`expect`. Usually, it's applied to
    the value you're making an assertion about:

        >>> expect(5) > 4
        expect(5)
        >>> expect(4) > 4
        Traceback (most recent call last):
        ...
        AssertionError: Expected something greater than 4 but got 4

    This works for comparisons as you'd expect:

        ==, !=, <, >, <=, >=

    Note that expect() *always* goes around the actual value: the value you're
    making an assertion about.

    There are other, non-binary expectations available. They're documented
    below.
    """
    def __init__(self, actual):
        self._actual = actual

    def __getattr__(self, name):
        is_custom_expectation = name in _custom_expectations
        if is_custom_expectation:
            predicate = _custom_expectations[name]
            return _CustomExpectation(predicate, self._actual)
        else:
            return getattr(super(expect, self), name)

    def __eq__(self, other):
        msg = 'Expected %s but got %s' % (repr(other), repr(self._actual))
        if isinstance(other, basestring) and isinstance(self._actual,
                basestring):
            msg += normalized_diff(other, self._actual)
        assert self._actual == other, msg
        return self

    def __ne__(self, other):
        assert self._actual != other, (
            'Expected anything except %s but got it' % repr(self._actual))
        return self

    def __lt__(self, other):
        assert self._actual < other, (
            'Expected something less than %s but got %s'
            % (repr(other), repr(self._actual)))
        return self

    def __gt__(self, other):
        assert self._actual > other, (
            'Expected something greater than %s but got %s'
            % (repr(other), repr(self._actual)))
        return self

    def __le__(self, other):
        assert self._actual <= other, (
            'Expected something less than or equal to %s but got %s'
            % (repr(other), repr(self._actual)))
        return self

    def __ge__(self, other):
        assert self._actual >= other, (
            'Expected something greater than or equal to %s but got %s'
            % (repr(other), repr(self._actual)))
        return self

    def __repr__(self):
        return 'expect(%s)' % repr(self._actual)

    def isinstance(self, expected_cls):
        """
        Ensures that the actual value is of type ``expected_cls`` (like ``assert isinstance(actual, MyClass)``).
        """
        if isinstance(expected_cls, tuple):
            cls_name = [c.__name__ for c in expected_cls]
            cls_name = ' or '.join(cls_name)
        else:
            cls_name = expected_cls.__name__
        assert isinstance(self._actual, expected_cls), (
            'Expected an instance of %s but got an instance of %s' % (
                cls_name, self._actual.__class__.__name__))

    def contains(self, other):
        """
        Ensure that ``other`` is in the actual value (like ``assert other in actual``).
        """
        assert other in self._actual, (
            "Expected %s to contain %s but it didn't" % (
                repr(self._actual), repr(other)))

    def does_not_contain(self, other):
        """
        Opposite of ``contains``
        """
        assert other not in self._actual, (
            "Expected %s to not contain %s but it did" % (
                repr(self._actual), repr(other)))

    @staticmethod
    def raises(expected_cls=Exception, message=None):
        """Ensure that an exception is raised. E.g.,

        ::

            with expect.raises(MyCustomError):
                func_that_raises_error()

        is equivalent to:

        ::

            try:
                func_that_raises_error()
                raise AssertionError('Error not raised!')
            except MyCustomError:
                pass
        """
        return _RaisesExpectation(expected_cls, message)


class _RaisesExpectation:
    """
    Internal context decorator created when you do:
        with expect.raises(SomeError):
            something()
    """
    def __init__(self, exception_class, message):
        self._exception_class = exception_class
        self.message = message

    def __enter__(self):
        pass

    def __exit__(self, exc_type, exc_value, traceback):
        success = not exc_type
        if success:
            raise AssertionError(
                'Expected an exception of type %s but got none'
                % self._exception_class.__name__)
        else:
            return self.validate_failure(exc_type, exc_value)

    def validate_failure(self, exc_type, exc_value):
        wrong_message_was_raised = (self.message and
                                    self.message != str(exc_value))
        if wrong_message_was_raised:
            raise AssertionError(
                "Expected %s('%s') but got %s('%s')" %
                 (self._exception_class.__name__,
                  str(self.message),
                  exc_type.__name__,
                  str(exc_value)))
        elif issubclass(exc_type, self._exception_class):
            return True
        else:
            pass


class _CustomExpectation:
    """
    Internal class representing a single custom expectation. Don't create these
    directly; use `expecter.add_expectation` instead.
    """
    negative_verbs = {"can": "it can't",
                      "is": "it isn't",
                      "will": "it won't",
                     }

    def __init__(self, predicate, actual):
        self._predicate = predicate
        self._actual = actual

    def __call__(self, *args, **kwargs):
        self.enforce(*args, **kwargs)

    def enforce(self, *args, **kwargs):
        if not self._predicate(self._actual, *args, **kwargs):
            predicate_name = self._predicate.__name__
            raise AssertionError('Expected that %s %s, but %s' %
                                 (repr(self._actual),
                                  predicate_name,
                                  self._negative_verb()))

    def _negative_verb(self):
        # XXX: getting name in multiple places
        first_word_in_predicate = self._predicate.__name__.split('_')[0]
        try:
            return self.negative_verbs[first_word_in_predicate]
        except KeyError:
            return "got False"


_custom_expectations = {}


def add_expectation(predicate):
    """
    Add a custom expectation. After being added, custom expectations can be
    used as if they were built-in:

        >>> def is_long(x): return len(x) > 5
        >>> add_expectation(is_long)
        >>> expect('loooooong').is_long()
        >>> expect('short').is_long()
        Traceback (most recent call last):
        ...
        AssertionError: Expected that 'short' is_long, but it isn't

    The name of the expectation is taken from the name of the function (as
    shown above).
    """
    _custom_expectations[predicate.__name__] = predicate


def clear_expectations():
    """Remove all custom expectations"""
    _custom_expectations.clear()


def normalized_diff(other, actual):
    diff = difflib.unified_diff(other.split('\n'),
            actual.split('\n'),
            lineterm='')
    diff = list(diff)
    return '\n'.join(['\nDiff:'] + diff[2:])

########NEW FILE########
__FILENAME__ = runtests
#!/usr/bin/env python

import sys

import nose


if __name__ == '__main__':
    nose_args = sys.argv + ['--config', 'test.cfg', '--with-doctest']
    if nose.run(argv=nose_args):
        sys.exit(0)
    else:
        sys.exit(1)


########NEW FILE########
__FILENAME__ = test_docs
from subprocess import check_call
from os.path import join, dirname


class describe_docs:
    def they_build_cleanly(self):
        docs_dir = join(dirname(__file__), "..", "..", "docs")
        command = "(cd %s && make html) > /dev/null" % docs_dir
        check_call(command, shell=True)


########NEW FILE########
__FILENAME__ = test_readme
import doctest


class describe_readme:
    def it_passes_as_a_doctest(self):
        test_results = doctest.testfile('README.markdown', module_relative=False)
        assert not test_results.failed


########NEW FILE########
__FILENAME__ = test_custom_matchers
from nose.tools import assert_raises

from tests.util import fail_msg
from expecter import expect, add_expectation, clear_expectations


class describe_custom_matchers:
    def is_a_potato(self, thing):
        return thing == 'potato'

    def teardown(self):
        clear_expectations()

    def they_can_succeed(self):
        add_expectation(self.is_a_potato)
        expect('potato').is_a_potato()

    def they_can_fail(self):
        add_expectation(self.is_a_potato)
        def _fails():
            expect('not a potato').is_a_potato()
        assert_raises(AssertionError, _fails)
        assert fail_msg(_fails) == (
            "Expected that 'not a potato' is_a_potato, but it isn't")

    def they_adjust_failure_message_for_expectation_name(self):
        def can_do_something(thing): return False
        def will_do_something(thing): return False

        for predicate in [can_do_something, will_do_something]:
            add_expectation(predicate)

        assert fail_msg(expect('walrus').can_do_something) == (
            "Expected that 'walrus' can_do_something, but it can't")
        assert fail_msg(expect('walrus').will_do_something) == (
            "Expected that 'walrus' will_do_something, but it won't")

    def they_have_default_failure_message(self):
        def predicate_with_bad_name(thing): return False
        add_expectation(predicate_with_bad_name)
        assert fail_msg(expect('walrus').predicate_with_bad_name) == (
            "Expected that 'walrus' predicate_with_bad_name, but got False")

    def they_can_be_cleared(self):
        clear_expectations()
        assert_raises(AttributeError, lambda: expect('potato').is_a_potato)

    def they_can_have_postional_arguments(self):
        def is_a(thing, vegetable):
            return thing == vegetable
        add_expectation(is_a)
        expect('potato').is_a('potato')

    def they_can_have_keyword_arguments(self):
        def is_a(thing, vegetable):
            return thing == vegetable
        add_expectation(is_a)
        expect('potato').is_a(vegetable='potato')

########NEW FILE########
__FILENAME__ = test_exceptions
from nose.tools import assert_raises

from tests.util import fail_msg
from expecter import expect


class describe_expecter_when_expecting_exceptions():
    def it_swallows_expected_exceptions(self):
        with expect.raises(KeyError):
            raise KeyError

    def it_requires_exceptions_to_be_raised(self):
        def _expects_raise_but_doesnt_get_it():
            with expect.raises(KeyError):
                pass
        assert_raises(AssertionError, _expects_raise_but_doesnt_get_it)
        assert fail_msg(_expects_raise_but_doesnt_get_it) == (
            'Expected an exception of type KeyError but got none')

    def it_does_not_swallow_exceptions_of_the_wrong_type(self):
        def _expects_key_error_but_gets_value_error():
            with expect.raises(KeyError):
                raise ValueError
        assert_raises(ValueError, _expects_key_error_but_gets_value_error)

    def it_can_expect_any_exception(self):
        with expect.raises():
            raise ValueError

    def it_can_expect_failure_messages(self):
        with expect.raises(ValueError, 'my message'):
            raise ValueError('my message')

    def it_can_require_failure_messages(self):
        def _fails():
            with expect.raises(ValueError, 'my message'):
                raise ValueError('wrong message')
        assert_raises(AssertionError, _fails)
        assert fail_msg(_fails) == (
            "Expected ValueError('my message') but got ValueError('wrong message')")


########NEW FILE########
__FILENAME__ = test_expecter
from __future__ import with_statement
from nose.tools import assert_raises

from tests.util import fail_msg
from expecter import expect


class describe_expecter:
    def it_expects_equals(self):
        expect(2) == 1 + 1
        def _fails(): expect(1) == 2
        assert_raises(AssertionError, _fails)
        assert fail_msg(_fails) == 'Expected 2 but got 1'

    def it_shows_diff_when_strings_differ(self):
        def _fails(): expect('foo\nbar') == 'foo\nbaz'
        assert_raises(AssertionError, _fails)
        assert fail_msg(_fails) == ("Expected 'foo\\nbaz' but got 'foo\\nbar'\n"
               "Diff:\n"
               "@@ -1,2 +1,2 @@\n"
               " foo\n"
               "-baz\n"
               "+bar"
               ), fail_msg(_fails)

    def it_expects_not_equals(self):
        expect(1) != 2
        def _fails(): expect(1) != 1
        assert_raises(AssertionError, _fails)
        assert fail_msg(_fails) == 'Expected anything except 1 but got it'

    def it_expects_less_than(self):
        expect(1) < 2
        def _fails(): expect(1) < 0
        assert_raises(AssertionError, _fails)
        assert fail_msg(_fails) == 'Expected something less than 0 but got 1'

    def it_expects_greater_than(self):
        expect(2) > 1
        def _fails(): expect(0) > 1
        assert_raises(AssertionError, _fails)
        assert fail_msg(_fails) == (
            'Expected something greater than 1 but got 0')

    def it_expects_less_than_or_equal(self):
        expect(1) <= 1
        expect(1) <= 2
        def _fails(): expect(2) <= 1
        assert_raises(AssertionError, _fails)
        assert fail_msg(_fails) == (
            'Expected something less than or equal to 1 but got 2')

    def it_expects_greater_than_or_equal(self):
        expect(1) >= 1
        expect(2) >= 1
        def _fails(): expect(1) >= 2
        assert_raises(AssertionError, _fails)
        assert fail_msg(_fails) == (
            'Expected something greater than or equal to 2 but got 1')

    def it_can_chain_comparison_expectations(self):
        # In each of these chains, the first expectation passes and the second
        # fails. This forces the first expectation to return self.
        failing_chains = [lambda: 1 == expect(1) != 1,
                          lambda: 1 != expect(2) != 2,
                          lambda: 1 < expect(2) != 2,
                          lambda: 1 > expect(0) != 0,
                          lambda: 1 <= expect(1) != 1,
                          lambda: 1 >= expect(1) != 1]
        for chain in failing_chains:
            assert_raises(AssertionError, chain)

        # Mote bug: if we leave the lambda in a local variable, it will try to
        # run it as a spec.
        del chain

    def it_expects_isinstance(self):
        expect(1).isinstance(int)
        def _fails():
            expect(1).isinstance(str)
        assert_raises(AssertionError, _fails)
        assert fail_msg(_fails) == (
            'Expected an instance of str but got an instance of int')

    def it_expects_isinstance_for_multiple_types(self):
        expect('str').isinstance((str, bytes))
        def _fails():
            expect('str').isinstance((int, tuple))
        assert_raises(AssertionError, _fails)
        assert fail_msg(_fails) == (
            'Expected an instance of int or tuple but got an instance of str')

    def it_expects_containment(self):
        expect([1]).contains(1)
        def _fails():
            expect([2]).contains(1)
        assert_raises(AssertionError, _fails)
        assert fail_msg(_fails) == (
            "Expected [2] to contain 1 but it didn't")

    def it_expects_non_containment(self):
        expect([1]).does_not_contain(0)
        def _fails():
            expect([1]).does_not_contain(1)
        assert_raises(AssertionError, _fails)
        assert fail_msg(_fails) == (
            "Expected [1] to not contain 1 but it did")

########NEW FILE########
__FILENAME__ = test_unicode
# vim: set fileencoding=utf-8 :
from __future__ import unicode_literals
from nose.tools import assert_raises

from tests.util import fail_msg
from expecter import expect

try:
    import builtins as __builtins__
except ImportError:
    import __builtin__ as __builtins__

unicode = getattr(__builtins__, 'unicode', str)

class describe_expecter:
    def it_shows_diff_when_unicode_strings_differ(self):
        value = 'ueber\ngeek'
        fixture = 'über\ngeek'
        assert isinstance(value, unicode), "value is a " + repr(type(value))
        assert isinstance(fixture, unicode), "fixture is a " + repr(type(fixture))
        def _fails(): expect(value) == fixture
        assert_raises(AssertionError, _fails)
        msg = ("Expected 'über\\ngeek' but got 'ueber\\ngeek'\n"
               "Diff:\n"
               "@@ -1,2 +1,2 @@\n"
               "-über\n"
               "+ueber\n"
               " geek"
               )
        #normalize real msg for differences in py2 and py3
        real = fail_msg(_fails).replace("u'", "'").replace(
                '\\xfc', '\xfc')
        assert  real == msg, '\n' + repr(real) + '\n' + repr(msg)


########NEW FILE########
__FILENAME__ = util
try:
    import builtins as __builtins__
except ImportError:
    import __builtin__ as __builtins__

str = getattr(__builtins__, 'unicode', str)

def fail_msg(callable_):
    try:
        callable_()
    except Exception as e:
        return str(e)


########NEW FILE########

How to build psycopg documentation
----------------------------------

- Install Sphinx, maybe in a virtualenv. Tested with Sphinx 0.6.4::

    ~$ virtualenv pd
    New python executable in pd/bin/python
    Installing setuptools............done.
    ~$ cd pd
    ~/pd$ source bin/activate
    (pd)~/pd$ 

- Install Sphinx in the env::

    (pd)~/pd$ easy_install sphinx
    Searching for sphinx
    Reading http://pypi.python.org/simple/sphinx/
    Reading http://sphinx.pocoo.org/
    Best match: Sphinx 0.6.4
    ...
    Finished processing dependencies for sphinx

- Build psycopg2 and ensure the package can be imported (it will be used for
  reading the version number, autodocs etc.)::

    (pd)~/pd/psycopg2$ python setup.py build
    (pd)~/pd/psycopg2$ python setup.py install
    running install
    ...
    creating ~/pd/lib/python2.6/site-packages/psycopg2
    ...

- Move to the ``doc`` dir and run ``make`` from there::

    (pd)~/pd/psycopg2$ cd doc/
    (pd)~/pd/psycopg2/doc$ make
    Running Sphinx v0.6.4
    ...

You should have the rendered documentation in ``./html`` and the text file
``psycopg2.txt`` now.


psycopg2 - Python-PostgreSQL Database Adapter
********************************************

psycopg2 is a PostgreSQL database adapter for the Python programming
language.  psycopg2 was written with the aim of being very small and fast,
and stable as a rock.

psycopg2 is different from the other database adapter because it was
designed for heavily multi-threaded applications that create and destroy
lots of cursors and make a conspicuous number of concurrent INSERTs or
UPDATEs. psycopg2 also provides full asynchronous operations and support
for coroutine libraries.

psycopg2 can compile and run on Linux, FreeBSD, Solaris, MacOS X and
Windows architecture. It supports Python versions from 2.4 onwards and
PostgreSQL versions from 7.4 onwards.

psycopg2 is free software ("free as in freedom" but I like beer too.)
It is licensed under the GNU Lesser General Public License, version 3 or
later plus an exception to allow OpenSSL (libpq) linking; see LICENSE for
more details.

Documentation
-------------

Start by reading the INSTALL file. More information about psycopg2 extensions
to the DBAPI-2.0 is available in the files located in the doc/ direcory.
Example code can be found in the examples/ directory. If you make any changes
to the code make sure to run the unit tests localed in tests/.

Online documentation can be found at: http://initd.org/psycopg/

If you stumble upon any bugs, please tell us at: http://psycopg.lighthouseapp.com/

Contributors
------------

For a list of contributors to the project, see the AUTHORS file.


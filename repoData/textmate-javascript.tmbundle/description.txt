# Installation

You can install this bundle in TextMate by opening the preferences and going to the bundles tab. After installation it will be automatically updated for you.

# General

* [Bundle Styleguide](http://kb.textmate.org/bundle_styleguide) — _before you make changes_
* [Commit Styleguide](http://kb.textmate.org/commit_styleguide) — _before you send a pull request_
* [Writing Bug Reports](http://kb.textmate.org/writing_bug_reports) — _before you report an issue_

# License

If not otherwise specified (see below), files in this repository fall under the following license:

	Permission to copy, use, modify, sell and distribute this
	software is granted. This software is provided "as is" without
	express or implied warranty, and with no claim as to its
	suitability for any purpose.

An exception is made for files in readable text which contain their own license information, or files where an accompanying file exists (in the same directory) with a “-license” suffix added to the base-name name of the original file, and an extension of txt, html, or similar. For example “tidy” is accompanied by “tidy-license.txt”.
# UNPACKERS SPECIFICATIONS

Nothing very difficult: an unpacker is a submodule placed in the directory
where this file was found. Each unpacker must define three symbols:

 * `PRIORITY`       : integer number expressing the priority in applying this
                      unpacker. Lower number means higher priority.
                      Makes sense only if a source file has been packed with
                      more than one packer.
 * `detect(source)` : returns `True` if source is packed, otherwise, `False`.
 * `unpack(source)` : takes a `source` string and unpacks it. Must always return
                      valid JavaScript. That is to say, your code should look
                      like:

```
if detect(source):
    return do_your_fancy_things_with(source)
else:
    return source
```

*You can safely define any other symbol in your module, as it will be ignored.*

`__init__` code will automatically load new unpackers, without any further step
to be accomplished. Simply drop it in this directory.


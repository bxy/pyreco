sqlalchemy-migrate is DEPRECATED.

All new migrations should be written using alembic.
Please see ceilometer/storage/sqlalchemy/alembic/README

To generate the sample ceilometer.conf file, run the following
command from the top-level ceilometer directory:

tox -egenconfig
ceilometer
==========

See the ReleaseNotes document and the project home for more info.

  http://launchpad.net/ceilometer

